# GGJ 2024 Project Repository

Project made with Unity 2022.3.17f1.

**Links:**
- [Unity 2022.3.17f1](unityhub://2022.3.17f1/4fc78088f837)
- [Google Drive](https://drive.google.com/drive/folders/1_w8R4f5grqfr1DgHcl1ERRstije3oA2C)

## Nomenclature

### Folders structure

|Folder|Description|Examples|
|------|-----------|--------|
|`_APP`|Contains the global application data|App icon, theme, fonts, cursor, API credentials, ...|
|`_CONTENT`|Contains the data assets created for the game|Characters, cards, dialogues, ...|
|`_DEV`|Contains the scripts of the game system|modules, low-level components, ...|
|`_GAMEPLAY`|Contains all the assets the player can see or the development team can use in the scenes|Game settings, pefabs, feedback or UI scripts, ...|
|`_RESOURCES`|Contains the generic assets or external packages used in the game|UI art, audio, prototype art or utilities, ...|
|*Other folders*|Everything outside the previous folders is related to third-party packages or Unity data|URP settings, [special Unity folders](https://docs.unity3d.com/Manual/SpecialFolders.html), imported files from the [Assets Store](https://assetstore.unity.com/), ...

The approach is to pack all related assets together by theme or "category", instead of doing it by type. As an example, the illustration of a card should be placed in the same directory as the asset that defines its content.

### Naming

For consistency and quick search purposes, we use naming conventions for all assets in the project, which generally looks as follows:

```
PREFIX_Directory_Name_00

Example:
/Assets/_CONTENT/Abilities (ABL)/Knight (KNT)/S_ABL_KNT_BasicAttack.asset
```

- **`PREFIX`** summarizes the asset type (eg. `S_` for *Sprite* in our example). See below for a list of common prefixes.
- **`Directory`** is the directory name of the asset, which should represent a "category" or a theme (eg. `Character`, `Card`, ...). You can abbreviate this directory name, but the actual directory must mention it in its name (eg. `ABL_` for *Abilities* in our example, as the `/Abilities` folder clearly mention that these assets can be abbreviated with `ABL`).
- **`Name`** is the actual asset name. Please be accurate and descriptive when naming things!
- **`##`** represents the iteration number, if it's applicable. If used, it must have at least 2 characters, by adding leading 0s if needed.

#### Naming conventions for scripts

- Avoid abbreviated prefixes
- For classes that inherit from [`MonoBehaviour`](https://docs.unity3d.com/ScriptReference/MonoBehaviour.html):
    - Add a suffix `Component`
    - Use [`[AddComponentMenu]`](https://docs.unity3d.com/ScriptReference/AddComponentMenu.html) attribute to display it with a clean name in the editor using spaces
- For classes that inherit from [`ScriptableObject`](https://docs.unity3d.com/ScriptReference/ScriptableObject.html):
    - Add a suffix `Asset`
    - Use [`[CreateAssetMenu]`](https://docs.unity3d.com/ScriptReference/CreateAssetMenuAttribute.html) attribute to allow the creation of that asset from a meaningful menu
- For classes or structures meant to be used as `[SignalData]`
    - Add a suffix `Info`

For system scripts, please use the following syntax rules. This only applies to scripts in the `/_FRAMEWORK` folder:

```cs
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Use PascalCase for namespaces and classes.
    /// Always use summaries to describe types.
    /// </summary>
    public class MyComponent : MonoBehaviour
    {
        // Use camelCase prefixed with "_" for private fields.
        [SerializeField]
        private PlayerInfo _playerInfo = default;

        // Use PascalCase for properties.
        // Always use doc inheritance to point the related field, making the code navigation easier.
        /// <inheritdoc cref="_playerInfo"/>
        public PlayerInfo PlayerInfo
        {
            get => _playerInfo;
            set => _playerInfo = value;
        }

        /// <summary>
        /// Use PascalCase for functions.
        /// Always use summaries to describe functions.
        /// </summary>
        /// <param name="amount">Use camelCase for parameters.</param>
        public void IncreaseScore(int amount)
        {
            /* ... */
        }
    }
}
```

```cs
namespace Game
{
    [System.Serializable]
    public struct PlayerInfo
    {
        // Use camelCase for public fields
        public string playerName;
        public int score;
    }
}
```

## Contributing

### As an artist

Place your assets in the appropriate directory:

|Target folder|Art asset usage|Examples|
|-------------|---------------|--------|
|`/_CONTENT`|The asset is used for game data|Character avatar, card illustration, weapon model, ...|
|`/_GAMEPLAY`|The asset is used in a very specific case|Specific prefab, VFX model, non-generic UI element, ...|
|`/_RESOURCES`|The asset is used in a very specific case|Specific prefab, VFX model, non-generic UI element, ...|

Please try to place your assets by following these rules as much as possible, but don't lose time by thinking too much about it: if you have a doubt, just place in `/_RESOURCES` and tell the team to review the asset. This allow you to continue your integration pass, cleaning the project can be done later.

If you integrate feedbacks, you may want to implement components to play them, you can create them in the `_GAMEPLAY` folder, where it's the most relevant. If possible, a script dedicated to a specific prefab must be placed in the same directory as that prefab.

### As a designer

Assets related to the game content (dialogues, characters, abilities, ...) is in the `/_CONTENT` folder.

Game configuration, prefabs and custom scripts for feedbacks or UI are placed in `/_GAMEPLAY`.

### As a programmer

System scripts (modules, system components, signal data, ...) are implemented in `/_FRAMEWORK`.

Other utility or feedback scripts are implemented in `/_GAMEPLAY`.