/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System.Collections.Generic;

using UnityEngine;

using SideXP.Core.EditorOnly;

namespace SideXP.Selection.EditorOnly
{

    /// <summary>
    /// Represents a bundle of selected objects.
    /// </summary>
    [System.Serializable]
    public class SelectionItem
    {

        /// <summary>
        /// The maximum objects to use to compose the selection label.
        /// </summary>
        private const int ObjectsCountForLabel = 3;

        [SerializeField]
        [Tooltip("The list of objects in this selection.")]
        private Object[] _objects = null;

        /// <inheritdoc cref="SelectionItem"/>
        /// <param name="selection">The list of objects in this selection.</param>
        public SelectionItem(Object[] selection)
        {
            _objects = selection;
        }

        /// <inheritdoc cref="_objects"/>
        public Object[] Objects => _objects;

        /// <summary>
        /// Computes a label for displaying this selection item on screen, based on what objects are in that selection.
        /// </summary>
        public string Label
        {
            get
            {
                List<string> labels = new List<string>();
                foreach (Object obj in _objects)
                {
                    if (string.IsNullOrEmpty(obj.name))
                        continue;

                    labels.Add(obj.name);
                    if (labels.Count >= ObjectsCountForLabel)
                    {
                        if (labels.Count < _objects.Length)
                            labels.Add("...");
                        break;
                    }
                }

                return labels.Count > 0
                    ? string.Join(", ", labels)
                    : $"Objects ({_objects.Length})";
            }
        }

        /// <summary>
        /// Checks if this selection contains scene objects (and so can't be saved entirely between editor reloads).
        /// </summary>
        public bool ContainsSceneObjects
        {
            get
            {
                foreach (Object obj in _objects)
                {
                    if (!obj.IsAsset())
                        return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Removes null objects from this item. Null objects can be caused by objects deletions or for scene items after reloading the
        /// editor.
        /// </summary>
        /// <returns>Returns true if this item is still valid, or false if it's now empty and can be removed.</returns>
        public bool Clean()
        {
            List<Object> objectsList = new List<Object>();
            if (_objects != null)
                objectsList.AddRange(_objects);

            for (int i = objectsList.Count - 1; i >= 0; i--)
            {
                if (objectsList[i] == null)
                    objectsList.RemoveAt(i);
            }

            _objects = objectsList.ToArray();
            return _objects.Length > 0;
        }

    }

}