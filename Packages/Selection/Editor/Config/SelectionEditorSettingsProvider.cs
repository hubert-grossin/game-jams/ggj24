/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;
using UnityEditor;

using SideXP.Core.EditorOnly;

namespace SideXP.Selection.EditorOnly
{

    /// <summary>
    /// Generate menus to edit Selection package settings.
    /// </summary>
    internal static class SelectionEditorSettingsProvider
    {

        [SettingsProvider]
        private static SettingsProvider RegisterPreferencesMenu()
        {
            return new SettingsProvider(EditorConstants.Preferences + "/Selection", SettingsScope.User, null)
            {
                guiHandler = str =>
                {
                    MoreEditorGUI.DrawDefaultInspector(SelectionEditorPrefsSettings.I);

                    EditorGUILayout.Space();
                    if (GUILayout.Button("Reset"))
                        SelectionEditorPrefsSettings.I.ResetToDefaults();

                    EditorGUILayout.Space();
                    MoreEditorGUI.HorizontalSeparator(true);
                    EditorGUILayout.Space();

                    MoreEditorGUI.DrawDefaultInspector(SavedSelectionsStorage.I);
                }
            };
        }

    }

}