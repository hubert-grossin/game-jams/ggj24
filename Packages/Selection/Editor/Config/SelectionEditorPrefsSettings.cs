/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

using SideXP.Core;

namespace SideXP.Selection.EditorOnly
{

    /// <summary>
    /// Editor settings for the Selection package, saved in user prefs.
    /// </summary>
    [Save(EDataScope.User, true)]
    public class SelectionEditorPrefsSettings : SOSingleton<SelectionEditorPrefsSettings>
    {

        #region Fields

        [Tooltip("The colors to use for the saved selections in the Selection History window.")]
        public Color[] SaveSelectionButtonColors = new Color[]
        {
            SideXP.Core.ColorUtility.FromHex("#d32f2f"),
            SideXP.Core.ColorUtility.FromHex("#7b1fa2"),
            SideXP.Core.ColorUtility.FromHex("#303f9f"),
            SideXP.Core.ColorUtility.FromHex("#0288d1"),
            SideXP.Core.ColorUtility.FromHex("#00796b"),
            SideXP.Core.ColorUtility.FromHex("#388e3c"),
            SideXP.Core.ColorUtility.FromHex("#afb42b"),
            SideXP.Core.ColorUtility.FromHex("#fbc02d"),
            SideXP.Core.ColorUtility.FromHex("#f57c00"),
            SideXP.Core.ColorUtility.FromHex("#ff5722"),
            SideXP.Core.ColorUtility.FromHex("#5d4037"),
            SideXP.Core.ColorUtility.FromHex("#455a64"),
        };

        [Tooltip("The number of saved selection buttons to display in the Selection History window, where X represents the number of buttons per row, and Y represents the number of rows.")]
        public Vector2Int SavedSelectionsGrid = new Vector2Int(6, 2);

        #endregion

    }

}