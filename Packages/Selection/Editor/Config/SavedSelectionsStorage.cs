/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System.Collections.Generic;

using UnityEngine;

using SideXP.Core;

using UnitySelection = UnityEditor.Selection;

namespace SideXP.Selection.EditorOnly
{

    /// <summary>
    /// Editor settings for the Selection package, saved in user settings folder.
    /// </summary>
    [Save(EDataScope.User)]
    public class SavedSelectionsStorage : SOSingleton<SavedSelectionsStorage>
    {

        #region Fields

        /// <summary>
        /// The list of all saved selections.
        /// </summary>
        [SerializeField]
        private List<SelectionItem> _savedSelections = new List<SelectionItem>();

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="SOSingleton{T}.Set"/>
        protected override void Set()
        {
            Clean();
        }

        #endregion


        #region Public API

        /// <summary>
        /// Gets the saved selection item at the given index.
        /// </summary>
        /// <param name="index">The index of the saved selection slot.</param>
        /// <returns>Returns the selection item at the given index.</returns>
        public SelectionItem this[int index] => _savedSelections[index];

        /// <summary>
        /// Checks if the saved selection at the given index exists and actually contains objects.
        /// </summary>
        /// <param name="index">The index of the saved selection to check.</param>
        /// <returns>Returns true if the saved selection is valid.</returns>
        public bool IsValidSelection(int index)
        {
            // Cancel if the index is out of range, or no selection is saved at that index
            if (!I._savedSelections.IsInRange(index) || I._savedSelections[index] == null)
                return false;

            if (!I._savedSelections[index].Clean())
            {
                I._savedSelections[index] = null;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Stores the given objects as a saved selection.
        /// </summary>
        /// <param name="objects">The objects in the selection.</param>
        /// <param name="index">The index at which the selection is saved in the list.</param>
        /// <returns>Returns true if the selection has been saved successfully.</returns>
        public bool SaveSelection(Object[] objects, int index)
        {
            // Filter invalid objects
            List<Object> objectsList = new List<Object>();
            foreach (Object obj in objects)
            {
                if (obj != null)
                    objectsList.Add(obj);
            }

            // Cancel if the selection to save is empty
            if (objectsList.Count <= 0)
                return false;

            // Extend the saved selections list to the given index
            while (_savedSelections.Count <= index)
                _savedSelections.Add(null);

            _savedSelections[index] = new SelectionItem(objects);
            return true;
        }

        /// <summary>
        /// Applies the saved selection at the given index, if it's valid.
        /// </summary>
        /// <param name="index">The index of the saved selection to apply.</param>
        /// <returns>Returns true if the selection has been applied successfully, otherwise false.</returns>
        public bool ApplySelection(int index)
        {
            if (!IsValidSelection(index))
                return false;

            UnitySelection.objects = _savedSelections[index].Objects;
            return true;
        }

        /// <summary>
        /// Removes the saved selection at the given index.
        /// </summary>
        /// <param name="index">The index of the saved selection to remove.</param>
        /// <returns>Returns true if the saved selection has been removed successfully.</returns>
        public bool DeleteSelection(int index)
        {
            if (!_savedSelections.IsInRange(index) || _savedSelections[index] == null)
                return false;

            _savedSelections[index] = null;
            Clean();
            return true;
        }

        #endregion


        #region Private API

        /// <summary>
        /// Removes all invalid saved selections.
        /// </summary>
        private void Clean()
        {
            bool foundValidSelection = false;
            for (int i = _savedSelections.Count - 1; i >= 0; i--)
            {
                if (_savedSelections[i] == null || !_savedSelections[i].Clean())
                {
                    _savedSelections[i] = null;
                    if (!foundValidSelection)
                        _savedSelections.RemoveAt(i);
                }
                else
                {
                    foundValidSelection = true;
                }
            }
        }

        #endregion

    }

}