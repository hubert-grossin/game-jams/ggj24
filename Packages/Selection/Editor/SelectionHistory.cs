/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using SideXP.Core;

using UnitySelection = UnityEditor.Selection;

namespace SideXP.Selection.EditorOnly
{

    /// <summary>
    /// Stores the history of selected objects in the editor, and allow user to navigate through them.
    /// </summary>
    [InitializeOnLoad]
    [Save(EDataScope.User)]
    public class SelectionHistory : SOSingleton<SelectionHistory>
    {

        #region Delegates

        /// <summary>
        /// Called when the selection history or the focused selection change.
        /// </summary>
        public delegate void HistoryChangeDelegate();

        /// <summary>
        /// Called when the focused selection change.
        /// </summary>
        /// <param name="cursor">The index of the focused selection in the history list.</param>
        public delegate void HistoryCursorChangeDelegate(int cursor);

        #endregion


        #region Fields

        private const int MaxHistoryEntries = 64;

        /// <inheritdoc cref="HistoryChangeDelegate"/>
        public static event HistoryChangeDelegate OnHistoryChange;

        /// <inheritdoc cref="HistoryCursorChangeDelegate"/>
        public static event HistoryCursorChangeDelegate OnCursorChange;

        /// <summary>
        /// The history of previous selections, where index [0] is the oldest, and [count - 1] is the latest.
        /// </summary>
        private List<SelectionItem> _history = new List<SelectionItem>();

        /// <summary>
        /// The current index of a selection picked from the history.
        /// </summary>
        private static int s_historyCursor = -1;

        /// <summary>
        /// Flag enabled when the selection in the editor changes because of a navigation in this history.
        /// </summary>
        private static bool s_skipNextSelection = false;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Static construcotr.
        /// </summary>
        static SelectionHistory()
        {
            UnitySelection.selectionChanged += HandleSelectionChanged;
        }

        #endregion


        #region Public API

        /// <summary>
        /// Checks if there's a previous selection to navigate.
        /// </summary>
        public static bool HasPreviousSelection => s_historyCursor < 0 ? I._history.Count > 1 : s_historyCursor > 0;

        /// <summary>
        /// Checks if there's a next selection to navigate.
        /// </summary>
        public static bool HasNextSelection => s_historyCursor >= 0 && s_historyCursor < I._history.Count - 1;

        /// <inheritdoc cref="s_historyCursor"/>
        public static int Cursor
        {
            get => s_historyCursor;
            set
            {
                if (I._history.Count > 0)
                {
                    s_historyCursor = Mathf.Clamp(value, 0, I._history.Count - 1);
                    SetSelection(I._history[s_historyCursor].Objects, true);
                    OnCursorChange?.Invoke(s_historyCursor);
                }
            }
        }

        /// <summary>
        /// The number of entries in the selection history.
        /// </summary>
        public static int Count => I._history.Count;

        /// <inheritdoc cref="s_history"/>
        public static SelectionItem[] History => I._history.ToArray();

        /// <summary>
        /// Navigate to previous selection.
        /// </summary>
        /// <returns>Returns the selected objects in the previous selection</returns>
        public static Object[] Previous()
        {
            // If no selection is currently focused in the history
            if (s_historyCursor < 0)
            {
                // Cancel if there's no previous item to select
                if (I._history.Count <= 1)
                    return null;

                // Focus the latest selection
                s_historyCursor = I._history.Count - 2;
            }
            // Else, if a selection is currently focused
            else
            {
                // Cancel if there's no previous selection to focus
                if (s_historyCursor == 0)
                    return null;

                // Select the previous selection
                s_historyCursor--;
            }

            SetSelection(I._history[s_historyCursor].Objects, true);
            OnCursorChange?.Invoke(s_historyCursor);
            return I._history[s_historyCursor].Objects;
        }

        /// <param name="selection">Outputs the selected objects in the previous selection.</param>
        /// <returns>Returns true if the navigation cursor has been set successfully.</returns>
        /// <inheritdoc cref="Previous()"/>
        public static bool Previous(out Object[] selection)
        {
            selection = Previous();
            return selection != null;
        }

        /// <summary>
        /// Navigate to next selection.
        /// </summary>
        /// <returns>Returns the selected objects in the next selection</returns>
        public static Object[] Next()
        {
            // Cancel if no selection is currently focused in the history, or if the focused selection is the latest one
            if (s_historyCursor < 0 || s_historyCursor >= I._history.Count - 1)
                return null;

            s_historyCursor++;
            SetSelection(I._history[s_historyCursor].Objects, true);
            OnCursorChange?.Invoke(s_historyCursor);
            return I._history[s_historyCursor].Objects;
        }

        /// <param name="selection">Outputs the selected objects in the next selection.</param>
        /// <returns>Returns true if the navigation cursor has been set successfully.</returns>
        /// <inheritdoc cref="Next()"/>
        public static bool Next(out Object[] selection)
        {
            selection = Next();
            return selection != null;
        }

        /// <summary>
        /// Delete all selection entries in the history.
        /// </summary>
        public static void Clear()
        {
            I._history.Clear();
            OnHistoryChange?.Invoke();

            if (s_historyCursor >= 0)
            {
                s_historyCursor = -1;
                OnCursorChange?.Invoke(s_historyCursor);
            }
        }

        #endregion


        #region Protected API

        /// <inheritdoc cref="SOSingleton{T}.Set"/>
        protected override void Set()
        {
            Clean();
        }

        #endregion


        #region Private API

        /// <summary>
        /// Called when the selection changes in the editor.
        /// </summary>
        private static void HandleSelectionChanged()
        {
            I.Clean();
            if (s_skipNextSelection)
            {
                s_skipNextSelection = false;
                return;
            }

            // Cancel if the new selection is empty
            if (UnitySelection.objects == null || UnitySelection.objects.Length == 0)
                return;

            // Reset navigation cursor
            s_historyCursor = -1;
            // Pull up a similar selection if applicable
            for (int i = 0; i < I._history.Count; i++)
            {
                if (CompareSelections(UnitySelection.objects, I._history[i].Objects))
                {
                    I._history.Move(i, I._history.Count);
                    OnHistoryChange?.Invoke();
                    return;
                }
            }

            I._history.Add(new SelectionItem(UnitySelection.objects));
            if (I._history.Count >= MaxHistoryEntries)
            {
                I._history.RemoveAt(0);
            }
            OnHistoryChange?.Invoke();
        }

        /// <summary>
        /// Sets the current selection in the editor.
        /// </summary>
        /// <param name="selection">The objects to select.</param>
        /// <param name="skipCallback">If enabled, the next selection changed event from Unity will be ignored.</param>
        /// <returns>Returns true if the selection has been applied successfully.</returns>
        private static bool SetSelection(Object[] selection, bool skipCallback = false)
        {
            // Cancel if the current selection is equivalent to the one to set
            if (CompareSelections(UnitySelection.objects, selection))
                return false;

            if (skipCallback)
                s_skipNextSelection = true;

            UnitySelection.objects = selection;
            return true;
        }

        /// <summary>
        /// Compare object selections.
        /// </summary>
        /// <returns>Returns true if the selections are equal.</returns>
        private static bool CompareSelections(Object[] a, Object[] b)
        {
            // Cancel if the selections doesn't contain the same number of objects
            if (a.Length != b.Length)
                return false;

            List<Object> remainigObjects = new List<Object>(a);
            // For each object in the selection B
            foreach(Object bItem in b)
            {
                int index = remainigObjects.IndexOf(bItem);
                // Cancel if the current object doesn't exist in the selection A
                if (index < 0)
                    return false;

                remainigObjects.Remove(bItem);
            }

            // Selections are equal if all the objects in A are also present in selection B
            return remainigObjects.Count == 0;
        }

        /// <summary>
        /// Removes all empty selections stored in the history. This will also move the cursor index as needed.
        /// </summary>
        private void Clean()
        {
            bool changedHistory = false;
            bool movedCursor = false;

            for (int i = _history.Count - 1; i >= 0; i--)
            {
                if (!_history[i].Clean())
                {
                    _history.RemoveAt(i);
                    changedHistory = true;

                    if (i <= s_historyCursor)
                    {
                        s_historyCursor--;
                        movedCursor = true;
                    }
                }
            }

            if (changedHistory)
                OnHistoryChange?.Invoke();
            if (movedCursor)
                OnCursorChange?.Invoke(s_historyCursor);
        }

        #endregion

    }

}