/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;

using SideXP.Core;
using SideXP.Core.EditorOnly;

using UnitySelection = UnityEditor.Selection;
using SideXP.UI.EditorOnly;

namespace SideXP.Selection.EditorOnly
{

    /// <summary>
    /// Utility window for saving selection and navigate through selection history.
    /// </summary>
    public class SelectionHistoryWindow : EditorWindow
    {
    
        #region Fields

        private const string WindowTitle = "Selection History";
        private const string MenuItem = EditorConstants.EditorWindowMenu + "/" + WindowTitle;

        private const float InvalidSavedSelectionAlpha = .2f;
        private const float InvalidSavedSelectionTextAlpha = .5f;
        private static readonly Color SaveSelectionButtonDefaultColor = Color.white;
        private static GUIStyle s_savedSelectionButtonLabelStyle = null;

        [SerializeField]
        private AnimBool _expanded = new AnimBool(false);

        private Vector2 _scrollPosition = Vector2.zero;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this window is open.
        /// </summary>
        private void OnEnable()
        {
            SelectionHistory.OnHistoryChange += Repaint;
        }

        /// <summary>
        /// Called when this winndow is closed.
        /// </summary>
        private void OnDisable()
        {
            SelectionHistory.OnHistoryChange -= Repaint;
        }

        #endregion


        #region Public API

        /// <summary>
        /// Opens this editor window.
        /// </summary>
        [MenuItem(MenuItem)]
        public static SelectionHistoryWindow Open()
        {
            SelectionHistoryWindow window = GetWindow<SelectionHistoryWindow>(false, WindowTitle, true);
            window.Show();
            return window;
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws the window GUI.
        /// </summary>
        private void OnGUI()
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                using (new EnabledScope(SelectionHistory.HasPreviousSelection))
                {
                    if (GUILayout.Button(EditorIcons.IconContent("arrow_back", "Previous", "Focus the previous selection.", true), MoreGUI.HeightOptS))
                        SelectionHistory.Previous();
                }

                if (GUILayout.Button(EditorIcons.IconContent("history", "Display/hide the full selection history.", true), MoreEditorGUI.IconButtonStyle, MoreGUI.HeightOptS, MoreGUI.WidthOptXS))
                {
                    _expanded.target = !_expanded.target;
                }

                using (new EnabledScope(SelectionHistory.HasNextSelection))
                {
                    if (GUILayout.Button(EditorIcons.IconContent("arrow_forward", "Next", "Focus the next selection.", true), MoreGUI.HeightOptS))
                        SelectionHistory.Next();
                }
            }

            if (_expanded.faded > 0)
            {
                using (new EditorGUILayout.FadeGroupScope(_expanded.faded))
                {
                    using (new EnabledScope(SelectionHistory.Count > 0))
                    {
                        if (GUILayout.Button(EditorIcons.IconContent("history_toggle_off", "Clear History", "Clear the selection history.", true), MoreEditorGUI.IconButtonStyle, MoreGUI.HeightOptS))
                        {
                            if (EditorUtility.DisplayDialog("Clear Selection History", "Are you sure you want to delete all selection entries in the history?", "Yes", "No"))
                                SelectionHistory.Clear();
                        }
                    }

                    EditorGUILayout.Space();
                    _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition, GUILayout.ExpandHeight(false));
                    {
                        HistoryListGUI();
                    }
                    EditorGUILayout.EndScrollView();
                }
            }

            EditorGUILayout.Space();
            float buttonWidth = (position.width - MoreGUI.HMargin * (SelectionEditorPrefsSettings.I.SavedSelectionsGrid.x + 5)) / SelectionEditorPrefsSettings.I.SavedSelectionsGrid.x;
            // For each row
            for (int y = 0; y < SelectionEditorPrefsSettings.I.SavedSelectionsGrid.y; y++)
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    // For each save selection button
                    for (int x = 0; x < SelectionEditorPrefsSettings.I.SavedSelectionsGrid.x; x++)
                    {
                        int index = x + y * SelectionEditorPrefsSettings.I.SavedSelectionsGrid.x;
                        bool valid = SavedSelectionsStorage.I.IsValidSelection(index);
                        Rect rect = EditorGUILayout.GetControlRect(false, GUILayout.Width(buttonWidth), MoreGUI.HeightOptM);

                        Color color = GetSavedSelectionColor(index);
                        color.a = valid ? 1f : InvalidSavedSelectionAlpha;

                        // Draw button rect
                        EditorGUI.DrawRect(rect, color);
                        // Draw button label
                        GUIStyle labelStyle = SavedSelectionButtonLabelStyle;
                        Color textColor = color.GetOverlayTint();
                        textColor.a = valid ? 1f : InvalidSavedSelectionTextAlpha;

                        GUIContent label = new GUIContent((index + 1).ToString());
                        SelectionItem item = valid ? SavedSelectionsStorage.I[index] : null;

                        if (item != null)
                        {
                            label.tooltip = item.Label;
                            if (item.ContainsSceneObjects)
                            {
                                label.text = "*" + label.text;
                                label.tooltip = "*" + label.tooltip;
                            }
                        }
                        else
                        {
                            label.tooltip = "Assign the current selection to this button.";
                        }
                        EditorGUI.LabelField(rect, label, labelStyle.FontColor(textColor));

                        // Draw cursor rect
                        if (!valid)
                        {
                            EditorGUIUtility.AddCursorRect(rect, MouseCursor.ArrowPlus);
                        }
                        else
                        {
                            EditorGUIUtility.AddCursorRect(rect, MouseCursor.Link);
                        }

                        HandleSaveSelectionButtonEvent(rect, index, valid);
                    }
                }
            }

            if (_expanded.isAnimating)
                Repaint();
        }

        /// <summary>
        /// Draws the list of all selections in the history.
        /// </summary>
        private void HistoryListGUI()
        {
            SelectionItem[] items = SelectionHistory.History;
            GUIStyle style = EditorStyles.label;

            for (int i = items.Length - 1; i >= 0; i--)
            {
                Rect rect = EditorGUILayout.GetControlRect(false);
                Color color = Color.clear;
                if ((SelectionHistory.Cursor < 0 && i == SelectionHistory.Count - 1) || i == SelectionHistory.Cursor)
                    color = GUI.skin.settings.selectionColor;
                else if (i % 2 != 0)
                    color.a = .2f;

                EditorGUI.DrawRect(rect, color);

                style.fontStyle = SelectionHistory.Cursor < 0 || i <= SelectionHistory.Cursor ? FontStyle.Bold : FontStyle.Normal;
                if (GUI.Button(rect, (items[i].ContainsSceneObjects ? "* " : "") + items[i].Label, style))
                {
                    SelectionHistory.Cursor = i;
                }
            }
        }

        /// <summary>
        /// Handles mouse events for the save selection button area.
        /// </summary>
        /// <param name="rect">The area of the save selection button.</param>
        /// <param name="index">The index of the save selection button.</param>
        /// <param name="valid">Is the saved selection at the given index valid? Assumes that this value is exact by using a previous
        /// <see cref="SavedSelectionsStorage.IsValidSelection(int)"/> call.</param>
        private void HandleSaveSelectionButtonEvent(Rect rect, int index, bool valid)
        {
            if (Event.current.type == EventType.MouseUp && rect.Contains(Event.current.mousePosition))
            {
                if (Event.current.button == 0)
                {
                    if (valid)
                    {
                        SavedSelectionsStorage.I.ApplySelection(index);
                    }
                    else
                    {
                        SavedSelectionsStorage.I.SaveSelection(UnitySelection.objects, index);
                        Repaint();
                    }
                }
                else if (Event.current.button == 1)
                {
                    GenericMenu menu = new GenericMenu();

                    if (valid)
                    {
                        menu.AddItem(new GUIContent("Select"), false, () => SavedSelectionsStorage.I.ApplySelection(index));
                        menu.AddSeparator(null);
                    }

                    GUIContent saveCurrentSelectionLabel = new GUIContent(valid ? "Override With Current Selection" : "Save Current Selection");
                    if (UnitySelection.objects == null || UnitySelection.objects.Length == 0)
                    {
                        menu.AddDisabledItem(saveCurrentSelectionLabel);
                    }
                    else
                    {
                        menu.AddItem(saveCurrentSelectionLabel, false, () =>
                        {
                            SavedSelectionsStorage.I.SaveSelection(UnitySelection.objects, index);
                            Repaint();
                        });
                    }

                    if (valid)
                    {
                        menu.AddItem(new GUIContent("Delete"), false, () => SavedSelectionsStorage.I.DeleteSelection(index));
                    }

                    menu.ShowAsContext();
                }
            }
        }

        #endregion


        #region Private API

        /// <inheritdoc cref="s_savedSelectionButtonLabelStyle"/>
        private GUIStyle SavedSelectionButtonLabelStyle
        {
            get
            {
                if (s_savedSelectionButtonLabelStyle == null)
                {
                    s_savedSelectionButtonLabelStyle = new GUIStyle(EditorStyles.label);
                    s_savedSelectionButtonLabelStyle.alignment = TextAnchor.MiddleCenter;
                    s_savedSelectionButtonLabelStyle.normal.textColor = Color.white;
                }
                return s_savedSelectionButtonLabelStyle;
            }
        }

        /// <summary>
        /// Gets the appropriate color for the saved selection button at a given index.
        /// </summary>
        /// <param name="index">The index of the saved selection to display.</param>
        /// <returns>Returns the related color.</returns>
        private Color GetSavedSelectionColor(int index)
        {
            if (SelectionEditorPrefsSettings.I.SaveSelectionButtonColors.Length <= 0)
                return SaveSelectionButtonDefaultColor;

            index %= SelectionEditorPrefsSettings.I.SaveSelectionButtonColors.Length;
            return SelectionEditorPrefsSettings.I.SaveSelectionButtonColors[index];
        }

        #endregion

    }

}