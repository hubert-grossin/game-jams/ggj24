/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;
using UnityEditor;

using SideXP.Core;
using SideXP.Core.EditorOnly;

namespace SideXP.Data.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="EnumAssetBase"/>
    /// </summary>
    [CustomEditor(typeof(EnumAssetBase), true)]
    public class EnumAssetEditor : Editor
    {

        #region Fields

        private const string EnumItemsArrayProp = "_enumItems";

        private SubassetsList _enumItemsSubassetsList = null;
        private Editor _selectedItemEditor = null;

        #endregion


        #region Lifecycle

        private void OnEnable()
        {
            EnumItemsSubassetsList.SelectWithCallback(0);
        }

        private void OnDisable()
        {
            if (_selectedItemEditor != null)
            {
                DestroyImmediate(_selectedItemEditor);
                _selectedItemEditor = null;
            }
        }

        #endregion


        #region UI

        public override void OnInspectorGUI()
        {
            EnumItemsSubassetsList.DoLayoutList();

            EditorGUILayout.Space();
            MoreEditorGUI.HorizontalSeparator(true);
            EditorGUILayout.LabelField("Selected Item", MoreEditorGUI.TitleStyle, MoreGUI.HeightOptS);

            if (_selectedItemEditor != null)
            {
                EditorGUILayout.LabelField($"<b>{_selectedItemEditor.target.name}</b> <i><size=9>({_selectedItemEditor.target.GetType().Name})</size></i>", EditorStyles.label.RichText(true));
                EditorGUILayout.Space();
                _selectedItemEditor.OnInspectorGUI();
            }
            else
            {
                EditorGUILayout.LabelField("No enum item selected", EditorStyles.label.Italic().FontSizeDiff(-2));
            }
        }

        #endregion


        #region Private API

        /// <inheritdoc cref="_enumItemsSubassetsList"/>
        private SubassetsList EnumItemsSubassetsList
        {
            get
            {
                if (_enumItemsSubassetsList == null)
                {
                    SubassetsList.SubassetsListOptions options = SubassetsList.SubassetsListOptions.Default;
                    options.Draggable = false;
                    options.ForceSubassetType = (target as EnumAssetBase).ItemType;

                    (target as EnumAssetBase).Sort();
                    _enumItemsSubassetsList = new SubassetsList(serializedObject, serializedObject.FindProperty(EnumItemsArrayProp), options);

                    _enumItemsSubassetsList.onSelectCallback = list =>
                    {
                        if (_selectedItemEditor != null)
                        {
                            DestroyImmediate(_selectedItemEditor);
                            _selectedItemEditor = null;
                        }

                        if (list.index >= 0 && list.serializedProperty.arraySize > 0)
                        {
                            Object asset = list.serializedProperty.GetArrayElementAtIndex(list.index).objectReferenceValue;
                            if (asset != null)
                                _selectedItemEditor = CreateEditor(asset);
                        }
                    };
                }
                return _enumItemsSubassetsList;
            }
        }

        #endregion

    }

}