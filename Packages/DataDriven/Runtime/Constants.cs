/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Data
{

    /// <summary>
    /// Miscellaneous constant values.
    /// </summary>
    public static class Constants
    {

        /// <inheritdoc cref="SideXP.Core.Constants.BaseHelpUrl"/>
        public const string BaseHelpUrl = SideXP.Core.Constants.BaseHelpUrl + "/data-driven";

        /// <inheritdoc cref="SideXP.Core.Constants.CreateAssetMenu"/>
        public const string CreateAssetMenu = SideXP.Core.Constants.CreateAssetMenu;

    }

}