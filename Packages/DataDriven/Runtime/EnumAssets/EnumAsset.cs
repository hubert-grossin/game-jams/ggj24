/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Data
{

    /// <summary>
    /// Generic enumeration asset.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    [CreateAssetMenu(fileName = "New" + nameof(EnumAsset), menuName = Constants.CreateAssetMenu + "/Enum Asset")]
    public class EnumAsset : EnumAssetGeneric<EnumItemAsset> { }

}