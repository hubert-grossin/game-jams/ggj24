/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;
using System.Collections.Generic;

using UnityEngine;

namespace SideXP.Data
{

    /// <inheritdoc cref="EnumAssetBase"/>
    [HelpURL(Constants.BaseHelpUrl)]
    public abstract class EnumAssetGeneric<TItem> : EnumAssetBase
        where TItem : ScriptableObject
    {

        #region Public API

        /// <inheritdoc cref="EnumAssetBase.Items"/>
        public new TItem[] Items
        {
            get
            {
                List<TItem> typedItems = new List<TItem>();
                foreach (ScriptableObject item in base.Items)
                {
                    if (item is TItem typedItem)
                        typedItems.Add(typedItem);
                }
                return typedItems.ToArray();
            }
        }

        /// <inheritdoc cref="EnumAssetBase.ItemType"/>
        public override Type ItemType => typeof(TItem);

        /// <inheritdoc cref="EnumAssetBase.GetItem(string)"/>
        public new TItem GetItem(string name)
        {
            foreach (TItem item in Items)
            {
                if (item.name == name)
                    return item;
            }
            return null;
        }

        /// <inheritdoc cref="EnumAssetBase.GetItem(string, out ScriptableObject)"/>
        public bool GetItem(string name, out TItem item)
        {
            item = GetItem(name);
            return item != null;
        }

        #endregion

    }

}