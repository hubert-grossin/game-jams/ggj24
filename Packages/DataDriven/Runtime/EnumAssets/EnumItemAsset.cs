/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Data
{

    /// <summary>
    /// Represents a basic enum item.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    public class EnumItemAsset : ScriptableObject
    {

        [SerializeField, TextArea(3, 6)]
        [Tooltip("Describes the usage of this enum item. This is meant to be used as notes for the development team.")]
        public string Description = string.Empty;

    }

}