/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using UnityEngine;

namespace SideXP.Data
{

    /// <summary>
    /// Base class that represents an enumeration that cna be modified from the editor.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    public abstract class EnumAssetBase : ScriptableObject
    {

        #region Fields

        [SerializeField]
        [Tooltip("The items in this enumeration.")]
        private ScriptableObject[] _enumItems = { };

        #endregion


        #region Public API

        /// <summary>
        /// Gets the enum items list.
        /// </summary>
        public ScriptableObject[] Items
        {
            get
            {
                Sort();
                return _enumItems;
            }
        }

        /// <summary>
        /// Gets the expected enum item asset type.
        /// </summary>
        public virtual Type ItemType => typeof(ScriptableObject);

        /// <summary>
        /// Gets an item in this enum by its name.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <returns>Returns the found enum item asset.</returns>
        public ScriptableObject GetItem(string name)
        {
            foreach (ScriptableObject item in Items)
            {
                if (item.name == name)
                    return item;
            }
            return null;
        }

        /// <param name="item">Outputs the found enum item asset.</param>
        /// <returns>Returns true if the named item has been found.</returns>
        /// <inheritdoc cref="GetItem(string)"/>
        public bool GetItem(string name, out ScriptableObject item)
        {
            item = GetItem(name);
            return item != null;
        }

        /// <summary>
        /// Checks if this enumeration asset contains the named item.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <returns>Returns true if this enumeration asset contains the named item.</returns>
        public bool Contains(string name)
        {
            return GetItem(name, out _);
        }

        /// <summary>
        /// Sorts enum items by name.
        /// </summary>
        public void Sort()
        {
            Array.Sort(_enumItems, (a, b) => a.name.CompareTo(b.name));
        }

        #endregion

    }

}