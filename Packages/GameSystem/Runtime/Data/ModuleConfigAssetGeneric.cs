/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using UnityEngine;

namespace SideXP.GameSystem
{

    /// <typeparam name="T">The type of module configured from this asset.</typeparam>
    /// <inheritdoc cref="ModuleConfigAsset"/>
    [HelpURL(Constants.BaseHelpUrl)]
    public class ModuleConfigAssetGeneric<T> : ModuleConfigAsset
        where T : Module
    {

        /// <inheritdoc cref="ModuleConfigAsset.ModuleType"/>
        public override Type ModuleType => typeof(T);

    }

}