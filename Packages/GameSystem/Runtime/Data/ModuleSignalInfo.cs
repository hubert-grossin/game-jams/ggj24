/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using SideXP.Broadcaster;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Groups informations about a <see cref="SignalAsset"/> that can be emitted from a module of a <see cref="GameRefAsset"/>.
    /// </summary>
    [System.Serializable]
    public class ModuleSignalInfo
    {

        /// <summary>
        /// The name of the field or the property in the module that requires this signal.
        /// </summary>
        public string PropName = null;

        /// <summary>
        /// The <see cref="System.Type.AssemblyQualifiedName"/> of the module that uses this signal.
        /// </summary>
        public string ModuleType = null;

        /// <summary>
        /// The signal asset itself.
        /// </summary>
        public SignalAsset Asset = null;

    }

}