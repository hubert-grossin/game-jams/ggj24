/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using UnityEngine;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Containss the configuration for a specific module in your game.<br/>
    /// You must inherit from this class to define which module is configured from this asset.
    /// </summary>
    /// <remarks>This type of assets are most likely generated from your modules implemenration using <see cref="ConfigAttribute"/> on
    /// fields and properties declared in those modules, so settings are automatically synchronized with the modules
    /// implementation.</remarks>
    [HelpURL(Constants.BaseHelpUrl)]
    public abstract class ModuleConfigAsset : ScriptableObject
    {

        /// <summary>
        /// The type of the module to which this config asset is related.
        /// </summary>
        public abstract Type ModuleType { get; }

    }

}