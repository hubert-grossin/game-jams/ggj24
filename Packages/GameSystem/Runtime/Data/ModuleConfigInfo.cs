/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Groups informations about a <see cref="ModuleConfigAsset"/> bound to a module. It can also be used to store overrides in a
    /// <see cref="GameRefAsset.Environment"/> or an environment.
    /// </summary>
    [System.Serializable]
    public class ModuleConfigInfo
    {

        /// <summary>
        /// The depth of the module to which this config asset is attached.
        /// </summary>
        public int Depth = 0;

        /// <summary>
        /// The index of the module to which this config asset is attached.
        /// </summary>
        public int Index = 0;

        /// <summary>
        /// The config asset itself.
        /// </summary>
        public ModuleConfigAsset Asset = null;

        /// <inheritdoc cref="ModuleConfigAsset.ModuleType"/>
        public Type ModuleType => Asset != null ? Asset.ModuleType : null;

        /// <summary>
        /// Checks if this config is bound to a given module.
        /// </summary>
        /// <param name="moduleRef">The reference of the module to check.</param>
        /// <returns>Returns true if this config is bound to a given module.</returns>
        public bool Match(ModuleRef moduleRef)
        {
            return
                moduleRef.Depth == Depth &&
                moduleRef.Index == Index &&
                moduleRef.ModuleType == ModuleType;
        }

    }

}