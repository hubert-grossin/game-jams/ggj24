/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

using System;

namespace SideXP.GameSystem
{

    /// <inheritdoc cref="GameRefAsset"/>
    [HelpURL(Constants.BaseHelpUrl)]
    public abstract class GameRefAssetGeneric<T> : GameRefAsset
        where T : Module
    {

        /// <inheritdoc cref="GameRefAsset.MainModuleType"/>
        public override Type MainModuleType => typeof(T);

        /// <inheritdoc cref="Game.MainModuleRef"/>
        public new ModuleRef<T> MainModuleRef => Game != null ? (Game.MainModuleRef as ModuleRef<T>) : null;

        /// <inheritdoc cref="Game.MainModule"/>
        public new T MainModule => Game != null ? Game.MainModule as T : null;

    }

}