/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;
using System.Collections.Generic;

using UnityEngine;

using SideXP.Core;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Contains an instance of the game (using the <see cref="Game"/> class, the signals that can be emitted from it, and defines the
    /// configuration to use.<br/>
    /// An asset of that type can be used as reference to any object that require access to a game instance, so it can find all the modules,
    /// values and signals required.<br/>
    /// You must create a class that inherit from it to declare the main module of you game. Also, if you need updates, you must place a
    /// <see cref="GameRunnerComponent"/> in the scene, and assign your custom <see cref="GameRefAsset"/> to it.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    public abstract class GameRefAsset : ScriptableObject
    {

        #region Subassets

        /// <summary>
        /// Overrides the default configuration of a <see cref="GameRefAsset"/>.
        /// </summary>
        [System.Serializable]
        public class Environment
        {

            [Tooltip("The name of this environment. Used to select it from a " + nameof(GameRefAsset) + ".")]
            public string Name = null;

            [TextArea(3, 6)]
            [Tooltip("The description of this environment. Used as in-editor documentation.")]
            public string Description = null;

            [Tooltip("The configurations to used with this environment.")]
            public ModuleConfigInfo[] ConfigOverrides = { };
        
        }

        #endregion


        #region Fields

#if SIDEXP_LIB_PROJECT
        [SerializeField]
#else
        [SerializeField, HideInInspector]
#endif
        [Tooltip("The default configuration assets used for this game instance.")]
        private ModuleConfigInfo[] _defaultConfigs = { };

#if SIDEXP_LIB_PROJECT
        [SerializeField]
#else
        [SerializeField, HideInInspector]
#endif
        [Tooltip("The signals that can be emitted from the modules in this game instance.")]
        private ModuleSignalInfo[] _signals = { };

#if SIDEXP_LIB_PROJECT
        [SerializeField]
#else
        [SerializeField, HideInInspector]
#endif
        [Tooltip("The custom environments used to override the default configuration on startup.")]
        private Environment[] _environments = { };

        [SerializeField]
        [Tooltip("The name of the selected environment used to override the default configuration of this game instance.")]
        private string _selectedEnvironmentName = null;

        /// <summary>
        /// The game instance itself.
        /// </summary>
        private Game _game = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this asset is loaded.
        /// </summary>
        private void OnEnable()
        {
            List<ModuleConfigInfo> configs = new List<ModuleConfigInfo>(_defaultConfigs);
            if (SelectedEnvironment != null)
            {
                foreach (ModuleConfigInfo configOverride in SelectedEnvironment.ConfigOverrides)
                {
                    int index = configs.FindIndex(i => i.Depth == configOverride.Depth && i.Index == configOverride.Index && i.ModuleType == configOverride.ModuleType);
                    if (index >= 0)
                        configs[index] = configOverride;
                }
            }

            _game = new Game(MainModuleType, configs.ToArray(), _signals);
        }

        /// <summary>
        /// Called when this asset is unloaded.
        /// </summary>
        private void OnDisable()
        {
            _game = null;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_game"/>
        public Game Game => _game;

        /// <summary>
        /// The type of the game instance's main module.
        /// </summary>
        public abstract Type MainModuleType { get; }

        /// <inheritdoc cref="Game.MainModuleRef"/>
        public ModuleRef MainModuleRef => _game != null ? _game.MainModuleRef : null;

        /// <inheritdoc cref="Game.MainModule"/>
        public IModule MainModule => _game != null ? _game.MainModule : null;

        /// <inheritdoc cref="Game.Initialize"/>
        public bool Initialize()
        {
            if (_game != null)
            {
                _game.Initialize();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Enables the main module of the game instance if it's not, disables it otherwise.
        /// </summary>
        public void ToggleEnabled()
        {
            Enabled = !Enabled;
        }

        /// <summary>
        /// Gets the configuration asset for a given module.
        /// </summary>
        /// <param name="moduleRef">The module of which you want to get the config asset.</param>
        /// <param name="isOverride">Is the found config asset an override from an environment?</param>
        /// <param name="ignoreEnvironment">If enabled, ignore the selected environment and get the default configuration asset.</param>
        /// <returns>Returns the found config asset.</returns>
        public ModuleConfigAsset GetConfig(ModuleRef moduleRef, out bool isOverride, bool ignoreEnvironment = false)
        {
            isOverride = false;

            // Evaluate overrides if applicable
            if (!ignoreEnvironment && SelectedEnvironment != null)
            {
                // For each environment override
                foreach (ModuleConfigInfo info in SelectedEnvironment.ConfigOverrides)
                {
                    if (info.Match(moduleRef))
                    {
                        isOverride = true;
                        return info.Asset;
                    }
                }
            }

            // For each default config
            foreach (ModuleConfigInfo info in _defaultConfigs)
            {
                if (info.Match(moduleRef))
                    return info.Asset;
            }

            return null;
        }

        /// <param name="configAsset">Outputs the found config asset.</param>
        /// <returns>Returns true if a config asset has been found.</returns>
        /// <inheritdoc cref="GetConfig(ModuleRef, out bool bool)"/>
        public bool GetConfig(ModuleRef moduleRef, out ModuleConfigAsset configAsset, out bool isOverride, bool ignoreEnvironment = false)
        {
            configAsset = GetConfig(moduleRef, out isOverride, ignoreEnvironment);
            return configAsset != null;
        }

        /// <inheritdoc cref="GetConfig(ModuleRef, out bool, bool)"/>
        public ModuleConfigAsset GetConfig(ModuleRef moduleRef, bool ignoreEnvironment = false)
        {
            return GetConfig(moduleRef, out bool _, ignoreEnvironment);
        }

        /// <inheritdoc cref="GetConfig(ModuleRef, out ModuleConfigAsset, out bool, bool)"/>
        public bool GetConfig(ModuleRef moduleRef, out ModuleConfigAsset configAsset, bool ignoreEnvironment = false)
        {
            configAsset = GetConfig(moduleRef, ignoreEnvironment);
            return configAsset != null;
        }

        /// <inheritdoc cref="Game.Enabled"/>
        public bool Enabled
        {
            get => _game != null && _game.Enabled;
            set
            {
                if (_game != null)
                    _game.Enabled = value;
            }
        }

        /// <inheritdoc cref="Game.Reset"/>
        public void ResetGame()
        {
            if (_game != null)
                _game.Reset();
        }

        /// <inheritdoc cref="Game.Update(float)"/>
        public void UpdateGame(float delta)
        {
            if (_game != null)
                _game.Update(delta);
        }

        #endregion


        #region Internal API

        /// <inheritdoc cref="_signals"/>
        internal ModuleSignalInfo[] SignalInfos => _signals;

        /// <inheritdoc cref="_defaultConfigs"/>
        internal ModuleConfigInfo[] DefaultConfigInfos => _defaultConfigs;

        /// <inheritdoc cref="_environments"/>
        internal Environment[] Environments => _environments;

        /// <summary>
        /// Gets the selected environment.
        /// </summary>
        internal Environment SelectedEnvironment
        {
            get
            {
                if (string.IsNullOrEmpty(_selectedEnvironmentName))
                    return null;

                foreach (Environment env in _environments)
                {
                    if (env.Name == _selectedEnvironmentName)
                        return env;
                }

                return null;
            }
        }

        /// <summary>
        /// Selects the named environment.
        /// </summary>
        /// <param name="environmentName">The name of the selected environment.</param>
        /// <returns>Returns true if the named environment exists.</returns>
        internal bool SelectEnvironment(string environmentName)
        {
            foreach (Environment env in _environments)
            {
                if (env.Name == environmentName)
                {
                    _selectedEnvironmentName = env.Name;
                    return true;
                }
            }

            _selectedEnvironmentName = null;
            return false;
        }

        #endregion


        #region Private API

        /// <summary>
        /// Gets the configuration assets to use for the game instance, taking account of the selected environment's overrides.
        /// </summary>
        /// <returns>Returns the configuration assets to use for the game instance.</returns>
        private ModuleConfigAsset[] GetConfigs()
        {
            List<ModuleConfigInfo> configs = new List<ModuleConfigInfo>(_defaultConfigs);

            // Find the selected environment
            Environment environment = null;
            if (!string.IsNullOrEmpty(_selectedEnvironmentName))
            {
                foreach (Environment env in _environments)
                {
                    if (env.Name == _selectedEnvironmentName)
                    {
                        environment = env;
                        break;
                    }
                }
            }

            // If a valid environment is selected
            if (environment != null)
            {
                // For each config override
                foreach (ModuleConfigInfo overrideInfo in environment.ConfigOverrides)
                {
                    // Skip if the configuration override is not valid
                    if (overrideInfo.Asset == null)
                    {
                        Debug.LogWarning($"The environment {environment.Name} in the {nameof(GameRefAsset)} \"{name}\" contains a null configuration override.", this);
                        continue;
                    }

                    // Get the index of the default configuration that has the same ids
                    int index = configs.FindIndex(i =>
                    {
                        return
                            i.ModuleType == overrideInfo.ModuleType &&
                            i.Depth == overrideInfo.Depth &&
                            i.Index == overrideInfo.Index;
                    });

                    // Skip if no default configuration match with the override ids
                    if (index < 0)
                    {
                        Debug.LogWarning($"The environment {environment.Name} in the {nameof(GameRefAsset)} \"{name}\" contains an invalid configuration override ({nameof(ModuleConfigInfo.ModuleType)} = {overrideInfo.ModuleType.FullName}, {nameof(ModuleConfigInfo.Depth)} = {overrideInfo.Depth}, {nameof(ModuleConfigInfo.Index)} = {overrideInfo.Index}).", this);
                        continue;
                    }

                    // Override config
                    configs[index] = overrideInfo;
                }
            }

            return configs.Map(i => i.Asset);
        }

        #endregion

    }

}