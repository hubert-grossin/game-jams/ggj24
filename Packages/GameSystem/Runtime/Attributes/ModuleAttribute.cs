/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Adds metadata to a <see cref="Module"/> implementation.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ModuleAttribute : Attribute
    {

        /// <summary>
        /// The path to which the module config asset script will be generated. If the given path is relative, it's resolved from the module
        /// script (unless <see cref="RelativeFromAssets"/> is enabled). If the path includes a file name, it's used as both the generated
        /// script and class name. If no path is provided, the script is generated in the module script directory, with default name (which
        /// is the name of the module type followed by "ConfigAsset").
        /// </summary>
        public string Path { get; set; } = null;

        /// <summary>
        /// If enabled, the given path will be resolved from the /Assets folder of the project instead of being resolved from this script's
        /// path.
        /// </summary>
        public bool RelativeFromAssets { get; set; } = false;

        /// <summary>
        /// The custom name of the config asset instance generated for this module.
        /// </summary>
        public string ConfigName { get; set; } = null;

    }

}