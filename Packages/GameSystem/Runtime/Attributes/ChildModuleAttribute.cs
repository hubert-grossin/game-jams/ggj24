/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Marks a <see cref="ModuleRef"/> field or a property in a module implementation as expected to contain that reference as a child
    /// module. This is the only way for a module to declare child ones.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class ChildModuleAttribute : Attribute
    {

        /// <summary>
        /// Defines if the child module is enabled the first time it's loaded.
        /// </summary>
        public bool Enabled { get; set; } = true;

    }

}