/**
* Sideways Experiments (c) 2024
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Marks a field or a property in a module implmentation as being used as parameter in the <see cref="FlowGraph"/> of a
    /// <see cref="Game"/> instance.<br/>
    /// Using this attribute exempts you to manually synchronize class data and <see cref="FlowGraph"/> parameters.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class FlowParamAttribute : Attribute
    {

        #region Fields
        #endregion


        #region Lifecycle
        #endregion


        #region Public API
        #endregion

    }

}