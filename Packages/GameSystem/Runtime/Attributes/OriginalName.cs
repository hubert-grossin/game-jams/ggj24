/**
* Sideways Experiments (c) 2024
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Defines the original name of a field or a property in an auto-generated configuration asset.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = true)]
    public class OriginalNameAttribute : Attribute
    {

        /// <summary>
        /// The name of the field or property in the module class.
        /// </summary>
        public string Name { get; private set; }

        /// <inheritdoc cref="OriginalNameAttribute"/>
        /// <param name="name">The name of the field or property in the module class.</param>
        public OriginalNameAttribute(string name)
        {
            Name = name;
        }

    }

}