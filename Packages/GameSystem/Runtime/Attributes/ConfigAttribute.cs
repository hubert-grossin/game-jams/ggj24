/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Marks a field or a property in a module implmentation as being exposed to a configuration asset.<br/>
    /// If a <see cref="Module"/> contains fields or properties marked with this attribute, the system will automatically generate a
    /// <see cref="ModuleConfigAssetGeneric{T}"/> script that include all these marked elements, making them configurable from the editor.
    /// </summary>
    /// <remarks>Only types natively serializable by Unity will be visible in configuration assets.</remarks>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class ConfigAttribute : Attribute { }

}