/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using SideXP.Broadcaster;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Marks a field or a property as being a signal emitted from a <see cref="Module"/> exposed in the editor.<br/>
    /// If a <see cref="Module"/> contains fields or properties marked with this attribute, the system will automatically generate a
    /// <see cref="SignalAssetGeneric{T}"/> asset for each one of them, so the signals can be referenced by objects in the scene or even
    /// other assets.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class SignalAttribute : Attribute
    {

        /// <summary>
        /// The name of the generated <see cref="SignalAsset"/> instance. You can use "/" to create submenus in custom editors that expect
        /// a <see cref="SignalAsset"/> reference.
        /// </summary>
        public string Name { get; set; } = null;

        /// <summary>
        /// The description of the signal. Mostly used as in-editor documentation.
        /// </summary>
        public string Description { get; set; }

        /// <inheritdoc cref="SignalAttribute(string, string)"/>
        public SignalAttribute()
            : this(null, null) { }

        /// <inheritdoc cref="SignalAttribute(string, string)"/>
        public SignalAttribute(string name)
            : this(name, null) { }

        /// <inheritdoc cref="SignalAttribute"/>
        /// <param name="name">The name of the generated <see cref="SignalAsset"/> instance. You can use "/" to create submenus in custom
        /// editors that expect a <see cref="SignalAsset"/> reference.</param>
        /// <param name="description">The description of the signal. Mostly used as in-editor documentation.</param>
        public SignalAttribute(string name, string description)
        {
            Name = name;
            Description = description;
        }

    }

}