/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using SideXP.Core;

using UnityEngine;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Intinialize and update a game instance.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    [AddComponentMenu(Constants.AddComponentMenu + "/Game Runner")]
    public class GameRunnerComponent : MonoBehaviour
    {

        #region Enums & Subclasses

        /// <summary>
        /// Defines how this runner should initialize the game instance and start running the game.
        /// </summary>
        private enum EInitialization
        {
            Manual,
            InitializeOnAwake,
            InitializeOnStart,
            InitializeAfterDelay
        }

        #endregion


        #region Fields

        [SerializeField]
        [Tooltip("The reference to the game instance to run.")]
        private GameRefAsset _gameRef = null;

        [Header("Initialization")]

        [SerializeField]
        [Tooltip("Defines how this runner should initialize the game instance.")]
        private EInitialization _initializationMode = EInitialization.InitializeOnStart;

        [SerializeField, Indent]
        [Tooltip("The delay before initializing the game instance. Used only if Initialization Mode is set to Initialize After Delay.")]
        private float _initializationDelay = 1f;

        [SerializeField]
        [Tooltip("If checked, the main module is enabled as soon as the game instance is initialized.")]
        private bool _enableOnInitialization = false;

        [Header("Update")]

        [SerializeField]
        [Tooltip("If checked, the game instance will be updated on " + nameof(Update) + "() message. Otherwise, you should call " + nameof(UpdateGame) + "() manually.")]
        private bool _autoUpdate = true;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this component is loaded.
        /// </summary>
        protected virtual void Awake()
        {
            if (_initializationMode == EInitialization.InitializeOnAwake)
                Initialize();
        }

        /// <summary>
        /// Called the first time this component is enabled.
        /// </summary>
        protected virtual void Start()
        {
            if (_initializationMode == EInitialization.InitializeOnStart)
            {
                Initialize();
            }
            else if (_initializationMode == EInitialization.InitializeAfterDelay)
            {
                if (_initializationDelay <= 0f)
                    Initialize();
                else
                    Invoke(nameof(Initialize), _initializationDelay);
            }
        }

        /// <summary>
        /// Called each frame.
        /// </summary>
        protected virtual void Update()
        {
            if (_autoUpdate)
                UpdateGame(Time.deltaTime);
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="GameRefAsset.Enabled"/>
        public bool Enabled
        {
            get => _gameRef.Enabled;
            set => _gameRef.Enabled = value;
        }

        /// <summary>
        /// Initializes the game instance.
        /// </summary>
        public void Initialize()
        {
            _gameRef.Initialize();
            if (_enableOnInitialization)
                Enabled = true;
        }

        /// <inheritdoc cref="GameRefAsset.ToggleEnabled"/>
        public void ToggleEnabled()
        {
            _gameRef.ToggleEnabled();
        }

        /// <summary>
        /// Updates the game instance.
        /// </summary>
        /// <param name="delta">The time elapsed since the previous call.</param>
        public void UpdateGame(float delta)
        {
            _gameRef.UpdateGame(delta);
        }

        /// <inheritdoc cref="GameRefAsset.ResetGame"/>
        public void ResetGame()
        {
            _gameRef.ResetGame();
        }

        #endregion


        #region Protected API

        /// <inheritdoc cref="_gameRef"/>
        protected GameRefAsset GameRef => _gameRef;

        #endregion

    }

}