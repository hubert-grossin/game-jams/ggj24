/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.GameSystem
{

    /// <summary>
    /// Qualifies a class as being part of a game system hierarchy. This can be used with native classes and components.
    /// </summary>
    public interface IModule
    {

        #region Properties

        /// <summary>
        /// Is this module enabled?
        /// </summary>
        bool Enabled { get; set; }

        /// <summary>
        /// Checks if this module (and this module only, not its parents) is enabled.
        /// </summary>
        bool EnabledSelf { get; set; }

        /// <summary>
        /// The <see cref="ModuleRef"/> that contains this module. Note that even if it can be read, only the system should be allowed to
        /// set it.
        /// </summary>
        ModuleRef ModuleRef { get; set; }

        #endregion


        #region API

        /// <summary>
        /// Notifies this module that its configuration has been setup from its <see cref="Game"/> instance.
        /// </summary>
        void NotifySetup();

        /// <summary>
        /// Resets this module to its default state.
        /// </summary>
        void ResetModule();

        /// <summary>
        /// Updates this module. This should be called from the <see cref="Game"/> instance or the direct parent module.
        /// </summary>
        /// <param name="delta">The time elapsed since the previous call.</param>
        void UpdateModule(float delta);

        /// <summary>
        /// Enables this module. Assumes that it has been initialized.
        /// </summary>
        void EnableModule();

        /// <summary>
        /// Disables this module. Assumes that it has been initialized.
        /// </summary>
        void DisableModule();

        #endregion

    }

}