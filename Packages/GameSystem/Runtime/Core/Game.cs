/**
* Sideways Experiments (c) 2024
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System;
using System.Collections.Generic;

using UnityEngine;

using SideXP.Broadcaster;
using SideXP.Core.Reflection;
using static UnityEngine.ParticleSystem;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Represents an instance of your game.
    /// </summary>
    public class Game
    {

        #region Fields

        /// <summary>
        /// The reference to the main module of this game instance.
        /// </summary>
        private ModuleRef _mainModuleRef = null;

        /// <summary>
        /// The configuration assets to use in this game instance.
        /// </summary>
        private ModuleConfigInfo[] _configs = null;

        /// <summary>
        /// The signals that can be emitted from the modules of this game instance.
        /// </summary>
        private ModuleSignalInfo[] _signals = null;

        /// <summary>
        /// The timestamp of the latest update.
        /// </summary>
        private long _previousUpdateTimestamp = 0;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="Game"/>
        /// <param name="mainModuleType">The type of the main module of this game instance.</param>
        /// <param name="configs">The configuration assets to use in this game instance.</param>
        /// <param name="signals">The signals that can be emitted from the modules of this game instance.</param>
        internal Game(Type mainModuleType, ModuleConfigInfo[] configs, ModuleSignalInfo[] signals)
        {
            _mainModuleRef = new ModuleRef(mainModuleType, this);

            _configs = configs;
            _signals = signals;

            _mainModuleRef.Load(out _);
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_mainModuleRef"/>
        public ModuleRef MainModuleRef => _mainModuleRef;

        /// <summary>
        /// The instance of this game's main module.
        /// </summary>
        public IModule MainModule => _mainModuleRef.I;

        /// <summary>
        /// Is this game instance's main module enabled?
        /// </summary>
        public bool Enabled
        {
            get => MainModule != null && MainModule.Enabled;
            set
            {
                if (MainModule != null)
                    MainModule.Enabled = value;
            }
        }

        /// <summary>
        /// Initializes this game instance.
        /// </summary>
        public void Initialize()
        {
            Setup(MainModuleRef);
        }

        /// <summary>
        /// Resets this game instance's main module.
        /// </summary>
        public void Reset()
        {
            if (MainModule != null)
                MainModule.ResetModule();
        }

        /// <summary>
        /// Updates this game instance's main module.
        /// </summary>
        /// <param name="delta">The time elapsed since the last update call. If 0 or negative, uses an internal delta time based on
        /// timestamp.</param>
        public void Update(float delta = 0)
        {
            if (MainModule == null)
                return;

            // Init timestamp
            if (_previousUpdateTimestamp <= 0)
                _previousUpdateTimestamp = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

            // Use internal delta time based on timestamp if invalid delta provided
            if (delta <= 0)
            {
                long timestamp = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                delta = (timestamp - _previousUpdateTimestamp) / 1000f;
            }

            MainModule.UpdateModule(delta);
        }

        #endregion


        #region Internal API

        /// <summary>
        /// Resets a given module in this game instance.
        /// </summary>
        /// <param name="moduleRef">The ref of the module to reset.</param>
        /// <returns>Returns true if the module has been reset successfully.</returns>
        internal bool Reset(ModuleRef moduleRef)
        {
            if (MainModule == null)
                return false;

            bool success = Setup(moduleRef);
            return success;
        }

        #endregion


        #region Private API

        /// <summary>
        /// Applies the configuration of a given module.
        /// </summary>
        /// <param name="moduleRef">The module for which to apply the configuration.</param>
        /// <param name="ignoreChildren">If enabled, only the given module will be setup, and its children in the hierarchy will be
        /// ignored.</param>
        /// <returns>Returns true if the module (and its children if applicable) has been setup successfully.</returns>
        private bool Setup(ModuleRef moduleRef, bool ignoreChildren = false)
        {
            // Cancel if the module can't be loaded
            if (!moduleRef.Load(out IModule moduleInstance))
                return false;

            ModuleConfigAsset config = GetConfig(moduleRef);
            bool success = true;

            // Apply the configuration if a configuration asset exists for the given module
            if (config != null)
                ApplyConfig(moduleInstance, config);

            // Provide the signal assets to the module
            if (!ProvideSignalAssets(moduleInstance))
                success = false;

            // If children should be setup too
            if (!ignoreChildren)
            {
                // For each child in the hierarchy
                foreach (ModuleRef child in moduleRef.Children)
                {
                    // Try to setup the current child module
                    if (!Setup(child))
                        success = false;
                }

                moduleInstance.NotifySetup();
            }

            return success;
        }

        /// <summary>
        /// Applies a configuration to a module.
        /// </summary>
        /// <param name="module">The module to configure.</param>
        /// <param name="configAsset">The configuration to apply.</param>
        private void ApplyConfig(IModule module, ModuleConfigAsset configAsset)
        {
            List<(FieldOrPropertyInfo field, string nameInModule)> configFieldsList = new List<(FieldOrPropertyInfo, string)>();
            List<FieldOrPropertyInfo> moduleFieldsList = new List<FieldOrPropertyInfo>(
                ReflectionUtility.GetFieldsAndPropertiesWithAttribute<ConfigAttribute>(module, true));

            // Find all fields and properties from config
            foreach (FieldOrPropertyInfo configField in ReflectionUtility.GetFieldsAndProperties(configAsset, true))
            {
                if (configField.GetCustomAttribute(out OriginalNameAttribute nameInModuleAttr))
                    configFieldsList.Add((configField, nameInModuleAttr.Name));
                else
                    configFieldsList.Add((configField, configField.Name));
            }

            // For each config field
            foreach ((FieldOrPropertyInfo field, string nameInModule) in configFieldsList)
            {
                int index = moduleFieldsList.FindIndex(i => i.Name == nameInModule && i.CanWrite);
                // Skip if the field doesn't exist in the module
                if (index < 0)
                    continue;

                // Set module's field value from config
                moduleFieldsList[index].SetValue(module, field.GetValue(configAsset));
                // Remvove processed module field from the list
                moduleFieldsList.RemoveAt(index);
            }
        }

        /// <summary>
        /// Provides the registered signal assets to the given module.
        /// </summary>
        /// <param name="module">The module for which to assign the signal assets.</param>
        /// <returns>Returns true if all the required signal assets have been assigned successfully to the module, or if it doesn't need any
        /// signal asset.</returns>
        private bool ProvideSignalAssets(IModule module)
        {
            bool success = true;
            List<FieldOrPropertyInfo> signalProps = new List<FieldOrPropertyInfo>(ReflectionUtility.GetFieldsAndPropertiesWithAttribute<SignalAttribute>(module, true));
            // Stop if there's no signal properties to assign in the given module
            if (signalProps.Count == 0)
                return success;

            Type moduleType = module.GetType();
            foreach (FieldOrPropertyInfo prop in signalProps)
            {
                SignalAsset signalAsset = GetSignal(moduleType, prop);

                // Skip if no signal asset has been found
                if (signalAsset == null)
                {
                    Debug.LogError($"No {nameof(Broadcaster.SignalAsset)} has been found for property {moduleType}.{prop.Name}. Please reimport your game ref assets to fix this issue.");
                    success = false;
                    continue;
                }

                if (prop.CanWrite)
                    prop.SetValue(module, signalAsset);
            }

            return success;
        }

        /// <summary>
        /// Gets the configuration asset for a given module.
        /// </summary>
        /// <param name="moduleRef">The module of which to get the configuration asset.</param>
        /// <returns>Returns the found configuration asset.</returns>
        private ModuleConfigAsset GetConfig(ModuleRef moduleRef)
        {
            foreach (ModuleConfigInfo configInfo in _configs)
            {
                if (configInfo.Match(moduleRef))
                    return configInfo.Asset;
            }
            return null;
        }

        /// <summary>
        /// Gets a signal asset for a given module signal property.
        /// </summary>
        /// <param name="moduleType">The type of the signal that owns the property.</param>
        /// <param name="prop">The property itself.</param>
        /// <returns>Returns the found signal asset.</returns>
        private SignalAsset GetSignal(Type moduleType, FieldOrPropertyInfo prop)
        {
            string moduleTypeName = moduleType.AssemblyQualifiedName;
            foreach (ModuleSignalInfo signalInfo in _signals)
            {
                if (signalInfo.ModuleType == moduleTypeName && signalInfo.PropName == prop.Name)
                    return signalInfo.Asset;
            }
            return null;
        }

        #endregion

    }

}