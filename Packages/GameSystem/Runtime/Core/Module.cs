

using System.Diagnostics;

/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */
namespace SideXP.GameSystem
{

    /// <summary>
    /// Represents a single game mechanic or an ensemble of modules.<br/>
    /// This is the base components of your game system logic.
    /// </summary>
    public abstract class Module : IModule
    {

        #region Delegates

        /// <summary>
        /// Called after this module is enabled or disabled.
        /// </summary>
        /// <param name="enabled">Is this module now enabled?</param>
        public delegate void EnabledStateChangeDelegate(bool enabled);

        /// <summary>
        /// Called after this module is reset to its default state.
        /// </summary>
        public delegate void ResetDelegate();

        #endregion


        #region Fields

        /// <inheritdoc cref="EnabledStateChangeDelegate"/>
        public event EnabledStateChangeDelegate OnEnabledStateChange;

        /// <inheritdoc cref="ResetDelegate"/>
        public event ResetDelegate OnReset;

        /// <summary>
        /// The <see cref="ModuleRef"/> instance from which this module has been loaded.
        /// </summary>
        private ModuleRef _moduleRef = null;

        /// <summary>
        /// Flag enabled after this module has been loaded and setup.
        /// </summary>
        private bool _initialized = false;

        /// <summary>
        /// Flag enabled the first time this module has been enabled.
        /// </summary>
        private bool _started = false;

        /// <summary>
        /// Is this module enabled?
        /// </summary>
        private bool _enabled = false;

        ///// <summary>
        ///// The part of the <see cref="FlowGraph"/> owned by this module. This is assigned after the system calls <see cref="BuildFlow"/> on
        ///// this module. Null if this module doesn't have its own flow.
        ///// </summary>
        //private Flow _flow = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called apply configuration values, signals and the <see cref="Flow"/> of this module has been assigned.
        /// </summary>
        /// <remarks>This is called before <see cref="DoReset"/> if this is not the first time this module is initialized.</remarks>
        protected virtual void DoPostSetup()
        {
            Enabled = true;
            UnityEngine.Debug.Log($"POST SETUP {GetType().Name}");
        }

        /// <summary>
        /// Called when this module is loaded, whatever it's enabled or not.
        /// </summary>
        /// <remarks>This is called after <see cref="DoPostSetup"/> is called for the first time.</remarks>
        protected virtual void DoAwake()
        {
            UnityEngine.Debug.Log($"AWAKE {GetType().Name}");
        }

        /// <summary>
        /// Called the first time this module is enabled.
        /// </summary>
        /// <remarks>This is called before <see cref="DoEnable"/> is called for the first time.</remarks>
        protected virtual void DoStart()
        {
            UnityEngine.Debug.Log($"START {GetType().Name}");
        }

        /// <summary>
        /// Called when this module is enabled.
        /// </summary>
        /// <remarks>This is called right after <see cref="DoStart"/> when this module is enabled for the first time.</remarks>
        protected virtual void DoEnable()
        {
            UnityEngine.Debug.Log($"ENABLE {GetType().Name}");
        }

        /// <summary>
        /// Called when this module is updated, if it's enabled.
        /// </summary>
        /// <param name="delta">The time elapsed since the previous update call.</param>
        protected virtual void DoUpdate(float delta)
        {
            UnityEngine.Debug.Log($"UPDATE {GetType().Name}");
        }

        /// <summary>
        /// Called when this module is reset to its initial state, whatever it's enabled or not.
        /// </summary>
        /// <remarks>This is called after <see cref="DoPostSetup"/> if this is not the first time this module is initialized.</remarks>
        protected virtual void DoReset()
        {
            UnityEngine.Debug.Log($"RESET {GetType().Name}");
        }

        /// <summary>
        /// Called after the whole hierarchy of this module has been reset (basically after <see cref="DoReset"/> has been called on all
        /// child modules), whatever it's enabled or not.
        /// </summary>
        protected virtual void DoPostReset()
        {
            UnityEngine.Debug.Log($"POST RESET {GetType().Name}");
        }

        /// <summary>
        /// Called when this module is disabled.
        /// </summary>
        protected virtual void DoDisable()
        {
            UnityEngine.Debug.Log($"DISABLE {GetType().Name}");
        }

        /// <summary>
        /// Called when this module is unloaded, whatever it's enabled or not.
        /// </summary>
        /// <remarks>If this module has a <see cref="Flow"/>, this function is called after that <see cref="Flow"/> has been removed from
        /// the game's <see cref="FlowGraph"/>.</remarks>
        protected virtual void DoDispose()
        {
            UnityEngine.Debug.Log($"DISPOSE {GetType().Name}");
        }

        #endregion


        #region Public API

        /// <summary>
        /// The instance of the game that uses this module.
        /// </summary>
        public Game Game => _moduleRef.Game;

        /// <inheritdoc cref="_moduleRef"/>
        public ModuleRef ModuleRef
        {
            get => _moduleRef;
            set
            {
                if (_moduleRef == null)
                    _moduleRef = value;
                else
                    UnityEngine.Debug.LogError($"Faield to assign {nameof(ModuleRef)}: A {nameof(ModuleRef)} has already been assigned to this module ({GetType().FullName}).");
            }
        }

        /// <inheritdoc cref="IModule.Enabled"/>
        public bool Enabled
        {
            get => _enabled && AreAllParentsEnabled;
            set
            {
                // Cancel if the enabled state of this module doesn't change
                if (_enabled == value)
                    return;

                _enabled = value;
                // Skip state change if this module is not initialized yet
                if (!_initialized)
                    return;

                // If the parent hierarchy of this module is enabled
                if (AreAllParentsEnabled)
                {
                    // Trigger state change
                    if (_enabled)
                        EnableModule();
                    else
                        DisableModule();
                }
            }
        }

        /// <inheritdoc cref="IModule.EnabledSelf"/>
        public bool EnabledSelf
        {
            get => _enabled;
            set => Enabled = value;
        }

        #endregion


        #region Public Framework Calls

        /// <inheritdoc cref="IModule.NotifySetup"/>
        public void NotifySetup()
        {
            DoPostSetup();

            if (!_initialized)
            {
                DoAwake();
                _initialized = true;
            }
        }

        /// <inheritdoc cref="IModule.ResetModule"/>
        public void ResetModule()
        {
            if (!Game.Reset(ModuleRef))
                return;

            DoReset();
            OnReset?.Invoke();

            // Reset all valid child modules
            foreach (ModuleRef child in ModuleRef.Children)
            {
                if (child.Instance != null)
                    child.Instance.ResetModule();
            }

            DoPostReset();
        }

        /// <inheritdoc cref="IModule.UpdateModule(float)"/>
        public void UpdateModule(float delta)
        {
            // Cancel if this module is not initialized
            if (!_initialized)
                return;

            if (Enabled)
            {
                DoUpdate(delta);
                foreach (ModuleRef child in ModuleRef.Children)
                {
                    if (child.Instance != null)
                        child.Instance.UpdateModule(delta);
                }
            }
        }

        /// <inheritdoc cref="IModule.EnableModule"/>
        public void EnableModule()
        {
            if (!_started)
            {
                DoStart();
                _started = true;
            }

            DoEnable();
            OnEnabledStateChange?.Invoke(true);

            // Notify state change to all child modules
            foreach (ModuleRef child in ModuleRef.Children)
            {
                // Enable child module if it's valid and enabled
                if (child.Instance != null && child.Instance.EnabledSelf)
                    child.Instance.EnableModule();
            }
        }

        /// <inheritdoc cref="IModule.DisableModule"/>
        public void DisableModule()
        {
            DoDisable();
            OnEnabledStateChange?.Invoke(false);

            // Notify state change to all child modules
            foreach (ModuleRef child in ModuleRef.Children)
            {
                // Enable child module if it's valid and enabled
                if (child.Instance != null && child.Instance.EnabledSelf)
                    child.Instance.DisableModule();
            }
        }

        #endregion


        #region Private API

        /// <summary>
        /// Checks if the parents of this module are all enabled (it doesn't check the state of this one).
        /// </summary>
        private bool AreAllParentsEnabled
        {
            get
            {
                ModuleRef moduleRef = ModuleRef.Parent;

                while (moduleRef != null)
                {
                    // Stop if a parent module is not loaded or is disabled
                    if (moduleRef.Instance == null || !moduleRef.Instance.EnabledSelf)
                        return false;

                    moduleRef = moduleRef.Parent;
                }

                return true;
            }
        }

        #endregion

    }

}