/**
* Sideways Experiments (c) 2024
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System;
using System.Collections.Generic;

using SideXP.Core.Reflection;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Contains the reference to a module.<br/>
    /// This is used to declare the modules hierarchy, and load/unload modules at will without breaking the references. This also provides a
    /// unique static ID to the module, so the related assets and features can identify it easily.
    /// </summary>
    public class ModuleRef
    {

        #region Fields

        /// <summary>
        /// The game instance that contains this module reference.
        /// </summary>
        private Game _game = null;

        /// <summary>
        /// The type of the module that can be loaded from this reference.
        /// </summary>
        private Type _moduleType = null;

        /// <summary>
        /// The loaded module instance. Can be null if the module has never been loaded from this reference.
        /// </summary>
        private IModule _moduleInstance = null;

        /// <summary>
        /// The parent module of the one in reference. Null if this module is the main one of the game instance.
        /// </summary>
        private ModuleRef _parent = null;

        /// <summary>
        /// The child modules from the one in reference.
        /// </summary>
        private List<ModuleRef> _children = new List<ModuleRef>();

        /// <summary>
        /// The index of this module ref in its hierarchy's layer.
        /// </summary>
        private int _index = -1;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="ModuleRef(ModuleRef, Type, Game)"/>
        /// <remarks>This constructor is meant to be called from a <see cref="GameSystem.Game"/> instance on initialization.</remarks>
        internal ModuleRef(Type moduleType, Game game)
            : this(null, moduleType, game) { }

        /// <remarks>This constructor is meant to be called internally when creating the modules hierarchy.</remarks>
        /// <param name="parent">The parent module reference.</param>
        /// <param name="moduleType">The type of the module that can be loaded from this reference.</param>
        /// <param name="game">The game instance that contains this module reference.</param>
        /// <inheritdoc cref="ModuleRef"/>
        private ModuleRef(ModuleRef parent, Type moduleType, Game game)
        {
            _parent = parent;
            _moduleType = moduleType;
            _game = game;

            // Build children module ref, based on fields and properties marked with [ChildModule]
            BuildChildRefs();
            // Assign indexes by depth if this module ref is the top-level one
            AssignIndexesByDepth();
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_game"/>
        public Game Game => _game;

        /// <inheritdoc cref="_parent"/>
        public ModuleRef Parent => _parent;

        /// <inheritdoc cref="_children"/>
        public ModuleRef[] Children => _children.ToArray();

        /// <summary>
        /// The type of the module set as reference.
        /// </summary>
        public Type ModuleType => _moduleType;

        /// <inheritdoc cref="_moduleInstance"/>
        public IModule Instance => _moduleInstance;

        /// <inheritdoc cref="_moduleInstance"/>
        /// <remarks>Alias of <see cref="Instance"/>.</remarks>
        public IModule I => _moduleInstance;

        /// <summary>
        /// Gets the depth of this module in the hierarchy.
        /// </summary>
        public int Depth
        {
            get
            {
                int depth = 0;
                ModuleRef parent = _parent;
                while (parent != null)
                {
                    parent = parent._parent;
                    depth++;
                }
                return depth;
            }
        }

        /// <inheritdoc cref="_index"/>
        public int Index => _index;

        /// <summary>
        /// Gets the whole hierarchy from this module as a 1D array.
        /// </summary>
        /// <returns>Returns the whole hierarchy from this module as a 1D array.</returns>
        public ModuleRef[] GetFlattenedHierarchy()
        {
            List<ModuleRef> moduleRefsList = new List<ModuleRef>();
            Stack<ModuleRef> remainingRefs = new Stack<ModuleRef>();
            remainingRefs.Push(this);

            while (remainingRefs.Count > 0)
            {
                ModuleRef current = remainingRefs.Pop();
                moduleRefsList.Add(current);

                for (int i = current._children.Count - 1; i >= 0; i--)
                    remainingRefs.Push(current._children[i]);
            }

            return moduleRefsList.ToArray();
        }

        /// <summary>
        /// Returns informations about the referenced module.
        /// </summary>
        public override string ToString()
        {
            return $"{nameof(ModuleRef)}(type = {ModuleType}, depth = {Depth}, index = {Index}, has valid instance = {_moduleInstance != null})";
        }

        #endregion


        #region Internal API

        /// <summary>
        /// Loads the module instance, or get the existing one.
        /// </summary>
        /// <returns>Returns the existing or loaded module instance.</returns>
        internal bool Load(out IModule module)
        {
            if (_moduleInstance == null)
            {
                if (typeof(Module).IsAssignableFrom(ModuleType))
                {
                    _moduleInstance = Activator.CreateInstance(ModuleType) as Module;
                    _moduleInstance.ModuleRef = this;
                }
                else
                {
                    UnityEngine.Debug.LogError($"Failed to load module instance: The module type {ModuleType} is not supported.");
                }
            }

            module = _moduleInstance;
            return module != null;
        }

        #endregion


        #region Private API

        /// <summary>
        /// Gets the expected child modules from the referenced one, and create the module refs as children accordingly.
        /// </summary>
        private void BuildChildRefs()
        {
            foreach (FieldOrPropertyInfo childModuleFieldOrProperty in ReflectionUtility.GetFieldsAndPropertiesWithAttribute<ChildModuleAttribute>(_moduleType))
            {
                if (typeof(IModule).IsAssignableFrom(childModuleFieldOrProperty.Type))
                {
                    ModuleRef childRef = new ModuleRef(this, childModuleFieldOrProperty.Type, Game);
                    _children.Add(childRef);
                }
            }
        }

        /// <summary>
        /// Assigns indexes of all module refs in the hierarchy by their depth.
        /// </summary>
        /// <remarks>Indexes are used to identify the assets of the projects related to the appropriate modules. This allows to deal with a
        /// same module type being instanced more than once at a specific depth layer, or even in the whole hierarchy.<br/>
        /// This is not a perfect solution though, as changing the hierarchy may cause indexes to also change. Knowing this, the system will
        /// try its best to rebind existing assets for the modules that have been moved in the hierarchy.</remarks>
        /// <returns>Returns true if the indexes have been assigned successfully, or false if this module ref is not the top-level
        /// one so it's not allowed to overwrite indexes.</returns>
        private bool AssignIndexesByDepth()
        {
            // Cancel if this module ref is not the top-level one
            if (_parent != null)
                return false;

            Dictionary<int, int> indexPerDepth = new Dictionary<int, int>();
            AssignIndexesByDepthRecursively(indexPerDepth, 0);
            return true;
        }

        /// <remarks>This function is used for recursion, and shouldnt't be used outside of <see cref="AssignIndexesByDepth"/>.</remarks>
        /// <param name="indexesPerDepth">A dictionary where keys represent a depth level, and values represent the index to assign to the
        /// next module found at that depth level.</param>
        /// <inheritdoc cref="AssignIndexesByDepth"/>
        private void AssignIndexesByDepthRecursively(Dictionary<int, int> indexesPerDepth, int currentDepth = 0)
        {
            if (!indexesPerDepth.ContainsKey(currentDepth))
                indexesPerDepth.Add(currentDepth, 0);

            _index = indexesPerDepth[currentDepth]++;

            foreach (ModuleRef child in _children)
                child.AssignIndexesByDepthRecursively(indexesPerDepth, currentDepth + 1);
        }

        #endregion

    }

    /// <typeparam name="T">The type of the module set as reference.</typeparam>
    /// <inheritdoc cref="ModuleRef"/>
    public class ModuleRef<T> : ModuleRef
        where T : IModule
    {

        #region Lifecycle

        /// <inheritdoc cref="ModuleRef(Type, Game)"/>
        internal ModuleRef(Game game)
            : base (typeof(T), game) { }

        #endregion

    }

}