/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.GameSystem
{

    /// <summary>
    /// Miscellaneous constant values used in this package.
    /// </summary>
    public static class Constants
    {
    
        // Package

        /// <inheritdoc cref="SideXP.Core.Constants.BaseHelpUrl"/>
        public const string BaseHelpUrl = SideXP.Core.Constants.BaseHelpUrl;

        /// <summary>
        /// The display name of this package.
        /// </summary>
        public const string PackageName = "Game System";

        /// <inheritdoc cref="SideXP.Core.Constants.AddComponentMenu"/>
        public const string AddComponentMenu = SideXP.Core.Constants.AddComponentMenu + "/" + PackageName;

        /// <inheritdoc cref="SideXP.Core.Constants.CreateAssetMenu"/>
        public const string CreateAssetMenu = SideXP.Core.Constants.CreateAssetMenu + "/" + PackageName;

        /// <inheritdoc cref="SideXP.Core.Constants.CreateAssetMenuDemos"/>
        public const string CreateAssetMenuDemos = CreateAssetMenu + "/Demos";

        // Common

        public const string AttributeSuffix = "Attribute";

    }

}