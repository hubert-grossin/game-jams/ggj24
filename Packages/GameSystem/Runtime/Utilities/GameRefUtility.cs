/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.GameSystem
{

    /// <summary>
    /// Miscellaneous utility functions for working with <see cref="GameRefAsset"/>.
    /// </summary>
    public static class GameRefUtility
    {

        /// <inheritdoc cref="GameRefAsset.SignalInfos"/>
        public static ModuleSignalInfo[] GetSignalInfos(GameRefAsset gameRef)
        {
            return gameRef.SignalInfos;
        }

        /// <inheritdoc cref="GameRefAsset.DefaultConfigInfos"/>
        public static ModuleConfigInfo[] GetDefaultConfigInfos(GameRefAsset gameRef)
        {
            return gameRef.DefaultConfigInfos;
        }

        /// <inheritdoc cref="GameRefAsset.DefaultConfigInfos"/>
        public static GameRefAsset.Environment[] GetEnvironments(GameRefAsset gameRef)
        {
            return gameRef.Environments;
        }

        /// <inheritdoc cref="GameRefAsset.SelectedEnvironment"/>
        public static GameRefAsset.Environment GetSelectedEnvironment(GameRefAsset gameRef)
        {
            return gameRef.SelectedEnvironment;
        }

        /// <inheritdoc cref="GameRefAsset.SelectEnvironment(string)"/>
        public static bool SelectEnvironment(GameRefAsset gameRef, string environmentName)
        {
            return gameRef.SelectEnvironment(environmentName);
        }

    }

}