/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.GameSystem
{

    /// <summary>
    /// State machine that handles the current state of the game.
    /// </summary>
    public class FlowGraph
    {

        #region Fields
        #endregion


        #region Lifecycle
        #endregion


        #region Public API
        #endregion

    }

}