/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.GameSystem
{

    /// <summary>
    /// Qualifies a class as an available target for a transition.
    /// </summary>
    public interface ITransitionTarget
    {

        IFlowGraphElement TransitionTarget { get; }

    }

}