/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.GameSystem
{

    /// <summary>
    /// Qualifies a class as being part of a <see cref="FlowGraph"/>.
    /// </summary>
    public interface IFlowGraphElement
    {

        

    }

}