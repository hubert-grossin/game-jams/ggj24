/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.GameSystem
{

    /// <summary>
    /// Basic state in a <see cref="Flow"/> sub-state machine.
    /// </summary>
    public class State : IFlowGraphElement
    {

        #region Fields
        #endregion


        #region Lifecycle
        #endregion


        #region Public API
        #endregion

    }

}