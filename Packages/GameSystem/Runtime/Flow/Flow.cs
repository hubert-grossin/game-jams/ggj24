/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.GameSystem
{

    /// <summary>
    /// Sub-state machine for <see cref="FlowGraph"/> that can contain its own states and transitions.
    /// </summary>
    public class Flow : IFlowGraphElement
    {

        #region Fields
        #endregion


        #region Lifecycle
        #endregion


        #region Public API
        #endregion

    }

}