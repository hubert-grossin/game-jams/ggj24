/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.GameSystem.Demos
{

    /// <summary>
    /// 
    /// </summary>
    public class ModuleB : Module
    {

        [ChildModule]
        private ModuleB1 _b1 = null;
        public ModuleB1 B1 => _b1;

        [ChildModule]
        private ModuleB2 _b2 = null;
        public ModuleB2 B2 => _b2;

        [ChildModule]
        private ModuleB3 _b3 = null;
        public ModuleB3 B3 => _b3;

        [Config]
        private int _valueB = 100;
        public int ValueB => _valueB;

    }

}