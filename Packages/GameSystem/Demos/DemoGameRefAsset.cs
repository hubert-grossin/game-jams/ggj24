/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.GameSystem.Demos
{

    /// <summary>
    /// 
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    [CreateAssetMenu(fileName = "New" + nameof(DemoGameRefAsset), menuName = Constants.CreateAssetMenuDemos + "/Demo Game Ref")]
    public class DemoGameRefAsset : GameRefAssetGeneric<MasterModule> { }

}