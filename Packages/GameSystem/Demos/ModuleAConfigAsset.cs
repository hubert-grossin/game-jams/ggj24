using SideXP.GameSystem;


namespace SideXP.GameSystem.Demos
{
    
    
    /// <summary>
    /// Auto-generated class that represents the <see cref="SideXP.GameSystem.Demos.ModuleA"/> module's configuration.
    /// </summary>
    public class ModuleAConfigAsset : ModuleConfigAssetGeneric<ModuleA>
    {
        
        [OriginalName("_valueA")]
        [Config]
        public int ValueA = 100;
    }
}
