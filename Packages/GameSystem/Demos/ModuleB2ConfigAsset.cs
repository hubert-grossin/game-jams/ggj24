using SideXP.GameSystem;
using System;


namespace SideXP.GameSystem.Demos
{
    
    
    /// <summary>
    /// Auto-generated class that represents the <see cref="SideXP.GameSystem.Demos.ModuleB2"/> module's configuration.
    /// </summary>
    public class ModuleB2ConfigAsset : ModuleConfigAssetGeneric<ModuleB2>
    {
        
        [OriginalName("Text")]
        [Config]
        public String Text;
    }
}
