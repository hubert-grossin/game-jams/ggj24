/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.GameSystem.Demos
{

    /// <summary>
    /// 
    /// </summary>
    public class ModuleB2 : Module
    {

        [ChildModule]
        private ModuleB2a _b2a = null;
        public ModuleB2a B2a => _b2a;

        [ChildModule]
        private ModuleB2b _b2b = null;
        public ModuleB2b B2b => _b2b;

        [Config]
        public string Text;

    }

}