using SideXP.GameSystem;


namespace SideXP.GameSystem.Demos
{
    
    
    /// <summary>
    /// Auto-generated class that represents the <see cref="SideXP.GameSystem.Demos.ModuleB"/> module's configuration.
    /// </summary>
    public class ModuleBConfigAsset : ModuleConfigAssetGeneric<ModuleB>
    {
        
        [OriginalName("_valueB")]
        [Config]
        public int ValueB = 100;
    }
}
