/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.GameSystem.Demos
{

    /// <summary>
    /// 
    /// </summary>
    public class ModuleA : Module
    {

        [ChildModule]
        private ModuleA1 _a1 = null;
        public ModuleA1 A1 => _a1;

        [ChildModule]
        private ModuleA2 _a2 = null;
        public ModuleA2 Aa2 => _a2;

        [Config]
        private int _valueA = 100;
        public int ValueA => _valueA;

    }

}