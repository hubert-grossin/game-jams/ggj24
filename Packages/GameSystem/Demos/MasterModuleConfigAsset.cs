using SideXP.GameSystem;


namespace SideXP.GameSystem.Demos
{
    
    
    /// <summary>
    /// Auto-generated class that represents the <see cref="SideXP.GameSystem.Demos.MasterModule"/> module's configuration.
    /// </summary>
    public class MasterModuleConfigAsset : ModuleConfigAssetGeneric<MasterModule>
    {
        
        [OriginalName("_testValue")]
        [Config]
        public int TestValue = 100;
    }
}
