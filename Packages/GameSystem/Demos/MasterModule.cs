/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using SideXP.Broadcaster;

using UnityEngine;

namespace SideXP.GameSystem.Demos
{

    /// <summary>
    /// 
    /// </summary>
    [Module(ConfigName = "Master")]
    public class MasterModule : Module
    {

        [ChildModule]
        private ModuleA _moduleA = null;
        public ModuleA A => _moduleA;

        [ChildModule]
        private ModuleB _moduleB = null;
        public ModuleB B => _moduleB;

        [Signal("Master Module/Test Signal", Description = "Test description")]
        public VoidSignalAsset TestSignal1 = null;
        [Signal("Master Module/PARENT/Test child 1")]
        public VoidSignalAsset TestSignal2 = null;
        [Signal("Master Module/PARENT/Test child 2")]
        public VoidSignalAsset TestSignal3 = null;
        [Signal("Master Module/AAA")]
        public VoidSignalAsset TestSignalA = null;
        [Signal("Master Module/ZZZZ")]
        public VoidSignalAsset TestSignalZ = null;

        [Config]
        private int _testValue = 100;
        public int TestValue => _testValue;

        //protected override Flow BuildFlow()
        //{
        //    Flow flow = new Flow();
        //    //flow.Transition(_tourModule);
        //    return flow;
        //}

    }

}