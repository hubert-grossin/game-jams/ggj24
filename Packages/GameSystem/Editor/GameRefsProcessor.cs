/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using UnityEditor;

using SideXP.Core.EditorOnly;

namespace SideXP.GameSystem.EditorOnly
{

    /// <summary>
    /// Process the imported <see cref="GameRefAsset"/> in the project.
    /// </summary>
    [InitializeOnLoad]
    public class GameRefsProcessor
    {

        /// <summary>
        /// Static constructor.
        /// </summary>
        static GameRefsProcessor()
        {
            // Handle import of Game Ref Asset or Game Ref Asset script
            AssetsProcessor.ListenImportAsset((path, isNew, didDomainReload, previousPath) =>
            {
                GameRefAsset gameRef = AssetDatabase.LoadAssetAtPath<GameRefAsset>(path);
                // If the imported asset is a Game Ref Asset, reload it
                if (gameRef != null)
                {
                    GameRefEditorUtility.Reload(gameRef);
                }
                // If the reimported asset is a Game Ref Asset script, reload all assets that use that script
                else if (ScriptUtility.GetScriptType(path, out Type gameRefType) && typeof(GameRefAsset).IsAssignableFrom(gameRefType))
                {
                    foreach (GameRefAsset gr in ObjectUtility.FindAllAssetsOfType(gameRefType))
                    {
                        if (gr.GetType() == gameRefType)
                            GameRefEditorUtility.Reload(gr);
                    }
                }
            });

            // Handle deletion of Game Ref Asset script
            AssetsProcessor.ListenBeforeDeleteAsset(path =>
            {
                // If the deleted asset is a Game Ref Asset script
                if (ScriptUtility.GetScriptType(path, out Type gameRefType) && typeof(GameRefAsset).IsAssignableFrom(gameRefType))
                {
                    if (ObjectUtility.FindAllAssetsOfType(gameRefType).Length > 0)
                    {
                        EditorUtility.DisplayDialog("Failed to delete Game Ref type", $"This script can't be deleted since some assets in this project use it.\nPlease remove those assets to allow that script deletion.", "Cancel");
                        return AssetDeleteResult.FailedDelete;
                    }
                }

                return AssetDeleteResult.DidNotDelete;
            });
        }

    }

}