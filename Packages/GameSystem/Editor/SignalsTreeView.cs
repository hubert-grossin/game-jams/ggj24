/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System.Collections.Generic;

using UnityEngine;
using UnityEditor.IMGUI.Controls;

using SideXP.Core;
using SideXP.Broadcaster;
using UnityEditor;

namespace SideXP.GameSystem.EditorOnly
{

    /// <summary>
    /// 
    /// </summary>
    public class SignalsTreeView : TreeView
    {

        #region Delegates

        /// <summary>
        /// Called when a signal item is selected in this tree view.
        /// </summary>
        /// <param name="signal">The selected signal asset.</param>
        public delegate void SelectSignalDelegate(SignalAsset signal);

        #endregion


        #region Subclasses

        /// <summary>
        /// Represents a signal asset as in the tree view.
        /// </summary>
        private class SignalTreeViewItem : TreeViewItem
        {

            /// <summary>
            /// The signal represented by this item.
            /// </summary>
            private SignalAsset _signalAsset = null;

            /// <inheritdoc cref="SignalTreeViewItem"/>
            public SignalTreeViewItem(SignalAsset signalAsset, TreeViewItem parentItem, string displayName)
            {
                _signalAsset = signalAsset;
                id = signalAsset.name.GetHashCode();
                parent = parentItem;
                children = new List<TreeViewItem>();
                this.displayName = displayName;
            }

            /// <inheritdoc cref="_signalAsset"/>
            public SignalAsset SignalAsset => _signalAsset;

        }

        #endregion


        #region Fields

        /// <inheritdoc cref="SelectSignalDelegate"/>
        public event SelectSignalDelegate OnSelectSignal;

        /// <summary>
        /// The game ref of which the signals are displayed.
        /// </summary>
        private GameRefAsset _gameRef = null;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="SignalsTreeView(GameRefAsset, TreeViewState)"/>
        public SignalsTreeView(GameRefAsset gameRef)
            : this(gameRef, new TreeViewState()) { }

        /// <param name="gameRef">The game ref of which the signals are displayed.</param>
        /// <param name="state">The initial state of this tree view.</param>
        /// <inheritdoc cref="SignalsTreeView"/>
        public SignalsTreeView(GameRefAsset gameRef, TreeViewState state)
            : base(state)
        {
            _gameRef = gameRef;
        }

        #endregion


        #region Build Tree View

        /// <summary>
        /// Creates the tree view hierarchy.
        /// </summary>
        /// <returns>Returns the root item of this tree view.</returns>
        protected override TreeViewItem BuildRoot()
        {
            TreeViewItem root = new TreeViewItem
            {
                id = 0,
                depth = -1,
                displayName = "ROOT",
                children = new List<TreeViewItem>()
            };

            List<SignalAsset> signalAssets = new List<SignalAsset>();
            foreach (ModuleSignalInfo signalInfo in GameRefUtility.GetSignalInfos(_gameRef))
            {
                if (signalInfo.Asset != null)
                    signalAssets.Add(signalInfo.Asset);
            }
            signalAssets.Sort((a, b) => a.name.CompareTo(b.name));

            foreach (SignalAsset signal in signalAssets)
            {
                string[] split = signal.name.Split('/');
                string path = string.Empty;
                TreeViewItem parentItem = root;
                for (int splitIndex = 0; splitIndex < split.Length - 1; splitIndex++)
                {
                    path += (!string.IsNullOrEmpty(path) ? "/" : "") + split[splitIndex];
                    bool foundParent = false;

                    // Find parent item
                    for (int i = 0; i < parentItem.children.Count; i++)
                    {
                        if (parentItem.children[i].displayName == split[splitIndex])
                        {
                            parentItem = parentItem.children[i];
                            foundParent = true;
                            break;
                        }
                    }

                    if (foundParent)
                        continue;

                    TreeViewItem item = new TreeViewItem
                    {
                        id = path.GetHashCode(),
                        displayName = split[splitIndex],
                        parent = parentItem,
                        children = new List<TreeViewItem>()
                    };

                    parentItem.children.Add(item);
                    parentItem = item;
                }

                SignalTreeViewItem signalItem = new SignalTreeViewItem(signal, parentItem, split[split.Length - 1]);
                parentItem.children.Add(signalItem);
            }

            SetupDepthsFromParentsAndChildren(root);
            return root;
        }

        /// <summary>
        /// Disable multi-selection.
        /// </summary>
        protected override bool CanMultiSelect(TreeViewItem item)
        {
            return false;
        }

        /// <summary>
        /// Handles selection change.
        /// </summary>
        /// <param name="selectedIds">The ids of the selected items.</param>
        protected override void SelectionChanged(IList<int> selectedIds)
        {
            if (selectedIds.Count == 0)
                return;

            TreeViewItem selectedItem = FindItem(selectedIds[0], rootItem);
            if (selectedItem is SignalTreeViewItem signalItem)
                OnSelectSignal?.Invoke(signalItem.SignalAsset);
        }

        /// <summary>
        /// Handle double-click on item.
        /// </summary>
        /// <param name="id">The id of the double-clicked item.</param>
        protected override void DoubleClickedItem(int id)
        {
            SetExpanded(id, !IsExpanded(id));
        }

        /// <summary>
        /// Custom row height.
        /// </summary>
        protected override float GetCustomRowHeight(int row, TreeViewItem item)
        {
            return MoreGUI.HeightS;
        }

        /// <summary>
        /// Custom row GUI.
        /// </summary>
        protected override void RowGUI(RowGUIArgs args)
        {
            Rect rect = args.rowRect;
            float indent = GetContentIndent(args.item);
            rect.x += indent;
            rect.width -= MoreGUI.WidthM + MoreGUI.WidthS + MoreGUI.HMargin * 2 + indent;
            bool foldout = EditorGUI.Foldout(rect, IsExpanded(args.item.id), args.item.displayName, (args.item.hasChildren ? EditorStyles.foldout : EditorStyles.label).TextAlignment(TextAnchor.MiddleLeft));
            SetExpanded(args.item.id, foldout);

            if (args.item is SignalTreeViewItem signalItem)
            {
                rect.x += rect.width + MoreGUI.HMargin;
                rect.width = MoreGUI.WidthM;
                EditorGUI.LabelField(rect, signalItem.SignalAsset.DataType != null ? signalItem.SignalAsset.DataType.Name : "Void", EditorStyles.label.TextAlignment(TextAnchor.MiddleLeft).Italic().FontSizeDiff(-2));

                rect.x += rect.width + MoreGUI.HMargin;
                rect.width = MoreGUI.WidthM;
                using (new EnabledScope(false))
                    EditorGUI.ObjectField(rect, signalItem.SignalAsset, typeof(SignalAsset), false);
            }
        }

        #endregion

    }

}