/**
* Sideways Experiments (c) 2024
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using SideXP.Core;
using SideXP.Core.EditorOnly;
using SideXP.Core.Reflection;
using SideXP.Broadcaster;

using Object = UnityEngine.Object;

namespace SideXP.GameSystem.EditorOnly
{

    /// <summary>
    /// Miscellaneous utility functions for working with <see cref="GameRefAsset"/> in the editor.
    /// </summary>
    public static class GameRefEditorUtility
    {

        private const string SignalsProp = "_signals";
        private const string DefaultConfigsProp = "_defaultConfigs";
        private const string EnvironmentsProp = "_environments";
        private const string SignalAssetDescriptionProp = "_description";

        /// <summary>
        /// Reloads all the <see cref="GameRefAsset"/> in the project.
        /// </summary>
        /// <returns>Returns true if at least one <see cref="GameRefAsset"/> has been reloaded successfully.</returns>
        public static bool ReloadAll()
        {
            bool success = false;
            foreach (GameRefAsset gameRef in ObjectUtility.FindAllAssetsOfType<GameRefAsset>(true))
            {
                if(Reload(gameRef))
                    success = true;
            }

            return success;
        }

        /// <summary>
        /// Reloads the given <see cref="GameRefAsset"/>, so it contains the config assets, signals and other assets that are related to its
        /// main module's hierarchy.
        /// </summary>
        /// <param name="gameRef">The asset to reload.</param>
        /// <returns>Returns true if the given asset has been reloaded successfully.</returns>
        public static bool Reload(GameRefAsset gameRef)
        {
            if (gameRef.MainModuleRef == null)
            {
                Debug.LogError($"The main module is not initialized on this {nameof(GameRefAsset)}.", gameRef);
                return false;
            }

            SerializedObject gameRefObj = new SerializedObject(gameRef);
            ModuleRef[] moduleRefs = gameRef.MainModuleRef.GetFlattenedHierarchy();

            bool didChange = false;

            if (GenerateSignalAssets(gameRefObj, moduleRefs))
                didChange = true;

            if (GenerateConfigAssets(gameRefObj, moduleRefs))
                didChange = true;

            if (didChange)
                gameRef.SaveAndReimport();

            AssetDatabase.Refresh();
            return true;
        }

        /// <summary>
        /// Create a new environment on a given Game Ref.
        /// </summary>
        /// <param name="gameRef">The Game Ref on which you want to create a new environment.</param>
        /// <param name="environmentName">The name of the environment to create.</param>
        public static void CreateEnvironment(GameRefAsset gameRef, string environmentName)
        {
            SerializedObject gameRefObj = new SerializedObject(gameRef);

            SerializedProperty enviromentsArrayProp = gameRefObj.FindProperty(EnvironmentsProp);
            int index = enviromentsArrayProp.arraySize;
            enviromentsArrayProp.InsertArrayElementAtIndex(index);

            SerializedProperty environmentProp = enviromentsArrayProp.GetArrayElementAtIndex(index);
            environmentProp.FindPropertyRelative(nameof(GameRefAsset.Environment.Name)).stringValue = environmentName;
            environmentProp.FindPropertyRelative(nameof(GameRefAsset.Environment.Description)).stringValue = string.Empty;
            environmentProp.FindPropertyRelative(nameof(GameRefAsset.Environment.ConfigOverrides)).ClearArray();

            gameRefObj.ApplyModifiedProperties();
        }

        /// <summary>
        /// Deletes a named environment from a given Gamme Ref.
        /// </summary>
        /// <param name="gameRef">The Game Ref from which you want to to delete the environment.</param>
        /// <param name="environmentName">The name of the environment to remove.</param>
        /// <returns>Returns true if an environment has been deleted.</returns>
        public static bool DeleteEnvironment(GameRefAsset gameRef, string environmentName)
        {
            SerializedObject gameRefObj = new SerializedObject(gameRef);

            SerializedProperty enviromentsArrayProp = gameRefObj.FindProperty(EnvironmentsProp);
            for (int i = 0; i < enviromentsArrayProp.arraySize; i++)
            {
                if (enviromentsArrayProp.GetArrayElementAtIndex(i).FindPropertyRelative(nameof(GameRefAsset.Environment.Name)).stringValue == environmentName)
                {
                    SerializedProperty configOverridesArrayProp = enviromentsArrayProp.GetArrayElementAtIndex(i).FindPropertyRelative(nameof(GameRefAsset.Environment.ConfigOverrides));
                    for (int j = 0; j < configOverridesArrayProp.arraySize; j++)
                    {
                        Object configAsset = configOverridesArrayProp.GetArrayElementAtIndex(j).FindPropertyRelative(nameof(ModuleConfigInfo.Asset)).objectReferenceValue;
                        if (configAsset != null)
                            configAsset.Destroy(true);
                    }

                    enviromentsArrayProp.DeleteArrayElementAtIndex(i);
                    gameRefObj.ApplyModifiedProperties();
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Creates a config override for a given module in a named environment.
        /// </summary>
        /// <param name="gameRef">The Game Ref that contains the named environment and the given module.</param>
        /// <param name="environmentName">The name of the environment on which you want to create a config override.</param>
        /// <param name="moduleRef">The module for which you want to create an override.</param>
        /// <returns>Returns true if a override has been created succeessfully.</returns>
        public static bool CreateOverride(GameRefAsset gameRef, string environmentName, ModuleRef moduleRef)
        {
            ModuleConfigAsset defaultConfig = gameRef.GetConfig(moduleRef, true);

            // Cancel if there's no default config for the given module
            if (defaultConfig == null)
                return false;

            foreach (GameRefAsset.Environment env in GameRefUtility.GetEnvironments(gameRef))
            {
                if (env.Name == environmentName)
                {
                    // Cancel if the named environment already defines an override for the given module
                    foreach (ModuleConfigInfo info in env.ConfigOverrides)
                    {
                        if (info.Match(moduleRef))
                            return false;
                    }

                    List<ModuleConfigInfo> configs = new List<ModuleConfigInfo>(env.ConfigOverrides);
                    ModuleConfigAsset overrideConfig = Object.Instantiate(defaultConfig);
                    overrideConfig.name = ModuleEditorUtility.GetExpectedConfigAssetName(moduleRef.ModuleType) + " (Override)";
                    gameRef.AttachObject(overrideConfig, true);

                    configs.Add(new ModuleConfigInfo
                    {
                        Depth = moduleRef.Depth,
                        Index = moduleRef.Index,
                        Asset = overrideConfig
                    });
                    env.ConfigOverrides = configs.ToArray();
                    gameRef.SaveAndReimport();
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Deletes a config override for a given module from a named environment.
        /// </summary>
        /// <param name="environmentName">The name of the environment from which you want to delete the config override.</param>
        /// <param name="moduleRef">The module for which you want to delete the override.</param>
        /// <returns>Returns true if a override has been deleted succeessfully.</returns>
        /// <inheritdoc cref="CreateOverride(GameRefAsset, string, ModuleRef)"/>
        public static bool DeleteOverride(GameRefAsset gameRef, string environmentName, ModuleRef moduleRef)
        {
            foreach (GameRefAsset.Environment env in GameRefUtility.GetEnvironments(gameRef))
            {
                if (env.Name == environmentName)
                {
                    List<ModuleConfigInfo> configs = new List<ModuleConfigInfo>(env.ConfigOverrides);
                    for (int i = configs.Count - 1; i >= 0; i--)
                    {
                        if (configs[i].Match(moduleRef))
                        {
                            if (configs[i].Asset != null)
                            {
                                Object.DestroyImmediate(configs[i].Asset, true);
                            }

                            configs.RemoveAt(i);
                            env.ConfigOverrides = configs.ToArray();
                            gameRef.SaveAndReimport();
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Generates and manages the signal assets related to a <see cref="GameRefAsset"/> and its modules hierarchy.
        /// </summary>
        /// <param name="gameRefObj">The serialized representation of the <see cref="GameRefAsset"/>.</param>
        /// <param name="moduleRefs">The modules hierarchy of the given <see cref="GameRefAsset"/> as a 1D array.</param>
        /// <returns>Returns true if the given game ref <see cref="GameRefAsset"/> did change.</returns>
        private static bool GenerateSignalAssets(SerializedObject gameRefObj, ModuleRef[] moduleRefs)
        {
            // Flag enabled if something did change on the Game Ref
            bool didChange = false;
            // List of all the existing entries from the Game Ref, before any operation is performed.
            List<ModuleSignalInfo> existingEntries = new List<ModuleSignalInfo>(GameRefUtility.GetSignalInfos(gameRefObj.targetObject as GameRefAsset));
            // List of all the existing entries that have been reused. This list is used for comparing it with the assigned entries.
            List<ModuleSignalInfo> reusedExistingEntries = new List<ModuleSignalInfo>();
            // List of all the entries to serialize at the end of the process.
            List<ModuleSignalInfo> assignedEntries = new List<ModuleSignalInfo>();

            // For each module in the hierarchy
            foreach (ModuleRef moduleRef in moduleRefs)
            {
                Type moduleType = moduleRef.ModuleType;

                // For each field or property maarked with [Signal]
                foreach (FieldOrPropertyInfo signalProp in ReflectionUtility.GetFieldsAndPropertiesWithAttribute<SignalAttribute>(moduleType, true))
                {
                    SignalAttribute signalAttr = signalProp.GetCustomAttribute<SignalAttribute>();
                    string signalAssetName = !string.IsNullOrEmpty(signalAttr.Name)
                        ? signalAttr.Name
                        : ObjectNames.NicifyVariableName(signalProp.Name);

                    int existingEntryIndex = existingEntries.FindIndex(i => i.Asset != null && i.PropName == signalProp.Name && i.ModuleType == moduleType.AssemblyQualifiedName);
                    // If an entry exist with a valid asset for the current configurable element
                    if (existingEntryIndex >= 0)
                    {
                        // Rename asset if needed
                        if (existingEntries[existingEntryIndex].Asset.name != signalAssetName)
                        {
                            existingEntries[existingEntryIndex].Asset.name = signalAssetName;
                            didChange = true;
                        }

                        // Register entry as assigned
                        assignedEntries.Add(existingEntries[existingEntryIndex]);
                        reusedExistingEntries.Add(existingEntries[existingEntryIndex]);
                        existingEntries.RemoveAt(existingEntryIndex);
                        continue;
                    }

                    // If no signal asset has been assigned yet, create the signal asset and assign it
                    SignalAsset signalAsset = ScriptableObject.CreateInstance(signalProp.Type) as SignalAsset;
                    signalAsset.name = signalAssetName;

                    // Attach the new asset to the game ref
                    gameRefObj.targetObject.AttachObject(signalAsset, true);
                    if (!string.IsNullOrEmpty(signalAttr.Description))
                    {
                        SerializedObject signalAssetObj = new SerializedObject(signalAsset);
                        signalAssetObj.FindProperty(SignalAssetDescriptionProp).stringValue = signalAsset.Description;
                        signalAssetObj.ApplyModifiedPropertiesWithoutUndo();
                    }

                    assignedEntries.Add(new ModuleSignalInfo
                    {
                        ModuleType = moduleType.AssemblyQualifiedName,
                        PropName = signalProp.Name,
                        Asset = signalAsset
                    });

                    didChange = true;
                }
            }

            // List of all the SignalAsset used in assigned entries
            List<SignalAsset> assignedSignalAssets = new List<SignalAsset>(assignedEntries.Map(i => i.Asset));
            // For each signal subasset
            foreach (SignalAsset signalSubasset in gameRefObj.targetObject.FindAllSubassetsOfType<SignalAsset>())
            {
                if (!assignedSignalAssets.Contains(signalSubasset))
                {
                    signalSubasset.Destroy(true);
                    didChange = true;
                }
            }

            // If no change has been applied yet
            if (!didChange && assignedEntries.Count == reusedExistingEntries.Count)
            {
                // For each assigned entry
                foreach (ModuleSignalInfo entry in assignedEntries)
                {
                    // If the assigned entry is not a reused existing entry, consider that changes have been done
                    if (!reusedExistingEntries.Contains(entry))
                    {
                        didChange = true;
                        break;
                    }
                }
            }

            // Stop if no change has been done on the Game Ref asset
            if (!didChange)
                return false;

            // Update the signals array
            SerializedProperty signalInfoArrayProp = gameRefObj.FindProperty(SignalsProp);
            signalInfoArrayProp.ClearArray();

            // For each signal asset info to assign
            foreach (ModuleSignalInfo signalInfo in assignedEntries)
            {
                int index = signalInfoArrayProp.arraySize;
                signalInfoArrayProp.InsertArrayElementAtIndex(index);
                SerializedProperty signalInfoItemProp = signalInfoArrayProp.GetArrayElementAtIndex(index);
                signalInfoItemProp.FindPropertyRelative(nameof(ModuleSignalInfo.ModuleType)).stringValue = signalInfo.ModuleType;
                signalInfoItemProp.FindPropertyRelative(nameof(ModuleSignalInfo.PropName)).stringValue = signalInfo.PropName;
                signalInfoItemProp.FindPropertyRelative(nameof(ModuleSignalInfo.Asset)).objectReferenceValue = signalInfo.Asset;
            }

            gameRefObj.ApplyModifiedPropertiesWithoutUndo();
            return true;
        }

        /// <summary>
        /// Generates and manages the config assets related to a <see cref="GameRefAsset"/> and its modules hierarchy.
        /// </summary>
        /// <inheritdoc cref="GenerateSignalAssets(SerializedObject, ModuleRef[])"/>
        private static bool GenerateConfigAssets(SerializedObject gameRefObj, ModuleRef[] moduleRefs)
        {
            // Flag enabled if something did change on the Game Ref
            bool didChange = false;
            // List of all the existing entries from the Game Ref, before any operation is performed.
            List<ModuleConfigInfo> existingEntries = new List<ModuleConfigInfo>(GameRefUtility.GetDefaultConfigInfos(gameRefObj.targetObject as GameRefAsset));
            // Remove all invalid entries
            if (existingEntries.RemoveAll(i => i.Asset == null) > 0)
                didChange = true;
            // List of all the existing entries that have been reused. This list is used for comparing it with the assigned entries.
            List<ModuleConfigInfo> reusedExistingEntries = new List<ModuleConfigInfo>();
            // List of all the entries to serialize at the end of the process.
            List<ModuleConfigInfo> assignedEntries = new List<ModuleConfigInfo>();
            // List of all the modules of the hierarchy to process in the second pass.
            List<ModuleRef> remainingModuleRefs = new List<ModuleRef>(moduleRefs);
            // The list of all the default config subassets that have not been assigned yet
            List<ModuleConfigAsset> remaininigConfigAssets = new List<ModuleConfigAsset>(gameRefObj.targetObject.FindAllSubassetsOfType<ModuleConfigAsset>());
            // The list of depth and indexes before and after a migration, meaning an existing asset has been assigned to another module of
            // the same type to avoid data loss.
            List<(int fromDepth, int toDepth, int fromIndex, int toIndex)> migrations = new List<(int fromDepth, int toDepth, int fromIndex, int toIndex)>();

            // FIRST PASS: Try to reuse the existing entries
            // For each module in the hierarchy
            foreach (ModuleRef moduleRef in moduleRefs)
            {
                Type moduleType = moduleRef.ModuleType;
                FieldOrPropertyInfo[] configurableElements = ReflectionUtility.GetFieldsAndPropertiesWithAttribute<ConfigAttribute>(moduleType, true);
                // Try to find an existing entry for the current module
                int index = existingEntries.FindIndex(i => i.Match(moduleRef));

                // If an entry exists for the current module
                if (index >= 0)
                {
                    // If the config should be removed because the module doesn't declare configurable elements
                    if (configurableElements.Length <= 0)
                    {
                        didChange = true;
                    }
                    // Else, consider the entry as assigned
                    else
                    {
                        // Fix asset name if needed
                        string expectedAssetName = ModuleEditorUtility.GetExpectedConfigAssetName(moduleType);
                        if (existingEntries[index].Asset.name != expectedAssetName)
                        {
                            existingEntries[index].Asset.name = expectedAssetName;
                            didChange = true;
                        }

                        assignedEntries.Add(existingEntries[index]);
                        reusedExistingEntries.Add(existingEntries[index]);
                        remaininigConfigAssets.Remove(existingEntries[index].Asset);
                    }

                    existingEntries.RemoveAt(index);
                    remainingModuleRefs.Remove(moduleRef);
                }
                // Else, if no entry has been found for the module but it doesn't need to be evaluated in the second pass
                else if (configurableElements.Length <= 0)
                {
                    remainingModuleRefs.Remove(moduleRef);
                }
            }

            // SECOND PASS: Migrate existing entries for which the modules have been moved or removed, or create new ones
            // For each module that have not been processed in the first pass
            foreach (ModuleRef moduleRef in remainingModuleRefs)
            {
                Type moduleType = moduleRef.ModuleType;
                int index = existingEntries.FindIndex(i => i.ModuleType == moduleType);
                ModuleConfigAsset configAsset = null;

                // If an entry that target the same module type already exists and has not been assigned yet
                if (index >= 0)
                {
                    // Assign the existing entry to the current module
                    migrations.Add((existingEntries[index].Depth, moduleRef.Depth, existingEntries[index].Index, moduleRef.Index));
                    existingEntries[index].Depth = moduleRef.Depth;
                    existingEntries[index].Index = moduleRef.Index;
                    configAsset = existingEntries[index].Asset;

                    assignedEntries.Add(existingEntries[index]);
                    remaininigConfigAssets.Remove(existingEntries[index].Asset);
                    existingEntries.RemoveAt(index);
                    didChange = true;
                }
                // Else, if no existing entry match the current module
                else
                {
                    // Skip if the config asset type can't be found
                    if (!ModuleEditorUtility.GetConfigAssetType(moduleType, out Type configAssetType))
                    {
                        Debug.LogError($"No {nameof(ModuleConfigAsset)} type found for the module of type {moduleType}. Try to reimport that module's script to generate its configuration asset.");
                        continue;
                    }

                    // Create the config asset
                    configAsset = ScriptableObject.CreateInstance(configAssetType) as ModuleConfigAsset;

                    // Attach the config asset to the game ref
                    gameRefObj.targetObject.AttachObject(configAsset, true);
                    assignedEntries.Add(new ModuleConfigInfo
                    {
                        Depth = moduleRef.Depth,
                        Index = moduleRef.Index,
                        Asset = configAsset
                    });

                    didChange = true;
                }

                // Fix asset name if needed
                string expectedAssetName = ModuleEditorUtility.GetExpectedConfigAssetName(moduleType);
                if (configAsset.name != expectedAssetName)
                {
                    configAsset.name = expectedAssetName;
                    didChange = true;
                }
            }

            if (ProcessEnvironments(gameRefObj, migrations, assignedEntries, remaininigConfigAssets))
                didChange = true;

            // Destroy unused config assets
            foreach (ModuleConfigAsset unusedAsset in remaininigConfigAssets)
            {
                unusedAsset.Destroy(true);
                didChange = true;
            }

            // If no change has been applied, check if the assigned entries and the reused ones are equivalent
            if (!didChange && assignedEntries.Count == reusedExistingEntries.Count)
            {
                foreach (ModuleConfigInfo info in assignedEntries)
                {
                    if (!reusedExistingEntries.Contains(info))
                    {
                        didChange = true;
                        break;
                    }
                }
            }

            // Cancel if nothing changed
            if (!didChange)
                return false;

            // Update the configs array
            SerializedProperty configInfoArrayProp = gameRefObj.FindProperty(DefaultConfigsProp);
            configInfoArrayProp.ClearArray();

            // For each signal asset info to assign
            foreach (ModuleConfigInfo info in assignedEntries)
            {
                int index = configInfoArrayProp.arraySize;
                configInfoArrayProp.InsertArrayElementAtIndex(index);
                SerializedProperty signalInfoItemProp = configInfoArrayProp.GetArrayElementAtIndex(index);
                signalInfoItemProp.FindPropertyRelative(nameof(ModuleConfigInfo.Depth)).intValue = info.Depth;
                signalInfoItemProp.FindPropertyRelative(nameof(ModuleConfigInfo.Index)).intValue = info.Index;
                signalInfoItemProp.FindPropertyRelative(nameof(ModuleConfigInfo.Asset)).objectReferenceValue = info.Asset;
            }

            gameRefObj.ApplyModifiedPropertiesWithoutUndo();
            return true;
        }

        /// <summary>
        /// Updates the existing environments in a given <see cref="GameRefAsset"/>, based on the changed applied during config asset
        /// reload.
        /// </summary>
        /// <param name="migrations">The list of depth and indexes before and after a migration, meaning an existing asset has been assigned
        /// to another module of the same type to avoid data loss.</param>
        /// <param name="assignedEntries">The configuration info assigned to the <see cref="GameRefAsset"/>.</param>
        /// ^<param name="remainingConfigAssets">This list of all the config subassets that have not been assigned yet.</param>
        /// <returns>Returns true if something changed about environments.</returns>
        /// <inheritdoc cref="GenerateSignalAssets(SerializedObject, ModuleRef[])"/>
        private static bool ProcessEnvironments(SerializedObject gameRefObj, List<(int fromDepth, int toDepth, int fromIndex, int toIndex)> migrations, IEnumerable<ModuleConfigInfo> assignedEntries, List<ModuleConfigAsset> remainingConfigAssets)
        {
            bool didChange = false;

            // For each environment in the given Game Ref
            foreach (GameRefAsset.Environment environment in GameRefUtility.GetEnvironments(gameRefObj.targetObject as GameRefAsset))
            {
                // The list of all the overrides to assign to the current environment.
                List<ModuleConfigInfo> overrides = new List<ModuleConfigInfo>();
                // For each override in the current environment
                foreach (ModuleConfigInfo overrideInfo in environment.ConfigOverrides)
                {
                    // Skip if the current override targets an invalid asset
                    if (overrideInfo.Asset == null)
                        continue;

                    int migrationIndex = migrations.FindIndex(i => i.fromDepth == overrideInfo.Depth && i.fromIndex == overrideInfo.Index);
                    // If the config asset did migrate to another module, update the override info
                    if (migrationIndex >= 0)
                    {
                        overrideInfo.Depth = migrations[migrationIndex].toDepth;
                        overrideInfo.Index = migrations[migrationIndex].toIndex;
                        didChange = true;
                    }

                    bool deleteOverride = true;
                    // If an assigned entry matches the current environment override, keep it
                    foreach (ModuleConfigInfo assignedEntry in assignedEntries)
                    {
                        if (assignedEntry.Depth == overrideInfo.Depth && assignedEntry.Index == overrideInfo.Index && assignedEntry.ModuleType == overrideInfo.ModuleType)
                        {
                            overrides.Add(overrideInfo);
                            remainingConfigAssets.Remove(overrideInfo.Asset);
                            deleteOverride = false;
                            break;
                        }
                    }

                    // At this step, the override doesn't match any assigned entry, so it should be deleted
                    if (deleteOverride)
                    {
                        overrideInfo.Asset.Destroy(true);
                        didChange = true;
                    }
                }

                // Update overrides
                environment.ConfigOverrides = overrides.ToArray();
            }

            return didChange;
        }

    }

}