/**
* Sideways Experiments (c) 2024
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System.Collections.Generic;

using UnityEngine;

using SideXP.Core;

namespace SideXP.GameSystem.EditorOnly
{

    /// <summary>
    /// Stores informations related to module scripts and generated elements related to them.
    /// </summary>
    [Save(nameof(GameSystem) + "_" + nameof(ModulesCache) + ".json", EDataScope.Project)]
    internal class ModulesCache : SOSingleton<ModulesCache>
    {

        #region Subclasses

        /// <summary>
        /// The informations related to the module.
        /// </summary>
        [System.Serializable]
        public class ModuleInfo
        {

            [Tooltip("The path to the module's script, relative from the project directory.")]
            [SerializeField]
            private string _moduleScriptPath = null;

            [SerializeField]
            [Tooltip("The path to the auto-generated configuration asset script bound to the module, relative from the project directory.")]
            private string _moduleConfigAssetPath = null;

            /// <inheritdoc cref="ModuleInfo"/>
            /// <param name="scriptPath"></param>
            public ModuleInfo(string scriptPath)
            {
                _moduleScriptPath = scriptPath.ToRelativePath();
            }

            /// <inheritdoc cref="_moduleScriptPath"/>
            public string ModuleScriptPath
            {
                get => _moduleScriptPath;
                set => _moduleScriptPath = value != null ? value.ToRelativePath() : string.Empty;
            }

            /// <inheritdoc cref="_moduleConfigAssetPath"/>
            public string ModuleConfigAssetPath
            {
                get => _moduleConfigAssetPath;
                set => _moduleConfigAssetPath = value != null ? value.ToRelativePath() : string.Empty;
            }

        }

        #endregion


        #region Fields

        /// <summary>
        /// The list of cached data related to modules.
        /// </summary>
        [SerializeField]
        private List<ModuleInfo> _moduleInfos = new List<ModuleInfo>();

        #endregion


        #region Public API

        /// <summary>
        /// Gets a cache entry for a given module.
        /// </summary>
        /// <param name="moduleScriptPath">The path to the module script.</param>
        /// <param name="createIfNotExist">If enabled, an entry is created for the module if no one exist yet.</param>
        /// <returns>Returns the found or created cache entry.</returns>
        public static ModuleInfo Get(string moduleScriptPath, bool createIfNotExist = false)
        {
            moduleScriptPath = moduleScriptPath.ToRelativePath(true);
            foreach (ModuleInfo moduleInfo in I._moduleInfos)
            {
                if (moduleInfo.ModuleScriptPath == moduleScriptPath)
                    return moduleInfo;
            }

            if (createIfNotExist)
            {
                ModuleInfo info = new ModuleInfo(moduleScriptPath);
                I._moduleInfos.Add(info);
                return info;
            }
            
            return null;
        }

        /// <param name="info">Outputs the found or created cache entry.<param>
        /// <returns>Returns true if a cache entry exists for the given module.</returns>
        /// <inheritdoc cref="Get(string, out ModuleInfo, bool)"/>
        public static bool Get(string moduleScriptPath, out ModuleInfo info, bool createIfNotExist = false)
        {
            info = Get(moduleScriptPath, createIfNotExist);
            return info != null;
        }

        /// <summary>
        /// Gets a cache entry for a given module, by its related config asset's script path.
        /// </summary>
        /// <param name="moduleConfigAssetScriptPath">The path to the module config asset script.</param>
        /// <returns>Returns the found cache entry.</returns>
        public static ModuleInfo GetByModuleConfigAssetScriptPath(string moduleConfigAssetScriptPath)
        {
            moduleConfigAssetScriptPath = moduleConfigAssetScriptPath.ToRelativePath(true);
            foreach (ModuleInfo info in I._moduleInfos)
            {
                if (info.ModuleConfigAssetPath == moduleConfigAssetScriptPath)
                    return info;
            }

            return null;
        }

        /// <param name="info">Outputs the found cache entry.</param>
        /// <returns>Returns true if a cache entry exists for the related module.</returns>
        /// <inheritdoc cref="GetByModuleConfigAssetScriptPath(string)"/>
        public static bool GetByModuleConfigAssetScriptPath(string moduleConfigAssetScriptPath, out ModuleInfo info)
        {
            info = GetByModuleConfigAssetScriptPath(moduleConfigAssetScriptPath);
            return info != null;
        }

        /// <summary>
        /// Deletes a cache entry for a given module.
        /// </summary>
        /// <param name="moduleScriptPath">The path to the deleted module script.</param>
        /// <returns>Returns true if a cache entry has been removed.</returns>
        public static bool Delete(string moduleScriptPath)
        {
            moduleScriptPath = moduleScriptPath.ToRelativePath();
            return I._moduleInfos.RemoveAll(i => i.ModuleScriptPath == moduleScriptPath) > 0;
        }

        #endregion

    }

}