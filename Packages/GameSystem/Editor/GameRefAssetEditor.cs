/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using SideXP.Core;
using SideXP.Core.EditorOnly;
using SideXP.UI.EditorOnly;
using UnityEditor.IMGUI.Controls;
using SideXP.Broadcaster;

namespace SideXP.GameSystem.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="GameRefAsset"/>.
    /// </summary>
    [CustomEditor(typeof(GameRefAsset), true)]
    public class GameRefAssetEditor : Editor
    {

        #region Enums & Subclasses

        /// <summary>
        /// Defines the tab open in this editor.
        /// </summary>
        private enum ETab
        {

            /// <summary>
            /// Displays both confiiguration and environment settings.
            /// </summary>
            Configuration,

            /// <summary>
            /// List all the available signals from the selected <see cref="GameRefAsset"/>. 
            /// </summary>
            Signals

        }

        #endregion


        #region Fields

        private const float DepthIndent = 16f;
        private const float FoldoutIndent = 10f;
        private const float ItemHorizontalPadding = 8f;
        private const float ItemVerticalPadding = 2f;
        private const string DefaultEnvironmentName = "Default";

        private static GUIContent[] s_tabsToolbarContents = null;

#if SIDEXP_LIB_PROJECT
        [SerializeField]
        private bool _displayBaseInspector = false;
#endif

        [SerializeField]
        private ETab _activeTab = ETab.Configuration;

        /// <summary>
        /// The ids of the module config items in the tree view of which the configuration settings are displayed.
        /// </summary>
        [SerializeField]
        private List<int> _moduleConfigsFoldedOut = new List<int>();

        [SerializeField]
        private bool _environmentFoldout = false;

        [SerializeField]
        private TreeViewState _signalsTreeViewState = null;

        [SerializeField]
        private SignalAsset _selectedSignal = null;

        /// <inheritdoc cref="SignalsTreeView"/>
        private SignalsTreeView _signalsTreeView = null;

        /// <summary>
        /// The created editor instance to display the module config asset settings.
        /// </summary>
        private Dictionary<int, Editor> _moduleConfigsEditors = new Dictionary<int, Editor>();

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this editor is closed.
        /// </summary>
        private void OnDisable()
        {
            foreach (KeyValuePair<int, Editor> moduleConfigEditor in _moduleConfigsEditors)
                DestroyImmediate(moduleConfigEditor.Value);

            _moduleConfigsEditors.Clear();
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws the header GUI of the selected object in the inspector.
        /// </summary>
        protected override void OnHeaderGUI()
        {
            EditorGUILayout.Space();
            using (new EditorGUILayout.HorizontalScope())
            {
                EditorGUILayout.Space(MoreGUI.HMargin * 2, false);

                // Draw asset icon
                Texture previewTexture = AssetDatabase.GetCachedIcon(AssetDatabase.GetAssetPath(target));
                Rect rect = EditorGUILayout.GetControlRect(false, MoreGUI.HeightL, GUILayout.Width(MoreGUI.HeightL));
                if (previewTexture != null)
                {
                    GUI.DrawTexture(rect, previewTexture, ScaleMode.ScaleToFit);
                }

                EditorGUILayout.Space(MoreGUI.HMargin * 2, false);
                // Draw asset name and type
                using (new EditorGUILayout.VerticalScope())
                {
                    EditorGUILayout.LabelField($"<b>{target.name}</b> <size=12>({target.GetType().Name})</size>", MoreEditorGUI.TitleStyle.FontStyle(FontStyle.Normal), MoreGUI.HeightOptS);
                    EditorGUILayout.LabelField($"Main module: <b>{(target as GameRefAsset).MainModuleType.Name}</b>", EditorStyles.label.FontSizeDiff(-2).Italic().RichText(true), MoreGUI.HeightOptXS);
                }

                serializedObject.ApplyModifiedProperties();
            }
            EditorGUILayout.Space();
            MoreEditorGUI.HorizontalSeparator(1f, true, true);
        }

        /// <summary>
        /// Draws this custom editor GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
#if SIDEXP_LIB_PROJECT
            _displayBaseInspector = EditorGUILayout.Foldout(_displayBaseInspector, "Base Inspector (debug only)");
            if (_displayBaseInspector)
            {
                using (new EditorGUILayout.VerticalScope(GUI.skin.box))
                {
                    base.OnInspectorGUI();
                    EditorGUILayout.Space();
                    MoreEditorGUI.HorizontalSeparator(true);
                    EditorGUILayout.Space();
                }
            }
#endif

            _activeTab = (ETab)GUILayout.Toolbar((int)_activeTab, TabsToolbarContents, MoreGUI.HeightOptM);
            EditorGUILayout.Space();
            MoreEditorGUI.HorizontalSeparator();
            EditorGUILayout.Space();

            switch (_activeTab)
            {
                case ETab.Signals:
                    DrawSignalsView();
                    break;

                default:
                    DrawConfigView();
                    break;
            }
        }

        /// <summary>
        /// Displays the configurations of this Game Ref based on the modules hierarchy, and allows to create and select environments to
        /// override its default configuration.
        /// </summary>
        private void DrawConfigView()
        {
            GameRefAsset gameRef = target as GameRefAsset;
            GameRefAsset.Environment selectedEnvironment = GameRefUtility.GetSelectedEnvironment(gameRef);

            // Draw environment selection
            Rect rect = EditorGUILayout.GetControlRect(true);
            Rect tmpRect = rect;
            tmpRect.width = EditorGUIUtility.labelWidth;
            _environmentFoldout = EditorGUI.Foldout(tmpRect, _environmentFoldout, "Environment", true, EditorStyles.foldout.Bold());

            // Draw environment selection popup
            tmpRect.x += tmpRect.width;
            tmpRect.width = rect.width - tmpRect.width - (tmpRect.height + MoreGUI.HMargin) * 2;
            string[] options = new string[GameRefUtility.GetEnvironments(gameRef).Length + 1];
            int selectedEnvironmentIndex = 0;
            options[0] = DefaultEnvironmentName;
            {
                int i = 1;
                foreach (GameRefAsset.Environment env in GameRefUtility.GetEnvironments(gameRef))
                {
                    if (env == selectedEnvironment)
                        selectedEnvironmentIndex = i;
                    options[i++] = env.Name;
                }
            }

            EditorGUI.BeginChangeCheck();
            selectedEnvironmentIndex = EditorGUI.Popup(tmpRect, selectedEnvironmentIndex, options);
            if (EditorGUI.EndChangeCheck())
            {
                GameRefUtility.SelectEnvironment(gameRef, selectedEnvironmentIndex <= 0
                    ? string.Empty
                    : GameRefUtility.GetEnvironments(gameRef)[selectedEnvironmentIndex - 1].Name);
            }

            // Draw "create environment" button
            tmpRect.x += tmpRect.width + MoreGUI.HMargin;
            tmpRect.width = tmpRect.height;
            if (GUI.Button(tmpRect, EditorIcons.IconContent("add", "Create a new environment, which is basically a set of configuration overrides.", true), MoreEditorGUI.IconButtonStyle))
            {
                GameRefEditorUtility.CreateEnvironment(gameRef, $"New{nameof(GameRefAsset.Environment)}");
                selectedEnvironmentIndex = GameRefUtility.GetEnvironments(gameRef).Length;
                GameRefUtility.SelectEnvironment(gameRef, selectedEnvironmentIndex <= 0
                    ? string.Empty
                    : GameRefUtility.GetEnvironments(gameRef)[selectedEnvironmentIndex - 1].Name);
                _environmentFoldout = true;
            }

            // Draw "delete environment" button
            tmpRect.x += tmpRect.width + MoreGUI.HMargin;
            tmpRect.width = tmpRect.height;
            using (new EnabledScope(selectedEnvironmentIndex > 0))
            {
                if (GUI.Button(tmpRect, EditorIcons.IconContent("delete", "Delete the selected environment.", true), MoreEditorGUI.IconButtonStyle))
                {
                    if (GameRefEditorUtility.DeleteEnvironment(gameRef, GameRefUtility.GetEnvironments(gameRef)[selectedEnvironmentIndex - 1].Name))
                        GameRefUtility.SelectEnvironment(gameRef, null);
                }
            }

            // Draw environment settings if applicable
            if (_environmentFoldout)
            {
                selectedEnvironment = GameRefUtility.GetSelectedEnvironment(gameRef);
                using (new EnabledScope(selectedEnvironment != null))
                {
                    using (new IndentedScope())
                    {
                        // Draw name field
                        if (selectedEnvironment != null)
                        {
                            EditorGUI.BeginChangeCheck();
                            selectedEnvironment.Name = EditorGUILayout.DelayedTextField("Name", selectedEnvironment.Name);
                            if (EditorGUI.EndChangeCheck())
                            {
                                GameRefUtility.SelectEnvironment(gameRef, selectedEnvironment.Name);
                                serializedObject.Update();
                            }
                        }
                        else
                        {
                            EditorGUILayout.TextField("Name", DefaultEnvironmentName);
                        }

                        // Draw description field
                        if (selectedEnvironment != null)
                        {
                            EditorGUI.BeginChangeCheck();
                            selectedEnvironment.Description = EditorGUILayout.TextField("Description", selectedEnvironment.Description, EditorStyles.textField.WordWrap(true), GUILayout.MinHeight(EditorGUIUtility.singleLineHeight * 4));
                            if (EditorGUI.EndChangeCheck())
                                serializedObject.Update();
                        }
                        else
                        {
                            EditorGUILayout.TextField("Description", "The default configuration of this game instance.", EditorStyles.textField.WordWrap(true), GUILayout.MinHeight(EditorGUIUtility.singleLineHeight * 4));
                        }
                    }
                }
            }

            EditorGUILayout.Space();
            MoreEditorGUI.HorizontalSeparator();
            EditorGUILayout.Space();

            // Draw modules hierarchy and configuration settings
            foreach (ModuleRef moduleRef in (target as GameRefAsset).MainModuleRef.GetFlattenedHierarchy())
            {
                int id = GetModuleId(moduleRef);

                rect = EditorGUILayout.GetControlRect(false, MoreGUI.HeightS + 4);
                rect.x = 0;
                rect.width = EditorGUIUtility.currentViewWidth;

                // Draw background
                EditorGUI.DrawRect(rect, MoreEditorGUI.DarkSeparatorColor);
                EditorGUI.DrawRect(new Rect(rect.x, rect.y, rect.width, 1f), MoreEditorGUI.DarkSeparatorColor);

                // Get the config asset related to the current module
                ModuleConfigAsset configAsset = gameRef.GetConfig(moduleRef, out bool isOverride);

                float indent = DepthIndent * (moduleRef.Depth + 1);
                if (configAsset == null)
                    indent += FoldoutIndent;
                rect.x += indent + ItemHorizontalPadding;
                rect.width -= indent + ItemHorizontalPadding;

                // Space for the controls on the right side
                rect.width -= MoreGUI.WidthL;

                if (configAsset != null)
                    EditorGUIUtility.AddCursorRect(rect, MouseCursor.Link);

                // Draw foldout
                EditorGUI.BeginChangeCheck();
                bool foldout = EditorGUI.Foldout
                (
                    rect,
                    _moduleConfigsFoldedOut.Contains(id),
                    ModuleEditorUtility.GetExpectedConfigAssetName(moduleRef.ModuleType),
                    true,
                    (configAsset != null ? EditorStyles.foldout : EditorStyles.label).FontSize(12).Bold().TextAlignment(TextAnchor.MiddleLeft)
                );
                if (EditorGUI.EndChangeCheck())
                {
                    if (foldout && !_moduleConfigsFoldedOut.Contains(id))
                        _moduleConfigsFoldedOut.Add(id);
                    else
                        _moduleConfigsFoldedOut.Remove(id);
                }

                // Draw config asset inspector if applicable and folded out
                if (foldout && configAsset != null)
                {
                    if (_moduleConfigsEditors.TryGetValue(id, out Editor editor))
                    {
                        if (editor.target != configAsset)
                        {
                            DestroyImmediate(editor);
                            _moduleConfigsEditors.Remove(id);
                        }
                    }

                    if (editor == null)
                    {
                        editor = CreateEditor(configAsset);
                        _moduleConfigsEditors.Add(id, editor);
                    }

                    EditorGUILayout.Space();
                    editor.OnInspectorGUI();
                    EditorGUILayout.Space();
                }

                rect.x += rect.width;
                rect.width = MoreGUI.WidthL;
                rect.height -= ItemVerticalPadding * 2;
                rect.y += ItemVerticalPadding;

                if (configAsset != null)
                {
                    if (isOverride)
                    {
                        Rect controlRect = new Rect(rect);
                        controlRect.width -= controlRect.height + MoreGUI.HMargin;
                        EditorGUI.HelpBox(controlRect, "Override", MessageType.Warning);

                        controlRect.x += controlRect.width + MoreGUI.HMargin;
                        controlRect.width = controlRect.height;
                        if (GUI.Button(controlRect, EditorIcons.IconContent("delete", "Remove the configuration override for this module from the selected environment.", true), MoreEditorGUI.IconButtonStyle.Padding(2, 2)))
                        {
                            if (GameRefEditorUtility.DeleteOverride(gameRef, selectedEnvironment.Name, moduleRef))
                                serializedObject.Update();
                        }
                    }
                    else
                    {
                        using (new EnabledScope(selectedEnvironment != null))
                        {
                            if (GUI.Button(rect, EditorIcons.IconContent("content_copy", " Create Override", "Creates a configuration override for this module in the selected environment.", true), MoreEditorGUI.IconButtonStyle.Padding(2, 2)))
                            {
                                if(GameRefEditorUtility.CreateOverride(gameRef, selectedEnvironment.Name, moduleRef))
                                    serializedObject.Update();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// List of all the signals that can be emitted from the modules of this Game Ref.
        /// </summary>
        private void DrawSignalsView()
        {
            bool hasSignals = GameRefUtility.GetSignalInfos(target as GameRefAsset).Length > 0;

            string label = "<b>Selected Signal</b>";
            if (_selectedSignal != null)
                label += $": <i>{(_selectedSignal.DataType != null ? _selectedSignal.DataType.Name : "void")}</i>";

            EditorGUILayout.LabelField(label, EditorStyles.label.RichText(true));

            if (hasSignals && _selectedSignal != null)
            {
                using (new EnabledScope(false))
                    EditorGUILayout.TextField("Name",  _selectedSignal.name);

                SerializedObject signalAssetObj = new SerializedObject(_selectedSignal);
                EditorGUILayout.PropertyField(signalAssetObj.FindProperty("_description"));
                signalAssetObj.ApplyModifiedProperties();
            }
            else
            {
                using (new EnabledScope(false))
                {
                    EditorGUILayout.TextField("Name", "");
                    EditorGUILayout.TextField("Description", "", EditorStyles.textField.WordWrap(true), GUILayout.Height(EditorGUIUtility.singleLineHeight * 3));
                }
            }

            EditorGUILayout.Space();
            MoreEditorGUI.HorizontalSeparator();
            EditorGUILayout.Space();

            if (hasSignals)
            {
                Rect rect = EditorGUILayout.GetControlRect(false, SignalsTreeView.totalHeight);
                SignalsTreeView.OnGUI(rect);
            }
            else
            {
                EditorGUILayout.HelpBox($"No signals emitted by the modules of this game instance.\nTo declare and emit signals, you must use [Signal] attribute in your {nameof(IModule)} implementations. This attribute can only be used on properties of types that derive from {nameof(SignalAsset)}.", MessageType.Info);
            }
        }

        #endregion


        #region Private API

        /// <inheritdoc cref="s_tabsToolbarContents"/>
        private static GUIContent[] TabsToolbarContents
        {
            get
            {
                if (s_tabsToolbarContents == null)
                {
                    s_tabsToolbarContents = new GUIContent[]
                    {
                        EditorIcons.IconContent("tune", nameof(ETab.Configuration), "Displays the configurations of this Game Ref based on the modules hierarchy, and allows to create and select environments to override its default configuration.", true),
                        EditorIcons.IconContent("cell_tower", nameof(ETab.Signals), "List of all the signals that can be emitted from the modules of this Game Ref.", true),
                    };
                }
                return s_tabsToolbarContents;
            }
        }

        /// <inheritdoc cref="_signalsTreeView"/>
        private SignalsTreeView SignalsTreeView
        {
            get
            {
                if (_signalsTreeView == null)
                {
                    if (_signalsTreeViewState == null)
                        _signalsTreeViewState = new TreeViewState();

                    _signalsTreeView = new SignalsTreeView(target as GameRefAsset, _signalsTreeViewState);
                    _signalsTreeView.OnSelectSignal += HandleSelectSignal;
                    _signalsTreeView.Reload();
                }
                return _signalsTreeView;
            }
        }

        /// <summary>
        /// Gets a unique id for a given module.
        /// </summary>
        /// <param name="moduleRef">The module ref for which to generate a unique id.</param>
        /// <returns>Returns the unique id of the given module.</returns>
        private int GetModuleId(ModuleRef moduleRef)
        {
            return $"{moduleRef.ModuleType.AssemblyQualifiedName}{moduleRef.Depth}{moduleRef.Index}".GetHashCode();
        }

        /// <inheritdoc cref="SignalsTreeView.SelectSignalDelegate"/>
        private void HandleSelectSignal(SignalAsset signal)
        {
            _selectedSignal = signal;
        }

        #endregion

    }

}