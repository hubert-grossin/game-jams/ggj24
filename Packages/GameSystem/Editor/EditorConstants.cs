/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System.CodeDom;

namespace SideXP.GameSystem
{

    /// <summary>
    /// Miscellaneous constant values used in this package.
    /// </summary>
    public static class EditorConstants
    {

        public const string ModuleSuffix = "Module";
        public const string ConfigAssetSuffix = "ConfigAsset";

        /// <summary>
        /// The namespace import of the <see cref="GameSystem"/> package.
        /// </summary>
        public static readonly CodeNamespaceImport GameSystemNamespace = new CodeNamespaceImport(typeof(Module).Namespace);

    }

}