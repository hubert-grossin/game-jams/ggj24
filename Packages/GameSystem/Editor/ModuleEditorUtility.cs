/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;
using System.Reflection;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;

using SideXP.Core;
using SideXP.Core.Reflection;

namespace SideXP.GameSystem.EditorOnly
{

    /// <summary>
    /// Miscellaneous utility functions for working with <see cref="Module"/> or related assets such as <see cref="ModuleConfigAsset"/>.
    /// </summary>
    public static class ModuleEditorUtility
    {

        /// <summary>
        /// Stores the module types as keys, and their related configuration type if it exists as values.
        /// </summary>
        private static Dictionary<Type, Type> s_configByModuleType = null;

        /// <summary>
        /// Gets the <see cref="ModuleConfigAsset"/> type related to a given module type.
        /// </summary>
        /// <param name="moduleType">The module type of which you want to get the configuration asset type.</param>
        /// <param name="configAssetType">Outputs the found <see cref="ModuleConfigAsset"/> type.</param>
        /// <returns>Returns true if a <see cref="ModuleConfigAsset"/> type has been found.</returns>
        public static bool GetConfigAssetType(Type moduleType, out Type configAssetType)
        {
            return ConfigByModuleType.TryGetValue(moduleType, out configAssetType);
        }

        /// <returns>Returns the found <see cref="ModuleConfigAsset"/> type.</returns>
        /// <inheritdoc cref="GetConfigAssetType(Type, out Type)"/>
        public static Type GetConfigAssetType(Type moduleType)
        {
            return GetConfigAssetType(moduleType, out Type configAssetType) ? configAssetType : null;
        }

        /// <typeparam name="T">The module type of which you want to get the configuration asset type.</typeparam>
        /// <inheritdoc cref="GetConfigAssetType(Type, out Type)"/>
        public static bool GetConfigAssetType<T>(out Type configAssetType)
            where T : Module
        {
            return GetConfigAssetType(typeof(T), out configAssetType);
        }

        /// <inheritdoc cref="GetConfigAssetType(Type)"/>
        /// <inheritdoc cref="GetConfigAssetType{T}(out Type)"/>
        public static Type GetConfigAssetType<T>()
            where T : Module
        {
            return GetConfigAssetType(typeof(T));
        }

        /// <summary>
        /// Gets the expected name of a <see cref="ModuleConfigAsset"/> related to a given module type.
        /// </summary>
        /// <param name="moduleType">The type of the module for which you want to get the <see cref="ModuleConfigAsset"/> name.</param>
        /// <returns>Returns the expected asset name.</returns>
        public static string GetExpectedConfigAssetName(Type moduleType)
        {
            // Use the name defined in [Module] attribute if available
            if (moduleType.GetCustomAttribute(out ModuleAttribute moduleAttr) && !string.IsNullOrEmpty(moduleAttr.ConfigName))
                return moduleAttr.ConfigName;

            string name;
            // Use config asset type as default name
            if(GetConfigAssetType(moduleType, out Type configAssetType))
            {
                name = configAssetType.Name.EndsWith(EditorConstants.ConfigAssetSuffix)
                    ? configAssetType.Name.Substring(0, configAssetType.Name.Length - EditorConstants.ConfigAssetSuffix.Length)
                    : configAssetType.Name;
            }
            // As a fallback, use the module type as default name
            else
            {
                name = moduleType.Name.EndsWith(EditorConstants.ModuleSuffix)
                    ? moduleType.Name.Substring(0, moduleType.Name.Length - EditorConstants.ModuleSuffix.Length)
                    : moduleType.Name;
            }

            return ObjectNames.NicifyVariableName(name);
        }

        /// <inheritdoc cref="s_configByModuleType"/>
        private static Dictionary<Type, Type> ConfigByModuleType
        {
            get
            {
                if (s_configByModuleType == null)
                {
                    s_configByModuleType = new Dictionary<Type, Type>();

                    List<ModuleConfigAsset> configAssets = new List<ModuleConfigAsset>();
                    // For each available config asset type in the project
                    foreach (Type configAssetType in ReflectionUtility.GetAllTypesInProjectAssignableFrom<ModuleConfigAsset>())
                    {
                        // Create an instance in memory of that config asset
                        try
                        {
                            ModuleConfigAsset configAsset = ScriptableObject.CreateInstance(configAssetType) as ModuleConfigAsset;
                            if (configAsset != null)
                                configAssets.Add(configAsset);
                        }
                        catch (Exception) { }
                    }

                    // For each available module type in the project
                    foreach (Type moduleType in ReflectionUtility.GetAllTypesInProjectAssignableFrom<Module>())
                    {
                        // Find the config asset related to the current module type
                        int index = configAssets.FindIndex(i => i.ModuleType == moduleType);
                        // Skip if no config asset has been found
                        if (index < 0)
                        {
                            if (ReflectionUtility.GetFieldsAndPropertiesWithAttribute<ConfigAttribute>(moduleType).Length > 0)
                                Debug.LogWarning($"No configuration asset type found related to the modules of type \"{moduleType.FullName}\", which still contain fields or properties marked with [Config] attribute.");
                            continue;
                        }

                        // Bind the module type to the configuration asset type
                        if (!s_configByModuleType.ContainsKey(moduleType))
                            s_configByModuleType.Add(moduleType, configAssets[index].GetType());
                        else if (s_configByModuleType[moduleType] == null)
                            s_configByModuleType[moduleType] = configAssets[index].GetType();

                        configAssets.RemoveAt(index);
                    }
                }

                return s_configByModuleType;
            }
        }

    }

}