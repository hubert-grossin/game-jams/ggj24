/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;
using System.IO;
using System.CodeDom;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using UnityEngine;
using UnityEditor;

using SideXP.Core;
using SideXP.Core.EditorOnly;
using SideXP.Core.Reflection;

namespace SideXP.GameSystem.EditorOnly
{

    /// <summary>
    /// Process scripts and assets related to modules as they are imported or modified.
    /// </summary>
    [InitializeOnLoad]
    public static class ModulesProcessor
    {

        private static readonly CodeTypeReference OriginalNameAttributeTypeRef = null;

        /// <summary>
        /// Patterns used to find find a field or property's attribute declarations.
        /// </summary>
        private static readonly string AttributeUsagesPatternPrefix = @"((\[[^[\]]*\])\s*)*.*\b";
        private static readonly string AttributeUsagesPatternSuffix = @"\s?[\n\r;={]";

        /// <summary>
        /// Pattern used to extract a single attribute line
        /// </summary>
        private static readonly Regex AttributeUsagePattern = new Regex(@"\[.+\]");

        /// <summary>
        /// Patterns used to get the line at which a field or property declaration starts and ends.
        /// </summary>
        private static readonly string FieldOrPropertyDeclarationPatternPrefix = @"(?<indent>[ \t]*)(?:public|protected|private)\s+.+\s+";
        private static readonly string FieldOrPropertyDeclarationPatternSuffix = @"[\s=;]";

        private static bool s_shouldReloadGameRefs = false;
        private static bool s_shouldWaitForConfigAssetScriptImport = false;

        /// <summary>
        /// Static constructor.
        /// </summary>
        static ModulesProcessor()
        {
            OriginalNameAttributeTypeRef = MakeAttributeTypeRef<OriginalNameAttribute>();

            // Handle import and move of module or config asset script
            AssetsProcessor.ListenImportAsset((path, isNew, didDomainReload, previousPath) =>
            {
                if (ProcessImportedModuleScript(path, previousPath, out bool didGenerateConfigAssetScript))
                {
                    s_shouldReloadGameRefs = true;
                    if (didGenerateConfigAssetScript)
                        s_shouldWaitForConfigAssetScriptImport = true;
                }
                else if (ProcessImportedModuleConfigAssetScript(path, previousPath))
                {
                    s_shouldReloadGameRefs = true;
                }
            });

            // Handle deletion of module script or config script
            AssetsProcessor.ListenBeforeDeleteAsset(path =>
            {
                if (ProcessDeletedModuleScript(path))
                {
                    s_shouldReloadGameRefs = true;
                }
                else if (ProcessDeletedModuleConfigAssetScript(path))
                {
                    s_shouldReloadGameRefs = true;
                }

                return AssetDeleteResult.DidNotDelete;
            });

            // Reload Game Refs if somethinng did change about the modules
            AssetsProcessor.ListenImportAllAssets(paths => ReloadGameRefsIfRequired());
            AssetsProcessor.ListenDeleteAllAssets(paths => ReloadGameRefsIfRequired());
        }

        /// <summary>
        /// Handles operations to perform when the script of an <see cref="IModule"/> type is imported.
        /// </summary>
        /// <param name="scriptPath">The path to the imported script.</param>
        /// <param name="scriptPreviousPath">The previous path to the imported script, in case it has moved.</param>
        /// <param name="didGenerateConfigAssetScript">If enabled, <see cref="GameRefAsset"/> should be reloaded only after the import of
        /// that config asset has been done.</param>
        /// <returns>Returns true if something did change about a module, and so the <see cref="GameRefAsset"/> should be
        /// reloaded.</returns>
        private static bool ProcessImportedModuleScript(string scriptPath, string scriptPreviousPath, out bool didGenerateConfigAssetScript)
        {
            didGenerateConfigAssetScript = false;

            // Cancel if the type of the imported script can't be extracted
            if (!ScriptUtility.GetScriptType(scriptPath, out Type moduleType))
                return false;

            // Cancel if the script doesn't implement IModule
            if (!typeof(IModule).IsAssignableFrom(moduleType))
                return false;

            // Get or create cache entry
            ModulesCache.ModuleInfo cacheInfo = null;
            if (!string.IsNullOrEmpty(scriptPreviousPath))
            {
                if (ModulesCache.Get(scriptPreviousPath, out cacheInfo))
                    cacheInfo.ModuleScriptPath = scriptPath;
            }
            if (cacheInfo == null)
                cacheInfo = ModulesCache.Get(scriptPath, true);

            if (!moduleType.GetCustomAttribute(out ModuleAttribute moduleAttr))
                moduleAttr = new ModuleAttribute();

            // Get configurable fields and properties
            FieldOrPropertyInfo[] configurableElements = ReflectionUtility.GetFieldsAndPropertiesWithAttribute<ConfigAttribute>(moduleType, true);

            // If the module doesn't declare any configurable element
            if (configurableElements.Length <= 0)
            {
                // If the module has a related config asset script (from cache), delete that script
                if (!string.IsNullOrEmpty(cacheInfo.ModuleConfigAssetPath))
                    AssetDatabase.DeleteAsset(cacheInfo.ModuleConfigAssetPath);
                
                return true;
            }
            // Else, if the module has one or more configurable elements
            else
            {
                string configAssetScriptPath = null;

                // If a config asset script path already exists in the cache
                if (!string.IsNullOrEmpty(cacheInfo.ModuleConfigAssetPath))
                {
                    // If that script exists, use its path
                    if (File.Exists(cacheInfo.ModuleConfigAssetPath.ToAbsolutePath()))
                        configAssetScriptPath = cacheInfo.ModuleConfigAssetPath;
                }
                
                // If no config asset script has been found
                if (string.IsNullOrEmpty(configAssetScriptPath))
                {
                    if (moduleAttr.RelativeFromAssets)
                    {
                        configAssetScriptPath = !string.IsNullOrEmpty(moduleAttr.Path)
                            ? Path.Combine(Application.dataPath, Path.HasExtension(moduleAttr.Path) ? Path.GetDirectoryName(moduleAttr.Path) : moduleAttr.Path)
                            : Path.Combine(Application.dataPath);
                    }
                    else
                    {
                        configAssetScriptPath = !string.IsNullOrEmpty(moduleAttr.Path)
                            ? Path.HasExtension(moduleAttr.Path) ? Path.GetDirectoryName(moduleAttr.Path) : moduleAttr.Path
                            : string.Empty;
                        configAssetScriptPath = Path.Combine(Path.GetDirectoryName(scriptPath), configAssetScriptPath);
                    }

                    string configAssetClassName = !string.IsNullOrEmpty(moduleAttr.Path) && Path.HasExtension(moduleAttr.Path)
                        ? Path.GetFileNameWithoutExtension(moduleAttr.Path)
                        : moduleType.Name + EditorConstants.ConfigAssetSuffix;

                    // At this step, configAssetScriptPath contains the expected directory. Computing the class name separately and
                    // combining the two in a second pass ensures the file name gets the same class name.
                    configAssetScriptPath = Path.GetFullPath(Path.Combine(configAssetScriptPath, configAssetClassName + ScriptUtility.CsExtension)).ToRelativePath();
                }

                // Generate config asset script coontent
                if (!GenerateModuleConfigAssetScript(scriptPath, moduleType, moduleAttr, configurableElements, Path.GetFileNameWithoutExtension(configAssetScriptPath), out string configAssetScriptContent))
                {
                    Debug.LogError($"Failed to generate the config asset script for module {moduleType.FullName} ({scriptPath})");
                    return false;
                }

                // Check if the config asset script content did change
                try
                {
                    // If that config asset script exists
                    if (File.Exists(configAssetScriptPath.ToAbsolutePath()))
                    {
                        // Cancel if that config asset script has the same content as the generated one (meaning nothing changed)
                        if (File.ReadAllText(configAssetScriptPath.ToAbsolutePath()) == configAssetScriptContent)
                            return true;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }

                // Write the config asset script file
                try
                {
                    string configAssetScriptAbsolutePath = configAssetScriptPath.ToAbsolutePath();
                    // Ensure the target directory exists
                    Directory.CreateDirectory(Path.GetDirectoryName(configAssetScriptAbsolutePath));
                    // Write the config asset script file
                    File.WriteAllText(configAssetScriptAbsolutePath, configAssetScriptContent);
                }
                catch(Exception e)
                {
                    Debug.LogException(e);
                }

                // Update cache & import the generated asset
                cacheInfo.ModuleConfigAssetPath = configAssetScriptPath;
                didGenerateConfigAssetScript = true;
                AssetDatabase.ImportAsset(configAssetScriptPath.ToRelativePath());
                return true;
            }
        }

        /// <summary>
        /// Handles operations to perform when the script of a <see cref="ModuleConfigAsset"/> type is imported.
        /// </summary>
        /// <inheritdoc cref="ProcessImportedModuleScript(string, string)"/>
        private static bool ProcessImportedModuleConfigAssetScript(string scriptPath, string scriptPreviousPath)
        {
            // Cancel if the type of the imported script can't be extracted
            if (!ScriptUtility.GetScriptType(scriptPath, out Type moduleType))
                return false;

            // Cancel if the script doesn't implement IModule
            if (!typeof(ModuleConfigAsset).IsAssignableFrom(moduleType))
                return false;

            // Update cache if the script has been moved
            if (!string.IsNullOrEmpty(scriptPreviousPath))
            {
                if (ModulesCache.GetByModuleConfigAssetScriptPath(scriptPreviousPath, out ModulesCache.ModuleInfo cacheInfo))
                    cacheInfo.ModuleConfigAssetPath = scriptPath;
            }

            return true;
        }

        /// <summary>
        /// Handles operations to perform when the script of an <see cref="IModule"/> type is deleted.
        /// </summary>
        /// <param name="scriptPath">The path to the deleted script.</param>
        /// <inheritdoc cref="ProcessImportedModuleScript(string, string)"/>
        private static bool ProcessDeletedModuleScript(string scriptPath)
        {
            if (ModulesCache.Get(scriptPath, out ModulesCache.ModuleInfo cacheInfo))
            {
                if (!string.IsNullOrEmpty(cacheInfo.ModuleConfigAssetPath))
                    AssetDatabase.DeleteAsset(cacheInfo.ModuleConfigAssetPath);

                ModulesCache.Delete(scriptPath);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handles operations to perform when the script of a <see cref="ModuleConfigAsset"/> type is deleted.
        /// </summary>
        /// <inheritdoc cref="ProcessDeletedModuleScript(string)"/>
        private static bool ProcessDeletedModuleConfigAssetScript(string scriptPath)
        {
            if (ModulesCache.GetByModuleConfigAssetScriptPath(scriptPath, out ModulesCache.ModuleInfo cacheInfo))
                cacheInfo.ModuleConfigAssetPath = null;

            // Cancel if the type of the imported script can't be extracted
            if (!ScriptUtility.GetScriptType(scriptPath, out Type type))
                return false;

            // Cancel if the script doesn't implement IModule
            if (!typeof(ModuleConfigAsset).IsAssignableFrom(type))
                return false;

            // Destroy all subassets that use the config asset script
            bool didDestroyAssets = false;
            foreach (ModuleConfigAsset configAsset in ObjectUtility.FindAllAssetsOfType(type, true))
            {
                if (configAsset.IsSubasset())
                {
                    configAsset.Destroy(true);
                    didDestroyAssets = true;
                }
            }

            return didDestroyAssets;
        }

        /// <summary>
        /// Trigger the reload of <see cref="GameRefAsset"/> in the project if something has changed about the modules.
        /// </summary>
        private static void ReloadGameRefsIfRequired()
        {
            /**
             *  If a module script has been imported, the processor may have generated the related module config asset script. When it
             *  happens, we call AssetDatabase.ImportAsset() on that script so Unity can trigger a domain reload, and so trigger another
             *  import pass.
             *  This means that during the module script import pass, the config asset type may not exist yet. So this condition avoids to
             *  reload the Game Refs while that type is not inclued in domain. The Game Refs will then be reloaded only when reimporting the
             *  generated config asset script.
             */
            if (s_shouldWaitForConfigAssetScriptImport)
            {
                s_shouldReloadGameRefs = false;
                s_shouldWaitForConfigAssetScriptImport = false;
                return;
            }

            if (s_shouldReloadGameRefs)
            {
                GameRefEditorUtility.ReloadAll();
                s_shouldReloadGameRefs = false;
            }
        }

        #region Script Generation

        /// <summary>
        /// Generates a <see cref="ModuleConfigAsset"/> script
        /// </summary>
        /// <param name="moduleScriptPath">The path to the module script.</param>
        /// <param name="moduleType">The type of the module for which the <see cref="ModuleConfigAsset"/> script is generated. Assumes that
        /// type implements <see cref="IModule"/>.</param>
        /// <param name="moduleAttr">The module attribute used for the given module type. Assumes that it has been extracted from the given
        /// module type.</param>
        /// <param name="configurableFieldAndProps">The list of all the fields and properties to include in the config asset. Assumes this
        /// list has been extracted from the given module type, and are all marked with <see cref="ConfigAttribute"/>.</param>
        /// <param name="moduleConfigAssetScriptContent">Outputs the generated content of the script as a string.</param>
        /// <returns>Returns true if the script content has been generated successfully.</returns>
        private static bool GenerateModuleConfigAssetScript
        (
            string moduleScriptPath,
            Type moduleType,
            ModuleAttribute moduleAttr,
            FieldOrPropertyInfo[] configurableFieldAndProps,
            string moduleConfigAssetClassName,
            out string moduleConfigAssetScriptContent
        )
        {
            IModule moduleInstance = null;
            GameObject tmpObj = null;
            if (typeof(Component).IsAssignableFrom(moduleType))
            {
                tmpObj = new GameObject();
                tmpObj.hideFlags = HideFlags.HideInInspector | HideFlags.HideInInspector | HideFlags.DontSave;
                moduleInstance = tmpObj.AddComponent(moduleType) as IModule;
            }
            else
            {
                try
                {
                    moduleInstance = Activator.CreateInstance(moduleType) as IModule;
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }

            CodeCompileUnit domCompileUnit = ScriptGeneratorUtility.MakeScriptCompileUnit(moduleType, out CodeNamespace importsNamespace, out CodeNamespace domNamespace);
            importsNamespace.Imports.Add(EditorConstants.GameSystemNamespace);

            if (moduleAttr == null)
                moduleAttr = new ModuleAttribute();

            // Declare config asset class
            CodeTypeDeclaration domClass = new CodeTypeDeclaration(moduleConfigAssetClassName);
            domClass.Comments.Add(new CodeCommentStatement("<summary>", true));
            domClass.Comments.Add(new CodeCommentStatement($"Auto-generated class that represents the <see cref=\"{moduleInstance.GetType().FullName}\"/> module's configuration.", true));
            domClass.Comments.Add(new CodeCommentStatement("</summary>", true));
            domClass.BaseTypes.Add(new CodeTypeReference { BaseType = typeof(ModuleConfigAssetGeneric<>).Name + $"[{moduleInstance.GetType().Name}]" });
            domNamespace.Types.Add(domClass);

            // Hydrate config asset script DOM with fields and properties marked with [Config]
            ExtractConfigFieldsAndProperties(moduleType, moduleInstance, domClass, importsNamespace, configurableFieldAndProps);
            // Create the script content
            moduleConfigAssetScriptContent = ScriptGeneratorUtility.GenerateScript(domCompileUnit);
            // Copy additional attributes
            CopyConfigFieldsAttributes(moduleScriptPath, ref moduleConfigAssetScriptContent, configurableFieldAndProps);

            if (tmpObj != null)
                tmpObj.Destroy();

            return true;
        }

        /// <summary>
        /// Extracts all the fields and properties marked with <see cref="ConfigAttribute"/> from a module instance.
        /// </summary>
        /// <param name="moduleType">The type of the module. Assumes it matches the given module instance.</param>
        /// <param name="moduleInstance">An instance of the module. Used to query fields and properties with default values. If null given,
        /// no default value will be defined in the generated script. Assumes it matches the given module type.</param>
        /// <param name="domClass">The type declaration of the configuration asset class. Used to add found configurable elements as members
        /// of that class.</param>
        /// <param name="importsNamespace">The namespace declaration used to store the imports.</param>
        /// <param name="configFieldsAndProperties">The list of all configurable elements from the module instance.</param>
        private static void ExtractConfigFieldsAndProperties(Type moduleType, IModule moduleInstance, CodeTypeDeclaration domClass, CodeNamespace importsNamespace, IEnumerable<FieldOrPropertyInfo> configFieldsAndProperties)
        {
            // For each field or property that declares a config or state value
            foreach (FieldOrPropertyInfo fieldOrProperty in configFieldsAndProperties)
            {
                string configFieldName = GetConfigFieldName(fieldOrProperty);

                // Create the field data
                CodeMemberField domField;

                // If the current field has a "complex" type that may require an import statement
                if (!string.IsNullOrEmpty(fieldOrProperty.Type.Namespace) && !fieldOrProperty.Type.IsPrimitive)
                {
                    // Add import statement if not already set
                    if (!moduleType.Namespace.StartsWith(fieldOrProperty.Type.Namespace))
                        importsNamespace.Imports.Add(new CodeNamespaceImport(fieldOrProperty.Type.Namespace));

                    // Use base type instead of full type name
                    CodeTypeReference typeRef = new CodeTypeReference(fieldOrProperty.Type);
                    typeRef.BaseType = fieldOrProperty.Type.Name;
                    domField = new CodeMemberField(typeRef, configFieldName);
                }
                // Else, if the current field has a primitive type or doesn't require an import statement
                else
                {
                    domField = new CodeMemberField(fieldOrProperty.Type, configFieldName);
                    if (moduleInstance != null)
                        domField.InitExpression = new CodePrimitiveExpression(fieldOrProperty.GetValue(moduleInstance));
                }

                // Import namespaces for custom attributes (note that attributes are copied from module implementation to generated
                // configuration asset class in a separate call
                foreach (Attribute attr in fieldOrProperty.GetCustomAttributes())
                {
                    Type attributeType = attr.GetType();
                    string attributeNamespace = attributeType.Namespace;

                    if (!string.IsNullOrEmpty(attributeNamespace) && !moduleType.Namespace.StartsWith(attributeNamespace))
                        importsNamespace.Imports.Add(new CodeNamespaceImport(attributeNamespace));
                }

                // Add NameInModule attribute
                domField.Attributes = MemberAttributes.Public;
                domField.CustomAttributes.Add(new CodeAttributeDeclaration(OriginalNameAttributeTypeRef,
                    new CodeAttributeArgument(new CodePrimitiveExpression(fieldOrProperty.Name))));

                // Add field to generated class
                domClass.Members.Add(domField);
            }
        }

        /// <summary>
        /// Copies the attributes from field and properties marked with <see cref="ConfigAttribute"/> from the module implementation into
        /// the generated config asset class script.
        /// </summary>
        /// <param name="moduleScriptPath">The path to the module script.</param>
        /// <param name="generatedConfigScriptContent">The content of the configuration asset script generated for the processed module.
        /// That content is modified through this function.</param>
        /// <param name="configFieldsAndProperties">The fields and properties extracted from the module's implementation.</param>
        /// <inheritdoc cref="ReimportModuleScript(string, string)"/>
        private static void CopyConfigFieldsAttributes(string moduleScriptPath, ref string generatedConfigScriptContent, IEnumerable<FieldOrPropertyInfo> configFieldsAndProperties)
        {
            string moduleScript = File.ReadAllText(moduleScriptPath);
            List<string> configScriptLines = new List<string>(generatedConfigScriptContent.SplitLines(StringSplitOptions.None));
            bool changed = false;

            // For each property in module script
            foreach (FieldOrPropertyInfo fieldOrProperty in configFieldsAndProperties)
            {
                // Try to find the property and its preceeding custom attributes
                string pattern = AttributeUsagesPatternPrefix + fieldOrProperty.Name + AttributeUsagesPatternSuffix;
                Match propMatch = Regex.Match(moduleScript, pattern);

                // Skip if the property doesn't match (which shouldn't happen)
                if (!propMatch.Success)
                    continue;

                string dataName = GetConfigFieldName(fieldOrProperty);
                string indent = "";
                // Find the line that contains the same property declaration in data script
                int lineIndex = configScriptLines.FindIndex(l =>
                {
                    Match match = Regex.Match(l, $"{FieldOrPropertyDeclarationPatternPrefix}{dataName}{FieldOrPropertyDeclarationPatternSuffix}");
                    if (!match.Success)
                        return false;

                    // Store indentation so custom attributes can be copied with the exact spacing
                    indent = match.Groups["indent"].Value;
                    return true;
                });

                // Skip if the property is not declared in the data script (which shouldn't happen)
                if (lineIndex < 0)
                    continue;

                // Copy each found custom attribute from the module script to the data script
                Match attrMatch = null;
                int cursor = 0;
                int count = 0;
                do
                {
                    attrMatch = AttributeUsagePattern.Match(propMatch.Value, cursor);
                    if (attrMatch.Success)
                    {
                        configScriptLines.Insert(lineIndex, indent + attrMatch.Value);
                        cursor = attrMatch.Index + attrMatch.Value.Length;
                        changed = true;
                        count++;
                    }
                }
                while (attrMatch != null && attrMatch.Success);

                // Reverse attributes, so they're used in the same order as they are in the module script
                if (count > 0)
                {
                    configScriptLines.Reverse(lineIndex, count);
                }
            }

            // Write new lines if applicable
            if (changed)
                generatedConfigScriptContent = string.Join("\r\n", configScriptLines);
        }

        /// <summary>
        /// Converts the name of a module field or property to match the coding conventions into the config class. It's admitted that in
        /// config class all field and properties are public.
        /// </summary>
        /// <param name="fieldOrPropertyFromModule">The informations about the field or property froom the module class.</param>
        /// <returns>Returns the converted name.</returns>
        public static string GetConfigFieldName(FieldOrPropertyInfo fieldOrPropertyFromModule)
        {
            string name = fieldOrPropertyFromModule.Name;
            // Remove underscore if the field is private and starts with that char
            if (fieldOrPropertyFromModule.IsPrivate && name.StartsWith("_"))
            {
                name = name.Substring(1);
            }

            // Force PascalCase by making the first letter of the name uppercase
            if (name.Length > 0)
            {
                name = char.ToUpper(name[0]) + name.Substring(1);
            }

            return name;
        }

        #endregion


        #region Utilities

        /// <summary>
        /// Creates a type reference for an attribute, removing the "-Attribute" suffix in the base type.
        /// </summary>
        /// <typeparam name="T">The type of the attribute.</typeparam>
        /// <param name="keepBaseType">If enabled, keeps the origin namespace.</param>
        /// <returns>Returns the created attribute type reference.</returns>
        private static CodeTypeReference MakeAttributeTypeRef<T>(bool keepBaseType = false)
            where T : Attribute
        {
            CodeTypeReference attrTypeRef = new CodeTypeReference(typeof(T));
            if (!keepBaseType)
                attrTypeRef.BaseType = typeof(T).Name;

            if (attrTypeRef.BaseType.EndsWith(Constants.AttributeSuffix))
                attrTypeRef.BaseType = attrTypeRef.BaseType.Substring(0, attrTypeRef.BaseType.Length - Constants.AttributeSuffix.Length);

            return attrTypeRef;
        }

        #endregion

    }

}