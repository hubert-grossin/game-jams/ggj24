using SideXP.Broadcaster;


namespace SideXP.Broadcaster.Demos
{
    
    
    /// <summary>
    /// Auto-generated class that represents a signal with <see cref="PlayerInfo"/> parameter.
    /// </summary>
    [SignalMetadata(Menu="Demos/Player Info")]
    public class PlayerInfoSignalAsset : SignalAssetGeneric<PlayerInfo>
    {
    }
}
