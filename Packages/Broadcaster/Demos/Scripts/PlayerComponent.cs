using UnityEngine;
using TMPro;

namespace SideXP.Broadcaster.Demos
{

    /// <summary>
    /// Illustrates how to subscribe to a signal asset and initialize the object with the latest data sent to that signal. This is useful
    /// when you need to spawn objects that need to get informations from a signal that already have been emitted. You can then get the data
    /// the same way you would update it as the game continues.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    [AddComponentMenu(Constants.AddComponentMenuDemos + "/Player")]
    public class PlayerComponent : MonoBehaviour
    {

        [Header("UI")]

        public TMP_Text PlayerName = null;
        public TMP_Text PlayerScore = null;

        [Header("Signal")]

        public PlayerInfoSignalAsset OnSendPlayerInfo = null;

        private void OnEnable()
        {
            OnSendPlayerInfo.Subscribe(HandlePlayerInfo, this, true);
        }

        private void OnDisable()
        {
            OnSendPlayerInfo.Unsubscribe(HandlePlayerInfo);
        }

        private void HandlePlayerInfo(PlayerInfo info)
        {
            PlayerName.text = info.Name;
            PlayerScore.text = info.Score.ToString("00000");
        }

    }

}