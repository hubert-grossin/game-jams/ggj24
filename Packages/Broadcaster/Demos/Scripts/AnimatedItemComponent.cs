using System.Collections;
using UnityEngine;
using SideXP.Core;

namespace SideXP.Broadcaster.Demos
{

    /// <summary>
    /// Illustrates how to subscribe to a signal asset. This component uses <see cref="SignalHandle.SetDelay(float)"/> to define the time to
    /// wait before the signal can be considered as completed, based on the duration of the animation.
    /// </summary>
    [AddComponentMenu(Constants.AddComponentMenuDemos + "/Animated Item")]
    public class AnimatedItemComponent : MonoBehaviour
    {

        [Header("Animation")]

        public Transform MovingObject = null;

        [AnimCurve(1, 1)]
        public AnimationCurve MovementXCurve = AnimationCurveUtility.EaseInOut;

        public float MovementXRange = 3f;

        [field: SerializeField]
        public float AnimationDuration { get; set; } = 1.6f;

        [Header("Signal")]

        public VoidSignalAsset OnAnimate = null;

        private Vector3 _origin = Vector3.zero;
        private Coroutine _animationCoroutine = null;

        private void Awake()
        {
            _origin = MovingObject.position;
            if (MovingObject == null)
                MovingObject = transform;
        }

        private void OnEnable()
        {
            OnAnimate.Subscribe(HandleAnimate, this);
        }

        private void OnDisable()
        {
            OnAnimate.Unsubscribe(HandleAnimate);
        }

        private void HandleAnimate(SignalHandle handle)
        {
            if (_animationCoroutine != null)
                StopCoroutine(_animationCoroutine);

            _animationCoroutine = StartCoroutine(Animate(AnimationDuration));
            handle.SetDelay(AnimationDuration);
        }

        private IEnumerator Animate(float duration)
        {
            float timer = 0f;
            Vector3 target = MovingObject.position + Vector3.up * MovementXRange;
            while (timer < duration)
            {
                timer += Time.deltaTime;
                if (timer >= duration)
                    break;

                MovingObject.position = Vector3.Lerp(_origin, target, MovementXCurve.Evaluate(timer / duration));
                yield return null;
            }

            MovingObject.position = _origin;
        }

    }

}