namespace SideXP.Broadcaster.Demos
{

    /// <summary>
    /// Simple data container that illustrates the script generation for <see cref="SignalAsset"/>.
    /// </summary>
    [SignalData("./Generated")]
    [SignalMetadata(Menu = "Demos/Player Info")]
    public struct PlayerInfo
    {
        public string Name;
        public int Score;
    }

}