using UnityEngine;

namespace SideXP.Broadcaster.Demos
{

    /// <summary>
    /// Illustrates how to subscribe to a <see cref="SignalAsset"/> just to check if that signal is playing.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    [AddComponentMenu(Constants.AddComponentMenu + "/" + nameof(RotateFeedbackComponent))]
    public class RotateFeedbackComponent : MonoBehaviour
    {

        [Header("Animation")]

        [Tooltip("The speed (in degrees per second) at which this object rotates.")]
        public float RotationSpeed = 180f;

        [Header("Signal")]

        public PlayerInfoSignalAsset OnSendPlayerInfo = null;

        private SignalHandle _signalHandle = null;
        private float _angle = 0f;

        private void OnEnable()
        {
            OnSendPlayerInfo.Subscribe(HandleSignal, this);
        }

        private void OnDisable()
        {
            OnSendPlayerInfo.Unsubscribe(HandleSignal);
        }

        private void Update()
        {
            if (_signalHandle != null && _signalHandle.IsRunning)
            {
                _angle += RotationSpeed * Time.deltaTime;
                _angle %= 360f;
                transform.rotation = Quaternion.AngleAxis(_angle, transform.forward);
            }
        }

        private void HandleSignal(SignalHandle handle)
        {
            _signalHandle = handle;
        }

    }

}