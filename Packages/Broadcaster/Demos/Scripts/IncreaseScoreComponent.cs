using UnityEngine;
using TMPro;

namespace SideXP.Broadcaster.Demos
{

    /// <summary>
    /// Illustrates how to subscribe to a signal asset. This component uses <see cref="SignalHandle.Ping()"/> to keep the signal alive while
    /// the animation is playing, so we don't have to calculate the animation duration.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    [AddComponentMenu(Constants.AddComponentMenuDemos + "/Increase Score")]
    public class IncreaseScoreComponent : MonoBehaviour
    {

        [Header("Animation")]

        public TMP_Text ScoreDisplay = null;

        [Header("Signal")]

        public PlayerInfoSignalAsset OnSendPlayerInfo = null;

        private SignalHandle _signalHandle = null;
        private int _score = 0;

        private void OnEnable()
        {
            OnSendPlayerInfo.Subscribe(HandlePlayerInfo, this);
        }

        private void OnDisable()
        {
            OnSendPlayerInfo.Unsubscribe(HandlePlayerInfo);
        }

        private void Update()
        {
            int.TryParse(ScoreDisplay.text, out int currentScore);
            if (currentScore != _score)
            {
                currentScore += (int)Mathf.Sign(_score - currentScore);
                if (_signalHandle != null)
                    _signalHandle.Ping();
            }
            ScoreDisplay.text = currentScore.ToString("00000");
        }

        private void HandlePlayerInfo(SignalHandle handle, PlayerInfo info)
        {
            _signalHandle = handle;
            _score = info.Score;
        }

    }

}