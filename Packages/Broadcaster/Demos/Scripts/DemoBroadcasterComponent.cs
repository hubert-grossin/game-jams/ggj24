using UnityEngine;
using TMPro;

namespace SideXP.Broadcaster.Demos
{

    /// <summary>
    /// Displays a runtime GUI to test how the <see cref="Broadcaster"/> system reacts.<br/>
    /// Also illustrates how to emit the signals.
    /// </summary>
    [AddComponentMenu(Constants.AddComponentMenuDemos + "/Demo Broadcaster")]
    public class DemoBroadcasterComponent : MonoBehaviour
    {

        [Header("References")]

        public TMP_InputField PlayerName = null;
        public TMP_InputField PlayerScore = null;

        [Header("Emitted Signals")]

        public VoidSignalAsset OnAnimate0 = null;
        public VoidSignalAsset OnAnimate1 = null;
        public VoidSignalAsset OnAnimate2 = null;
        public PlayerInfoSignalAsset OnSendPlayerInfo = null;

        public void AnimateAllAtOnce()
        {
            Broadcaster.Emit(this, OnAnimate0.Channel, OnAnimate0, OnAnimate1, OnAnimate2);
        }

        public void AnimateAllInSequence()
        {
            OnAnimate0.Emit(this);
            OnAnimate1.Emit(this);
            OnAnimate2.Emit(this);
        }

        public void SendPlayerInfo()
        {
            OnSendPlayerInfo.Emit(this, new PlayerInfo
            {
                Name = PlayerName.text,
                Score = System.Convert.ToInt32(PlayerScore.text)
            });
        }

    }

}