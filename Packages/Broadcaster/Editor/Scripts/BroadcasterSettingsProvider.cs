/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEditor;

using SideXP.Core.EditorOnly;

namespace SideXP.Broadcaster.EditorOnly
{

    /// <summary>
    /// Generate menus to edit broadcaster package settings.
    /// </summary>
    internal class BroadcasterSettingsProvider : DefaultEditorSettingsProvider
    {

        [SettingsProvider]
        private static SettingsProvider RegisterPreferencesMenu()
        {
            return MakeSettingsProvider(BroadcasterSettings.I, EditorConstants.Preferences + "/Broadcaster", SettingsScope.User);
        }

    }

}