/**
* Sideways Experiments (c) 2023
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System;
using System.IO;
using System.CodeDom;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;

using SideXP.Core;
using SideXP.Core.EditorOnly;
using SideXP.Core.Reflection;

namespace SideXP.Broadcaster.EditorOnly
{

    /// <summary>
    /// Miscellaneous functions for working with <see cref="SignalAsset"/>.
    /// </summary>
    public class SignalAssetUtility
    {

        public const string AssetSuffix = "Asset";
        public const string SignalAssetSuffix = "Signal" + AssetSuffix;
        public const string BroadcasterContainerProp = "_brodcasterContainer";

        /// <summary>
        /// The available signal assets in the project.
        /// </summary>
        private static SignalAssetTypeInfo[] s_signalAssetTypes = null;

        /// <inheritdoc cref="s_signalAssetTypes"/>
        public static SignalAssetTypeInfo[] SignalAssetTypes
        {
            get
            {
                if (s_signalAssetTypes == null)
                {
                    List<SignalAssetTypeInfo> signalAssetTypesList = new List<SignalAssetTypeInfo>();
                    // For each signal asset type
                    foreach (Type signalAssetType in ReflectionUtility.GetAllTypesInProjectAssignableFrom<SignalAsset>())
                    {
                        // Cancel if the type is not usable
                        if (signalAssetType.IsAbstract)
                            continue;

                        signalAssetTypesList.Add(new SignalAssetTypeInfo(signalAssetType));
                    }

                    // Sort signal asset types by order ascending
                    signalAssetTypesList.Sort((a, b) =>
                    {
                        // Move the item up if it contains a submmenu
                        if (a.HasSubmenu)
                        {
                            if (!b.HasSubmenu)
                                return -1;
                        }
                        // Else, if the item doesn't contain a submenu but the other does, move the item down
                        else if (b.HasSubmenu)
                        {
                            return 1;
                        }

                        return a.Order != b.Order
                            ? a.Order.CompareTo(b.Order)
                            : a.Menu.CompareTo(b.Menu);
                    });
                    s_signalAssetTypes = signalAssetTypesList.ToArray();
                }
                return s_signalAssetTypes;
            }
        }

        /// <summary>
        /// Generates a <see cref="SignalAsset"/> script for a given data type.
        /// </summary>
        /// <param name="signalDataScriptPath">The path to the data type's script.</param>
        /// <param name="previousPath">The previous path of the signal data script if it has been moved.</param>
        /// <returns>Returns true if the <see cref="SignalAsset"/> script has been generated sucessfully.</returns>
        public static bool GenerateSignalAssetScript(string signalDataScriptPath, string previousPath)
        {
            // Cancel if the given data type is not valid
            if (!ScriptUtility.GetScriptType(signalDataScriptPath, out Type signalDataType))
                return false;

            // Cancel if the given type is not marked with SignalData attribute
            if (!signalDataType.GetCustomAttribute(out SignalDataAttribute signalData))
                return false;

            SignalsCache.SignalInfo cacheInfo = null;
            // Try to get cached data using previous path if the script did move
            if (!string.IsNullOrEmpty(previousPath))
            {
                if (SignalsCache.Get(previousPath, out cacheInfo, false))
                    cacheInfo.SignalDataScriptPath = signalDataScriptPath;
            }
            // Get or create cache data
            if (cacheInfo == null)
                cacheInfo = SignalsCache.Get(signalDataScriptPath, true);

            CodeCompileUnit domCompileUnit = ScriptGeneratorUtility.MakeScriptCompileUnit(signalDataType, out CodeNamespace importsNamespace, out CodeNamespace domNamespace);
            importsNamespace.Imports.Add(new CodeNamespaceImport(typeof(SignalAsset).Namespace));

            // The generated Signal Asset class name should use the file name from defined path if applicable, or the signal data type
            // followed by the default suffix
            string extension = Path.GetExtension(signalDataScriptPath);
            bool isFilePath = !string.IsNullOrEmpty(extension);
            string signalAssetClassName = isFilePath && !string.IsNullOrEmpty(signalData.Path) && signalData.Path.EndsWith(extension)
                ? Path.GetFileNameWithoutExtension(signalData.Path)
                : signalDataType.Name + SignalAssetSuffix;

            // Declare Signal Asset class
            CodeTypeDeclaration domClass = new CodeTypeDeclaration(signalAssetClassName);
            domClass.Comments.Add(new CodeCommentStatement("<summary>", true));
            domClass.Comments.Add(new CodeCommentStatement($"Auto-generated class that represents a signal with <see cref=\"{signalDataType.Name}\"/> parameter.", true));
            domClass.Comments.Add(new CodeCommentStatement("</summary>", true));
            domClass.BaseTypes.Add(new CodeTypeReference { BaseType = $"{typeof(SignalAssetGeneric<>).Name}[{signalDataType.Name}]" });
            domNamespace.Types.Add(domClass);

            // Add [SignalMetadataAttribute] if applicable
            if (signalDataType.GetCustomAttribute(out SignalMetadataAttribute signalMetadata))
            {
                CodeAttributeDeclaration domSignalMetadataAttr = new CodeAttributeDeclaration(new CodeTypeReference { BaseType = nameof(SignalMetadataAttribute).Slice(0, -nameof(Attribute).Length) });

                if (!string.IsNullOrEmpty(signalMetadata.Menu))
                    domSignalMetadataAttr.Arguments.Add(new CodeAttributeArgument(nameof(SignalMetadataAttribute.Menu), new CodePrimitiveExpression(signalMetadata.Menu)));

                if (!string.IsNullOrEmpty(signalMetadata.Name))
                    domSignalMetadataAttr.Arguments.Add(new CodeAttributeArgument(nameof(SignalMetadataAttribute.Name), new CodePrimitiveExpression(signalMetadata.Name)));

                if (signalMetadata.Order != 0)
                    domSignalMetadataAttr.Arguments.Add(new CodeAttributeArgument(nameof(SignalMetadataAttribute.Order), new CodePrimitiveExpression(signalMetadata.Order)));

                domClass.CustomAttributes.Add(domSignalMetadataAttr);
            }

            // Generate the script content
            string signalAssetScriptContent = ScriptGeneratorUtility.GenerateScript(domCompileUnit);

            string signalAssetScriptPath = null;
            // If a signal asset script has been generated before for the current signal and that file is still valid
            if (!string.IsNullOrEmpty(cacheInfo.SignalAssetScriptPath) && File.Exists(cacheInfo.SignalAssetScriptPath.ToAbsolutePath()))
            {
                signalAssetScriptPath = cacheInfo.SignalAssetScriptPath.ToAbsolutePath();
                // Cancel if the existing file has the same content as the generated one (meaning the file doesn't need an update)
                if (signalAssetScriptContent == File.ReadAllText(signalAssetScriptPath))
                    return true;

                // Replace the existing files, avoiding doubles if files have been moved
                signalAssetScriptPath = cacheInfo.SignalAssetScriptPath;
            }

            // Compute signal asset script path
            if (string.IsNullOrEmpty(signalAssetScriptPath))
            {
                if (signalData.RelativeFromAssets)
                {
                    signalAssetScriptPath = Path.Combine(Application.dataPath, signalData.Path).ToAbsolutePath();
                }
                else
                {
                    string path = !string.IsNullOrEmpty(signalData.Path) ? signalData.Path : string.Empty;
                    if (Path.HasExtension(path))
                        path = Path.GetDirectoryName(path);
                    signalAssetScriptPath = Path.Combine(Path.GetDirectoryName(signalDataScriptPath), path);
                }
                signalAssetScriptPath = Path.GetFullPath(Path.Combine(signalAssetScriptPath, domClass.Name + ScriptUtility.CsExtension)).ToRelativePath();
            }

            try
            {
                // Ensure the target directory exists
                Directory.CreateDirectory(Path.GetDirectoryName(signalAssetScriptPath));
                // Write the config asset script file
                File.WriteAllText(signalAssetScriptPath, signalAssetScriptContent);
                cacheInfo.SignalAssetScriptPath = signalAssetScriptPath;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Debug.LogError($"Unable to create the signal asset script for the signal data {signalDataType.FullName}. See previous error for more information.");
                return false;
            }

            return true;
        }

    }

}