/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using SideXP.Core;
using SideXP.Core.EditorOnly;

namespace SideXP.Broadcaster.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="BroadcasterAsset"/>.
    /// </summary>
    [CustomEditor(typeof(BroadcasterAsset))]
    public class BroadcasterAssetEditor : Editor
    {

        #region Fields

        private List<SignalAsset> _signalSubassetsList = new List<SignalAsset>();
        private ReorderableList _signalsReorderableList = null;

        private Editor _selectedSignalAssetEditor = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this editor is loaded.
        /// </summary>
        private void OnEnable()
        {
            if (!target.IsAsset())
                return;

            _signalSubassetsList.Clear();
            _signalSubassetsList.AddRange(target.FindAllSubassetsOfType<SignalAsset>(false));
            Select(0);
        }

        /// <summary>
        /// Called when this editor is unloaded.
        /// </summary>
        private void OnDisable()
        {
            if (_selectedSignalAssetEditor != null)
            {
                DestroyImmediate(_selectedSignalAssetEditor);
                _selectedSignalAssetEditor = null;
            }
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws the GUI of this custom editor.
        /// </summary>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space();
            ReorderableSignalAssetsList.DoLayoutList();

            EditorGUILayout.Space();
            MoreEditorGUI.HorizontalSeparator(true);
            EditorGUILayout.LabelField("Selected Signal Asset", MoreEditorGUI.TitleStyle, MoreGUI.HeightOptS);

            if (_selectedSignalAssetEditor != null)
            {
                string label = $"<i><b>{_selectedSignalAssetEditor.target.GetType().Name}</b>";
                if (_selectedSignalAssetEditor.target is SignalAsset signalAsset && signalAsset.DataType != null)
                    label += $" ({signalAsset.DataType.Name})";
                label += "</i>";

                EditorGUILayout.LabelField(label, EditorStyles.label.RichText(true).FontSizeDiff(-2));
                EditorGUILayout.Space();
                _selectedSignalAssetEditor.OnInspectorGUI();
            }
            else
            {
                EditorGUILayout.LabelField("No Signal Asset selected", EditorStyles.label.Italic().FontSizeDiff(-2));
            }
        }

        #endregion


        #region Private API

        /// <inheritdoc cref="_signalsReorderableList"/>
        private ReorderableList ReorderableSignalAssetsList
        {
            get
            {
                if (_signalsReorderableList == null)
                {
                    _signalsReorderableList = new ReorderableList(_signalSubassetsList, typeof(SignalAsset));

                    _signalsReorderableList.drawHeaderCallback = rect => EditorGUI.LabelField(rect, "Signals");

                    _signalsReorderableList.drawElementCallback = (rect, index, isActive, isFocused) =>
                    {
                        Rect tmpRect = new Rect(rect);
                        tmpRect.height = EditorGUIUtility.singleLineHeight;
                        tmpRect.y += (rect.height - tmpRect.height) / 2;
                        tmpRect.width -= MoreGUI.HMargin + MoreGUI.WidthS;

                        // Draw name as text field (allowing rename)
                        EditorGUI.BeginChangeCheck();
                        string name = EditorGUI.DelayedTextField(tmpRect, _signalSubassetsList[index].name);
                        if (EditorGUI.EndChangeCheck())
                            _signalSubassetsList[index].Rename(name);

                        // Draw object field
                        tmpRect.x += tmpRect.width + MoreGUI.HMargin;
                        tmpRect.width = MoreGUI.WidthS;
                        using (new EnabledScope(false))
                            EditorGUI.ObjectField(tmpRect, _signalSubassetsList[index], typeof(SignalAsset), false);
                    };

                    _signalsReorderableList.onAddDropdownCallback = (rect, list) =>
                    {
                        if (SignalAssetUtility.SignalAssetTypes.Length <= 0)
                        {
                            Debug.LogWarning("No available Signal Asset type found in the project.");
                            return;
                        }

                        GenericMenu menu = new GenericMenu();
                        bool didAddSeparator = false;
                        foreach (SignalAssetTypeInfo typeInfo in SignalAssetUtility.SignalAssetTypes)
                        {
                            if (!typeInfo.HasSubmenu && !didAddSeparator)
                            {
                                menu.AddSeparator("");
                                didAddSeparator = true;
                            }

                            menu.AddItem(new GUIContent(typeInfo.Menu), false, () =>
                            {
                                SignalAsset newSignal = target.CreateSubasset(typeInfo.Type, typeInfo.AssetName) as SignalAsset;

                                // Assign Broadcaster asset
                                SerializedObject newSignalObj = new SerializedObject(newSignal);
                                newSignalObj.FindProperty(SignalAssetUtility.BroadcasterContainerProp).objectReferenceValue = target;
                                newSignalObj.ApplyModifiedPropertiesWithoutUndo();

                                _signalSubassetsList.Add(newSignal);
                                Select(_signalSubassetsList.Count - 1);
                            });
                        }
                        menu.ShowAsContext();
                    };

                    _signalsReorderableList.onRemoveCallback = list =>
                    {
                        if (!EditorUtility.DisplayDialog("Remove Signal Asset", "Are you sure you want to remove this signal?", "Yes", "No"))
                            return;

                        SignalAsset removedSignal = _signalSubassetsList[list.index];
                        _signalSubassetsList.RemoveAt(list.index);
                        Select(list.index - 1);
                        DestroyImmediate(removedSignal, true);
                        target.SaveAndReimport();
                    };

                    _signalsReorderableList.onSelectCallback = list => Select(list.index);
                }
                return _signalsReorderableList;
            }
        }

        /// <summary>
        /// Selects a <see cref="SignalAsset"/> in the list at the given index.
        /// </summary>
        /// <param name="index">The index of the item to select.</param>
        private void Select(int index, bool append = false)
        {
            ReorderableSignalAssetsList.Select(index, append);

            if (_selectedSignalAssetEditor != null)
            {
                DestroyImmediate(_selectedSignalAssetEditor);
                _selectedSignalAssetEditor = null;
            }

            if (_signalSubassetsList.IsInRange(index))
                _selectedSignalAssetEditor = CreateEditor(_signalSubassetsList[index]);
        }

        #endregion

    }

}