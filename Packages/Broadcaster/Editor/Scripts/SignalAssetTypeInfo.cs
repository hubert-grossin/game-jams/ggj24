/**
* Sideways Experiments (c) 2023
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System;

using SideXP.Core;
using UnityEditor;

namespace SideXP.Broadcaster.EditorOnly
{

    /// <summary>
    /// Groups informations about an available signal asset type in the project.
    /// </summary>
    public class SignalAssetTypeInfo
    {

        /// <inheritdoc cref="SignalMetadataAttribute.Menu"/>
        public string Menu { get; private set; } = null;

        /// <summary>
        /// The <see cref="SignalAsset"/> type itself.
        /// </summary>
        public Type Type { get; private set; } = null;

        /// <inheritdoc cref="SignalMetadataAttribute.AssetName"/>
        public string AssetName { get; private set; } = null;

        /// <inheritdoc cref="SignalMetadataAttribute.Order"/>
        public int Order { get; private set; } = 0;

        /// <summary>
        /// Checks if the menu for this signal asset has path split characters.
        /// </summary>
        public bool HasSubmenu => Menu.Contains('/');

        /// <inheritdoc cref="SignalAssetTypeInfo"/>
        /// <param name="signalAssetType">The <see cref="SignalAsset"/> type to process, assuming it's actually inheriting from
        /// <see cref="SignalAsset"/>.</param>
        internal SignalAssetTypeInfo(Type signalAssetType)
        {
            Type = signalAssetType;

            // Use metadata if available
            if (signalAssetType.GetCustomAttribute(out SignalMetadataAttribute metadata))
            {
                Menu = metadata.Menu;
                AssetName = metadata.Name;
                Order = metadata.Order;
            }

            // Fix names
            string nicifiedName = signalAssetType.Name;
            if (nicifiedName.EndsWith(SignalAssetUtility.AssetSuffix))
                nicifiedName = nicifiedName.Slice(0, -SignalAssetUtility.AssetSuffix.Length);

            if (string.IsNullOrEmpty(Menu))
                Menu = ObjectNames.NicifyVariableName(nicifiedName);

            if (string.IsNullOrEmpty(AssetName))
                AssetName = "New" + nicifiedName;
        }

    }

}