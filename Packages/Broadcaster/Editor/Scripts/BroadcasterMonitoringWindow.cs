/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using SideXP.Core;
using SideXP.Core.EditorOnly;
using SideXP.Broadcaster.Monitoring;
using SideXP.UI.EditorOnly;

namespace SideXP.Broadcaster.EditorOnly
{

    /// <summary>
    /// Shows the events that happened in the broadcasting system as timelines.
    /// </summary>
    public class BroadcasterMonitoringWindow : EditorWindow
    {
    
        #region Fields

        private const string WindowTitle = "Broadcaster Monitor";
        private const string MenuItem = EditorConstants.EditorWindowMenu + "/" + WindowTitle;

        private const int MinimumDisplayedFrames = 20;
        private const float ZoomRatio = .1f;

        private const float ToolbarHeight = MoreGUI.HeightS;
        private const float CursorFrameNavigationButtonWidth = 24f;

        private const float TimelineHeight = 36f;
        private const float TimelineIconsWidth = 16f;
        private const float AddBroadcastSectionHeight = 12f;
        private const float AddBroadcastSectionSeparatorHeight = 1f;
        private const float BroadcastEventSectionHeight = TimelineHeight - AddBroadcastSectionHeight - AddBroadcastSectionSeparatorHeight;
        private const float AddBroadcastRectWidth = 4f;
        private const float MinBroadcastEventRangeRectWidth = AddBroadcastRectWidth;
        private const float ChannelsSeparatorHeight = 1f;
        private const float TimelinesScrollbarHeight = 16f;
        private const float CursorWidth = 2f;

        private static readonly Color[] BroadcastColors = new Color[]
        {
            new Color(211/255f, 47/255f, 47/255f, .5f),
            new Color(255/255f, 179/255f, 0, .5f),
            new Color(124/255f, 179/255f, 66/255f, .5f),
            new Color(29/255f, 233/255f, 182/255f, .5f),
            new Color(3/255f, 155/255f, 229/255f, .5f),
            new Color(156/255f, 39/255f, 176/255f, .5f),
        };

        /// <summary>
        /// The frame from which the timeline are displayed.
        /// </summary>
        private int _framesRangeStart = 0;

        /// <summary>
        /// The number of frame to display in the timelines.
        /// </summary>
        private int _framesRangeLength = int.MaxValue;

        /// <summary>
        /// The inspected frame.
        /// </summary>
        private int _frameCursor = 0;

        private Vector2 _inspectorScrollPosition = Vector2.zero;

        private int _lastBroadcastColorIndex = -1;
        private List<(Broadcast broadcast, Color color)> _broadcastColors = new List<(Broadcast broadcast, Color color)>();

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this window is opened.
        /// </summary>
        private void OnEnable()
        {
            _framesRangeStart = 0;
            _framesRangeLength = int.MaxValue;
            _broadcastColors.Clear();

            if (Broadcaster.Logbook != null)
                Broadcaster.Logbook.OnAddEntry += HandleAddEntry;
        }

        /// <summary>
        /// Called when this window is closed.
        /// </summary>
        private void OnDisable()
        {
            if (Broadcaster.Logbook != null)
                Broadcaster.Logbook.OnAddEntry -= HandleAddEntry;
        }

        /// <summary>
        /// Called once per frame.
        /// </summary>
        private void Update()
        {
            if (Application.isPlaying)
                Repaint();
        }

        #endregion


        #region Public API

        /// <summary>
        /// Opens this editor window.
        /// </summary>
        [MenuItem(MenuItem)]
        public static BroadcasterMonitoringWindow Open()
        {
            BroadcasterMonitoringWindow window = GetWindow<BroadcasterMonitoringWindow>(false, WindowTitle, true);
            window.Show();
            return window;
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws this window GUI.
        /// </summary>
        private void OnGUI()
        {
            // Cancel if the logbook is not available
            if (Broadcaster.Logbook == null)
            {
                EditorGUILayout.HelpBox($"The monitoring is disabled.", MessageType.Warning);
                return;
            }

            DrawToolbarGUI();
            DrawTimelinesGUI();
            DrawInspectorGUI();
        }

        private void DrawToolbarGUI()
        {
            Rect rect = EditorGUILayout.GetControlRect(false, ToolbarHeight);
            GUI.Box(rect, "", EditorStyles.toolbar);

            Rect fieldRect = new Rect(rect);
            fieldRect.width = MoreGUI.WidthS + MoreGUI.WidthXS / 2;
            EditorGUI.LabelField(fieldRect, "Frames Range");

            fieldRect.x += fieldRect.width;
            fieldRect.width = MoreGUI.WidthXS;
            _framesRangeStart = EditorGUI.IntField(fieldRect, _framesRangeStart);

            fieldRect.x += fieldRect.width;
            fieldRect.width = MoreGUI.WidthXS / 2;
            EditorGUI.LabelField(fieldRect, "to", EditorStyles.label.AlignCenter());

            fieldRect.x += fieldRect.width;
            fieldRect.width = MoreGUI.WidthXS;
            _framesRangeLength = EditorGUI.IntField(fieldRect, _framesRangeStart + FramesRangeLength) - _framesRangeStart;
            if (_framesRangeLength >= Time.frameCount - _framesRangeStart)
                _framesRangeLength = int.MaxValue;

            Rect cursorToolbarRect = new Rect(rect);
            cursorToolbarRect.width = CursorFrameNavigationButtonWidth * 4 + MoreGUI.WidthS + MoreGUI.HMargin * 2;
            cursorToolbarRect.x += (rect.width - cursorToolbarRect.width) / 2;

            fieldRect = cursorToolbarRect;
            fieldRect.width = CursorFrameNavigationButtonWidth;
            if (GUI.Button(fieldRect, EditorIcons.IconContent("skip_previous", true), MoreEditorGUI.IconButtonStyle))
                FrameCursor -= Application.targetFrameRate > 0 ? Application.targetFrameRate : 60;

            fieldRect.x += fieldRect.width + MoreGUI.HMargin;
            fieldRect.width = CursorFrameNavigationButtonWidth;
            if (GUI.Button(fieldRect, EditorIcons.IconContent("navigate_before", true), MoreEditorGUI.IconButtonStyle))
                FrameCursor--;

            fieldRect.x += fieldRect.width + MoreGUI.HMargin;
            fieldRect.width = MoreGUI.WidthS;
            FrameCursor = EditorGUI.IntField(fieldRect, FrameCursor);

            fieldRect.x += fieldRect.width + MoreGUI.HMargin;
            fieldRect.width = CursorFrameNavigationButtonWidth;
            if (GUI.Button(fieldRect, EditorIcons.IconContent("navigate_next", true), MoreEditorGUI.IconButtonStyle))
                FrameCursor++;

            fieldRect.x += fieldRect.width + MoreGUI.HMargin;
            if (GUI.Button(fieldRect, EditorIcons.IconContent("skip_next", true), MoreEditorGUI.IconButtonStyle))
                FrameCursor += Application.targetFrameRate > 0 ? Application.targetFrameRate : 60;

            Debug.Log(Application.targetFrameRate);
        }

        private void DrawTimelinesGUI()
        {
            Rect rect = EditorGUILayout.GetControlRect(false, Broadcaster.Channels.Length * (MoreGUI.HeightL + ChannelsSeparatorHeight) + TimelinesScrollbarHeight + MoreGUI.VMargin);
            Rect channelRect = new Rect(rect);
            channelRect.height = MoreGUI.HeightL;
            List<BroadcasterLogbookEntry> entries = new List<BroadcasterLogbookEntry>(Broadcaster.Logbook.GetEntriesWithinRange(_framesRangeStart, _framesRangeStart + _framesRangeLength));
            float totalTimelineFramesCount = (FramesRangeStart + FramesRangeLength) - FramesRangeStart;

            Rect timelinesViewRect = new Rect(rect);
            timelinesViewRect.x += MoreGUI.WidthL + TimelineIconsWidth;
            timelinesViewRect.width -= MoreGUI.WidthL - TimelineIconsWidth;
            timelinesViewRect.height -= TimelinesScrollbarHeight + MoreGUI.VMargin + Broadcaster.Channels.Length * ChannelsSeparatorHeight;
            
            // If the user clicks in the timeline view, place the cursor accordingly
            if (Event.current.type == EventType.MouseUp && timelinesViewRect.Contains(Event.current.mousePosition))
            {
                float diff = Event.current.mousePosition.x - timelinesViewRect.x;
                FrameCursor = _framesRangeStart + Mathf.FloorToInt((diff / timelinesViewRect.width) * FramesRangeLength);
                Debug.Break();
                Event.current.Use();
            }

            // Draw timelines
            foreach (Channel channel in Broadcaster.Channels)
            {
                // Draw timeline label
                Rect timelineRect = new Rect(channelRect);
                timelineRect.height -= ChannelsSeparatorHeight;
                timelineRect.width = MoreGUI.WidthL;
                EditorGUI.LabelField(timelineRect, channel.Name, EditorStyles.label.TextAlignment(TextAnchor.MiddleLeft));

                // Draw timeline icons
                Rect timelineIconRect = new Rect(timelineRect);
                timelineIconRect.height = AddBroadcastSectionHeight;
                timelineIconRect.width = TimelineIconsWidth;
                timelineIconRect.x += timelineRect.width;
                EditorGUI.LabelField(timelineIconRect, EditorIcons.IconContent("add", true), EditorStyles.label.AlignCenter().Padding(new RectOffset()));

                timelineIconRect.y += timelineIconRect.height;
                timelineIconRect.height = timelineRect.height - AddBroadcastSectionHeight;
                EditorGUI.LabelField(timelineIconRect, EditorIcons.IconContent("schedule", true), EditorStyles.label.AlignCenter().Padding(new RectOffset()));

                // Draw timeline background
                timelineRect.x = timelinesViewRect.x;
                timelineRect.width = channelRect.width - MoreGUI.WidthL - TimelineIconsWidth;
                EditorGUI.DrawRect(timelineRect, new Color(1, 1, 1, .2f));
                
                // Filter entries related to the current channel
                List<BroadcasterLogbookEntry> channelEntries = entries.FindAll(i => i.Channel == channel);
                // Remove current channel entries from the common list
                for (int i = 0; i < channelEntries.Count; i++)
                    entries.Remove(channelEntries[i]);

                // Init
                List<(int startFrame, int endFrame, Broadcast broadcast)> broadcastRanges = new List<(int, int, Broadcast)>();

                // Process entries
                while (channelEntries.Count > 0)
                {
                    // Create block if the entry is an "add broadcast" one
                    if (channelEntries[0] is AddBroadcastEntry addBroadcastEntry)
                    {
                        Rect eventRect = new Rect(timelineRect);
                        eventRect.height = Mathf.Min(eventRect.height, AddBroadcastSectionHeight);
                        eventRect.width = Mathf.Max(1 / totalTimelineFramesCount * timelinesViewRect.width, AddBroadcastRectWidth);
                        eventRect.x += (addBroadcastEntry.Frame - FramesRangeStart) / totalTimelineFramesCount * timelinesViewRect.width;
                        eventRect.width = Mathf.Max(eventRect.width, timelineRect.x - eventRect.x);

                        EditorGUI.DrawRect(eventRect, GetBroadcastColor(addBroadcastEntry.Broadcast));
                    }
                    // Begin range if the entry is an "emit broadcast" one
                    else if (channelEntries[0] is EmitBroadcastEntry emitBroadcastEntry)
                    {
                        int start = channelEntries[0].Frame;
                        int end = -1;
                        Broadcast broadcast = emitBroadcastEntry.Broadcast;

                        // End the range if applicable
                        if (channelEntries.Count > 0)
                        {
                            EndBroadcastEntry closingBroadcastEntry = channelEntries.Find(i => i is EndBroadcastEntry end && end.Broadcast == broadcast) as EndBroadcastEntry;
                            if (closingBroadcastEntry != null)
                            {
                                end = closingBroadcastEntry.Frame;
                                channelEntries.Remove(closingBroadcastEntry);
                            }
                        }

                        broadcastRanges.Add((start, end, broadcast));
                    }
                    // Begin range from minimum frame to the one in the current entry if it's an "end broadcast" one (meaning there's no
                    // corresponding "add broadcast" entry in the current frames range).
                    else if (channelEntries[0] is EndBroadcastEntry endBroadcastEntry)
                    {
                        broadcastRanges.Add((-1, channelEntries[0].Frame, endBroadcastEntry.Broadcast));
                    }

                    channelEntries.RemoveAt(0);
                }

                // For each found emit->dequeue event ranges
                foreach ((int startFrame, int endFrame, Broadcast broadcast) range in broadcastRanges)
                {
                    int startFrame = range.startFrame < 0 ? FramesRangeStart : range.startFrame;
                    int endFrame = range.endFrame < 0 ? FramesRangeStart + FramesRangeLength : range.endFrame;

                    if (startFrame == endFrame)
                        endFrame = startFrame + 1;

                    Rect eventRect = new Rect(timelineRect);
                    eventRect.width = Mathf.Max((endFrame - startFrame) / totalTimelineFramesCount * timelinesViewRect.width, MinBroadcastEventRangeRectWidth);
                    eventRect.x += (startFrame - FramesRangeStart) / totalTimelineFramesCount * timelinesViewRect.width;
                    eventRect.height = Mathf.Min(BroadcastEventSectionHeight, eventRect.height);
                    eventRect.y += timelineRect.height - eventRect.height;

                    //Debug.Log($"Drawing rect: color = {GetBroadcastColor(range.broadcast)}, rect = {eventRect}, start frame = {startFrame}, end frame = {endFrame}, total timeline frames count = {totalTimelineFramesCount}");
                    EditorGUI.DrawRect(eventRect, GetBroadcastColor(range.broadcast));
                }

                // Draw section separator
                EditorGUI.DrawRect(new Rect(timelineRect.x, timelineRect.y + AddBroadcastSectionHeight, timelinesViewRect.width, AddBroadcastSectionSeparatorHeight), new Color(0, 0, 0, .3f));

                // Draw bottom separator
                EditorGUI.DrawRect(new Rect(channelRect.x, channelRect.y + channelRect.height - ChannelsSeparatorHeight, channelRect.width, ChannelsSeparatorHeight), MoreEditorGUI.SeparatorColor);
                channelRect.y += channelRect.height;
            }

            // Draw the cursor, if in the current frames range
            if (FrameCursor >= FramesRangeStart && FrameCursor <= FramesRangeStart + FramesRangeLength)
            {
                Rect cursorRect = new Rect(timelinesViewRect);
                //cursorRect.height -= TimelinesScrollbarHeight + Broadcaster.Channels.Length * ChannelsSeparatorHeight + MoreGUI.VMargin;
                cursorRect.width = CursorWidth;
                cursorRect.x += (FrameCursor - FramesRangeStart) / totalTimelineFramesCount * timelinesViewRect.width;
                EditorGUI.DrawRect(cursorRect, Color.white);
            }

            // Draw scrollbar
            Rect scrollbarRect = new Rect(rect);
            scrollbarRect.width -= MoreGUI.WidthL;
            scrollbarRect.x += MoreGUI.WidthL;
            scrollbarRect.height = TimelinesScrollbarHeight;
            scrollbarRect.y = rect.y + rect.height - TimelinesScrollbarHeight;

            // Draw scroll bar
            float frameCount = Mathf.Max(Time.frameCount, MinimumDisplayedFrames);
            float framesRangeStartRatio = FramesRangeStart / frameCount;
            framesRangeStartRatio = GUI.HorizontalScrollbar(scrollbarRect, framesRangeStartRatio, FramesRangeLength / frameCount, 0f, 1f);
            _framesRangeStart = Mathf.FloorToInt(framesRangeStartRatio * frameCount);

            if (Event.current.type == EventType.ScrollWheel && rect.Contains(Event.current.mousePosition))
            {
                // If user is dezooming (scroll wheel down)
                if (Event.current.delta.y > 0)
                {
                    _framesRangeLength = _framesRangeLength + Mathf.CeilToInt(_framesRangeLength * ZoomRatio);
                    // If the value goes negative, it means the operation did go farther than the maximum integer value
                    if (_framesRangeLength < 0)
                        _framesRangeLength = int.MaxValue;

                    // If the length is higher than the actual displayed range
                    if (_framesRangeLength > Time.frameCount - _framesRangeStart)
                    {
                        _framesRangeLength = int.MaxValue;
                        // If the start frame is higher than 0, decrease it
                        if (_framesRangeStart > 0)
                        {
                            _framesRangeStart = _framesRangeStart - Mathf.CeilToInt(_framesRangeStart * ZoomRatio);
                            if (_framesRangeStart < 0)
                                _framesRangeStart = 0;
                        }
                    }
                }
                // If user is zooming (scroll wheel up)
                else if (Event.current.delta.y < 0)
                {
                    if (_framesRangeLength == int.MaxValue)
                        _framesRangeLength = Time.frameCount - _framesRangeStart;

                    _framesRangeLength -= Mathf.CeilToInt(_framesRangeLength * ZoomRatio);
                    if (_framesRangeLength <= _framesRangeStart)
                        _framesRangeLength = _framesRangeStart + 1;
                }

                Event.current.Use();
                Repaint();
            }
        }

        private void DrawInspectorGUI()
        {
            List<BroadcasterLogbookEntry> entries = new List<BroadcasterLogbookEntry>(Broadcaster.Logbook.GetEntriesWithinRange(FrameCursor, FrameCursor + 1));
            
            List<Channel> channels = new List<Channel>();
            foreach (BroadcasterLogbookEntry entry in entries)
            {
                if (!channels.Contains(entry.Channel))
                    channels.Add(entry.Channel);
            }
            channels.Sort((a, b) => a.Name.CompareTo(b.Name));

            _inspectorScrollPosition = EditorGUILayout.BeginScrollView(_inspectorScrollPosition);
            {
                foreach (Channel channel in channels)
                {
                    using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        EditorGUILayout.LabelField("Channel: " + channel.Name, EditorStyles.largeLabel.Bold(), MoreGUI.HeightOptS);
                        using (new EditorGUILayout.VerticalScope())
                        {

                        }
                    }
                }
            }
            EditorGUILayout.EndScrollView();
        }

        #endregion


        #region Private API

        /// <inheritdoc cref="_framesRangeStart"/>
        private int FramesRangeStart => Mathf.Max(0, _framesRangeStart);

        /// <inheritdoc cref="_framesRangeLength"/>
        private int FramesRangeLength => Mathf.Min(Time.frameCount - _framesRangeStart, _framesRangeLength);

        /// <inheritdoc cref="_frameCursor"/>
        private int FrameCursor
        {
            get => _frameCursor;
            set
            {
                _frameCursor = Mathf.Clamp(value, 0, Time.frameCount);
                _inspectorScrollPosition = Vector2.zero;
            }
        }

        /// <summary>
        /// Gets the color that represents a given broadcast in the timelines.
        /// </summary>
        /// <param name="broadcast">The broadcast of which to get the bound color.</param>
        /// <returns>Returns the broadcast color.</returns>
        private Color GetBroadcastColor(Broadcast broadcast)
        {
            int broadcastColorIndex = _broadcastColors.FindIndex(i => i.broadcast == broadcast);
            if (broadcastColorIndex < 0)
            {
                _lastBroadcastColorIndex++;
                if (_lastBroadcastColorIndex >= BroadcastColors.Length)
                    _lastBroadcastColorIndex = 0;

                broadcastColorIndex = _broadcastColors.Count;
                _broadcastColors.Add((broadcast, BroadcastColors[_lastBroadcastColorIndex]));
            }
            return _broadcastColors[broadcastColorIndex].color;
        }

        /// <inheritdoc cref="BroadcasterLogbook.OnAddEntry"/>
        private void HandleAddEntry(BroadcasterLogbookEntry entry)
        {
            Repaint();
        }

        #endregion

    }

}