/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using SideXP.Core.EditorOnly;

namespace SideXP.Broadcaster.EditorOnly
{

    /// <summary>
    /// Miscellaneous functions for working with <see cref="BroadcasterAsset"/>.
    /// </summary>
    public static class BroadcasterAssetUtility
    {

        /// <summary>
        /// Reloads all the <see cref="BroadcasterAsset"/> in the project.
        /// </summary>
        public static void ReloadAll()
        {
            foreach (BroadcasterAsset broadcasterAsset in ObjectUtility.FindAllAssetsOfType<BroadcasterAsset>(true))
                Reload(broadcasterAsset);
        }

        /// <summary>
        /// Reloads a given <see cref="BroadcasterAsset"/>.
        /// </summary>
        /// <param name="broadcasterAsset">The asset to reload.</param>
        public static void Reload(BroadcasterAsset broadcasterAsset)
        {
            broadcasterAsset.SaveAndReimport();
        }

    }

}