/**
* Sideways Experiments (c) 2024
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System.Collections.Generic;

using UnityEngine;

using SideXP.Core;
using static SideXP.Broadcaster.EditorOnly.SignalsCache;
using System.Collections;

namespace SideXP.Broadcaster.EditorOnly
{

    /// <summary>
    /// Stores informations related to module scripts and generated elements related to them.
    /// </summary>
    [Save(nameof(Broadcaster) + "_" + nameof(SignalsCache) + ".json", EDataScope.Project)]
    internal class SignalsCache : SOSingleton<SignalsCache>, IEnumerable<SignalInfo>
    {

        #region Subclasses

        /// <summary>
        /// The informations related to the signal.
        /// </summary>
        [System.Serializable]
        public class SignalInfo
        {

            /// <summary>
            /// The path to the signal data script, relative from the project directory.
            /// </summary>
            [SerializeField]
            private string _signalDataScriptPath = null;

            /// <summary>
            /// The path to the auto-generated <see cref="SignalAsset"/> script for the related signal data, relative from the project's
            /// root.
            /// </summary>
            [SerializeField]
            private string _signalAssetScriptPath = null;

            /// <inheritdoc cref="SignalInfo"/>
            /// <param name="signalDataScriptPath">The path to the signal data script, relative from the project directory.</param>
            public SignalInfo(string scriptPath)
            {
                _signalDataScriptPath = scriptPath.ToRelativePath(true);
            }

            /// <inheritdoc cref="_signalDataScriptPath"/>
            public string SignalDataScriptPath
            {
                get => _signalDataScriptPath;
                set => _signalDataScriptPath = value != null ? value.ToRelativePath(true) : string.Empty;
            }

            /// <inheritdoc cref="_signalAssetScriptPath"/>
            public string SignalAssetScriptPath
            {
                get => _signalAssetScriptPath;
                set => _signalAssetScriptPath = value != null ? value.ToRelativePath(true) : string.Empty;
            }

        }

        #endregion


        #region Fields

        /// <summary>
        /// The list of cached data related to signals.
        /// </summary>
        [SerializeField]
        private List<SignalInfo> _signalInfos = new List<SignalInfo>();

        #endregion


        #region Public API

        /// <summary>
        /// Gets the cache entry for a given script that declares a type marked with <see cref="SignalDataAttribute"/>.
        /// </summary>
        /// <param name="signalDataScriptPath">The path to the signal data script, relative from the project directory.</param>
        /// <param name="createIfNotExist">If enabled, this function will create a cache entry for the given path if it doesn't exist
        /// yet.</param>
        /// <returns>Returns the found or created cache entry.</returns>
        public static SignalInfo Get(string signalDataScriptPath, bool createIfNotExist = false)
        {
            signalDataScriptPath = signalDataScriptPath.ToRelativePath(true);
            foreach(SignalInfo signalInfo in I._signalInfos)
            {
                if (signalInfo.SignalDataScriptPath == signalDataScriptPath)
                    return signalInfo;
            }

            if (createIfNotExist)
            {
                SignalInfo signalInfo = new SignalInfo(signalDataScriptPath);
                I._signalInfos.Add(signalInfo);
                return signalInfo;
            }

            return null;
        }

        /// <param name="info">Outputs the found or created cache entry.<param>
        /// <returns>Returns true if a cache entry exists for the given path.</returns>
        /// <inheritdoc cref="Get(string, bool)"/>
        public static bool Get(string signalDataScriptPath, out SignalInfo info, bool createIfNotExist = false)
        {
            info = Get(signalDataScriptPath, createIfNotExist);
            return info != null;
        }

        /// <summary>
        /// Gets the cache entry of a signal that uses a given <see cref="SignalAsset"/> type.
        /// </summary>
        /// <returns>Returns the found cache entry.</returns>
        /// <inheritdoc cref="Get(string, bool)"/>
        public static SignalInfo GetByAssetScript(string signalAssetScriptPath)
        {
            signalAssetScriptPath = signalAssetScriptPath.ToRelativePath(true);
            foreach (SignalInfo signalInfo in I._signalInfos)
            {
                if (signalInfo.SignalAssetScriptPath == signalAssetScriptPath)
                    return signalInfo;
            }

            return null;
        }

        /// <param name="info">Outputs the found cache entry.</param>
        /// <inheritdoc cref="GetByAssetScript(string)"/>
        /// <inheritdoc cref="Get(string, out SignalInfo, bool)"/>
        public static bool GetByAssetScript(string signalAssetScriptPatth, out SignalInfo info)
        {
            info = GetByAssetScript(signalAssetScriptPatth);
            return info != null;
        }

        /// <summary>
        /// Deletes a cache entry for a given module.
        /// </summary>
        /// <param name="signalDataScriptPath">The path to the deleted signal data script.</param>
        /// <returns>Returns true if a cache entry has been removed.</returns>
        public static bool Delete(string signalDataScriptPath)
        {
            signalDataScriptPath = signalDataScriptPath.ToRelativePath(true);
            return I._signalInfos.RemoveAll(i => i.SignalDataScriptPath == signalDataScriptPath) > 0;
        }

        /// <summary>
        /// Iterates through the stored signal cache items.
        /// </summary>
        public IEnumerator<SignalInfo> GetEnumerator()
        {
            return ((IEnumerable<SignalInfo>)_signalInfos).GetEnumerator();
        }

        /// <inheritdoc cref="GetEnumerator"/>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_signalInfos).GetEnumerator();
        }

        #endregion

    }

}