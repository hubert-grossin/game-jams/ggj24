/**
* Sideways Experiments (c) 2023
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System;
using System.IO;

using UnityEngine;
using UnityEditor;

using SideXP.Core;
using SideXP.Core.EditorOnly;

namespace SideXP.Broadcaster.EditorOnly
{

    /// <summary>
    /// Handles automatic asset operations in the Broadcaster package.
    /// </summary>
    [InitializeOnLoad]
    public class BroadcasterProcessor
    {

        /// <summary>
        /// Flag enabled if a <see cref="SignalAsset"/> script has been generated during an import process. Used to refresh the assets
        /// database after generating the scripts.
        /// </summary>
        private static bool s_didGenerateSignalAssetScript = false;

        /// <summary>
        /// Flag enabled if a <see cref="SignalAsset"/> script is deleted during a process. Used to reload
        /// <see cref="BroadcasterAsset"/> if needed.
        /// </summary>
        private static bool s_didDeleteSignalAssetScript = false;

        /// <summary>
        /// Static constructor.
        /// </summary>
        static BroadcasterProcessor()
        {
            /**
             *  When a type marked with [SignalData] is imported:
             *      - Generate or update the SignalAsset script
             *      - Push info in cache
             */
            AssetsProcessor.ListenImportAsset((path, isNew, didDomainReload, previousPath) =>
            {
                if (SignalAssetUtility.GenerateSignalAssetScript(path, previousPath))
                    s_didGenerateSignalAssetScript = true;
            });

            /**
             *  Refresh asset database if scripts have been generated during import process.
             */
            AssetsProcessor.ListenImportAllAssets(paths =>
            {
                if (s_didGenerateSignalAssetScript)
                    AssetDatabase.Refresh();

                s_didGenerateSignalAssetScript = false;
            });

            /**
             *  Update cache data if [SignalData] or SignalAsset script did move.
             */
            AssetsProcessor.ListenAfterMoveAsset((fromPath, toPath) =>
            {
                if (SignalsCache.Get(fromPath, out SignalsCache.SignalInfo info))
                    info.SignalDataScriptPath = toPath;
                else if (SignalsCache.GetByAssetScript(fromPath, out info))
                    info.SignalAssetScriptPath = toPath;
            });

            /**
             *  We listen for "before delete" events to handle properly the deletion of a selected [SignalData] or SignalAsset script.
             */
            AssetsProcessor.ListenBeforeDeleteAsset(path =>
            {
                HandleDeleteSignalScript(path);
                return AssetDeleteResult.DidNotDelete;
            });

            /**
             *  We listen for "after delete" events to handle the deletion of a  [SignalData] or SignalAsset script contained in a deleted
             *  directory.
             *  The script is not readable at this step since it has already been deleted. But we can still update the cache data.
             */
            AssetsProcessor.ListenAfterDeleteAsset(path => HandleDeleteSignalScript(path));

            /**
             *  Reload Broadcaster assets if a SignalAsset script has been deleted.
             */
            AssetsProcessor.ListenDeleteAllAssets(deletedAssets =>
            {
                if (s_didDeleteSignalAssetScript)
                    BroadcasterAssetUtility.ReloadAll();

                s_didDeleteSignalAssetScript = false;
            });
        }

        /// <summary>
        /// Process a [SignalData] or <see cref="SignalAsset"/> script deletion.
        /// </summary>
        /// <param name="path">The path to the deleted script.</param>
        /// <returns>Returns true if the given path was an expected script type.</returns>
        private static bool HandleDeleteSignalScript(string path)
        {
            // If the deleted asset is a [SignalData], delete related Signal Asset script
            if (SignalsCache.Get(path, out SignalsCache.SignalInfo info))
            {
                if (!string.IsNullOrEmpty(info.SignalAssetScriptPath))
                    AssetDatabase.DeleteAsset(info.SignalAssetScriptPath);
                
                SignalsCache.Delete(path);
                return true;
            }
            // Else, if the deleted asset is a Signal Asset script
            else if (SignalsCache.GetByAssetScript(path, out info))
            {
                info.SignalAssetScriptPath = null;
                s_didDeleteSignalAssetScript = true;

                if (ScriptUtility.GetScriptType(path, out Type signalAssetType))
                {
                    // Destroy all assets and subassets that used the deleted script
                    foreach (SignalAsset signalAsset in ObjectUtility.FindAllAssetsOfType(signalAssetType, true))
                        signalAsset.Destroy(true);
                }
                return true;
            }

            return false;
        }

    }

}