/**
* Sideways Experiments (c) 2023
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using UnityEngine;

using SideXP.Core;

namespace SideXP.Broadcaster.EditorOnly
{

    /// <summary>
    /// Editor settings for the Broadcaster package.
    /// </summary>
    [Save(EDataScope.User)]
    public class BroadcasterSettings : SOSingleton<BroadcasterSettings>
    {

        [Header("Monitor")]

        [Tooltip("If enabled, pauses the play mode when a broadcast is selected in the monitor window.")]
        public bool PauseOnSelection = true;

    }

}