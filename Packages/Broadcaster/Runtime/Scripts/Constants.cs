/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Miscellaneous constant values used in this package.
    /// </summary>
    public static class Constants
    {

        /// <inheritdoc cref="SideXP.Core.Constants.BaseHelpUrl"/>
        public const string BaseHelpUrl = SideXP.Core.Constants.BaseHelpUrl + "/broadcaster";

        /// <inheritdoc cref="SideXP.Core.Constants.AddComponentMenu"/>
        public const string AddComponentMenu = SideXP.Core.Constants.AddComponentMenu + "/Broadcaster";

        /// <inheritdoc cref="SideXP.Core.Constants.AddComponentMenuDemos"/>
        public const string AddComponentMenuDemos = SideXP.Core.Constants.AddComponentMenuDemos + "/Broadcaster";

        /// <inheritdoc cref="SideXP.Core.Constants.CreateAssetMenu"/>
        public const string CreateAssetMenu = SideXP.Core.Constants.CreateAssetMenu + "/Broadcaster";

    }

}