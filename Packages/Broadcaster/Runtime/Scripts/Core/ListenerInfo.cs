/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Groups the source of a listener and its callback.
    /// </summary>
    internal class ListenerInfo<T>
        where T : System.Delegate
    {

        /// <summary>
        /// The object that owns the callback.
        /// </summary>
        private object _listener = null;

        /// <summary>
        /// The callback to invoke when the listened signal is emitted.
        /// </summary>
        private T _callback = null;

        /// <inheritdoc cref="ListenerInfo"/>
        /// <param name="listener">The object that owns the callback.</param>
        /// <param name="callback">The callback to invoke when the listened signal is emitted.</param>
        public ListenerInfo(object listener, T callback)
        {
            _listener = listener;
            _callback = callback;
        }

        /// <inheritdoc cref="_listener"/>
        public object Listener => _listener;

        /// <inheritdoc cref="_callback"/>
        public T Callback => _callback;

    }

}