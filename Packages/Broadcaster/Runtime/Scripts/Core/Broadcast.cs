/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Broadcaster
{

    /// <summary>
    /// A collection of signals being processed in parallel.
    /// </summary>
    public class Broadcast
    {

        #region Fields

        public const int NameSignalsLimit = 2;

        /// <summary>
        /// The signal handles generated for the signals in this broadcast.
        /// </summary>
        private SignalHandle[] _handles = null;

        /// <summary>
        /// The channel from which this broadcast is emitted.
        /// </summary>
        private Channel _channel = null;

        /// <summary>
        /// The time elapsed since this broadcast has been emitted.
        /// </summary>
        private float _timer = 0f;

        /// <summary>
        /// The object that emitted this broadcast.
        /// </summary>
        private object _emitter = null;

        /// <summary>
        /// The data to send to listeners when this broadcast is emitted.
        /// </summary>
        private object _data = null;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="Broadcast"/>
        /// <param name="emitter">The object that emitted this broadcast.</param>
        /// <param name="data">The data to send to listeners when this broadcast is emitted.</param>
        /// <param name="channel">The channel from which this signal is emitted.</param>
        /// <param name="signals">The signals in this broadcast.</param>
        internal Broadcast(object emitter, object data, Channel channel, params Signal[] signals)
        {
            _emitter = emitter;
            _handles = new SignalHandle[signals.Length];
            _channel = channel;
            _data = data;
            for (int i = 0; i < signals.Length; i++)
            {
                _handles[i] = new SignalHandle(this, signals[i]);
            }
        }

        #endregion


        #region Public API

        /// <summary>
        /// Gets the name of this broadcast, based on the names of the signals inside it.
        /// </summary>
        public string Name
        {
            get
            {
                if (_handles.Length == 1)
                {
                    return _handles[0].Signal.Name;
                }
                else if (_handles.Length > 1)
                {
                    string name = string.Empty;

                    for (int i = 0; i < _handles.Length; i++)
                    {
                        if (i > 0)
                            name += ", ";
                        name += _handles[i].Signal.Name;
                    }

                    if (_handles.Length > NameSignalsLimit)
                        name += ", ...";

                    return name;
                }
                else
                {
                    return "Invalid Broadcast";
                }
            }
        }

        /// <inheritdoc cref="_emitter"/>
        public object Emitter => _emitter;

        /// <summary>
        /// The channel for which this broadcast has been emitted.
        /// </summary>
        public Channel Channel => _channel;

        #endregion


        #region Internal API

        /// <inheritdoc cref="_timer"/>
        internal float Timer => _timer;

        /// <inheritdoc cref="Channel.IsEnqueued(Broadcast)"/>
        internal bool IsEnqueued => _channel != null && _channel.IsEnqueued(this);

        /// <inheritdoc cref="Channel.IsActive(Broadcast)"/>
        internal bool IsActive => _channel != null && _channel.IsActive(this);

        /// <summary>
        /// Checks if this broadcast is ended, meaning it's neither enqueued or active in its channel.
        /// </summary>
        internal bool IsEnded => !IsEnqueued;

        /// <summary>
        /// Checks if the signals in this broadcast contains at least 1 deferred listener.
        /// </summary>
        internal bool IsDeferred
        {
            get
            {
                foreach (SignalHandle handle in _handles)
                {
                    if (handle.Signal.IsDeferred)
                        return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Checks if at least one listener of a signal in this broadcast uses handles. If not, the broadcast should be resolved instantly.
        /// </summary>
        internal bool UseHandles
        {
            get
            {
                foreach (SignalHandle handle in _handles)
                {
                    if (handle.Signal.UseHandle)
                        return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Makes the signals in this broadcast invoke the synced listeners.
        /// </summary>
        internal void InvokeSyncedListeners()
        {
            foreach (SignalHandle handle in _handles)
                handle.Signal.InvokeSyncedListeners(_data);
        }

        /// <summary>
        /// Makes the signals in this broadcast invoke the deferred listeners.
        /// </summary>
        internal void InvokeDeferredListeners()
        {
            foreach (SignalHandle handle in _handles)
                handle.Signal.InvokeDeferredListeners(handle, _data);
        }

        /// <summary>
        /// Updates the timer of this broadcast.
        /// </summary>
        /// <param name="delta">The time elapsed since the previous update.</param>
        /// <returns>Returns true if this broadcast has been updated successfully, or false if it ended up and can be discarded.</returns>
        internal bool Update(float delta)
        {
            bool didEnd = true;

            _timer += delta;
            foreach (SignalHandle handle in _handles)
            {
                if (handle.IsCancelled)
                    continue;

                // Skip if the signal did receive a ping
                if (handle.ConsumePing())
                {
                    didEnd = false;
                    continue;
                }

                if (_timer < handle.Delay)
                    didEnd = false;
            }

            return !didEnd;
        }

        /// <summary>
        /// Cancels this broadcast, so it will be dequeued from its channel.
        /// </summary>
        internal void Cancel()
        {
            foreach (SignalHandle handle in _handles)
                handle.CancelSignal();
        }

        #endregion

    }

}