/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;
using System.Collections.Generic;

using UnityEngine;

using SideXP.Broadcaster.Monitoring;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Main class for interacting with the broadcasting system.
    /// </summary>
    public static class Broadcaster
    {

        #region Fields

        /// <summary>
        /// The name of the default channel, used if no channel is specified when emitting a signal.
        /// </summary>
        public const string DefaultChannelName = "Default";

        /// <summary>
        /// The list of all existing channels.
        /// </summary>
        private static List<Channel> s_channels = new List<Channel>();

        /// <summary>
        /// The timestamp of the latest update.
        /// </summary>
        private static long s_previousUpdateTimestamp = 0;

        /// <summary>
        /// The logbook containing all the events that happened in the broadcasting system. Used only in development build or editor mode.
        /// </summary>
        private static BroadcasterLogbook s_logbook = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Static constructor.
        /// </summary>
        static Broadcaster()
        {
            s_logbook = Application.isEditor || Debug.isDebugBuild
                ? new BroadcasterLogbook()
                : null;
        }

        #endregion


        #region Public API

        /// <summary>
        /// Gets the main channel used to emit signals when no channel is specified.
        /// </summary>
        public static Channel MainChannel => GetOrCreateChannel(DefaultChannelName);

        /// <inheritdoc cref="_channels"/>
        public static Channel[] Channels => s_channels.ToArray();

        /// <inheritdoc cref="s_logbook"/>
        public static BroadcasterLogbook Logbook => s_logbook;

        /// <summary>
        /// Creates and register a new channel.
        /// </summary>
        /// <param name="name">The name of the channel. If not provided, generates a GUID instead.</param>
        /// <returns>Returns the created channel.</returns>
        public static Channel CreateChannel(string name = null)
        {
            return GetOrCreateChannel(name);
        }

        /// <summary>
        /// Gets an existing channel by its name, or create a new one with that given name.
        /// </summary>
        /// <param name="name">The name of the channel.</param>
        /// <returns>Returns the found or created channel.</returns>
        public static Channel GetOrCreateChannel(string name)
        {
            Channel channel = s_channels.Find(i => i.Name == name);
            if (channel == null)
            {
                channel = new Channel(name);
                s_channels.Add(channel);
                s_channels.Sort((a, b) => a.Name.CompareTo(b.Name));
                
                // Create logbook entry & subscribe to channel events
                if (s_logbook != null)
                {
                    s_logbook.AddEntry(new AddChannelEntry(channel));
                    channel._onAddBroadcast += (channel, broadcast) => s_logbook.AddEntry(new AddBroadcastEntry(broadcast));
                    channel._onEmitBroadcast += (channel, broadcast) => s_logbook.AddEntry(new EmitBroadcastEntry(broadcast));
                    channel._onEndBroadcast += (channel, broadcast) => s_logbook.AddEntry(new EndBroadcastEntry(broadcast));
                }
            }
            return channel;
        }

        /// <inheritdoc cref="Emit(object, object, Signal[])"/>
        public static void Emit(object emitter, params Signal[] signals)
        {
            Emit(emitter, null, null, signals);
        }

        /// <inheritdoc cref="Emit(object, object, Channel, Signal[])"/>
        public static void Emit(object emitter, Channel channel, params Signal[] signals)
        {
            Emit(emitter, null, channel, signals);
        }

        /// <summary>
        /// Emit signals from the default channel.
        /// </summary>
        /// <inheritdoc cref="Emit(object, object, Channel, Signal[])"/>
        public static void Emit(object emitter, object data, params Signal[] signals)
        {
            Emit(emitter, data, null, signals);
        }

        /// <summary>
        /// Emit signals for a given channel.
        /// </summary>
        /// <param name="emitter">The object from which the signals have been emitted.</param>
        /// <param name="channel">The channel for which to emit the signals. If null given, uses the main channel instead.</param>
        /// <param name="data">The data to send to the emitted signals' listeners.</param>
        /// <param name="signals">The emitted signals.</param>
        public static void Emit(object emitter, object data, Channel channel, params Signal[] signals)
        {
            // Fix channel if not valid
            if (channel == null)
                channel = MainChannel;

            // Register channel if not
            if (!s_channels.Contains(channel))
                s_channels.Add(channel);

            Broadcast broadcast = new Broadcast(emitter, data, channel, signals);
            channel.Enqueue(broadcast);
        }

        /// <summary>
        /// Updates all the channels.
        /// </summary>
        /// <param name="delta">The time elapsed since the previous update. If 0 or negative, uses an internal delta time based on
        /// timestamp.</param>
        public static void Update(float delta = 0)
        {
            // Init timestamp
            if (s_previousUpdateTimestamp <= 0)
                s_previousUpdateTimestamp = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

            // Use internal delta time based on timestamp if invalid delta provided
            if (delta <= 0)
            {
                long timestamp = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
                delta = (timestamp - s_previousUpdateTimestamp) / 1000f;
            }

            // Update each channel
            foreach (Channel channel in s_channels)
                channel.Update(delta);
        }

        #endregion

    }

}