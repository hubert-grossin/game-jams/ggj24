/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;
using System.Collections.Generic;

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Represents an event that can be emitted from <see cref="Broadcaster"/>.
    /// </summary>
    public class Signal
    {

        #region Subclasses

        /// <summary>
        /// Binds a deferred listener without <see cref="SignalHandle"/> parameter to its "with handle" version.
        /// </summary>
        private class DeferredListenerWithoutHandleInfo
        {
            public SignalListenerDelegate Original { get; private set; }
            public SignalListenerWithHandleDelegate WithHandle { get; private set; }

            public DeferredListenerWithoutHandleInfo(SignalListenerDelegate original)
            {
                Original = original;
                WithHandle = handle => original();
            }
        }

        #endregion


        #region Delegates

        /// <summary>
        /// Callback function invoked when a signal is enqueued in a <see cref="Channel"/>.
        /// </summary>
        public delegate void SignalListenerDelegate();

        /// <summary>
        /// Callback function invoked when a listened signal is emitted from <see cref="Broadcaster"/>.
        /// </summary>
        /// <param name="handle">The handle of the signal being emitted.</param>
        public delegate void SignalListenerWithHandleDelegate(SignalHandle handle);

        /// <inheritdoc cref="SignalListenerDelegate"/>
        /// <typeparam name="T">The type of the data sent by the signal.</typeparam>
        /// <param name="data">The data to send to the invoked listener.</param>
        public delegate void SignalListenerDelegate<T>(T data);

        /// <inheritdoc cref="SignalListenerWithHandleDelegate"/>
        /// <inheritdoc cref="SignalListenerDelegate{T}"/>
        public delegate void SignalListenerWithHandleDelegate<T>(SignalHandle handle, T data);

        /// <summary>
        /// Called when this signal invokes its registered synced listeners.
        /// </summary>
        internal delegate void InvokeSyncedListenersDelegate();

        /// <summary>
        /// Called when this signal invokes its registered deferred listeners.
        /// </summary>
        internal delegate void InvokeListenersDelegate();

        #endregion


        #region Fields

        /// <inheritdoc cref="InvokeSyncedListenersDelegate"/>
        internal event InvokeSyncedListenersDelegate _onInvokeSyncedListeners;

        /// <inheritdoc cref="InvokeListenersDelegate"/>
        internal event InvokeListenersDelegate _onInvokeListeners;

        /// <summary>
        /// The name of this signal.
        /// </summary>
        private string _name = null;

        /// <summary>
        /// The listeners to invoke as soon as this signal is emitted.
        /// </summary>
        private List<ListenerInfo<SignalListenerDelegate>> _syncedListeners = new List<ListenerInfo<SignalListenerDelegate>>();

        /// <summary>
        /// The listeners to invoke when this signal is processed by <see cref="Broadcaster"/>.
        /// </summary>
        private List<ListenerInfo<SignalListenerWithHandleDelegate>> _deferredListeners = new List<ListenerInfo<SignalListenerWithHandleDelegate>>();

        /// <summary>
        /// Stores the callbacks generated for listeners that doesn't require a <see cref="SignalHandle"/> parameter.
        /// </summary>
        /// <remarks>Listeners without handle are "converted" into listeners with handle, so they can be added into the deferred listeners
        /// list without making a separate list. It's mostly meant to keep the listeners be invoked in the order they have been added to the
        /// unique list instead of invoking a list then another separately, breaking that order.</remarks>
        private List<DeferredListenerWithoutHandleInfo> _deferredListenersWithoutHandle = new List<DeferredListenerWithoutHandleInfo>();

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="Signal"/>
        /// <param name="name">The name of this signal.</param>
        public Signal(string name = null)
        {
            _name = !string.IsNullOrEmpty(name)
                ? name
                : System.Guid.NewGuid().ToString();
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_name"/>
        public string Name => _name;

        /// <summary>
        /// The expected data type for this signal.
        /// </summary>
        public virtual Type DataType => null;

        /// <summary>
        /// Adds a listener to this signal that will be invoked as soon as this signal is enqueued in a <see cref="Channel"/>, instead of
        /// waiting for the channel to process this signal.
        /// </summary>
        /// <param name="callback">The function to invoke when this signal is emitted.</param>
        /// <param name="listener">The object that owns the callback.</param>
        public void SubscribeSync(SignalListenerDelegate callback, object listener)
        {
            _syncedListeners.Add(new ListenerInfo<SignalListenerDelegate>(listener, callback));
        }

        /// <inheritdoc cref="Subscribe(SignalListenerWithHandleDelegate, object)"/>
        public void Subscribe(SignalListenerDelegate callback, object listener)
        {
            DeferredListenerWithoutHandleInfo info = new DeferredListenerWithoutHandleInfo(callback);
            _deferredListenersWithoutHandle.Add(info);
            Subscribe(info.WithHandle, listener);
        }

        /// <summary>
        /// Adds a listener to this signal.
        /// </summary>
        /// <inheritdoc cref="SubscribeSync(SignalListenerDelegate, object)"/>
        public void Subscribe(SignalListenerWithHandleDelegate callback, object listener)
        {
            _deferredListeners.Add(new ListenerInfo<SignalListenerWithHandleDelegate>(listener, callback));
        }

        /// <summary>
        /// Removes a listener from this signal.
        /// </summary>
        /// <inheritdoc cref="SubscribeSync(object, SignalListenerDelegate)"/>
        public bool Unsubscribe(SignalListenerDelegate callback)
        {
            // Remove callback from sync listeners
            bool success = _syncedListeners.RemoveAll(i => i.Callback == callback) > 0;

            // Also remove callback from deferred listeners, if it has been added as a listener without handle parameter
            List<DeferredListenerWithoutHandleInfo> callbackWithHandleVersions = _deferredListenersWithoutHandle.FindAll(i => i.Original == callback);
            foreach (DeferredListenerWithoutHandleInfo info in callbackWithHandleVersions)
            {
                if (Unsubscribe(info.WithHandle))
                    success = true;

                _deferredListenersWithoutHandle.Remove(info);
            }

            return success;
        }

        /// <inheritdoc cref="Unsubscribe(SignalListenerDelegate)"/>
        public bool Unsubscribe(SignalListenerWithHandleDelegate callback)
        {
            return _deferredListeners.RemoveAll(i => i.Callback == callback) > 0;
        }

        /// <summary>
        /// Emits this signal for the main channel.
        /// </summary>
        /// <inheritdoc cref="Emit(object, Channel)"/>
        public void Emit(object emitter)
        {
            Emit(emitter, null);
        }

        /// <summary>
        /// Emits this signal for a given channel.
        /// </summary>
        /// <param name="emitter">The object that emitted this signal.</param>
        /// <param name="channel">The channel for which to emit this signal.</param>
        public void Emit(object emitter, Channel channel)
        {
            Broadcaster.Emit(emitter, channel, this);
        }

        #endregion


        #region Internal API

        /// <summary>
        /// Checks if this signal has at least one deferred listener.
        /// </summary>
        internal virtual bool IsDeferred => _deferredListeners.Count > 0;

        /// <summary>
        /// Checks if at least one listener use <see cref="SignalHandle"/>. If not, this signal should be enqueued but instantly resolved.
        /// </summary>
        internal virtual bool UseHandle => _deferredListeners.Count > _deferredListenersWithoutHandle.Count;

        /// <summary>
        /// Called from <see cref="Channel"/> when this signal is processed from <see cref="Broadcaster"/>.
        /// </summary>
        /// <param name="data">The data to pass to the listeners.</param>
        internal virtual bool InvokeSyncedListeners(object data)
        {
            if (_syncedListeners.Count == 0)
                return true;

            bool success = true;
            foreach (ListenerInfo<SignalListenerDelegate> listener in new List<ListenerInfo<SignalListenerDelegate>>(_syncedListeners))
            {
                try
                {
                    listener.Callback?.Invoke();
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    success = false;
                }
            }

            _onInvokeSyncedListeners?.Invoke();
            return success;
        }

        /// <summary>
        /// Called from <see cref="Channel"/> when this signal is enqueued.
        /// </summary>
        /// <inheritdoc cref="InvokeSyncedListeners(object)"/>
        internal virtual bool InvokeDeferredListeners(SignalHandle handle, object data)
        {
            bool success = true;
            foreach (ListenerInfo<SignalListenerWithHandleDelegate> listener in new List<ListenerInfo<SignalListenerWithHandleDelegate>>(_deferredListeners))
            {
                try
                {
                    listener.Callback?.Invoke(handle);
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    success = false;
                }
            }

            _onInvokeListeners?.Invoke();
            return success;
        }

        #endregion

    }

    /// <inheritdoc cref="Signal"/>7
    /// <typeparam name="T">The type of data that can be passed as parameter to listeneres.</typeparam>
    public class Signal<T> : Signal
    {

        #region Subclasses

        /// <summary>
        /// Binds a deferred listener without <see cref="SignalHandle"/> parameter to its "with handle" version.
        /// </summary>
        private class DeferredListenerWithoutHandleInfo
        {
            public SignalListenerDelegate<T> Original { get; private set; }
            public SignalListenerWithHandleDelegate<T> WithHandle { get; private set; }

            public DeferredListenerWithoutHandleInfo(SignalListenerDelegate<T> original)
            {
                Original = original;
                WithHandle = (handle, data) => original(data);
            }
        }

        #endregion


        #region Fields

        /// <summary>
        /// The listeners to invoke as soon as this signal is emitted.
        /// </summary>
        private List<ListenerInfo<SignalListenerDelegate<T>>> _syncedTypedListeners = new List<ListenerInfo<SignalListenerDelegate<T>>>();

        /// <summary>
        /// The listeners to invoke when this signal is processed by <see cref="Broadcaster"/>.
        /// </summary>
        private List<ListenerInfo<SignalListenerWithHandleDelegate<T>>> _deferredTypedListeners = new List<ListenerInfo<SignalListenerWithHandleDelegate<T>>>();

        /// <inheritdoc cref="Signal._deferredListenersWithoutHandle"/>
        private List<DeferredListenerWithoutHandleInfo> _deferredTypedListenersWithoutHandle = new List<DeferredListenerWithoutHandleInfo>();

        /// <summary>
        /// Stores the latest data sent to synced listeners. Used for "init" behavior when a listener is added.
        /// </summary>
        private T _lastSyncedData = default;

        /// <summary>
        /// Stores the latest data sent to deferred listeners. Used for "init" behavior when a listener is added.
        /// </summary>
        private T _lastDeferredData = default;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="Signal(string)"/>
        public Signal(string name = null)
            : base(name) { }

        #endregion


        #region Public API

        /// <inheritdoc cref="Signal.DataType"/>
        public override Type DataType => typeof(T);

        /// <param name="init">If enabled, the listener is invoked instantly after being registered in this signal, receiving the latest
        /// data sent.</param>
        /// <inheritdoc cref="Signal.SubscribeSync(SignalListenerDelegate, object)"/>
        public void SubscribeSync(SignalListenerDelegate<T> callback, object listener, bool init = false)
        {
            _syncedTypedListeners.Add(new ListenerInfo<SignalListenerDelegate<T>>(listener, callback));

            if (init)
                callback?.Invoke(_lastSyncedData);
        }

        /// <inheritdoc cref="Subscribe(SignalListenerWithHandleDelegate{T}, object, bool)"/>
        public void Subscribe(SignalListenerDelegate<T> callback, object listener, bool init = false)
        {
            DeferredListenerWithoutHandleInfo info = new DeferredListenerWithoutHandleInfo(callback);
            _deferredTypedListenersWithoutHandle.Add(info);
            Subscribe(info.WithHandle, listener, init);
        }

        /// <inheritdoc cref="Signal.Subscribe(SignalListenerWithHandleDelegate, object)"/>
        /// <inheritdoc cref="SubscribeSync(SignalListenerDelegate{T}, object, bool)"/>
        public void Subscribe(SignalListenerWithHandleDelegate<T> callback, object listener, bool init = false)
        {
            _deferredTypedListeners.Add(new ListenerInfo<SignalListenerWithHandleDelegate<T>>(listener, callback));

            if (init)
                callback?.Invoke(new SignalHandle(null, this), _lastDeferredData);
        }

        /// <inheritdoc cref="Signal.Unsubscribe(SignalListenerDelegate)"/>
        public bool Unsubscribe(SignalListenerDelegate<T> callback)
        {
            // Remove callback from sync listeners
            bool success = _syncedTypedListeners.RemoveAll(i => i.Callback == callback) > 0;

            // Also remove callback from deferred listeners, if it has been added as a listener without handle parameter
            List<DeferredListenerWithoutHandleInfo> callbackWithHandleVersions = _deferredTypedListenersWithoutHandle.FindAll(i => i.Original == callback);
            foreach (DeferredListenerWithoutHandleInfo info in callbackWithHandleVersions)
            {
                if (Unsubscribe(info.WithHandle))
                    success = true;

                _deferredTypedListenersWithoutHandle.Remove(info);
            }

            return success;
        }

        /// <inheritdoc cref="Signal.Unsubscribe(SignalListenerWithHandleDelegate)"/>
        public bool Unsubscribe(SignalListenerWithHandleDelegate<T> callback)
        {
            return _deferredTypedListeners.RemoveAll(i => i.Callback == callback) > 0;
        }

        /// <inheritdoc cref="Signal.Emit(object)"/>
        /// <inheritdoc cref="Emit(object, Channel, T)"/>
        public void Emit(object emitter, T data)
        {
            Emit(emitter, data);
        }

        /// <param name="data">The data to send to this signal's listeners.</param>
        /// <inheritdoc cref="Signal.Emit(object, Channel)"/>
        public void Emit(object emitter, T data, Channel channel)
        {
            Broadcaster.Emit(emitter, data, channel, this);
        }

        #endregion


        #region Internal API

        /// <inheritdoc cref="Signal.IsDeferred"/>
        internal override bool IsDeferred => base.IsDeferred || _deferredTypedListeners.Count > 0;

        /// <inheritdoc cref="Signal.UseHandle"/>
        internal override bool UseHandle => base.UseHandle && _deferredTypedListeners.Count > _deferredTypedListenersWithoutHandle.Count;

        /// <inheritdoc cref="Signal.InvokeSyncedListeners(object)"/>
        internal override bool InvokeSyncedListeners(object data)
        {
            bool success = true;

            T typedData = default;
            // Convert data if applicable
            if (data != null)
            {
                try
                {
                    typedData = (T)data;
                }
                catch (InvalidCastException)
                {
                    Debug.LogError($"Invalid data type provided for signal {Name} (received {data.GetType()}, expected {typeof(T).Name}). Listeners will receive a default value as fallback.");
                }
            }
            _lastSyncedData = typedData;

            foreach (ListenerInfo<SignalListenerDelegate<T>> listener in new List<ListenerInfo<SignalListenerDelegate<T>>>(_syncedTypedListeners))
            {
                try
                {
                    listener.Callback?.Invoke(typedData);
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    success = false;
                }
            }

            if (!base.InvokeSyncedListeners(data))
                success = false;

            return success;
        }

        /// <inheritdoc cref="Signal.InvokeDeferredListeners(SignalHandle, object)"/>
        internal override bool InvokeDeferredListeners(SignalHandle handle, object data)
        {
            bool success = true;

            T typedData = default;
            // Convert data if applicable
            if (data != null)
            {
                try
                {
                    typedData = (T)data;
                }
                catch (InvalidCastException)
                {
                    Debug.LogError($"Invalid data type provided for signal {Name} (received {data.GetType()}, expected {typeof(T).Name}). Listeners will receive a default value as fallback.");
                }
            }
            _lastDeferredData = typedData;

            foreach (ListenerInfo<SignalListenerWithHandleDelegate<T>> listener in new List<ListenerInfo<SignalListenerWithHandleDelegate<T>>>(_deferredTypedListeners))
            {
                try
                {
                    listener.Callback?.Invoke(handle, typedData);
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                    success = false;
                }
            }

            if (!base.InvokeDeferredListeners(handle, data))
                success = false;

            return success;
        }

        #endregion

    }

}