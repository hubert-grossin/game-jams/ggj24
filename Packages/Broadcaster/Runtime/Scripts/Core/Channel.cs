/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System.Collections.Generic;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Represents a channel from which signals can be emitted. Channels allow signals that are not related to be played in parallel.
    /// </summary>
    public class Channel
    {

        #region Delegates

        /// <summary>
        /// Called when a broadcast is enqueued.
        /// </summary>
        /// <param name="channel">The channel in which the broadcast is enqueued.</param>
        /// <param name="broadcast">The enqueued broadcast.</param>
        internal delegate void AddBroadcastDelegate(Channel channel, Broadcast broadcast);

        /// <summary>
        /// Called when a broadcast is being processed (and so the deferred listeners did just been called.
        /// </summary>
        /// <inheritdoc cref="AddBroadcastDelegate"/>
        internal delegate void EmitBroadcastDelegate(Channel channel, Broadcast broadcast);

        /// <summary>
        /// Called when a broadcast is updated.
        /// </summary>
        /// <inheritdoc cref="AddBroadcastDelegate"/>
        internal delegate void UpdateBroadcastDelegate(Channel channel, Broadcast broadcast);

        /// <summary>
        /// Called when a broadcast is dequeued from the channel.
        /// </summary>
        /// <inheritdoc cref="AddBroadcastDelegate"/>
        internal delegate void EndBroadcastDelegate(Channel channel, Broadcast broadcast);

        #endregion


        #region Fields

        /// <inheritdoc cref="AddBroadcastDelegate"/>
        internal event AddBroadcastDelegate _onAddBroadcast;

        /// <inheritdoc cref="EmitBroadcastDelegate"/>
        internal event EmitBroadcastDelegate _onEmitBroadcast;

        /// <inheritdoc cref="UpdateBroadcastDelegate"/>
        internal event UpdateBroadcastDelegate _onUpdateBroadcast;

        /// <inheritdoc cref="EndBroadcastDelegate"/>
        internal event EndBroadcastDelegate _onEndBroadcast;

        /// <summary>
        /// The name of this channel.
        /// </summary>
        private string _name = null;

        /// <summary>
        /// The enqueued brodcasts to process.
        /// </summary>
        private Queue<Broadcast> _broadcasts = new Queue<Broadcast>();

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="Channel"/>
        /// <param name="name">The name of this channel. if not defined, generates a GUID instead.</param>
        internal Channel(string name = null)
        {
            _name = !string.IsNullOrEmpty(name)
                ? name
                : System.Guid.NewGuid().ToString();
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_name"/>
        public string Name => _name;

        #endregion


        #region Internal API

        /// <summary>
        /// Checks if a given broadcast is enqueued in this channel.
        /// </summary>
        /// <param name="broadcast">The broadcast to check.</param>
        /// <returns>Returns true if the given broadcast in enqueued in this channel.</returns>
        internal bool IsEnqueued(Broadcast broadcast)
        {
            return _broadcasts.Contains(broadcast);
        }

        /// <summary>
        /// Checks if a given broadcast is the one being processed by this channel.
        /// </summary>
        /// <param name="broadcast"></param>
        /// <returns></returns>
        internal bool IsActive(Broadcast broadcast)
        {
            return _broadcasts.Peek() == broadcast;
        }

        /// <summary>
        /// Called from <see cref="Broadcaster"/> to enqueue a new broadcast.
        /// </summary>
        /// <param name="broadcast">The broadcast to enqueue.</param>
        /// <returns>Returns true if the broadcast has been enqueued successfully.</returns>
        internal void Enqueue(Broadcast broadcast)
        {
            /**
             * Enqueue the broadcast only if its signals contain at least 1 deferred listener.
             * This avoid pure-sync signals or updated signals to be enqueued when it's not necessary.
             */
            if (broadcast.IsDeferred)
                _broadcasts.Enqueue(broadcast);

            _onAddBroadcast?.Invoke(this, broadcast);
            broadcast.InvokeSyncedListeners();

            /** 
             * For generic signals, the "last deferred data" is only set when deferred listeners are invoked. This line ensures the signal
             * is up to date with the latest data, even if no deferred listeners are stored. This allow the listeners that may register in
             * the future to get the correct data if they're initialized on subscribe.
             */
            if (!broadcast.IsDeferred)
            {
                broadcast.InvokeDeferredListeners();
                _onEmitBroadcast?.Invoke(this, broadcast);
                _onEndBroadcast?.Invoke(this, broadcast);
            }
            // If the enqueued broadcast is the only one in the list, invoke deferred listeners instantly
            else if (_broadcasts.Count == 1)
            {
                broadcast.InvokeDeferredListeners();
                _onEmitBroadcast?.Invoke(this, broadcast);
            }
        }

        /// <summary>
        /// Updates the current broadcast if applicable, or make the next one active.
        /// </summary>
        /// <param name="delta">The time elapsed since the previous call.</param>
        internal void Update(float delta)
        {
            // Cancel if no broadcast is currently enqueued in this channel
            if (_broadcasts.Count <= 0)
                return;

            Broadcast current = _broadcasts.Peek();
            bool isStillActive = current.Update(delta);
            _onUpdateBroadcast?.Invoke(this, current);

            // If the broadcast has been updated but did just end
            if (!isStillActive)
            {
                // remove the broadcast from the list
                _broadcasts.Dequeue();
                _onEndBroadcast?.Invoke(this, current);

                // Emit the next broadcast if applicable
                current = _broadcasts.Count > 0 ? _broadcasts.Peek() : null;
                if (current != null)
                {
                    current.InvokeDeferredListeners();
                    _onEmitBroadcast?.Invoke(this, current);

                    if (!current.UseHandles)
                        Update(0f);
                }
            }
        }

        #endregion

    }

}