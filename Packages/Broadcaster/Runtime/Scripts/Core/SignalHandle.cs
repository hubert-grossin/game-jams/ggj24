/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Object provided to all listeners of a signal that allow them to provide a delay or update to prevent timeout.
    /// </summary>
    public class SignalHandle
    {

        #region Fields

        /// <summary>
        /// The broadcast that generated this handle.
        /// </summary>
        private Broadcast _broadcast = null;

        /// <summary>
        /// The emitted signal.
        /// </summary>
        private Signal _signal = null;

        /// <summary>
        /// The delay to wait before considering the signal as fully processed. If 0, the signal will last only the next update, unless a
        /// listener "pings" to keep the signal alive.
        /// </summary>
        private float _delay = 0f;

        /// <summary>
        /// Flag enabled if a listener maintains this signal alive, even if the defined delay is elapsed.
        /// </summary>
        private bool _pingReceived = false;

        /// <summary>
        /// Flag enabled if a listener did maintain this signal alive the previous update.
        /// </summary>
        private bool _didPingPreviousFrame = false;

        /// <summary>
        /// Flag enabled if a listener did cancel this signal.
        /// </summary>
        private bool _cancelled = false;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="SignalHandle"/>
        /// <param name="broadcast">The broadcast that generated this handle.. This value can be null if the listeners are invoked on
        /// initialization from <see cref="Signal{T}.Subscribe(Signal.SignalListenerWithHandleDelegate{T}, object, bool)"/>.</param>
        /// <param name="signal">The emitted signal.</param>
        internal SignalHandle(Broadcast broadcast, Signal signal)
        {
            _broadcast = broadcast;
            _signal = signal;
        }

        #endregion


        #region Public API

        /// <summary>
        /// The channel for which the signal is emitted. <br/>
        /// This value can be null if the listeners are invoked on initialization from
        /// <see cref="Signal{T}.Subscribe(object, Signal.SignalListenerWithHandleDelegate{T}, bool)"/>.
        /// </summary>
        public Channel Channel => _broadcast != null ? _broadcast.Channel : null;

        /// <inheritdoc cref="_signal"/>
        public Signal Signal => _signal;

        /// <inheritdoc cref="_delay"/>
        public float Delay
        {
            get => _delay;
            set => SetDelay(value);
        }

        /// <summary>
        /// Checks if the <see cref="Broadcast"/> that contains this signal handle is running, and the related signal is still alive.
        /// </summary>
        public bool IsRunning => _broadcast.Timer > 0 && (_broadcast.Timer <= _delay || DidPing);

        /// <summary>
        /// Checks if the <see cref="Broadcast"/> that contains this signal handle is running, and the related signal is ended.
        /// </summary>
        public bool IsEnded => _broadcast.Timer > 0 && (_broadcast.Timer > _delay || !DidPing);

        /// <inheritdoc cref="_cancelled"/>
        public bool IsCancelled => _cancelled;

        /// <summary>
        /// Sets the delay to wait before considering the signal as fully processed.
        /// </summary>
        /// <param name="delay">The delay to set. Note that this function always use the maximum value between the given delay and the one
        /// currently set.</param>
        public void SetDelay(float delay)
        {
            _delay = Mathf.Max(delay, _delay);
        }

        /// <summary>
        /// Pings the signal, keeping it alive even if the defined delay is elapsed.
        /// </summary>
        public void Ping()
        {
            _pingReceived = true;
            _didPingPreviousFrame = true;
        }

        /// <summary>
        /// Cancels this signal, so the deferred listeners won't be called.
        /// </summary>
        public void CancelSignal()
        {
            _cancelled = true;
        }

        /// <summary>
        /// Cancels this signals and all other signals in the broadcast, so the deferred listeners won't be called, and the broadcast will
        /// be dequeued from its channel.
        /// </summary>
        public void CancelBroadcast()
        {
            _broadcast.Cancel();
        }

        #endregion


        #region Internal API

        /// <summary>
        /// Checks if the signal has received a ping this update, and reset the ping state.
        /// </summary>
        /// <returns>Returns true if the signal has received a ping this update.</returns>
        internal bool ConsumePing()
        {
            bool ping = _pingReceived;
            _didPingPreviousFrame = ping;
            _pingReceived = false;
            return ping;
        }

        #endregion


        #region Private API

        /// <summary>
        /// Checks if a listener did maintain this signal alive, whether this frame or the previous one.
        /// </summary>
        private bool DidPing => _pingReceived || _didPingPreviousFrame;

        #endregion

    }

}