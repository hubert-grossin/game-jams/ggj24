/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Converts a signal into a Unity event, so you can use it easily in the scene.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    public abstract class SignalTriggerComponent : MonoBehaviour { }

}