/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Updates the <see cref="Broadcaster"/> system.
    /// </summary>
    /// <remarks>This component updates the <see cref="Broadcaster"/> system on LateUpdate() message, ensuring the deferred listeners did
    /// ping their handle if needed. Aditionnaly, we can add <see cref="DefaultExecutionOrder"/> attribute to make sure the component is
    /// updated after everything else.</remarks>
    [HelpURL(Constants.BaseHelpUrl)]
    [AddComponentMenu(Constants.AddComponentMenu + "/Broadcaster")]
    [DefaultExecutionOrder(1000)]
    public class BroadcasterComponent : MonoBehaviour
    {

        [Tooltip("By default, this component updates the Broadcaster system using Time.deltaTime. If enabled, the system will calculate a delta time by using the absolute time of the OS.")]
        public bool UseAbsoluteUpdateTime = false;

        private void LateUpdate()
        {
            Broadcaster.Update(!UseAbsoluteUpdateTime ? Time.deltaTime : 0);
        }

    }

}