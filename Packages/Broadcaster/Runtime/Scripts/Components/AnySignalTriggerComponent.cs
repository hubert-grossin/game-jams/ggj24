/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

using UnityEngine.Events;

namespace SideXP.Broadcaster
{

    /// <inheritdoc cref="SignalTriggerComponent"/>
    [HelpURL(Constants.BaseHelpUrl)]
    [AddComponentMenu(Constants.AddComponentMenu + "/Signal Trigger")]
    public class AnySignalTriggerComponent : SignalTriggerComponent
    {

        [SerializeField]
        [Tooltip("The signal to listen.")]
        private SignalAsset _signal = null;

        [SerializeField]
        [Tooltip("Invoked when the signal is received.")]
        private UnityEvent _onReceiveSignal = new UnityEvent();

        /// <summary>
        /// Called when this component is enabled.
        /// </summary>
        protected virtual void OnEnable()
        {
            _signal.Subscribe(HandleSignal, this);
        }

        /// <summary>
        /// Called when this component is disabled.
        /// </summary>
        protected virtual void OnDisable()
        {
            _signal.Unsubscribe(HandleSignal);
        }

        /// <inheritdoc cref="_onReceiveSignal"/>
        private void HandleSignal()
        {
            _onReceiveSignal.Invoke();
        }

    }

}