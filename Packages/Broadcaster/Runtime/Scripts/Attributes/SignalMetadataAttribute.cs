/**
* Sideways Experiments (c) 2023
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Additional options for <see cref="SignalAsset"/> types. This can also be used for types marked with
    /// <see cref="SignalDataAttribute"/>, so the generated <see cref="SignalAsset"/> script will get the same metadata.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public class SignalMetadataAttribute : Attribute
    {

        /// <summary>
        /// The path for selecting this signal asset type from a list. You can use "/" to create submenus.
        /// </summary>
        public string Menu { get; set; } = null;

        /// <summary>
        /// The default name of an instanced signal asset of this type.
        /// </summary>
        public string Name { get; set; } = null;

        /// <summary>
        /// The order of this signal asset type in a list. Default is 0, negative is placed before, positive is placed after.
        /// </summary>
        public int Order { get; set; } = 0;

    }

}