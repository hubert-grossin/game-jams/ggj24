/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Marks this class as being usable as data for a <see cref="Signal"/>. This is used to automatically generate a
    /// <see cref="SignalAsset"/> script that takes this type of data as parameter.
    /// </summary>
    /// <remarks>You can use this attribute in combination with <see cref="SignalMetadataAttribute"/>, so the generated
    /// <see cref="SignalAsset"/> script will get the same metadata.</remarks>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public class SignalDataAttribute : Attribute
    {

        /// <summary>
        /// The path to which the signal asset script should be generated. If the given path is relative, it's resolved from this script
        /// (unless <see cref="RelativeFromAssets"/> is enabled). If the path include a file name, it's used as both the
        /// generated script and class name. If no path is provided, the script is generated in this class' script directory, with default
        /// name.
        /// </summary>
        public string Path { get; set; } = null;

        /// <summary>
        /// If enabled, the given path will be resolved from the /Assets folder of the project instead of being resolved from this script's
        /// path.
        /// </summary>
        public bool RelativeFromAssets { get; set; } = false;

        /// <inheritdoc cref="SignalDataAttribute(string, bool)"/>
        public SignalDataAttribute() { }

        /// <inheritdoc cref="SignalDataAttribute"/>
        /// <param name="path">The path to which the signal asset script should be generated.</param>
        /// <param name="relativeFromAssets">If enabled, the given path will be resolved from the /Assets folder of the project instead of
        /// being resolved from this script's path.</param>
        public SignalDataAttribute(string path, bool relativeFromAssets = false)
        {
            Path = path;
            RelativeFromAssets = relativeFromAssets;
        }

    }

}