/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Represents a <see cref="Signal"/> instance that takes an input as parameter in the <see cref="Broadcaster"/> system.
    /// </summary>
    /// <typeparam name="T">The type of the data that can be sent as parameter to listeners.</typeparam>
    [HelpURL(Constants.BaseHelpUrl)]
    public abstract class SignalAssetGeneric<T> : SignalAsset
    {

        #region Fields

        /// <summary>
        /// The <see cref="SideXP.Broadcaster.Signal"/> instance related to this asset in the <see cref="Broadcaster"/> system.
        /// </summary>
        private Signal<T> _signal = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this asset is loaded.
        /// </summary>
        private void OnEnable()
        {
            _signal = new Signal<T>(name);
        }

        /// <summary>
        /// Called when this asset is unloaded.
        /// </summary>
        private void OnDisable()
        {
            _signal = null;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="SignalAsset.Signal"/>
        public override Signal Signal => _signal;

        /// <inheritdoc cref="SignalAsset.DataType"/>
        public override Type DataType => typeof(T);

        /// <inheritdoc cref="Signal{T}.SubscribeSync(Signal.SignalListenerDelegate{T}, object, bool)"/>
        public void SubscribeSync(Signal.SignalListenerDelegate<T> callback, object listener, bool init = false)
        {
            if (_signal != null)
                _signal.SubscribeSync(callback, listener, init);
        }

        /// <inheritdoc cref="Signal{T}.Subscribe(Signal.SignalListenerDelegate{T}, object, bool)"/>
        public void Subscribe(Signal.SignalListenerDelegate<T> callback, object listener, bool init = false)
        {
            if (_signal != null)
                _signal.Subscribe(callback, listener, init);
        }

        /// <inheritdoc cref="Signal{T}.Subscribe(Signal.SignalListenerWithHandleDelegate{T}, object, bool)"/>
        public void Subscribe(Signal.SignalListenerWithHandleDelegate<T> callback, object listener, bool init = false)
        {
            if (_signal != null)
                _signal.Subscribe(callback, listener, init);
        }

        /// <inheritdoc cref="Signal{T}.Unsubscribe(Signal.SignalListenerDelegate{T})"/>
        public bool Unsubscribe(Signal.SignalListenerDelegate<T> callback)
        {
            return _signal != null && _signal.Unsubscribe(callback);
        }

        /// <inheritdoc cref="Signal{T}.Unsubscribe(Signal.SignalListenerWithHandleDelegate{T})"/>
        public bool Unsubscribe(Signal.SignalListenerWithHandleDelegate<T> callback)
        {
            return _signal != null && _signal.Unsubscribe(callback);
        }

        /// <param name="data">The data to send to this signal's listeners.</param>
        /// <inheritdoc cref="SignalAsset.Emit(object)"/>
        public void Emit(object emitter, T data)
        {
            Emit(emitter, data, Channel);
        }

        /// <inheritdoc cref="SignalAsset.Emit(object, Channel)"/>
        /// <inheritdoc cref="Emit(object, T)"/>
        public void Emit(object emitter, T data, Channel channel)
        {
            Broadcaster.Emit(emitter, data, channel, Signal);
        }

        #endregion

    }

}