/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Represents a <see cref="SideXP.Broadcaster.Signal"/> instance in the <see cref="Broadcaster"/> system.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    public abstract class SignalAsset : ScriptableObject
    {

        #region Fields

        [SerializeField]
        [Tooltip("The broadcaster asset that contains this signal. If no channel is assigned to this signal, the one defined in the broadcaster asset's is used instead.")]
        private BroadcasterAsset _brodcasterContainer = null;

        [SerializeField, TextArea(3, 6)]
        [Tooltip("Notes about the usage of this signal. This is meant to be used as documentation notes for the development team.")]
        private string _description = null;

        [SerializeField]
        [Tooltip("The channel for which this signal is used by default.")]
        private ChannelAsset _channelAsset = null;

        #endregion


        #region Public API

        /// <inheritdoc cref="_description"/>
        public string Description => _description;

        /// <summary>
        /// The signal instance in the <see cref="Broadcaster"/> system.
        /// </summary>
        public abstract Signal Signal { get; }

        /// <summary>
        /// The expected data type of this signal.
        /// </summary>
        public virtual Type DataType => null;

        /// <inheritdoc cref="_channelAsset"/>
        public ChannelAsset ChannelAsset => _channelAsset != null ? _channelAsset : _brodcasterContainer?.ChannelAsset;

        /// <inheritdoc cref="ChannelAsset.Channel"/>
        public Channel Channel => ChannelAsset != null ? ChannelAsset.Channel : null;

        /// <inheritdoc cref="Signal.Subscribe(Signal.SignalListenerWithHandleDelegate, object)"/>
        public void SubscribeSync(Signal.SignalListenerDelegate callback, object listener)
        {
            if (Signal != null)
                Signal.SubscribeSync(callback, listener);
        }

        /// <inheritdoc cref="Signal.Subscribe(Signal.SignalListenerDelegate, object)"/>
        public void Subscribe(Signal.SignalListenerDelegate callback, object listener)
        {
            if (Signal != null)
                Signal.Subscribe(callback, listener);
        }

        /// <inheritdoc cref="Signal.Subscribe(Signal.SignalListenerWithHandleDelegate, object)"/>
        public void Subscribe(Signal.SignalListenerWithHandleDelegate callback, object listener)
        {
            if (Signal != null)
                Signal.Subscribe(callback, listener);
        }

        /// <inheritdoc cref="Signal.Unsubscribe(Signal.SignalListenerDelegate)"/>
        public bool Unsubscribe(Signal.SignalListenerDelegate callback)
        {
            return Signal != null && Signal.Unsubscribe(callback);
        }

        /// <inheritdoc cref="Signal.Unsubscribe(Signal.SignalListenerWithHandleDelegate)"/>
        public bool Unsubscribe(Signal.SignalListenerWithHandleDelegate callback)
        {
            return Signal != null && Signal.Unsubscribe(callback);
        }

        /// <summary>
        /// <inheritdoc cref="Emit(object)"/><br/>
        /// This overload doesn't pass the emitter object. It can seem convenient, but as the emitter is not sent, the monitor window and
        /// other debug features won't be able to display it. This implmentation is mostly meant for prototyping or editor callbacks.
        /// </summary>
        /// <inheritdoc cref="Emit(object)"/>
        public void Emit()
        {
            Emit(null);
        }

        /// <summary>
        /// Emits this signal.
        /// </summary>
        /// <inheritdoc cref="Emit(object, Channel)"/>
        public void Emit(object emitter)
        {
            Emit(emitter, Channel);
        }

        /// <summary>
        /// Emits this signal for a given channel.
        /// </summary>
        /// <param name="emitter">The object that emitted this signal.</param>
        /// <param name="channel">The channel for which this signal is emitted.</param>
        public void Emit(object emitter, Channel channel)
        {
            Broadcaster.Emit(emitter, channel, Signal);
        }

        #endregion


        #region Protecetd API

        /// <inheritdoc cref="_brodcasterContainer"/>
        protected BroadcasterAsset BroadcasterContainer => _brodcasterContainer;

        #endregion


        #region Operators

        /// <summary>
        /// Converts a <see cref="SignalAsset"/> into a <see cref="SideXP.Broadcaster.Signal"/> instance.
        /// </summary>
        /// <param name="asset">The asset to convert.</param>
        public static implicit operator Signal(SignalAsset asset)
        {
            return asset.Signal;
        }

        #endregion

    }

}