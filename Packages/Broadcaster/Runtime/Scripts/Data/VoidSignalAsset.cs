/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Represents a parameterless <see cref="Signal"/> instance in the <see cref="Broadcaster"/> system.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    [SignalMetadata(Menu = "Void (no parameter)", Name = "NewSignal")]
    public class VoidSignalAsset : SignalAsset
    {

        /// <summary>
        /// The <see cref="SideXP.Broadcaster.Signal"/> instance related to this asset in the <see cref="Broadcaster"/> system.
        /// </summary>
        private Signal _signal = null;

        /// <summary>
        /// Called when this asset is loaded.
        /// </summary>
        private void OnEnable()
        {
            _signal = new Signal(name);
        }

        /// <summary>
        /// Called when this asset is unloaded.
        /// </summary>
        private void OnDisable()
        {
            _signal = null;
        }

        /// <inheritdoc cref="SignalAsset.Signal"/>
        public override Signal Signal => _signal;

    }

}