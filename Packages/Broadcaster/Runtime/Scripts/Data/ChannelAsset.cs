/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Represents a <see cref="SideXP.Broadcaster.Channel"/> instance in the <see cref="Broadcaster"/> system.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    [CreateAssetMenu(fileName = "New" + nameof(ChannelAsset), menuName = Constants.CreateAssetMenu + "/Channel Asset")]
    public class ChannelAsset : ScriptableObject
    {

        #region Fields

        [SerializeField, TextArea(3, 6)]
        [Tooltip("Notes about the usage of this channel asset. This is meant to be used as documentation notes for the development team.")]
        private string _description = null;

        /// <summary>
        /// The <see cref="SideXP.Broadcaster.Channel"/> instance related to this asset in the <see cref="Broadcaster"/> system.
        /// </summary>
        private Channel _channel = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this asset is loaded.
        /// </summary>
        private void OnEnable()
        {
            _channel = Broadcaster.CreateChannel(name);
        }

        /// <summary>
        /// Called when this asset is unloaded.
        /// </summary>
        private void OnDisable()
        {
            _channel = null;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_channel"/>
        public Channel Channel => _channel;

        /// <inheritdoc cref="_description"/>
        public string Description => _description;

        #endregion

    }

}