/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <summary>
    /// Container for channels and signals.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    [CreateAssetMenu(fileName = "New" + nameof(BroadcasterAsset), menuName = Constants.CreateAssetMenu + "/Broadcaster Asset")]
    public class BroadcasterAsset : ScriptableObject
    {

        #region Fields

        [SerializeField, TextArea(3, 6)]
        [Tooltip("Notes about the usage of this broadcaster asset and the signals bound to it. This is meant to be used as documentation notes for the development team.")]
        private string _description = null;

        [SerializeField]
        [Tooltip("The channel asset to use if no channel is defined in signals. If no channel provided, the main channel from Broadcaster is used instead.")]
        private ChannelAsset _channelAsset = null;

        #endregion


        #region Public API

        /// <inheritdoc cref="_channelAsset"/>
        public ChannelAsset ChannelAsset => _channelAsset;

        /// <inheritdoc cref="ChannelAsset.Channel"/>
        public Channel Channel => _channelAsset != null ? _channelAsset.Channel : null;

        /// <inheritdoc cref="_description"/>
        public string Description => _description;

        #endregion

    }

}