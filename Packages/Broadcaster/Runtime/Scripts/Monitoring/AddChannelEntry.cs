/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Broadcaster.Monitoring
{

    /// <summary>
    /// Represents a <see cref="BroadcasterLogbookEntry"/> for an added channel.
    /// </summary>
    public class AddChannelEntry : BroadcasterLogbookEntry
    {

        #region Fields

        /// <summary>
        /// The added channel.
        /// </summary>
        private Channel _channel = null;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="AddChannelEntry"/>
        /// <param name="channel">The added channel.</param>
        public AddChannelEntry(Channel channel)
            : base()
        {
            _channel = channel;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="BroadcasterLogbookEntry.Channel"/>
        public override Channel Channel => _channel;

        #endregion

    }

}