/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Broadcaster.Monitoring
{

    /// <summary>
    /// Represents an entry in the <see cref="BroadcasterLogbook"/>, if enabled.
    /// </summary>
    public abstract class BroadcasterLogbookEntry
    {

        #region Fields

        /// <summary>
        /// The id of the frame at which this entry was created.
        /// </summary>
        private int _frame = 0;

        /// <summary>
        /// The time elapsed since the application did start when this entry was created.
        /// </summary>
        private double _time = 0;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="BroadcasterLogbookEntry"/>
        public BroadcasterLogbookEntry()
        {
            _frame = UnityEngine.Time.frameCount;
            _time = UnityEngine.Time.realtimeSinceStartupAsDouble;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_frame"/>
        public int Frame => _frame;

        /// <inheritdoc cref="_time"/>
        public double Time => _time;

        /// <summary>
        /// The channel to which this entry is related.
        /// </summary>
        public abstract Channel Channel { get; }

        #endregion

    }

}