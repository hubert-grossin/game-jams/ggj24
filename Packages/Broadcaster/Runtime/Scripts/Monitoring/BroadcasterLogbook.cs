/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System.Collections.Generic;

namespace SideXP.Broadcaster.Monitoring
{

    /// <summary>
    /// Stores all the events that happened in the broadcasting system.
    /// </summary>
    public class BroadcasterLogbook
    {

        #region Delegates

        /// <summary>
        /// Called when a new entry is added to this logbook.
        /// </summary>
        /// <param name="entry">The entry that has been added.</param>
        public delegate void AddEntryDelegate(BroadcasterLogbookEntry entry);

        #endregion


        #region Fields

        /// <inheritdoc cref="AddEntryDelegate"/>
        public event AddEntryDelegate OnAddEntry;

        /// <summary>
        /// The list of all the events that happened in the broadcasting system.
        /// </summary>
        private List<BroadcasterLogbookEntry> _entries = new List<BroadcasterLogbookEntry>();

        #endregion


        #region Public API

        /// <inheritdoc cref="_entries"/>
        public BroadcasterLogbookEntry[] Entries => _entries.ToArray();

        /// <summary>
        /// Adds a given entry to this logbook.
        /// </summary>
        /// <param name="entry">The entry to add.</param>
        public void AddEntry(BroadcasterLogbookEntry entry)
        {
            _entries.Add(entry);
            OnAddEntry?.Invoke(entry);
        }

        /// <summary>
        /// Gets all the entries included in the given frames range.
        /// </summary>
        /// <param name="frameStart">The minimum frame index of the range (included).</param>
        /// <param name="frameEnd">The maximum frame index of the range (included).</param>
        /// <returns>Returns the found entries.</returns>
        public BroadcasterLogbookEntry[] GetEntriesWithinRange(int frameStart, int frameEnd)
        {
            List<BroadcasterLogbookEntry> entries = new List<BroadcasterLogbookEntry>();
            foreach (BroadcasterLogbookEntry entry in _entries)
            {
                // If the current entry is an "emmit broadcast" entry
                if (entry is EmitBroadcastEntry emitEntry)
                {
                    EndBroadcastEntry endEntry = GetRelatedEntry(emitEntry);
                    // If the broadcast has an "end" entry
                    if (endEntry != null)
                    {
                        // Add both entries If the emit->end entries are in the range
                        if (emitEntry.Frame <= frameEnd && endEntry.Frame >= frameStart)
                        {
                            entries.Add(entry);
                            entries.Add(endEntry);
                            continue;
                        }
                    }
                }

                if (!entries.Contains(entry) && entry.Frame >= frameStart && entry.Frame <= frameEnd)
                    entries.Add(entry);
            }
            return entries.ToArray();
        }

        /// <summary>
        /// Gets an "end broadcast" entry related to the same broadcast as a given "emit broadcast" entry.
        /// </summary>
        /// <param name="emitBroadcastEntry">The "emit broadcast" entry of which to get the related "end broadcast" entry.</param>
        /// <returns>Returns the related "end broadcast" entry.</returns>
        public EndBroadcastEntry GetRelatedEntry(EmitBroadcastEntry emitBroadcastEntry)
        {
            foreach (BroadcasterLogbookEntry entry in _entries)
            {
                if (entry is EndBroadcastEntry endBroadcastEntry && endBroadcastEntry.Broadcast == emitBroadcastEntry.Broadcast)
                    return endBroadcastEntry;
            }
            return null;
        }

        /// <summary>
        /// Gets an "emit broadcast" entry related to the same broadcast as a given "end broadcast" entry.
        /// </summary>
        /// <param name="endBroadcastEntry">The "end broadcast" entry of which to get the related "emit broadcast" entry.</param>
        /// <returns>Returns the related "emit broadcast" entry.</returns>
        public EmitBroadcastEntry GetRelatedEntry(EndBroadcastEntry endBroadcastEntry)
        {
            foreach (BroadcasterLogbookEntry entry in _entries)
            {
                if (entry is EmitBroadcastEntry emitBroadcastEntry && emitBroadcastEntry.Broadcast == endBroadcastEntry.Broadcast)
                    return emitBroadcastEntry;
            }
            return null;
        }

        /// <summary>
        /// Gets all the entries of a given type.
        /// </summary>
        /// <typeparam name="T">The type of the entries to get.</typeparam>
        /// <returns>Returns the found entries of the given type.</returns>
        public T[] Filter<T>()
            where T : BroadcasterLogbookEntry
        {
            List<T> entries = new List<T>();
            foreach (BroadcasterLogbookEntry entry in _entries)
            {
                if (entry.GetType() == typeof(T))
                    entries.Add(entry as T);
            }
            return entries.ToArray();
        }

        #endregion

    }

}