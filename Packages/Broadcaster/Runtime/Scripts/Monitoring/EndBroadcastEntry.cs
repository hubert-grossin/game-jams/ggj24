/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Broadcaster.Monitoring
{

    /// <summary>
    /// Represents a <see cref="BroadcasterLogbookEntry"/> for a broadcast removed from a channel.
    /// </summary>
    public class EndBroadcastEntry : BroadcasterLogbookEntry
    {

        #region Fields

        /// <summary>
        /// The removed broadcast.
        /// </summary>
        private Broadcast _broadcast = null;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="EndBroadcastEntry"/>
        /// <param name="broadcast">The removed broadcast.</param>
        public EndBroadcastEntry(Broadcast broadcast)
            : base()
        {
            _broadcast = broadcast;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_broadcast"/>
        public Broadcast Broadcast => _broadcast;

        /// <inheritdoc cref="BroadcasterLogbookEntry.Channel"/>
        public override Channel Channel => _broadcast.Channel;

        #endregion

    }

}