/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Broadcaster.Monitoring
{

    /// <summary>
    /// Represents a <see cref="BroadcasterLogbookEntry"/> for a broadcast added to a channel.
    /// </summary>
    public class AddBroadcastEntry : BroadcasterLogbookEntry
    {

        #region Fields

        /// <summary>
        /// The added broadcast.
        /// </summary>
        private Broadcast _broadcast = null;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="AddBroadcastEntry"/>
        /// <param name="broadcast">The added broadcast.</param>
        public AddBroadcastEntry(Broadcast broadcast)
            : base()
        {
            _broadcast = broadcast;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_broadcast"/>
        public Broadcast Broadcast => _broadcast;

        /// <inheritdoc cref="BroadcasterLogbookEntry.Channel"/>
        public override Channel Channel => _broadcast.Channel;

        #endregion

    }

}