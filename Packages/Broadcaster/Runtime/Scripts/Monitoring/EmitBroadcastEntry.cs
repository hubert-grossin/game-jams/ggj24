/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Broadcaster.Monitoring
{

    /// <summary>
    /// Represents a <see cref="BroadcasterLogbookEntry"/> for a broadcast that invokes the deferred listeners from a channel.
    /// </summary>
    public class EmitBroadcastEntry : BroadcasterLogbookEntry
    {

        #region Fields

        /// <summary>
        /// The emitted broadcast.
        /// </summary>
        private Broadcast _broadcast = null;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="EmitBroadcastEntry"/>
        /// <param name="broadcast">The emitted broadcast.</param>
        public EmitBroadcastEntry(Broadcast broadcast)
            : base()
        {
            _broadcast = broadcast;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_broadcast"/>
        public Broadcast Broadcast => _broadcast;

        /// <inheritdoc cref="BroadcasterLogbookEntry.Channel"/>
        public override Channel Channel => _broadcast.Channel;

        #endregion

    }

}