/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <inheritdoc cref="SignalAssetGeneric{T}"/>
    [HelpURL(Constants.BaseHelpUrl)]
    [SignalMetadata(Menu = "Unity/Vector2", Name = "NewVector2Signal")]
    public class Vector2SignalAsset : SignalAssetGeneric<Vector2> { }

}