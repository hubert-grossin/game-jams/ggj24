/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <inheritdoc cref="SignalAssetGeneric{T}"/>
    [HelpURL(Constants.BaseHelpUrl)]
    [SignalMetadata(Menu = "Float", Name = "NewFloatSignal")]
    public class FloatSignalAsset : SignalAssetGeneric<float> { }

}