/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Broadcaster
{

    /// <inheritdoc cref="SignalAssetGeneric{T}"/>
    [HelpURL(Constants.BaseHelpUrl)]
    [SignalMetadata(Menu = "Bool", Name = "NewBoolSignal")]
    public class BoolSignalAsset : SignalAssetGeneric<bool> { }

}