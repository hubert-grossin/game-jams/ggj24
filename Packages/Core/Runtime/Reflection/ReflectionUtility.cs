/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using static UnityEngine.GraphicsBuffer;

namespace SideXP.Core.Reflection
{

    /// <summary>
    /// Miscellaneous functions for working with C# reflection.
    /// </summary>
    public static class ReflectionUtility
    {

        #region Fields

        /// <summary>
        /// Targets elements declared on the instance, public and non-public.
        /// </summary>
        public const BindingFlags InstanceFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

        /// <summary>
        /// Targets elements declared static, public and non-public.
        /// </summary>
        public const BindingFlags StaticFlags = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

        /// <summary>
        /// The name of the main Unity C# scripts assembly.
        /// </summary>
        public const string UnityCSharpAssemblyName = "Assembly-CSharp";

        /// <summary>
        /// The name of the main Unity C# scripts for editor assembly.
        /// </summary>
        public const string UnityEditorCSharpAssemblyName = "Assembly-CSharp-Editor";

        /// <summary>
        /// The name of the assemblies that are included in Unity C# subset, but are not part of the project.
        /// </summary>
        public static readonly string[] NonProjectAssemblies =
        {
            "mscorlib",
            "UnityEngine",
            "UnityEditor",
            "Unity.",
            "System",
            "Mono.",
            "netstandard",
            "Microsoft"
        };

        #endregion


        #region Type Search

        /// <summary>
        /// Gets all assemblies in the current app domain.
        /// </summary>
        /// <param name="excludedAssembliesPrefixes">The prefixes of the assembly names to exclude from this query.</param>
        /// <returns>Returns the found assemblies.</returns>
        public static Assembly[] GetAllAssemblies(IList<string> excludedAssembliesPrefixes = null)
        {
            List<Assembly> assemblies = new List<Assembly>();
            // For each assembly in the current domain
            foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
            {
                string assemblyName = a.GetName().Name;

                // If assmeblies are filtered
                if (excludedAssembliesPrefixes != null)
                {
                    bool isExcluded = false;
                    // For each excluded assembly prefix
                    foreach (string assemblyPrefix in excludedAssembliesPrefixes)
                    {
                        // Mark the current assembly as excluded if the prefix matches
                        if (assemblyName.StartsWith(assemblyPrefix))
                        {
                            isExcluded = true;
                            break;
                        }
                    }

                    // Skip if the current assembly is excluded
                    if (isExcluded)
                    {
                        continue;
                    }
                }

                assemblies.Add(a);
            }

            return assemblies.ToArray();
        }

        /// <summary>
        /// Get all assemblies related to the current Unity project.
        /// </summary>
        /// <inheritdoc cref="GetAllAssemblies(IList{string})"/>
        public static Assembly[] GetProjectAssemblies()
        {
            return GetAllAssemblies(NonProjectAssemblies);
        }

        /// <summary>
        /// Gets all the types that implement the given type in the given assembly.<br/>
        /// NOTE: This function is inspired by this answer by Nick VanderPyle: https://stackoverflow.com/a/8645519/6699339
        /// </summary>
        /// <param name="parentType">The type of the class you want to find inheritors. You can pass a generic type in this paramater, by using the "open generic" syntax. As an example, if the parent class is MyGenericClass{T}, use typeof(MyGenericClass{}), without any value inside the less-than/greater-than characters.</param>
        /// <param name="assembly">The assembly where you want to find the given type implementations.</param>
        /// <returns>Returns an enumerable that contains all the found types.</returns>
        public static Type[] GetAllTypesAssignableFrom(Type parentType, Assembly assembly)
        {
            IEnumerable<Type> foundTypes =
                // type = The current type among the assembly's
                from type in assembly.GetTypes()
                    // baseType = The type of which the current type inherits
                let baseType = type.BaseType
                where
                (
                    // If the query type is an interface
                    parentType.IsInterface &&
                    (
                        // Include the current type if it implements the interface
                        !type.IsGenericType && type.GetInterfaces().Contains(parentType)
                    ) ||
                    // Or, if the current type inherits from another type
                    baseType != null &&
                    (
                        // Include the current type only if the type of which the current type inherits is generic, the current type itself is
                        // not generic, and the inherited type is assignable from the queried one
                        (baseType.IsGenericType && !type.IsGenericType && parentType.IsAssignableFrom(baseType.GetGenericTypeDefinition())) ||
                        // Include the current type only if the type of which the current type inherits is not generic, the current type itself
                        // is not generic too, and if the inherited type is assignable from the queried one
                        (!baseType.IsGenericType && !type.IsGenericType && parentType.IsAssignableFrom(baseType))
                    )
                )
                select type;
            return foundTypes.ToArray();
        }

        /// <inheritdoc cref="GetAllTypesAssignableFrom(Type, Assembly)"/>
        /// <typeparam name="T">The type of the class you want to find inheritors.</typeparam>
        public static Type[] GetAllTypesAssignableFrom<T>(Assembly assembly)
        {
            return GetAllTypesAssignableFrom(typeof(T), assembly);
        }

        /// <inheritdoc cref="GetAllTypesAssignableFrom(Type, Assembly)"/>
        /// <param name="assemblies">The assemblies where you want to find the given type implementations.</param>
        public static Type[] GetAllTypesAssignableFrom(Type parentType, IList<Assembly> assemblies)
        {
            List<Type> types = new List<Type>();
            foreach (Assembly a in assemblies)
            {
                types.AddRange(GetAllTypesAssignableFrom(parentType, a));
            }
            return types.ToArray();
        }

        /// <inheritdoc cref="GetAllTypesAssignableFrom(Type, IList{Assembly})"/>
        /// <inheritdoc cref="GetAllTypesAssignableFrom{T}(Assembly)"/>
        public static Type[] GetAllTypesAssignableFrom<T>(IList<Assembly> assemblies)
        {
            return GetAllTypesAssignableFrom(typeof(T), assemblies);
        }

        /// <summary>
        /// Gets all the types that implement the given type in the project assemblies.
        /// </summary>
        /// <inheritdoc cref="GetAllTypesAssignableFrom(Type, IList{Assembly})"/>
        public static Type[] GetAllTypesInProjectAssignableFrom(Type parentType)
        {
            return GetAllTypesAssignableFrom(parentType, GetProjectAssemblies());
        }

        /// <summary>
        /// Gets all the types that implement the given type in the project assemblies.
        /// </summary>
        /// <inheritdoc cref="GetAllTypesAssignableFrom{T}(IList{Assembly})"/>
        public static Type[] GetAllTypesInProjectAssignableFrom<T>()
        {
            return GetAllTypesAssignableFrom(typeof(T), GetProjectAssemblies());
        }

        /// <summary>
        /// Gets all types available that have the given attribute type.
        /// </summary>
        /// <typeparam name="T">The type of the expected attribute.</typeparam>
        /// <returns>Returns the found types.</returns>
        public static Type[] GetAllTypesInProjectWithAttribute<T>()
            where T : Attribute
        {
            List<Type> types = new List<Type>();
            Type attributeType = typeof(T);
            foreach (Assembly assembly in GetProjectAssemblies())
            {
                foreach (Type t in assembly.GetTypes())
                {
                    if (t.IsDefined(attributeType))
                        types.Add(t);
                }
            }
            return types.ToArray();
        }

        /// <summary>
        /// Gets a type in the project by its full name (namespace and type name).
        /// </summary>
        /// <param name="className">The name of the class to find.</param>
        /// <param name="namespaceName">The name of the namespace that should contain the type.</param>
        /// <param name="type">Outputs the found type.</param>
        /// <returns>Returns true if a type has been found.</returns>
        public static bool FindType(string className, string namespaceName, out Type type)
        {
            Assembly[] assemblies = GetProjectAssemblies();
            type = null;
            string typeFullName = !string.IsNullOrEmpty(namespaceName)
                ? $"{namespaceName}.{className}"
                : className;

            // For each assembly in the project
            foreach (Assembly assembly in assemblies)
            {
                // Check if the current assembly contains the expected type.
                type = assembly.GetType(typeFullName);
                if (type != null)
                    break;
            }

            return type != null;
        }

        #endregion


        #region Members

        /// <summary>
        /// Gets the named field or property info.
        /// </summary>
        /// <param name="type">The type from which you want to get the reflection data.</param>
        /// <param name="name">The name of the field or property you want to get.</param>
        /// <param name="inherited">If enabled, this function will try to get a field or property in the parent classes if not found on the
        /// given type.</param>
        /// <param name="bindingFlags">The binding flages use for querying the reflection data.</param>
        /// <returns>Returns the found field or property info.</returns>
        public static FieldOrPropertyInfo GetFieldOrProperty(Type type, string name, bool inherited = false, BindingFlags bindingFlags = InstanceFlags)
        {
            foreach (FieldInfo field in type.GetFields(bindingFlags))
            {
                if (field.Name == name)
                    return new FieldOrPropertyInfo { Field = field };
            }

            foreach (PropertyInfo property in type.GetProperties(bindingFlags))
            {
                if (property.Name == name)
                    return new FieldOrPropertyInfo { Property = property };
            }

            return (inherited && type.BaseType != null)
                ? GetFieldOrProperty(type.BaseType, name, inherited, bindingFlags)
                : null;
        }

        /// <inheritdoc cref="GetFieldOrProperty(Type, string, bool, BindingFlags)"/>
        /// <typeparam name="T">The type from which you want to get the reflection data.</typeparam>
        public static FieldOrPropertyInfo GetFieldOrProperty<T>(string name, bool inherited = false, BindingFlags bindingFlags = InstanceFlags)
        {
            return GetFieldOrProperty(typeof(T), name, inherited, bindingFlags);
        }

        /// <inheritdoc cref="GetFieldOrProperty(Type, string, bool, BindingFlags)"/>
        /// <param name="target">The object from which you want to get the type's reflection data.</param>
        public static FieldOrPropertyInfo GetFieldOrProperty(object target, string name, bool inherited = false, BindingFlags bindingFlags = InstanceFlags)
        {
            return GetFieldOrProperty(target.GetType(), name, inherited, bindingFlags);
        }

        /// <returns>Returns the informations about all the fields and properties on the target type.</returns>
        /// <inheritdoc cref="GetFieldsAndPropertiesRecursively(Type, bool, BindingFlags, List{FieldOrPropertyInfo})"/>
        public static FieldOrPropertyInfo[] GetFieldsAndProperties(Type type, bool inherited = false, BindingFlags bindingFlags = InstanceFlags)
        {
            List<FieldOrPropertyInfo> fieldOrPropertiesList = new List<FieldOrPropertyInfo>();
            GetFieldsAndPropertiesRecursively(type, inherited, bindingFlags, fieldOrPropertiesList);
            return fieldOrPropertiesList.ToArray();
        }

        /// <inheritdoc cref="GetFieldsAndProperties(Type, bool, BindingFlags)"/>
        /// <typeparam name="T">The type from which you want to get the reflection data.</typeparam>
        public static FieldOrPropertyInfo[] GetFieldsAndProperties<T>(bool inherited = false, BindingFlags bindingFlags = InstanceFlags)
        {
            return GetFieldsAndProperties(typeof(T), inherited, bindingFlags);
        }

        /// <inheritdoc cref="GetFieldsAndProperties(Type, bool BindingFlags)"/>
        /// <inheritdoc cref="GetFieldOrProperty(object, string, bool, BindingFlags)"/>
        public static FieldOrPropertyInfo[] GetFieldsAndProperties(object target, bool inherited = false, BindingFlags bindingFlags = InstanceFlags)
        {
            return GetFieldsAndProperties(target.GetType(), inherited, bindingFlags);
        }

        /// <inheritdoc cref="GetFieldsAndProperties(Type, bool, BindingFlags)"/>
        /// <inheritdoc cref="GetFieldsAndPropertiesWithAttributeRecursively(Type, Type, bool, BindingFlags, List{FieldOrPropertyInfo})"/>
        public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute(Type type, Type attributeType, bool inherited = false, BindingFlags bindingFlags = InstanceFlags)
        {
            List<FieldOrPropertyInfo> fieldOrPropertiesList = new List<FieldOrPropertyInfo>();
            GetFieldsAndPropertiesWithAttributeRecursively(type, attributeType, inherited, bindingFlags, fieldOrPropertiesList);
            return fieldOrPropertiesList.ToArray();
        }

        /// <inheritdoc cref="GetFieldsAndPropertiesWithAttribute(Type, Type, bool, BindingFlags)"/>
        /// <inheritdoc cref="GetFieldOrProperty(object, string, bool, BindingFlags)"/>
        public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute(object target, Type attributeType, bool inherited = false, BindingFlags bindingFlags = InstanceFlags)
        {
            return GetFieldsAndPropertiesWithAttribute(target.GetType(), attributeType, inherited, bindingFlags);
        }

        /// <inheritdoc cref="GetFieldsAndPropertiesWithAttribute{TType, TAttribute}(bool, BindingFlags)"/>
        public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute<TAttribute>(Type type, bool inherited = false,  BindingFlags bindingFlags = InstanceFlags)
        {
            return GetFieldsAndPropertiesWithAttribute(type, typeof(TAttribute), inherited, bindingFlags);
        }

        /// <inheritdoc cref="GetFieldsAndPropertiesWithAttribute(Type, Type, bool, BindingFlags)"/>
        /// <typeparam name="TType">The type from which you want to get the reflection data.</typeparam>
        /// <typeparam name="TAttribute">The type of the attribute the filtered fields and properties must have.</typeparam>
        public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute<TType, TAttribute>(bool inherited = false, BindingFlags bindingFlags = InstanceFlags)
            where TAttribute : Attribute
        {
            return GetFieldsAndPropertiesWithAttribute(typeof(TType), typeof(TAttribute), inherited, bindingFlags);
        }

        /// <inheritdoc cref="GetFieldsAndPropertiesWithAttribute(object, Type, bool, BindingFlags)"/>
        /// <inheritdoc cref="GetFieldsAndPropertiesWithAttribute{TType, TAttribute}(bool, BindingFlags)"/>
        public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute<TAttribute>(object target, bool inherited = false, BindingFlags bindingFlags = InstanceFlags)
            where TAttribute : Attribute
        {
            return GetFieldsAndPropertiesWithAttribute(target.GetType(), typeof(TAttribute), inherited, bindingFlags);
        }

        /// <summary>
        /// Gets the field or property info from property path.
        /// </summary>
        /// <param name="type">The type from which you want to parse the path.</param>
        /// <param name="path">The property path (as you can get it from <see cref="UnityEditor.SerializedProperty.propertyPath"/>).</param>
        /// <returns>Returns the found field or property data.</returns>
        /// <inheritdoc cref="GetFieldOrProperty(Type, string, bool, BindingFlags)"/>
        public static FieldOrPropertyInfo GetFieldOrPropertyFromPath(Type type, string path, bool inherited = false, BindingFlags bindingFlags = InstanceFlags)
        {
            // Deal specific array representation from a Unity property path, as you can get it from SerializedProperty
            path = path.Replace(".Array.data[", "[");
            // Split the property path
            string[] propertyPathSplit = path.Split('.');

            string part = propertyPathSplit[0];
            FieldOrPropertyInfo partProperty;

            // Parse array part if applicable
            if (part.Contains("["))
            {
                string elementName = part.Substring(0, part.IndexOf("["));
                partProperty = GetFieldOrProperty(type, elementName, inherited, bindingFlags);
            }
            else
            {
                partProperty = GetFieldOrProperty(type, part, inherited, bindingFlags);
            }

            // Stop if the current part is the last one in the path (and so, is the one to find)
            if (propertyPathSplit.Length == 1)
            {
                return partProperty;
            }
            // Else, navigate to the next part. If the current part property's type is an array, navigate from its element type
            else
            {
                Type partPropertyType = partProperty.Type.IsArray
                    ? partProperty.Type.GetElementType()
                    : partProperty.Type;

                return GetFieldOrPropertyFromPath(partPropertyType, path.Substring(part.Length + 1), inherited);
            }
        }

        /// <summary>
        /// Gets a reference or value of an object given a property path (like myObject.myContainer[3].myProperty).<br/>
        /// This function is inspired by the SpacePuppy Unity Framework: https://github.com/lordofduct/spacepuppy-unity-framework-4.0/blob/master/Framework/com.spacepuppy.core/Editor/src/EditorHelper.cs
        /// </summary>
        /// <param name="source">The object in which you want to get the nested object.</param>
        /// <param name="propertyPath">The property path for navigating to the expected object (like myObject.myContainer[3].myProperty).</param>
        /// <returns>Returns the found nested object, or null if the path didn't lead to a valid object.</returns>
        public static object GetNestedObject(object source, string propertyPath)
        {
            // Deal specific array representation from a Unity property path, as you can get it from SerializedProperty
            propertyPath = propertyPath.Replace(".Array.data[", "[");
            // Split the property path
            string[] propertyPathSplit = propertyPath.Split('.');
            // For each part in the property path
            foreach (string part in propertyPathSplit)
            {
                // If the part targets an item in an array
                if (part.Contains("["))
                {
                    // Extract the name of the property that contains the array
                    string elementName = part.Substring(0, part.IndexOf("["));
                    // Extract the index value
                    int index = Convert.ToInt32(part.Substring(part.IndexOf("[")).Replace("[", "").Replace("]", ""));
                    // Get the item at the expected position in the array
                    source = GetSubObject(source, elementName, index);
                }
                // Else, if the part targets an object
                else
                {
                    // Get that part's target
                    source = GetSubObject(source, part);
                }
            }
            return source;
        }

        /// <inheritdoc cref="GetNestedObject(object, string)"/>
        /// <typeparam name="T">The type of the expected target object.</typeparam>
        public static T GetNestedObject<T>(object source, string propertyPath)
        {
            return (T)GetNestedObject(source, propertyPath);
        }

        #endregion


        #region Custom Attributes

        /// <summary>
        /// Gets a custom attribute on the given type.
        /// </summary>
        /// <param name="type">The type from which you want to get the attribute.</param>
        /// <param name="attributeType">The type of the attribute you want to get.</param>
        /// <param name="attribute">Outputs the found attribute.</param>
        /// <returns>Returns true if the attribute is defined on the given type.</returns>
        public static bool GetCustomAttribute(Type type, Type attributeType, out Attribute attribute)
        {
            attribute = Attribute.GetCustomAttribute(type, attributeType);
            return attribute != null;
        }

        /// <inheritdoc cref="GetCustomAttribute(Type, Type, out Attribute)"/>
        /// <typeparam name="T">The type of the attribute you want to get.</typeparam>
        public static bool GetCustomAttribute<T>(Type type, out T attribute)
            where T : Attribute
        {
            GetCustomAttribute(type, typeof(T), out Attribute tmpAttribute);
            attribute = tmpAttribute as T;
            return attribute != null;
        }

        /// <summary>
        /// Checks if this type has a given class attribute.
        /// </summary>
        /// <inheritdoc cref="GetCustomAttribute(Type, Type, out Attribute)"/>
        public static bool HasAttribute(Type type, Type attributeType)
        {
            return Attribute.IsDefined(type, attributeType);
        }

        /// <inheritdoc cref="HasAttribute(Type, Type)"/>
        /// <inheritdoc cref="GetCustomAttribute{T}(Type, out Attribute)"/>
        public static bool HasAttribute<T>(Type type)
            where T : Attribute
        {
            return HasAttribute(type, typeof(T));
        }

        #endregion


        #region Private API

        /// <summary>
        /// Gets a container from the given source object by just using its field or property name.
        /// </summary>
        /// <param name="source">The source object from which you want to get the sub object.</param>
        /// <param name="fieldOrPropertyName">The name of the field or property for the container you want to get.</param>
        /// <returns>Returns the found sub-object.</returns>
        private static object GetSubObject(object source, string fieldOrPropertyName)
        {
            if (source == null)
                return null;

            Type type = source.GetType();
            while (type != null)
            {
                FieldOrPropertyInfo fieldOrProperty = GetFieldOrProperty(source.GetType(), fieldOrPropertyName);
                if (fieldOrProperty != null)
                    return fieldOrProperty.GetValue(source);
                type = type.BaseType;
            }
            return null;
        }

        /// <summary>
        /// Gets a container from the given source object by just using its field or property name, and the index of its source array.
        /// </summary>
        /// <inheritdoc cref="GetSubObject(object, string)"/>
        /// <param name="index">The index of the property for the container you want to find.</param>
        private static object GetSubObject(object source, string fieldOrPropertyName, int index)
        {
            IEnumerable enumerable = GetNestedObject(source, fieldOrPropertyName) as IEnumerable;
            if (enumerable == null)
                return null;

            // Iterate through the container to find the expected index
            IEnumerator enm = enumerable.GetEnumerator();
            for (int i = 0; i <= index; i++)
            {
                if (!enm.MoveNext())
                    return null;
            }
            return enm.Current;
        }

        /// <summary>
        /// Extracts all the fields and properties of a given type.
        /// </summary>
        /// <param name="fieldOrPropertiesList">The list of all found fields and properties.</param>
        /// <returns></returns>
        /// <inheritdoc cref="GetFieldOrProperty(Type, string, bool, BindingFlags)"/>
        private static void GetFieldsAndPropertiesRecursively(Type type, bool inherited, BindingFlags bindingFlags, List<FieldOrPropertyInfo> fieldOrPropertiesList)
        {
            foreach (FieldInfo field in type.GetFields(bindingFlags))
            {
                fieldOrPropertiesList.Add(new FieldOrPropertyInfo { Field = field });
            }

            foreach (PropertyInfo property in type.GetProperties(bindingFlags))
            {
                fieldOrPropertiesList.Add(new FieldOrPropertyInfo { Property = property });
            }

            if (inherited && type.BaseType != null)
            {
                GetFieldsAndPropertiesRecursively(type.BaseType, inherited, bindingFlags, fieldOrPropertiesList);
            }
        }

        /// <summary>
        /// Extracts all the fields and properties of a given type that have a specific attribute.
        /// </summary>
        /// <param name="attributeType">The type of the attribute the filtered fields and properties must have.</param>
        /// <returns></returns>
        /// <inheritdoc cref="GetFieldsAndPropertiesRecursively(Type, bool, BindingFlags, List{FieldOrPropertyInfo})"/>
        private static void GetFieldsAndPropertiesWithAttributeRecursively(Type type, Type attributeType, bool inherited, BindingFlags bindingFlags, List<FieldOrPropertyInfo> fieldOrPropertiesList)
        {
            foreach (FieldInfo field in type.GetFields(bindingFlags))
            {
                if (field.GetCustomAttribute(attributeType) != null)
                {
                    fieldOrPropertiesList.Add(new FieldOrPropertyInfo { Field = field });
                }
            }

            foreach (PropertyInfo property in type.GetProperties(bindingFlags))
            {
                if (property.GetCustomAttribute(attributeType) != null)
                {
                    fieldOrPropertiesList.Add(new FieldOrPropertyInfo { Property = property });
                }
            }

            if (inherited && type.BaseType != null)
            {
                GetFieldsAndPropertiesWithAttributeRecursively(type.BaseType, attributeType, inherited, bindingFlags, fieldOrPropertiesList);
            }
        }

        #endregion

    }

}