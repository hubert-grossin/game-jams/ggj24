/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;
using System.Reflection;
using System.Collections.Generic;

namespace SideXP.Core.Reflection
{

    /// <summary>
    /// Groups informations about a field or a property being processed through C# reflection.<br/>
    /// This is used by <see cref="ReflectionUtility.GetFieldsAndProperties(Type, BindingFlags)"/>, and is useful to unifies operations
    /// over both fields and properties, avoiding code duplications.
    /// </summary>
    public class FieldOrPropertyInfo
    {

        #region Fields

        /// <summary>
        /// Informations about the field. Null if the element is a property (see <see cref="Property"/>).
        /// </summary>
        public FieldInfo Field;

        /// <summary>
        /// Informations about the property. Null if the element is a field (see <see cref="Field"/>).
        /// </summary>
        public PropertyInfo Property;

        #endregion


        #region Public API

        /// <summary>
        /// Gets the name of the element.
        /// </summary>
        public string Name => Field != null ? Field.Name : Property.Name;

        /// <summary>
        /// Gets the type of the element.
        /// </summary>
        public Type Type => Field != null ? Field.FieldType : Property.PropertyType;

        /// <summary>
        /// Checks if the value of the element can be read.
        /// </summary>
        public bool CanRead => Field != null || Property.CanRead;

        /// <summary>
        /// Checks if the value of the element can be set.
        /// </summary>
        public bool CanWrite => Field != null || Property.CanWrite;

        /// <summary>
        /// Checks if the element is public.
        /// </summary>
        public bool IsPublic
        {
            get
            {
                if (Field != null)
                    return Field.IsPublic;

                MethodInfo accessor = Property.GetGetMethod(false);
                if (accessor != null && accessor.IsPublic)
                    return true;

                accessor = Property.GetSetMethod(false);
                if (accessor != null && accessor.IsPublic)
                    return true;

                return false;
            }
        }

        /// <summary>
        /// Checks if the element is private.
        /// </summary>
        public bool IsPrivate
        {
            get
            {
                if (Field != null)
                    return Field.IsPrivate;

                MethodInfo accessor = Property.GetGetMethod(true);
                if (accessor != null && accessor.IsPrivate)
                    return true;

                accessor = Property.GetSetMethod(true);
                if (accessor != null && accessor.IsPrivate)
                    return true;

                return false;
            }
        }

        /// <summary>
        /// Gets the value of this field or property.
        /// </summary>
        /// <param name="target">The object that owns the field or property to read.</param>
        /// <returns>Returns the value of the field or property.</returns>
        public object GetValue(object target)
        {
            return Field != null ? Field.GetValue(target) : Property.GetValue(target);
        }

        /// <summary>
        /// Sets the value of this field or property.
        /// </summary>
        /// <param name="target">The object that owns the field or property to set.</param>
        /// <param name="value">The value you want to set for the field or property.</param>
        public void SetValue(object target, object value)
        {
            if (Field != null)
            {
                Field.SetValue(target, value);
            }
            else if (Property != null)
            {
                Property.SetValue(target, value);
            }
        }

        /// <summary>
        /// Gets a custom attribute on the current field or property.
        /// </summary>
        /// <param name="attributeType">The type of the attribute.</param>
        /// <param name="inherit">If enabled, checks if the attribute exists on the element's ancestors.</param>
        /// <returns>Returns the found attribute.</returns>
        public Attribute GetCustomAttribute(Type attributeType, bool inherit = false)
        {
            if (Field != null)
            {
                return Field.GetCustomAttribute(attributeType, inherit);
            }
            else if (Property != null)
            {
                return Property.GetCustomAttribute(attributeType, inherit);
            }

            return null;
        }

        /// <param name="attribute">Outputs the found attribute.</param>
        /// <returns>Returns true if the expected attribute has been found on the element.</returns>
        /// <inheritdoc cref="GetCustomAttribute(Type, bool)"/>
        public bool GetCustomAttribute(Type attributeType, out Attribute attribute, bool inherit = false)
        {
            attribute = GetCustomAttribute(attributeType, inherit);
            return attribute != null;
        }

        /// <typeparam name="T">The type of the attribute.</typeparam>
        /// <inheritdoc cref="GetCustomAttribute(Type, bool)"/>
        public T GetCustomAttribute<T>(bool inherit = false)
            where T : Attribute
        {
            return GetCustomAttribute(typeof(T), inherit) as T;
        }

        /// <inheritdoc cref="GetCustomAttribute(Type, out Attribute, bool)"/>
        /// <inheritdoc cref="GetCustomAttribute{T}(bool)"/>
        public bool GetCustomAttribute<T>(out T attribute, bool inherit = false)
            where T : Attribute
        {
            attribute = GetCustomAttribute<T>(inherit);
            return attribute != null;
        }

        /// <summary>
        /// Gets all custom attributes on the element.
        /// </summary>
        /// <returns>Returns all custom attributes found on the element.</returns>
        /// <inheritdoc cref="GetCustomAttribute(Type, bool)"/>
        public Attribute[] GetCustomAttributes(bool inherit = false)
        {
            List<Attribute> attributes = new List<Attribute>();

            if (Field != null)
            {
                foreach (Attribute attr in Field.GetCustomAttributes(inherit))
                {
                    attributes.Add(attr);
                }
            }

            if (Property != null)
            {
                foreach (Attribute attr in Property.GetCustomAttributes(inherit))
                {
                    attributes.Add(attr);
                }
            }

            return attributes.ToArray();
        }

        /// <summary>
        /// Checks if field or property has the given custom attribute.
        /// </summary>
        /// <inheritdoc cref="GetCustomAttribute(Type, out Attribute, bool)"/>
        public bool HasCustomAttribute(Type attributeType, bool inherit = false)
        {
            return GetCustomAttribute(attributeType, inherit) != null;
        }

        /// <inheritdoc cref="HasCustomAttribute(Type, bool)"/>
        /// <inheritdoc cref="GetCustomAttribute{T}(out T, bool)"/>
        public bool HasCustomAttribute<T>(bool inherit = false)
            where T : Attribute
        {
            return GetCustomAttribute<T>(inherit) != null;
        }

        #endregion

    }

}