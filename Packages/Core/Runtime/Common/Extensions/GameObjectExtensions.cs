/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Core
{

    /// <summary>
    /// Extension functions for <see cref="GameObject"/> instances.
    /// </summary>
    /// <remarks>@todo Move these functions to core library.</remarks>
    public static class GameObjectExtensions
    {

        #region Public API

        /// <inheritdoc cref="RuntimeObjectUtility.IsPrefab(GameObject)"/>
        public static bool IsPrefab(this GameObject gameObject)
        {
            return RuntimeObjectUtility.IsPrefab(gameObject);
        }

        /// <returns>Returns the object bounds.</returns>
        /// <inheritdoc cref="GetBounds(GameObject, out Bounds, bool, bool)"/>
        public static Bounds GetBounds(this GameObject gameObject, bool preferCollider = false, bool includeChildren = true)
        {
            return GetBounds(gameObject, out Bounds bounds, preferCollider, includeChildren) ? bounds : default;
        }

        /// <summary>
        /// Gets the bounds of an object, using its renderer or collider.
        /// </summary>
        /// <param name="gameObject">The object to get the bounds.</param>
        /// <param name="bounds">Outputs the object bounds.</param>
        /// <param name="preferCollider">If enabled, try to get the object bounds from the collider first, instead of the renderer
        /// first.</param>
        /// <param name="includeChildren">If enabled and the given object doesn't have a component that may contain bounds, this function
        /// will search for that component in children.</param>
        /// <returns>Returns true if the object bounds have been found.</returns>
        public static bool GetBounds(this GameObject gameObject, out Bounds bounds, bool preferCollider = false, bool includeChildren = true)
        {
            if (preferCollider)
            {
                if (gameObject.TryGetComponent(out Collider collider) && ColliderExtensions.GetColliderBounds(collider, out bounds))
                    return true;
                else if (gameObject.TryGetComponent(out Collider2D collider2D) && Collider2DExtensions.GetColliderBounds(collider2D, out bounds))
                    return true;
                else if (gameObject.TryGetComponent(out Renderer renderer) && RendererExtensions.GetRendererBounds(renderer, out bounds))
                    return true;
            }
            else
            {
                if (gameObject.TryGetComponent(out Renderer renderer) && RendererExtensions.GetRendererBounds(renderer, out bounds))
                    return true;
                else if (gameObject.TryGetComponent(out Collider collider) && ColliderExtensions.GetColliderBounds(collider, out bounds))
                    return true;
                else if (gameObject.TryGetComponent(out Collider2D collider2D) && Collider2DExtensions.GetColliderBounds(collider2D, out bounds))
                    return true;
            }

            if (includeChildren)
            {
                if (preferCollider)
                {
                    Collider collider = gameObject.GetComponentInChildren<Collider>();
                    if (collider != null && ColliderExtensions.GetColliderBounds(collider, out bounds))
                        return true;

                    Collider2D collider2D = gameObject.GetComponentInChildren<Collider2D>();
                    if (collider2D != null && Collider2DExtensions.GetColliderBounds(collider2D, out bounds))
                        return true;

                    Renderer renderer = gameObject.GetComponentInChildren<Renderer>();
                    if (renderer != null && RendererExtensions.GetRendererBounds(renderer, out bounds))
                        return true;
                }
               else
                {
                    Renderer renderer = gameObject.GetComponentInChildren<Renderer>();
                    if (renderer != null && RendererExtensions.GetRendererBounds(renderer, out bounds))
                        return true;

                    Collider collider = gameObject.GetComponentInChildren<Collider>();
                    if (collider != null && ColliderExtensions.GetColliderBounds(collider, out bounds))
                        return true;

                    Collider2D collider2D = gameObject.GetComponentInChildren<Collider2D>();
                    if (collider2D != null && Collider2DExtensions.GetColliderBounds(collider2D, out bounds))
                        return true;
                }
            }

            bounds = default;
            return false;
        }

        #endregion

    }

}