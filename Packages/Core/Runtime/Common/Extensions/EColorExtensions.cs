/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Core
{

    /// <summary>
    /// Extension functions for <see cref="EColor"/> values.
    /// </summary>
    public static class EColorExtensions
    {

        public const byte MaxColorValue = 255;
        public const byte HalfColorValue = MaxColorValue / 2;

        /// <summary>
        /// Gets the color value from this color enum.
        /// </summary>
        /// <param name="color">The enum value of which you want to get the color.</param>
        /// <param name="ignoreAlpha">If enabled, returns a color with 100% alpha.</param>
        /// <returns>Returns the processed color.</returns>
        public static Color ToColor(this EColor color, bool ignoreAlpha = false)
        {
            Color output = Color.clear;
            // Cancel if no flag is enabled on the color value
            if (color == 0)
            {
                return output;
            }

            if (color.HasFlag(EColor.Red))
            { output.r = 1f; }
            else if (color.HasFlag(EColor.Maroon))
            { output.r = .5f; }

            if (color.HasFlag(EColor.Green))
            { output.g = 1f; }
            else if (color.HasFlag(EColor.Lime))
            { output.g = .5f; }

            if (color.HasFlag(EColor.Blue))
            { output.b = 1f; }
            else if (color.HasFlag(EColor.Navy))
            { output.b = .5f; }

            if (ignoreAlpha)
            {
                output.a = 1f;
            }
            else
            {
                if (color.HasFlag(EColor.Alpha0))
                { output.a = 0f; }
                else if (color.HasFlag(EColor.Alpha12))
                { output.a = 1f / 8; }
                else if (color.HasFlag(EColor.Alpha25))
                { output.a = 1f / 4; }
                else if (color.HasFlag(EColor.Alpha50))
                { output.a = 1f / 2; }
                else if (color.HasFlag(EColor.Alpha75))
                { output.a = .75f; }
                else if (color.HasFlag(EColor.Alpha87))
                { output.a = .87f; }
                else if (color.HasFlag(EColor.Alpha100))
                { output.a = 1f; }
            }

            return output;
        }

        /// <inheritdoc cref="ToColor(EColor, bool)"/>
        /// <param name="alpha">The fixed alpha value (between 0 and 1) of the color (ignoring the alpha value from the enum).</param>
        public static Color ToColor(this EColor color, float alpha)
        {
            Color output = color.ToColor(true);
            output.a = alpha;
            return output;
        }

        /// <inheritdoc cref="ToColor(EColor, bool)"/>
        public static Color32 ToColor32(this EColor color, bool ignoreAlpha = false)
        {
            Color32 output = new Color32(0, 0, 0, 0);
            // Cancel if no flag is enabled on the color value
            if (color == 0)
            {
                return output;
            }

            if (color.HasFlag(EColor.Red))
            { output.r = MaxColorValue; }
            else if (color.HasFlag(EColor.Maroon))
            { output.r = HalfColorValue; }

            if (color.HasFlag(EColor.Green))
            { output.g = MaxColorValue; }
            else if (color.HasFlag(EColor.Lime))
            { output.g = HalfColorValue; }

            if (color.HasFlag(EColor.Blue))
            { output.b = MaxColorValue; }
            else if (color.HasFlag(EColor.Navy))
            { output.b = HalfColorValue; }

            if (ignoreAlpha)
            {
                output.a = MaxColorValue;
            }
            else
            {
                if (color.HasFlag(EColor.Alpha0))
                { output.a = 0; }
                else if (color.HasFlag(EColor.Alpha12))
                { output.a = MaxColorValue / 8; }
                else if (color.HasFlag(EColor.Alpha25))
                { output.a = MaxColorValue / 4; }
                else if (color.HasFlag(EColor.Alpha50))
                { output.a = MaxColorValue / 2; }
                else if (color.HasFlag(EColor.Alpha75))
                { output.a = (byte)(MaxColorValue * .75f); }
                else if (color.HasFlag(EColor.Alpha87))
                { output.a = (byte)(MaxColorValue * .87f); }
                else if (color.HasFlag(EColor.Alpha100))
                { output.a = MaxColorValue; }
            }

            return output;
        }

        /// <inheritdoc cref="ToColor(EColor, float)"/>
        /// <param name="alpha">The fixed alpha value (between 0 and 255) of the color (ignoring the alpha value from the enum).</param>
        public static Color32 ToColor32(this EColor color, byte alpha)
        {
            Color32 output = color.ToColor32(true);
            output.a = alpha;
            return output;
        }

        /// <inheritdoc cref="ToColor(EColor, float)"/>
        public static Color32 ToColor32(this EColor color, float alpha)
        {
            return color.ToColor32((byte)(alpha != 0 ? (MaxColorValue / alpha) : 0));
        }

    }

}