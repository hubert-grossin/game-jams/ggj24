/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using UnityEngine;

namespace SideXP.Core
{

    /// <summary>
    /// Extension functions for <see cref="Transform"/> instances.
    /// </summary>
    public static class TransformExtensions
    {

        /// <inheritdoc cref="TransformUtility.ForEachChild(Transform, Action{Transform})"/>
        public static void ForEachChild(this Transform transform, Action<Transform> action)
        {
            TransformUtility.ForEachChild(transform, action);
        }

        /// <inheritdoc cref="TransformUtility.GetChildren(Transform)"/>
        public static Transform[] GetChildren(this Transform transform)
        {
            return TransformUtility.GetChildren(transform);
        }

        /// <inheritdoc cref="TransformUtility.ClearHierarchy(Transform)"/>
        public static void ClearHierarchy(this Transform transform)
        {
            TransformUtility.ClearHierarchy(transform);
        }

        /// <inheritdoc cref="TransformUtility.RemoveChildrenOfType(Transform, Type)"/>
        public static int RemoveChildrenOfType(this Transform transform, Type componentType)
        {
            return TransformUtility.RemoveChildrenOfType(transform, componentType);
        }

        /// <inheritdoc cref="TransformUtility.RemoveChildrenOfType{T}(Transform)"/>
        public static int RemoveChildrenOfType<T>(this Transform transform)
            where T : Component
        {
            return TransformUtility.RemoveChildrenOfType<T>(transform);
        }

    }

}