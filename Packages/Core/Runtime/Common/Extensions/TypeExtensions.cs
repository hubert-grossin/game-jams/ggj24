/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using SideXP.Core.Reflection;

namespace SideXP.Core
{

    /// <summary>
    /// Extension functions for <see cref="Type"/> instances.
    /// </summary>
    public static class TypeExtension
    {

        /// <summary>
        /// Gets the full <see cref="Type"/> name string, including the assmebly name. The output string looks like: "TypeNamespace.TypeName, AssemblyName, Version=0.0.0.0, Culture=neutral, PublicKeyKoken=null"
        /// </summary>
        /// <param name="type">The <see cref="Type"/> of which you want to get the full name string.</param>
        /// <returns>Returns the computed full name string, or null if the given <see cref="Type"/> is null.</returns>
        public static string GetFullNameWithAssembly(this Type type)
        {
            return type != null ? $"{type.FullName}, {type.Assembly}" : null;
        }

        /// <inheritdoc cref="ReflectionUtility.GetCustomAttribute(Type, Type, out Attribute)"/>
        public static bool GetCustomAttribute(this Type type, Type attributeType, out Attribute attribute)
        {
            return ReflectionUtility.GetCustomAttribute(type, attributeType, out attribute);
        }

        /// <inheritdoc cref="ReflectionUtility.GetCustomAttribute{T}(Type, out T)"/>
        public static bool GetCustomAttribute<T>(this Type type, out T attribute)
            where T : Attribute
        {
            return ReflectionUtility.GetCustomAttribute(type, out attribute);
        }

        /// <inheritdoc cref="ReflectionUtility.HasAttribute(Type, Type)"/>
        public static bool HasAttribute(this Type type, Type attributeType)
        {
            return ReflectionUtility.HasAttribute(type, attributeType);
        }

        /// <inheritdoc cref="ReflectionUtility.HasAttribute{T}(Type)"/>
        public static bool HasAttribute<T>(this Type type)
            where T : Attribute
        {
            return ReflectionUtility.HasAttribute<T>(type);
        }

    }

}