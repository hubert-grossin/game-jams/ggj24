/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Core
{

    /// <summary>
    /// Miscellaneous functions and values for drawing user interfaces.
    /// </summary>
    public static class MoreGUI
    {

        #region Fields

        public const float HeightXL = 48f;
        public const float HeightL = 36f;
        public const float HeightM = 28f;
        public const float HeightS = 20f;
        public const float HeightXS = 16f;

        public const float WidthXL = 200f;
        public const float WidthL = 148f;
        public const float WidthM = 112f;
        public const float WidthS = 80f;
        public const float WidthXS = 40f;

        /// <summary>
        /// Size of an horizontal margin.
        /// </summary>
        public const float HMargin = 2f;

        /// <summary>
        /// Size of a vertical margin.
        /// </summary>
        public const float VMargin = 2f;

        public static readonly GUILayoutOption HeightOptXL = GUILayout.Height(HeightXL);
        public static readonly GUILayoutOption HeightOptL = GUILayout.Height(HeightL);
        public static readonly GUILayoutOption HeightOptM = GUILayout.Height(HeightM);
        public static readonly GUILayoutOption HeightOptS = GUILayout.Height(HeightS);
        public static readonly GUILayoutOption HeightOptXS = GUILayout.Height(HeightXS);

        public static readonly GUILayoutOption WidthOptXL = GUILayout.Width(WidthXL);
        public static readonly GUILayoutOption WidthOptL = GUILayout.Width(WidthL);
        public static readonly GUILayoutOption WidthOptM = GUILayout.Width(WidthM);
        public static readonly GUILayoutOption WidthOptS = GUILayout.Width(WidthS);
        public static readonly GUILayoutOption WidthOptXS = GUILayout.Width(WidthXS);

        #endregion

    }

}