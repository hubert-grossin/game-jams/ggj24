/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine.Events;

namespace SideXP.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/> for bool values.
    /// </summary>
    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }

}