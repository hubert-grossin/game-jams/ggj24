/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace SideXP.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/> for <see cref="Vector2Int"/> values.
    /// </summary>
    [System.Serializable]
    public class Vector2IntEvent : UnityEvent<Vector2Int> { }

}