/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine.Events;

namespace SideXP.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/> for float values.
    /// </summary>
    [System.Serializable]
    public class FloatEvent : UnityEvent<float> { }

}