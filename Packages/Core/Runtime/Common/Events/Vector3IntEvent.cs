/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace SideXP.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/> for <see cref="Vector3Int"/> values.
    /// </summary>
    [System.Serializable]
    public class Vector3IntEvent : UnityEvent<Vector3Int> { }

}