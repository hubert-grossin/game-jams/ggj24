/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace SideXP.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/> for <see cref="Transform"/> instances.
    /// </summary>
    [System.Serializable]
    public class TransformEvent : UnityEvent<Transform> { }

}