/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace SideXP.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/> for <see cref="Object"/> instances.
    /// </summary>
    [System.Serializable]
    public class ObjectEvent : UnityEvent<Object> { }

}