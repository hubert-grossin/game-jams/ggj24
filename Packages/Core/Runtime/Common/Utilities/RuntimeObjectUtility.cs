/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Core
{

    /// <summary>
    /// Miscellaneous functions for working with <see cref="Object"/>s and assets at runtime.<br/>
    /// </summary>
    public static class RuntimeObjectUtility
    {

        /// <summary>
        /// Gets the <see cref="Transform"/> component of an object that can be placed in a scene.
        /// </summary>
        /// <param name="obj">The object from which you want to get the <see cref="Transform"/> component.</param>
        /// <returns>Returns the found <see cref="Transform"/> component.</returns>
        public static Transform GetTransform(Object obj)
        {
            if (obj is GameObject go)
            {
                return go.transform;
            }
            else if (obj is Component comp)
            {
                return comp.transform;
            }
            else
            {
                return null;
            }
        }

        /// <inheritdoc cref="GetTransform(Object)"/>
        /// <param name="transform">Outputs the found <see cref="Transform"/> component.</param>
        /// <returns>Returns true if the given object has a <see cref="Transform"/> component.</returns>
        public static bool GetTransform(Object obj, out Transform transform)
        {
            transform = GetTransform(obj);
            return transform != null;
        }

        /// <summary>
        /// Gets the <see cref="GameObject"/> to which the given object is attached.
        /// </summary>
        /// <param name="obj">The object of which you want to get the attached <see cref="GameObject"/>.</param>
        /// <returns>Returns the found attached <see cref="GameObject"/>.</returns>
        public static GameObject GetGameObject(Object obj)
        {
            if (obj is GameObject go)
            {
                return go;
            }
            else if (obj is Component comp)
            {
                return comp.gameObject;
            }
            else
            {
                return null;
            }
        }

        /// <inheritdoc cref="GetGameObject(Object)"/>
        /// <param name="gameObject">Outputs the found attached <see cref="GameObject"/>.</param>
        /// <returns>Returns true if the given object is attached to a <see cref="GameObject"/>.</returns>
        public static bool GetGameObject(Object obj, out GameObject gameObject)
        {
            gameObject = GetGameObject(obj);
            return gameObject != null;
        }

        /// <summary>
        /// Checks if a <see cref="GameObject"/> is a prefab at runtime.
        /// </summary>
        /// <param name="gameObject">The <see cref="GameObject"/> to check.</param>
        /// <returns>Returns true if the given object is a prefab.</returns>
        public static bool IsPrefab(GameObject gameObject)
        {
            return gameObject.scene.name == null;
        }

        /// <summary>
        /// Calls <see cref="Object.Destroy(Object)"/> or <see cref="Object.DestroyImmediate(Object, bool)"/> depending on the player state.
        /// </summary>
        /// <inheritdoc cref="Object.DestroyImmediate(Object, bool)"/>
        public static bool Destroy(Object obj, bool allowDestroyingAssets = false)
        {
            if (obj == null)
                return false;

            if (Application.isPlaying)
                Object.Destroy(obj);
            else
                Object.DestroyImmediate(obj, allowDestroyingAssets);

            return true;
        }

    }

}