/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;
using System.Collections.Generic;

using UnityEngine;

using Object = UnityEngine.Object;

namespace SideXP.Core
{

    /// <summary>
    /// Miscellaneous functions for working with <see cref="Transform"/> component.
    /// </summary>
    public static class TransformUtility
    {

        /// <summary>
        /// Performs an operation for each child of a given <see cref="Transform"/> (excluding itself). Note that this function iterates only over the direct children, not recursively in the hierarchy.
        /// </summary>
        /// <param name="transform">The <see cref="Transform"/> component to process.</param>
        /// <param name="action">The operation to perform for each child.</param>
        public static void ForEachChild(Transform transform, Action<Transform> action)
        {
            foreach (Transform child in transform)
            {
                if (child == transform)
                    continue;

                action(child);
            }
        }

        /// <summary>
        /// Gets all the children of a given <see cref="Transform"/> (excluding itself). Note that this function iterates only over the direct children, not recursively in the hierarchy.
        /// </summary>
        /// <inheritdoc cref="ForEachChild(Transform, Action{Transform})"/>
        public static Transform[] GetChildren(Transform transform)
        {
            List<Transform> children = new List<Transform>();
            ForEachChild(transform, child => children.Add(child));
            return children.ToArray();
        }

        /// <summary>
        /// Destroy all transform in the given one's hierarchy.
        /// </summary>
        /// <inheritdoc cref="ForEachChild(Transform, Action{Transform})"/>
        public static void ClearHierarchy(Transform transform)
        {
            ForEachChild(transform, child =>
            {
                if (Application.isPlaying)
                    Object.Destroy(child.gameObject);
                else
                    Object.DestroyImmediate(child.gameObject);
            });
        }

        /// <summary>
        /// Removes all children in the hierarchy if they have a given component attached.
        /// </summary>
        /// <param name="transform">The transform of which to clean the hierarchy.</param>
        /// <param name="componentType">The component type of the children to remove.</param>
        /// <returns>Returns the number of removed children.</returns>
        public static int RemoveChildrenOfType(Transform transform, Type componentType)
        {
            int count = 0;
            ForEachChild(transform, child =>
            {
                if (child.TryGetComponent(componentType, out Component _))
                {
                    child.gameObject.Destroy();
                    count++;
                }
            });
            return count;
        }

        /// <typeparam name="T">The component type of the children to remove.</typeparam>
        /// <inheritdoc cref="RemoveChildrenOfType(Transform, Type)"/>
        public static int RemoveChildrenOfType<T>(Transform transform)
            where T : Component
        {
            return RemoveChildrenOfType(transform, typeof(T));
        }

    }

}