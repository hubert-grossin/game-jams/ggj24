/**
* Sideways Experiments (c) 2023
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/


using System;

using UnityEngine;

namespace SideXP.Core
{

    /// <summary>
    /// Represents a collection of features meant for composite assets.<br>
    /// When an asset contains a property of this type, features are attached as subassets to the main one. If not, features are created at
    /// the root of the project by default.
    /// </summary>
    /// <remarks>A feature of a given type can only appear once in this collection.</remarks>
    [System.Serializable]
    public abstract class FeaturesCollection
    {

        #region Fields

        [SerializeField]
        [Tooltip("The features attached to the main object.")]
        private ScriptableObject[] _features = { };

        #endregion


        #region Public API

        public virtual Type FeaturesBaseType => typeof(ScriptableObject);

        /// <inheritdoc cref="_features"/>
        public ScriptableObject[] Features => _features;

        #endregion

    }

}