/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using UnityEngine;

namespace SideXP.Core
{

    /// <summary>
    /// Represents an asset meant to contain a given type of asset as subassets.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    public abstract class SubassetsContainerAsset<T> : ScriptableObject
        where T : ScriptableObject
    {

#if !SIDEXP_LIB_PROJECT
        [HideInInspector]
#endif
        [SerializeField]
        [Tooltip("The list of subassets.")]
        private T[] _elements = { };

        /// <inheritdoc cref="_elements"/>
        public T[] Elements => _elements;

        /// <summary>
        /// Gets the type of an expected subasset element of this container.
        /// </summary>
        public Type ElementType => typeof(T);

    }

}