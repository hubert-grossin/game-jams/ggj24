/**
* Sideways Experiments (c) 2023
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace SideXP.Core
{

    /// <inheritdoc cref="FeaturesCollection"/>
    /// <typeparam name="TBaseType">The base type of the features in this list.</typeparam>
    [System.Serializable]
    public class FeaturesCollectionGeneric<TBaseType> : FeaturesCollection, IEnumerable<TBaseType>
        where TBaseType : ScriptableObject
    {

        #region Public API

        /// <inheritdoc cref="_features"/>
        public new TBaseType[] Features
        {
            get
            {
                List<TBaseType> features = new List<TBaseType>();
                foreach (ScriptableObject f in base.Features)
                {
                    TBaseType typedFeature = f as TBaseType;
                    if(typedFeature != null)
                        features.Add(typedFeature);
                }
                return features.ToArray();
            }
        }

        /// <summary>
        /// Gets a feature by its type in this collection.
        /// </summary>
        /// <param name="featureType">The type of the feature to get.</param>
        /// <param name="inherit">If enabled, this function can also return features that inherit from the given type.</param>
        /// <returns>Returns the found feature asset.</returns>
        public TBaseType GetFeature(Type featureType, bool inherit = false)
        {
            foreach (ScriptableObject feature in base.Features)
            {
                if ((inherit && featureType.IsAssignableFrom(feature.GetType())) || feature.GetType() == featureType)
                    return feature as TBaseType;
            }
            return null;
        }

        /// <param name="feature">Outputs the found feature asset.</param>
        /// <returns>Returns true if a feature asset has been found.</returns>
        /// <inheritdoc cref="GetFeature(Type, bool)"/>
        public bool GetFeature(Type featureType, out TBaseType feature, bool inherit = false)
        {
            feature = GetFeature(featureType, inherit);
            return feature != null;
        }

        /// <typeparam name="TFeatureType">The type of the feature to get.</typeparam>
        /// <inheritdoc cref="GetFeature(Type, bool)"/>
        public TFeatureType GetFeature<TFeatureType>(bool inherit = false)
            where TFeatureType : TBaseType
        {
            return GetFeature(typeof(TFeatureType), inherit) as TFeatureType;
        }

        /// <inheritdoc cref="GetFeature(Type, out TBaseType, bool)"/>
        /// <inheritdoc cref="GetFeature{TFeatureType}(bool)"/>
        public bool GetFeature<TFeatureType>(out TFeatureType feature, bool inherit = false)
            where TFeatureType : TBaseType
        {
            feature = GetFeature<TFeatureType>(inherit);
            return feature != null;
        }

        /// <summary>
        /// Iterates through features list.
        /// </summary>
        public IEnumerator<TBaseType> GetEnumerator()
        {
            return ((IEnumerable<TBaseType>)Features).GetEnumerator();
        }

        /// <inheritdoc cref="GetEnumerator"/>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return Features.GetEnumerator();
        }

        #endregion

    }

}