/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Core
{

    /// <summary>
    /// Draws an animation curve field with custom settings (using custom property drawer).
    /// </summary>
    public class AnimCurveAttribute : FieldOrPropertyAttribute
    {

        #region Fields

        /// <summary>
        /// The minimum time value (along the X axis).
        /// </summary>
        public float MinTime = 0f;

        /// <summary>
        /// The maximum time value (along the X axis).
        /// </summary>
        public float MaxTime = 1f;

        /// <summary>
        /// The minimum curve value (along the Y axis).
        /// </summary>
        public float MinValue = 0f;

        /// <summary>
        /// The maximum curve value (along the Y axis).
        /// </summary>
        public float MaxValue = 1f;

        /// <summary>
        /// The color of the curve in the inspector.
        /// </summary>
        public Color CurveColor = Color.green;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="AnimCurveAttribute(float, float, float, float, EColor)"/>
        public AnimCurveAttribute() { }

        /// <summary>
        /// Clamps the animation curve between 0 and 1 for both time and value.
        /// </summary>
        /// <inheritdoc cref="AnimCurveAttribute(float, float, float, float, EColor)"/>
        public AnimCurveAttribute(EColor color)
        {
            CurveColor = color.ToColor(true);
        }

        /// <summary>
        /// Clamps the animation curve between 0 and the given maximums for both time and value.
        /// </summary>
        /// <inheritdoc cref="AnimCurveAttribute(float, float, float, float, EColor)"/>
        public AnimCurveAttribute(float maxTime, float maxValue, EColor color = EColor.Green)
        {
            MaxTime = maxTime;
            MaxValue = maxValue;
            CurveColor = color.ToColor(true);
        }

        /// <summary>
        /// Clamps the animation curve between the given values, for both time and value.
        /// </summary>
        /// <param name="minTime">The minimum value along the X axis.</param>
        /// <param name="maxTime">The maximum value along the X axis.</param>
        /// <param name="minValue">The minimum value along the Y axis.</param>
        /// <param name="maxValue">The maximum value along the Y axis.</param>
        /// <param name="color">The color of the curve in the inspector.</param>
        public AnimCurveAttribute(float minTime, float maxTime, float minValue, float maxValue, EColor color = EColor.Green)
        {
            MinTime = minTime;
            MaxTime = maxTime;
            MinValue = minValue;
            MaxValue = maxValue;
            CurveColor = color.ToColor(true);
        }

        #endregion


        #region Public API

        /// <summary>
        /// Creates a <see cref="Rect"/> representing the curve's ranges:
        ///     - x is min time
        ///     - y is min value
        ///     - width is (abs(min time) + abs(max time)
        ///     - height is (abs(min value) + abs(max value)
        /// </summary>
        public Rect Ranges
        {
            get
            {
                return new Rect
                (
                    MinTime,
                    MinValue,
                    Mathf.Abs(MinTime) + Mathf.Abs(MaxTime),
                    Mathf.Abs(MinValue) + Mathf.Abs(MaxValue)
                );
            }
        }

        #endregion

    }

}