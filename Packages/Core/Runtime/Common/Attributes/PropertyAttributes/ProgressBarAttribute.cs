/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Core
{

    /// <summary>
    /// Draws a progress bar next to a numeric field in the inspector.
    /// </summary>
    public class ProgressBarAttribute : FieldOrPropertyAttribute
    {

        #region Fields

        public const EColor DefaultColor = EColor.Cyan | EColor.Alpha100;

        /// <summary>
        /// The minimum value of the progress bar.
        /// </summary>
        public float Min = 0f;

        /// <summary>
        /// The maximum value of the progress bar.
        /// </summary>
        public float Max = 100f;

        /// <summary>
        /// The prefix to write before the value on the progress bar.
        /// </summary>
        public string Prefix = string.Empty;

        /// <summary>
        /// The suffix to write after the value on the progress bar.
        /// </summary>
        public string Suffix = string.Empty;

        /// <summary>
        /// The color of the progress bar.
        /// </summary>
        public Color Color = Color.blue;

        /// <summary>
        /// If enabled, the field's value will be clamped between min and max.
        /// </summary>
        public bool Clamp = false;

        /// <summary>
        /// If enabled, the field is completely hidden by the progress bar.
        /// </summary>
        public bool ReplaceField = false;

        /// <summary>
        /// Should the field be readonly?
        /// </summary>
        public bool Readonly = false;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Displays a progress bar that represents a value from 0 to 1.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(EColor color = DefaultColor, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, 1, null, null, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value from 0 to 1.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(string suffix, EColor color = DefaultColor, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, 1, null, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value from 0 to 1.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(string prefix, string suffix, EColor color = DefaultColor, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, 1, prefix, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between minimum and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float min, float max, string prefix, string suffix, EColor color = DefaultColor, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(min, max, prefix, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between minimum and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float min, float max, string suffix, EColor color = DefaultColor, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(min, max, null, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between minimum and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float min, float max, EColor color = DefaultColor, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(min, max, null, null, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between 0 and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float max, string prefix, string suffix, EColor color = DefaultColor, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, max, prefix, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between 0 and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float max, EColor color = DefaultColor, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, max, null, null, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between 0 and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float max, string suffix, EColor color = DefaultColor, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, max, null, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Initializes the values of this attribute.
        /// </summary>
        /// <param name="min">The minimum value of the progress bar.</param>
        /// <param name="max">The maximum value of the progress bar.</param>
        /// <param name="prefix">The prefix to write before the value on the progress bar.</param>
        /// <param name="suffix">The suffix to write after the value on the progress bar.</param>
        /// <param name="color">The color of the progress bar.</param>
        /// <param name="clamp">If enabled, the field's value will be clamped between min and max.</param>
        /// <param name="replaceField">If enabled, the field is completely hidden by the progress bar.</param>
        /// <param name="readonlyField">Should the field be readonly?</param>
        private void Init(float min, float max, string prefix, string suffix, EColor color, bool clamp, bool replaceField, bool readonlyField)
        {
            Min = Mathf.Min(min, max);
            Max = Mathf.Max(min, max);
            Prefix = !string.IsNullOrEmpty(prefix) ? prefix : string.Empty;
            Suffix = !string.IsNullOrEmpty(suffix) ? suffix : string.Empty;
            Color = color.ToColor(true);
            Clamp = clamp;
            ReplaceField = replaceField;
            Readonly = readonlyField;
        }

        #endregion


        #region Public API

        /// <summary>
        /// Gets the label of the progress bar, using the defined prefix and suffix.
        /// </summary>
        /// <param name="value">The current value of the field.</param>
        /// <returns>Returns the computed label.</returns>
        public string GetLabel(float value)
        {
            return $"{Prefix}{value}{Suffix}";
        }

        #endregion

    }

}