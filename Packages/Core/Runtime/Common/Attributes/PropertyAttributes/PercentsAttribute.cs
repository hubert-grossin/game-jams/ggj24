/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

namespace SideXP.Core
{

    /// <summary>
    /// Displays a value in percents on GUI, but remap it between 0 and 1 in the target field.
    /// </summary>s
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true)]
    public class PercentsAttribute : RemapAttribute
    {

        /// <inheritdoc cref="PercentsAttribute"/>
        public PercentsAttribute()
            : base(0, 100, 0, 1, "%") { }

    }

}