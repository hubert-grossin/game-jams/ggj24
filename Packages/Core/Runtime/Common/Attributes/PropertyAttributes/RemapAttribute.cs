/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

namespace SideXP.Core
{

    /// <summary>
    /// Displays a value on GUI, but remap this value in the target field.
    /// </summary>s
    [AttributeUsage(AttributeTargets.Field, Inherited = true)]
    public class RemapAttribute : FieldOrPropertyAttribute
    {

        #region Fields

        /// <summary>
        /// The minium value displayed on GUI.
        /// </summary>
        public float FromMin = 0f;

        /// <summary>
        /// The maximum value displayed on GUI.
        /// </summary>
        public float FromMax = 100f;

        /// <summary>
        /// The minimum field value.
        /// </summary>
        public float ToMin = 0f;

        /// <summary>
        /// The maximum field value.
        /// </summary>
        public float ToMax = 1f;

        /// <summary>
        /// The unit to display on GUI.
        /// </summary>
        public string Units = string.Empty;

        /// <summary>
        /// If enabled, the value is clamped between its allowed bounds.
        /// </summary>
        public bool Clamped = false;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="RemapAttribute(float, float, float, float, string)"/>
        public RemapAttribute(float fromMin, float fromMax, float toMin, float toMax, bool clamped = false)
        {
            FromMin = fromMin;
            FromMax = fromMax;
            ToMin = toMin;
            ToMax = toMax;
            Clamped = clamped;
        }

        /// <inheritdoc cref="RemapAttribute"/>
        /// <param name="fromMin">The minium value displayed on GUI.</param>
        /// <param name="fromMax">The maximum value displayed on GUI.</param>
        /// <param name="toMin">The minimum field value.</param>
        /// <param name="toMax">The maximum field value.</param>
        /// <param name="units">The unit to display on GUI.</param>
        /// <param name="clamped">If enabled, the value is clamped between its allowed bounds.</param>
        public RemapAttribute(float fromMin, float fromMax, float toMin, float toMax, string units, bool clamped = false)
        {
            FromMin = fromMin;
            FromMax = fromMax;
            ToMin = toMin;
            ToMax = toMax;
            Units = units;
            Clamped = clamped;
        }

        #endregion

    }

}