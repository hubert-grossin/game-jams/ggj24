/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Core
{

    /// <summary>
    /// Draws a separator line before the next property (using custom property drawer).
    /// </summary>
    public class SeparatorAttribute : FieldOrPropertyAttribute
    {

        /// <summary>
        /// If enabled, the separator will have the exact current view width.
        /// </summary>
        public bool Wide = false;

        /// <inheritdoc cref="SeparatorAttribute"/>
        /// <param name="wide">If enabled, the separator will have the exact current view width.</param>
        public SeparatorAttribute(bool wide = false)
        {
            Wide = wide;
        }

    }

}