/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

namespace SideXP.Core
{

    /// <summary>
    /// Makes a property readonly, so it's visible but not editable in the inspector (using custom property drawer).
    /// </summary>
    public class ReadonlyAttribute : FieldOrPropertyAttribute { }

}