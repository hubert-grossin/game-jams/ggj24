/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using UnityEngine;

namespace SideXP.Core
{

    /// <summary>
    /// Overrides <see cref="PropertyAttribute"/> in order to set attributes meant to have custom property drawers also allowed on properties.
    /// </summary>s
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public abstract class FieldOrPropertyAttribute : PropertyAttribute { }

}