/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Core
{

    /// <summary>
    /// Makes a field enabled only if a named boolean property is not checked.
    /// </summary>
    public class IfNotCheckedAttribute : FieldOrPropertyAttribute
    {

        /// <summary>
        /// The name of the property that shouldn't be checked..
        /// </summary>
        public string PropertyName { get; private set; }

        /// <summary>
        /// If enabled, the field is hidden if the condition is not fulfilled, instead of being just disabled.
        /// </summary>
        public bool HideIfDisabled { get; set; } = false;

        /// <inheritdoc cref="IfCheckedAttribute"/>
        /// <param name="propertyName">The name of the property that shouldn't be checked.</param>
        public IfNotCheckedAttribute(string propertyName)
        {
            PropertyName = propertyName;
        }

    }

}