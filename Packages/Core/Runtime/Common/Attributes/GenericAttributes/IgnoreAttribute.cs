/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

namespace SideXP.Core
{

    /// <summary>
    /// Marks an object ignored in a specific context.
    /// </summary>
    public class IgnoreAttribute : Attribute { }

}