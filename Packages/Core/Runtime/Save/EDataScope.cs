/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Core
{

    /// <summary>
    /// Defines a scope for a data, in order to filter or sort it when displaying it in an inspector or saving it into files or prefs.
    /// </summary>
    public enum EDataScope
    {

        /// <summary>
        /// The data is meant to be displayed in game and saved in game files.
        /// </summary>
        Player = 0,

        /// <summary>
        /// Only for editor context. The data should be displayed in Project Settings window, and shared with other members of the
        /// development team.
        /// </summary>
        Project = 1,

        /// <summary>
        /// Only for editor context. The data should be displayed in Preferences window, and available only for the current user.
        /// </summary>
        User = 2,

        /// <summary>
        /// The data is meant to be hidden, and saved into temporary folders.
        /// </summary>
        Temp = 3

    }

}