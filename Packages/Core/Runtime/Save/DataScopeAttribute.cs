/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

namespace SideXP.Core
{

    /// <inheritdoc cref="EDataScope"/>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Field | AttributeTargets.Property)]
    public class DataScopeAttribute : Attribute
    {

        /// <summary>
        /// The scope of the data.
        /// </summary>
        public EDataScope Scope = EDataScope.Player;

        /// <param name="scope">The scope of the data.</param>
        /// <inheritdoc cref="DataScopeAttribute"/>
        public DataScopeAttribute(EDataScope scope)
        {
            Scope = scope;
        }

    }

}