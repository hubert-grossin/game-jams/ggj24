/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Core
{

    /// <summary>
    /// Qualifies a class as being savable, so it can receive callbacks before being saved or after being loaded using <see cref="SaveUtility"/>.
    /// </summary>
    public interface ISavable
    {

        /// <summary>
        /// Called before this object is saved, using <see cref="SaveUtility"/>.
        /// </summary>
        void BeforeSave();

        /// <summary>
        /// Called after this object is loaded, using <see cref="SaveUtility"/>.
        /// </summary>
        void AfterLoad();

    }


}