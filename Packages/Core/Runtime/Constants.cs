/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

namespace SideXP.Core
{

    /// <summary>
    /// Miscellaneous constant values.
    /// </summary>
    public static class Constants
    {

        /// <summary>
        /// Name of the company.
        /// </summary>
        public const string CompanyName = "Sideways Experiments";

        /// <summary>
        /// Base URL for online documentation of the framework. You must concatenate your own path starting with "/".
        /// </summary>
        public const string BaseHelpUrl = "https://gitlab.com/sideways-experiments/frameworks/unity/library";

        /// <summary>
        /// The submenu for demo features (from "Add Component" button, "Asset > Create" and the main toolbar menus. Starts with "/". You must concatenate your own path starting with "/".
        /// </summary>
        public const string DemosSubmenu = "/Demos";

        /// <summary>
        /// The define symbol used to enable demos of the framework.
        /// </summary>
        public const string DemosDefine = "SIDEXP_DEMOS";

        /// <summary>
        /// Base path used for <see cref="UnityEngine.AddComponentMenu"/>. You must concatenate your own path starting with "/".
        /// </summary>
        public const string AddComponentMenu = CompanyName;

        /// <summary>
        /// Base path used for <see cref="UnityEngine.AddComponentMenu"/> with demo components. You must concatenate your own path starting with "/".
        /// </summary>
        public const string AddComponentMenuDemos = CompanyName + DemosSubmenu;

        /// <summary>
        /// Base path used for <see cref="UnityEngine.AddComponentMenu"/> with demo components. You must concatenate your own path starting with "/".
        /// </summary>
        public const string AddComponentMenuDemosCore = AddComponentMenuDemos + "/Core";

        /// <summary>
        /// Base path used for <see cref="UnityEngine.CreateAssetMenuAttribute"/>. You must concatenate your own path starting with "/".
        /// </summary>
        public const string CreateAssetMenu = CompanyName;

        /// <summary>
        /// Base path used for <see cref="UnityEngine.CreateAssetMenuAttribute"/> with demo assets. You must concatenate your own path starting with "/".
        /// </summary>
        public const string CreateAssetMenuDemos = CompanyName + DemosSubmenu;

    }

}