using UnityEngine;
using UnityEditor;

namespace SideXP.Core.EditorOnly.Demos
{

    /// <summary>
    /// Illustrates string extensions usage.
    /// </summary>
    public class StringExtensionsDemoWindow : EditorWindow
    {

        private const string WindowTitle = "String Extensions Demo";
        public string Text = Constants.CompanyName;
        public int SliceStart = 0;
        public int SliceEnd = Constants.CompanyName.Length;

        [MenuItem(EditorConstants.EditorWindowMenuDemosCore + "/" + WindowTitle)]
        public static StringExtensionsDemoWindow Open()
        {
            StringExtensionsDemoWindow window = GetWindow<StringExtensionsDemoWindow>(false, WindowTitle, true);
            window.Show();
            return window;
        }

        private void OnGUI()
        {
            EditorGUILayout.LabelField("Inputs", EditorStyles.boldLabel);

            Text = EditorGUILayout.TextField("Text", Text);
            SliceStart = EditorGUILayout.IntField("Slice Start", SliceStart);
            SliceEnd = EditorGUILayout.IntField("Slice End", SliceEnd);

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Outputs", EditorStyles.boldLabel);

            EditorGUILayout.TextField($"{nameof(StringExtensions.Slice)}()", Text.Slice(SliceStart, SliceEnd));
            EditorGUILayout.TextField($"{nameof(StringExtensions.RemoveDiacritics)}()", Text.RemoveDiacritics());
            EditorGUILayout.TextField($"{nameof(StringExtensions.RemoveSpecialChars)}()", Text.RemoveSpecialChars());
            GUI.enabled = true;
        }

    }

}