using UnityEngine;
using UnityEditor;

namespace SideXP.Core.EditorOnly.Demos
{

    /// <summary>
    /// Illustrates <see cref="AnimationCurveUtility"/> and extensions usage.
    /// </summary>
    public class AnimationCurveDemoWindow : EditorWindow
    {

        private const string WindowTitle = "Animation Curve Demo";
        public AnimationCurve Curve = new AnimationCurve();

        [MenuItem(EditorConstants.EditorWindowMenuDemosCore + "/" + WindowTitle)]
        public static AnimationCurveDemoWindow Open()
        {
            AnimationCurveDemoWindow window = GetWindow<AnimationCurveDemoWindow>(false, WindowTitle, true);
            window.Show();
            return window;
        }

        private void OnGUI()
        {
            EditorGUILayout.LabelField("Inputs", EditorStyles.boldLabel);

            Curve = EditorGUILayout.CurveField(Curve);
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                for (int i = 0; i < Curve.keys.Length; i++)
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        EditorGUILayout.LabelField(i.ToString(), MoreGUI.WidthOptXS);
                        EditorGUILayout.LabelField($"{Curve.keys[i].time}, {Curve.keys[i].value}, {Curve.keys[i].inTangent}, {Curve.keys[i].outTangent}, {Curve.keys[i].inWeight}, {Curve.keys[i].outWeight}");
                    }
                }
            }

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Outputs", EditorStyles.boldLabel);

            EditorGUILayout.FloatField($"{nameof(AnimationCurveUtility.GetMinTime)}", Curve.GetMinTime());
            EditorGUILayout.FloatField($"{nameof(AnimationCurveUtility.GetMaxTime)}", Curve.GetMaxTime());
            EditorGUILayout.FloatField($"{nameof(AnimationCurveUtility.GetMinValue)}", Curve.GetMinValue());
            EditorGUILayout.FloatField($"{nameof(AnimationCurveUtility.GetMaxValue)}", Curve.GetMaxValue());
            EditorGUILayout.FloatField($"{nameof(AnimationCurveUtility.GetDuration)}", Curve.GetDuration());
            EditorGUILayout.FloatField($"{nameof(AnimationCurveUtility.GetRange)}", Curve.GetRange());

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Presets", EditorStyles.boldLabel);

            EditorGUILayout.CurveField($"{nameof(AnimationCurveUtility.Linear)}", AnimationCurveUtility.Linear);
            EditorGUILayout.CurveField($"{nameof(AnimationCurveUtility.EaseIn)}", AnimationCurveUtility.EaseIn);
            EditorGUILayout.CurveField($"{nameof(AnimationCurveUtility.EaseOut)}", AnimationCurveUtility.EaseOut);
            EditorGUILayout.CurveField($"{nameof(AnimationCurveUtility.EaseInOut)}", AnimationCurveUtility.EaseInOut);
        }

    }

}