using UnityEngine;
using UnityEditor;

namespace SideXP.Core.EditorOnly.Demos
{

    /// <summary>
    /// Illustrates <see cref="PathUtility"/> and path string extensions usage.
    /// </summary>
    public class PathDemoWindow : EditorWindow
    {

        private const string WindowTitle = "Path Demo";
        public string Path = string.Empty;
        public string Combine = string.Empty;

        [MenuItem(EditorConstants.EditorWindowMenuDemosCore + "/" + WindowTitle)]
        public static PathDemoWindow Open()
        {
            PathDemoWindow window = GetWindow<PathDemoWindow>(false, WindowTitle, true);
            window.Show();
            return window;
        }

        private void OnEnable()
        {
            if (string.IsNullOrEmpty(Path))
                Path = PathUtility.ProjectPath;
        }

        private void OnGUI()
        {
            EditorGUILayout.LabelField("Inputs", EditorStyles.boldLabel);

            Path = EditorGUILayout.TextField("Path", Path);
            Combine = EditorGUILayout.TextField("Combine", Combine);

            using (new EditorGUILayout.HorizontalScope())
            {
                EditorGUILayout.Space();
                string tmpPath = string.Empty;

                if (GUILayout.Button("Browse File"))
                    tmpPath = EditorUtility.OpenFilePanel("Browse File...", "Assets", "*");

                if (GUILayout.Button("Browse Folder"))
                    tmpPath = EditorUtility.OpenFolderPanel("Browse Folder...", "Assets", "");

                if (!string.IsNullOrEmpty(tmpPath))
                    Path = tmpPath;
            }

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Outputs", EditorStyles.boldLabel);

            EditorGUILayout.TextField($"{nameof(PathUtility.ToAbsolutePath)}()", Path.ToAbsolutePath());
            EditorGUILayout.TextField($"{nameof(PathUtility.ToRelativePath)}()", Path.ToRelativePath());
            EditorGUILayout.TextField($"{nameof(System.IO.Path.Combine)}()", System.IO.Path.Combine(Path, Combine).ToAbsolutePath());
            EditorGUILayout.Toggle($"{nameof(PathUtility.IsProjectPath)}()", Path.IsProjectPath());
        }

    }

}