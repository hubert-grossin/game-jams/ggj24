using UnityEditor;

namespace SideXP.Core.EditorOnly.Demos
{

    /// <summary>
    /// Illustrates <see cref="Math"/> usage.
    /// </summary>
    public class MathDemoWindow : EditorWindow
    {

        private const string WindowTitle = "Math Demo";

        public float Value = 5f;
        public float FromMin = 0f;
        public float FromMax = 10f;
        public float ToMin = 100f;
        public float ToMax = 200f;

        [MenuItem(EditorConstants.EditorWindowMenuDemosCore + "/" + WindowTitle)]
        public static MathDemoWindow Open()
        {
            MathDemoWindow window = GetWindow<MathDemoWindow>(false, WindowTitle, true);
            window.Show();
            return window;
        }

        private void OnGUI()
        {
            EditorGUILayout.LabelField("Inputs", EditorStyles.boldLabel);

            Value = EditorGUILayout.FloatField("Value", Value);
            FromMin = EditorGUILayout.FloatField("From Min", FromMin);
            FromMax = EditorGUILayout.FloatField("From Max", FromMax);
            ToMin = EditorGUILayout.FloatField("To Min", ToMin);
            ToMax = EditorGUILayout.FloatField("To Max", ToMax);

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Outputs", EditorStyles.boldLabel);

            EditorGUILayout.FloatField($"{nameof(Math.Remap)}()", Math.Remap(Value, FromMin, FromMax, ToMin, ToMax));
            EditorGUILayout.FloatField($"{nameof(Math.Ratio)}()", Math.Ratio(Value, FromMin, FromMax));
            EditorGUILayout.FloatField($"{nameof(Math.Percents)}()", Math.Percents(Value, FromMin, FromMax));
        }

    }

}