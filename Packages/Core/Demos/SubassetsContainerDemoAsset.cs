using UnityEngine;

namespace SideXP.Core.Demos
{

    /// <summary>
    /// Illustrates how to use <see cref="SubassetsContainerAsset{T}"/>.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    [CreateAssetMenu(fileName = "NewSubassetsContainerDemoAsset", menuName = Constants.CreateAssetMenuDemos + "/Subassets Container")]
    public class SubassetsContainerDemoAsset : SubassetsContainerAsset<PlayerAsset> { }

}