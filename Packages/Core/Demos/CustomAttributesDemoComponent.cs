using UnityEngine;

namespace SideXP.Core.Demos
{

    /// <summary>
    /// Illustrates usage of custom attributes for inspector GUI.
    /// </summary>
    [AddComponentMenu(Constants.AddComponentMenuDemosCore + "/Custom Attributes Demo")]
    public class CustomAttributesDemoComponent : MonoBehaviour
    {

        [Space]

        [Readonly]
        public string Readonly = "Readonly";

        [Separator]

        [ProgressBar]
        public float ProgressBar = .3f;

        [ProgressBar(100, "%", EColor.Red, true)]
        public float ProgressBarPercents = 60f;

        [ProgressBar(-100, 100, EColor.Green, true, true)]
        public float ProgressBarFull = -20f;

        [Separator]

        [Indent]
        public string Indent = "Indent";

        [Indent(2)]
        public string Indent2 = "Indent2";

        [Separator]

        [AnimCurve(EColor.Orange)]
        [Tooltip("This animation curve is clamped between 0 and 1 for both axes.")]
        public AnimationCurve AnimCurve = AnimationCurveUtility.Linear;

        [AnimCurve(1, 5, EColor.Cyan)]
        [Tooltip("This animation curve is clamped between 0 and 1 along X axis, and between 0 and 5 along Y axis.")]
        public AnimationCurve AnimCurve2 = AnimationCurveUtility.EaseIn;

        [AnimCurve(-1, 1, -1, 1, EColor.Magenta)]
        [Tooltip("This animation curve is clamped between -1 and 1 for both axes.")]
        public AnimationCurve AnimCurve3 = AnimationCurveUtility.EaseOut;

        [Separator]

        [Remap(0, 100, 0, 1, "%", true)]
        public float RemapPercentsToRatio = .5f;

        [Remap(-10, 10, -1, 1)]
        public int RemapUnbound = 500;

        [Remap(-10, 10, -1, 1, true)]
        [Tooltip("Note that this will remap the vector components, but won't normalize the vector itself.")]
        public Vector2 RemapAxis = Vector2.up;

        [Separator]

        [Tooltip("If checked, make the next field enabled.")]
        public bool EnableNextField = true;

        [IfChecked(nameof(EnableNextField))]
        [Tooltip("This field is enabled only if the previous one is checked.")]
        public bool DisableNextField = false;

        [IfNotChecked(nameof(DisableNextField))]
        [Tooltip("The field is enabled only if the previous one is not checked.")]
        public string DemoEnable = "Field enabled only if previous one is not checked";

    }

}