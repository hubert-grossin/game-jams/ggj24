using UnityEngine;

namespace SideXP.Core.Demos
{

    /// <summary>
    /// Demo asset that represents a player, used in several feature demos.
    /// </summary>
    [HelpURL(Constants.BaseHelpUrl)]
    [CreateAssetMenu(fileName = "NewPlayerAsset", menuName = Constants.CreateAssetMenuDemos + "/Player Asset")]
    public class PlayerAsset : ScriptableObject
    {

        public string Username = string.Empty;
        public int Score = 0;

    }

}