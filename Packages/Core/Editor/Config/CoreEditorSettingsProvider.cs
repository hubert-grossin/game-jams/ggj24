/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEditor;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Generate menus to edit core lib settings.
    /// </summary>
    internal class CoreEditorSettingsProvider : DefaultEditorSettingsProvider
    {

        [SettingsProvider]
        private static SettingsProvider RegisterProjectSettingsMenu()
        {
            return MakeSettingsProvider(CoreEditorSettings.I, EditorConstants.ProjectSettings, SettingsScope.Project);
        }

        [SettingsProvider]
        private static SettingsProvider RegisterProjectSettingsSubmenu()
        {
            return MakeSettingsProvider(CoreEditorSettings.I, EditorConstants.ProjectSettings + "/General", SettingsScope.Project);
        }

    }

}