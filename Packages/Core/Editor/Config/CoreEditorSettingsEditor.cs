/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;
using UnityEditor;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="CoreEditorSettings"/> objects.
    /// </summary>
    [CustomEditor(typeof(CoreEditorSettings))]
    public class CoreEditorSettingsEditor : Editor
    {

        /// <summary>
        /// Draws the GUI for the edited object(s).
        /// </summary>
        public override void OnInspectorGUI()
        {
            bool enableDemos = EditorGUILayout.Toggle(EditorHelpers.GetLabel<CoreEditorSettings>("_enableDemos"), CoreEditorSettings.I.EnableDemos);
            if (CoreEditorSettings.I.EnableDemos != enableDemos)
                CoreEditorSettings.I.EnableDemos = enableDemos;
        }

    }

}