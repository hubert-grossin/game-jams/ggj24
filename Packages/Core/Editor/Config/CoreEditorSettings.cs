/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;

using RuntimeConstants = SideXP.Core.Constants;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Editor settings for the core library package.
    /// </summary>
    [Save(EDataScope.User)]
    public class CoreEditorSettings : SOSingleton<CoreEditorSettings>
    {

        #region Fields

        [SerializeField]
        [Tooltip("If enabled, adds a define to your project, so you can see \"Demos\" menus from the main toolbar and in \"Add Component\" popup.")]
        private bool _enableDemos = false;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="SOSingleton{T}.Set"/>
        protected override void Set()
        {
            _enableDemos = DemosEnabledForCurrentPlatform;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_enableDemos"/>
        public bool EnableDemos
        {
            get => _enableDemos;
            set
            {
                DemosEnabledForCurrentPlatform = value;
                _enableDemos = value;
            }
        }

        #endregion


        #region Private API

        /// <summary>
        /// Checks if the demos define exists in Player Settings.
        /// </summary>
        private bool DemosEnabledForCurrentPlatform
        {
            get => DefinesUtility.IsDefined(RuntimeConstants.DemosDefine);
            set
            {
                if (value)
                    DefinesUtility.AddDefine(RuntimeConstants.DemosDefine);
                else
                    DefinesUtility.RemoveDefine(RuntimeConstants.DemosDefine);
            }
        }

        #endregion

    }

}