/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEditor;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Called before assets are imported, moved or deleted, and report the information to <see cref="AssetsProcessor"/>.
    /// </summary>
    internal class InternalAssetsModificationProcessor : AssetModificationProcessor
    {

        /// <inheritdoc cref="AssetsProcessor.NotifyCreateAsset(string)"/>
        private static void OnWillCreateAsset(string assetName)
        {
            AssetsProcessor.NotifyCreateAsset(assetName);
        }

        /// <inheritdoc cref="AssetsProcessor.NotifyWillDeleteAsset(string, RemoveAssetOptions)"/>
        private static AssetDeleteResult OnWillDeleteAsset(string path, RemoveAssetOptions options)
        {
            return AssetsProcessor.NotifyWillDeleteAsset(path, options);
        }

        /// <inheritdoc cref="AssetsProcessor.NotifyWillMoveAsset(string, string)"/>
        private static AssetMoveResult OnWillMoveAsset(string fromPath, string toPath)
        {
            return AssetsProcessor.NotifyWillMoveAsset(fromPath, toPath);
        }

    }

}