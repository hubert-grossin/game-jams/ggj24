/**
* Sideways Experiments (c) 2024
* https://sideways-experiments.com
* Contact: dev@side-xp.com
*/

using UnityEditor;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Called after assets are imported, moved or deleted, and report the information to <see cref="AssetsProcessor"/>.
    /// </summary>
    internal class InternalAssetsPostprocessor : AssetPostprocessor
    {

        /// <inheritdoc cref="AssetsProcessor.NotifyPostProcess(string[], string[], string[], string[], bool)"/>
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths, bool didDomainReload)
        {
            AssetsProcessor.NotifyPostProcess(importedAssets, deletedAssets, movedAssets, movedFromAssetPaths, didDomainReload);
        }

    }

}