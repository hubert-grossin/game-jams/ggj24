/**
 * Sideways Experiments (c) 2024
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

//#define DISPLAY_LOGS

using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Handles and invoke callbacks for assets creation, move or deletion.
    /// </summary>
    public static class AssetsProcessor
    {

        #region Delegates

        /// <summary>
        /// Called when a new asset is created (not invoked when an asset is reimported).
        /// </summary>
        /// <param name="path">The path to the asset, relative from the project's root.</param>
        public delegate void AfterCreateAssetDelegate(string path);

        /// <summary>
        /// Called when an asset has been imported or reimported.
        /// </summary>
        /// <inheritdoc cref="AfterCreateAssetDelegate"/>
        public delegate void AfterImportAssetDelegate(string path);

        /// <inheritdoc cref="AfterImportAssetDelegate"/>
        /// <param name="isNew">Is the asset imported for the first time (meaning it has just been created)?</param>
        /// <param name="didDomainReload">Boolean set to true if this process is invoked after a domain reload.</param>
        /// <param name="previousPath">The previous path of the asset, in case that asset is reimported by Unity after being moved.</param>
        public delegate void AfterImportAssetWithDetailsDelegate(string path, bool isNew, bool didDomainReload, string previousPath);

        /// <summary>
        /// Called after all imported assets have been processed.
        /// </summary>
        /// <param name="paths">The path of all the imported assets, reative from the project's root.</param>
        public delegate void AfterImportAllDelegate(string[] paths);

        /// <summary>
        /// Called when an asset is about to move in the project.
        /// </summary>
        /// <param name="fromPath">The current path to the asset, before it moves, relative from the project's root.</param>
        /// <param name="toPath">The target path of the asset, relative from the project's root.</param>
        /// <returns>Returns the operation result.</returns>
        public delegate AssetMoveResult BeforeMoveAssetDelegate(string fromPath, string toPath);

        /// <summary>
        /// Called when a asset did move in the project.
        /// </summary>
        /// <param name="fromPath">The previous path of the asset, before it moved, relative from the project's root.</param>
        /// <param name="toPath">The current path of the asset, after it moved, relative from the project's root.</param>
        public delegate void AfterMoveAssetDelegate(string fromPath, string toPath);

        /// <summary>
        /// Called after all moved assets have been processed.
        /// </summary>
        /// <param name="fromPaths">The previous path of the assets, before they moved, reative from the project's root.</param>
        /// <param name="toPaths">The current path of the assets, after they moved, reative from the project's root.</param>
        public delegate void AfterMoveAllDelegate(string[] fromPaths, string[] toPaths);

        /// <summary>
        /// Called when an asset is about to be deleted from the project.
        /// </summary>
        /// <param name="path">The path to the asset, before it's deleted, relative from the project's root.</param>
        /// <returns>Returns the operation result.</returns>
        public delegate AssetDeleteResult BeforeDeleteAssetDelegate(string path);

        /// <param name="options">The options to use for deleting the asset.</param>
        /// <inheritdoc cref="BeforeDeleteAssetDelegate"/>
        public delegate AssetDeleteResult BeforeDeleteAssetWithDetailsDelegate(string path, RemoveAssetOptions options);

        /// <summary>
        /// Called after an asset has been deleted from the project.
        /// </summary>
        /// <param name="path">The previous path of the asset, brefore its deletion, relative from the project's root.</param>
        public delegate void AfterDeleteAssetDelegate(string path);

        /// <summary>
        /// Called after all deleted assets have been processed.
        /// </summary>
        /// <param name="paths">The previous path of all deleted assets, before their deletion, reative from the project's root.</param>
        public delegate void AfterDeleteAllDelegate(string[] paths);

        #endregion


        #region Fields

        /// <summary>
        /// The list of all created assets this process pass. This is used to determine if an asset is new or not when calling import
        /// callbacks.
        /// </summary>
        private static List<string> s_createdAssetPaths = new List<string>();

        /// <inheritdoc cref="AfterCreateAssetDelegate"/>
        private static List<AfterCreateAssetDelegate> s_afterCreateAssetCallbacks = new List<AfterCreateAssetDelegate>();

        /// <inheritdoc cref="AfterImportAssetWithDetailsDelegate"/>
        private static List<AfterImportAssetWithDetailsDelegate> s_afterImportAssetCallbacks = new List<AfterImportAssetWithDetailsDelegate>();

        /// <summary>
        /// The callbacks of this list register the original callbacks that require no details, and bind them to the detailed version.
        /// </summary>
        /// <remarks>
        /// This is used to keep the callbacks in order from the <see cref="s_afterImportAssetCallbacks"/> list, but still allow the
        /// callbacks to be removed.
        /// </remarks>
        private static Dictionary<AfterImportAssetDelegate, AfterImportAssetWithDetailsDelegate> s_afterImportAssetNoDetailsCallbacks = new Dictionary<AfterImportAssetDelegate, AfterImportAssetWithDetailsDelegate>();

        /// <inheritdoc cref="AfterImportAllDelegate"/>
        private static List<AfterImportAllDelegate> s_afterImportAllCallbacks = new List<AfterImportAllDelegate>();

        /// <inheritdoc cref="BeforeMoveAssetDelegate"/>
        private static List<BeforeMoveAssetDelegate> s_beforeMoveAssetCallbacks = new List<BeforeMoveAssetDelegate>();

        /// <inheritdoc cref="AfterMoveAssetDelegate"/>
        private static List<AfterMoveAssetDelegate> s_afterMoveAssetCallbacks = new List<AfterMoveAssetDelegate>();

        /// <inheritdoc cref="AfterMoveAllDelegate"/>
        private static List<AfterMoveAllDelegate> s_afterMoveAllCallbacks = new List<AfterMoveAllDelegate>();

        /// <inheritdoc cref="BeforeDeleteAssetWithDetailsDelegate"/>
        private static List<BeforeDeleteAssetWithDetailsDelegate> s_beforeDeleteAssetCallbacks = new List<BeforeDeleteAssetWithDetailsDelegate>();

        /// <inheritdoc cref="s_afterImportAssetNoDetailsCallbacks"/>
        /// <remarks>
        /// This is used to keep the callbacks in order from the <see cref="s_beforeDeleteAssetCallbacks"/> list, but still allow the
        /// callbacks to be removed.
        /// </remarks>
        private static Dictionary<BeforeDeleteAssetDelegate, BeforeDeleteAssetWithDetailsDelegate> s_beforeDeleteAssetNoDetailsCallbacks = new Dictionary<BeforeDeleteAssetDelegate, BeforeDeleteAssetWithDetailsDelegate>();

        /// <inheritdoc cref="AfterDeleteAssetDelegate"/>
        private static List<AfterDeleteAssetDelegate> s_afterDeleteAssetCallbacks = new List<AfterDeleteAssetDelegate>();

        /// <inheritdoc cref="AfterDeleteAllDelegate"/>
        private static List<AfterDeleteAllDelegate> s_afterDeleteAllCallbacks = new List<AfterDeleteAllDelegate>();

        #endregion


        #region Public API

        /// <param name="callback">The function to call when this event occurs.</param>
        /// <returns>Returns true if the callback has been registered successfully.</returns>
        /// <inheritdoc cref="AfterCreateAssetDelegate"/>
        public static bool ListenCreateAsset(AfterCreateAssetDelegate callback)
        {
            if (s_afterCreateAssetCallbacks.Contains(callback))
                return false;

            s_afterCreateAssetCallbacks.Add(callback);
            return true;
        }

        /// <inheritdoc cref="AfterImportAssetDelegate"/>
        /// <inheritdoc cref="ListenCreateAsset(AfterCreateAssetDelegate)"/>
        public static bool ListenImportAsset(AfterImportAssetDelegate callback)
        {
            if (s_afterImportAssetNoDetailsCallbacks.ContainsKey(callback))
                return false;

            s_afterImportAssetNoDetailsCallbacks.Add(callback, (path, isNew, didDomainReload, previousPath) => callback(path));
            s_afterImportAssetCallbacks.Add(s_afterImportAssetNoDetailsCallbacks[callback]);
            return true;
        }

        /// <inheritdoc cref="AfterImportAssetWithDetailsDelegate"/>
        /// <inheritdoc cref="ListenCreateAsset(AfterCreateAssetDelegate)"/>
        public static bool ListenImportAsset(AfterImportAssetWithDetailsDelegate callback)
        {
            if (s_afterImportAssetCallbacks.Contains(callback))
                return false;

            s_afterImportAssetCallbacks.Add(callback);
            return true;
        }

        /// <inheritdoc cref="AfterImportAllDelegate"/>
        /// <inheritdoc cref="ListenCreateAsset(AfterCreateAssetDelegate)"/>
        public static bool ListenImportAllAssets(AfterImportAllDelegate callback)
        {
            if (s_afterImportAllCallbacks.Contains(callback))
                return false;

            s_afterImportAllCallbacks.Add(callback);
            return true;
        }

        /// <inheritdoc cref="BeforeMoveAssetDelegate"/>
        /// <inheritdoc cref="ListenCreateAsset(AfterCreateAssetDelegate)"/>
        public static bool ListenBeforeMoveAsset(BeforeMoveAssetDelegate callback)
        {
            if (s_beforeMoveAssetCallbacks.Contains(callback))
                return false;

            s_beforeMoveAssetCallbacks.Add(callback);
            return true;
        }

        /// <inheritdoc cref="AfterMoveAssetDelegate"/>
        /// <inheritdoc cref="ListenCreateAsset(AfterCreateAssetDelegate)"/>
        public static bool ListenAfterMoveAsset(AfterMoveAssetDelegate callback)
        {
            if (s_afterMoveAssetCallbacks.Contains(callback))
                return false;

            s_afterMoveAssetCallbacks.Add(callback);
            return true;
        }

        /// <inheritdoc cref="AfterMoveAllDelegate"/>
        /// <inheritdoc cref="ListenCreateAsset(AfterCreateAssetDelegate)"/>
        public static bool ListenMovetAllAssets(AfterMoveAllDelegate callback)
        {
            if (s_afterMoveAllCallbacks.Contains(callback))
                return false;

            s_afterMoveAllCallbacks.Add(callback);
            return true;
        }

        /// <inheritdoc cref="BeforeDeleteAssetDelegate"/>
        /// <inheritdoc cref="ListenCreateAsset(AfterCreateAssetDelegate)"/>
        public static bool ListenBeforeDeleteAsset(BeforeDeleteAssetDelegate callback)
        {
            if (s_beforeDeleteAssetNoDetailsCallbacks.ContainsKey(callback))
                return false;

            s_beforeDeleteAssetNoDetailsCallbacks.Add(callback, (path, options) => callback(path));
            s_beforeDeleteAssetCallbacks.Add(s_beforeDeleteAssetNoDetailsCallbacks[callback]);
            return true;
        }

        /// <inheritdoc cref="BeforeDeleteAssetWithDetailsDelegate"/>
        /// <inheritdoc cref="ListenCreateAsset(AfterCreateAssetDelegate)"/>
        public static bool ListenBeforeDeleteAsset(BeforeDeleteAssetWithDetailsDelegate callback)
        {
            if (s_beforeDeleteAssetCallbacks.Contains(callback))
                return false;

            s_beforeDeleteAssetCallbacks.Add(callback);
            return true;
        }

        /// <inheritdoc cref="AfterDeleteAssetDelegate"/>
        /// <inheritdoc cref="ListenCreateAsset(AfterCreateAssetDelegate)"/>
        public static bool ListenAfterDeleteAsset(AfterDeleteAssetDelegate callback)
        {
            if (s_afterDeleteAssetCallbacks.Contains(callback))
                return false;

            s_afterDeleteAssetCallbacks.Add(callback);
            return true;
        }

        /// <inheritdoc cref="AfterDeleteAllDelegate"/>
        /// <inheritdoc cref="ListenCreateAsset(AfterCreateAssetDelegate)"/>
        public static bool ListenDeleteAllAssets(AfterDeleteAllDelegate callback)
        {
            if (s_afterDeleteAllCallbacks.Contains(callback))
                return false;

            s_afterDeleteAllCallbacks.Add(callback);
            return true;
        }

        /// <summary>
        /// Removes a listener of an asset processor event.
        /// </summary>
        /// <param name="callback">The function to remove from the event callbacks.</param>
        /// <returns>Returns true if the callback has been removed successfully.</returns>
        public static bool RemoveListener(AfterCreateAssetDelegate callback)
        {
            return s_afterCreateAssetCallbacks.Remove(callback);
        }

        /// <inheritdoc cref="RemoveListener(AfterCreateAssetDelegate)"/>
        public static bool RemoveListener(AfterImportAssetDelegate callback)
        {
            if (!s_afterImportAssetNoDetailsCallbacks.TryGetValue(callback, out AfterImportAssetWithDetailsDelegate detailedCallback))
                return false;

            if (RemoveListener(detailedCallback))
            {
                s_afterImportAssetNoDetailsCallbacks.Remove(callback);
                return true;
            }

            return false;
        }

        /// <inheritdoc cref="RemoveListener(AfterCreateAssetDelegate)"/>
        public static bool RemoveListener(AfterImportAssetWithDetailsDelegate callback)
        {
            return s_afterImportAssetCallbacks.Remove(callback);
        }

        /// <inheritdoc cref="RemoveListener(AfterCreateAssetDelegate)"/>
        public static bool RemoveListener(AfterImportAllDelegate callback)
        {
            return s_afterImportAllCallbacks.Remove(callback);
        }

        /// <inheritdoc cref="RemoveListener(AfterCreateAssetDelegate)"/>
        public static bool RemoveListener(BeforeMoveAssetDelegate callback)
        {
            return s_beforeMoveAssetCallbacks.Remove(callback);
        }

        /// <inheritdoc cref="RemoveListener(AfterCreateAssetDelegate)"/>
        public static bool RemoveListener(AfterMoveAssetDelegate callback)
        {
            return s_afterMoveAssetCallbacks.Remove(callback);
        }

        /// <inheritdoc cref="RemoveListener(AfterCreateAssetDelegate)"/>
        public static bool RemoveListener(AfterMoveAllDelegate callback)
        {
            return s_afterMoveAllCallbacks.Remove(callback);
        }

        /// <inheritdoc cref="RemoveListener(AfterCreateAssetDelegate)"/>
        public static bool RemoveListener(BeforeDeleteAssetDelegate callback)
        {
            if (!s_beforeDeleteAssetNoDetailsCallbacks.TryGetValue(callback, out BeforeDeleteAssetWithDetailsDelegate detailedCallback))
                return false;

            if (RemoveListener(detailedCallback))
            {
                s_beforeDeleteAssetNoDetailsCallbacks.Remove(callback);
                return true;
            }

            return false;
        }

        /// <inheritdoc cref="RemoveListener(AfterCreateAssetDelegate)"/>
        public static bool RemoveListener(BeforeDeleteAssetWithDetailsDelegate callback)
        {
            return s_beforeDeleteAssetCallbacks.Remove(callback);
        }

        /// <inheritdoc cref="RemoveListener(AfterCreateAssetDelegate)"/>
        public static bool RemoveListener(AfterDeleteAssetDelegate callback)
        {
            return s_afterDeleteAssetCallbacks.Remove(callback);
        }

        /// <inheritdoc cref="RemoveListener(AfterCreateAssetDelegate)"/>
        public static bool RemoveListener(AfterDeleteAllDelegate callback)
        {
            return s_afterDeleteAllCallbacks.Remove(callback);
        }

        #endregion


        #region Internal API

        /// <summary>
        /// Called when Unity is about to create an asset that has not been imported yet.
        /// </summary>
        /// <param name="path">The path to the created asset, relative from the project's root.</param>
        internal static void NotifyCreateAsset(string path)
        {
#if DISPLAY_LOGS
            Debug.Log($"{nameof(AssetsProcessor)}.{nameof(NotifyCreateAsset)}({path})");
#endif

            s_createdAssetPaths.Add(path);
            foreach (AfterCreateAssetDelegate callback in s_afterCreateAssetCallbacks)
            {
                try
                {
                    callback(path);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }

        /// <summary>
        /// Called when Unity is about to move an asset in the project.
        /// </summary>
        /// <param name="fromPath">The path from which the asset is moved.</param>
        /// <param name="toPath">The path to which the asset is moved.</param>
        /// <returns>Returns the result of this operation.</returns>
        internal static AssetMoveResult NotifyWillMoveAsset(string fromPath, string toPath)
        {
#if DISPLAY_LOGS
            Debug.Log($"{nameof(AssetsProcessor)}.{nameof(NotifyWillMoveAsset)}({fromPath}, {toPath})");
#endif

            foreach (BeforeMoveAssetDelegate callback in s_beforeMoveAssetCallbacks)
            {
                try
                {
                    AssetMoveResult result = callback(fromPath, toPath);
                    if (result != AssetMoveResult.DidNotMove)
                        return result;
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
            return AssetMoveResult.DidNotMove;
        }

        /// <summary>
        /// Called when Unity is about to delete an asset.
        /// </summary>
        /// <param name="path">The path to the asset that will be deleted.</param>
        /// <param name="options">The options used for this operation.</param>
        /// <returns>Returns the result of this operation.</returns>
        internal static AssetDeleteResult NotifyWillDeleteAsset(string path, RemoveAssetOptions options)
        {
#if DISPLAY_LOGS
            Debug.Log($"{nameof(AssetsProcessor)}.{nameof(NotifyWillDeleteAsset)}({path}, {options})");
#endif

            foreach (BeforeDeleteAssetWithDetailsDelegate callback in s_beforeDeleteAssetCallbacks)
            {
                try
                {
                    AssetDeleteResult result = callback(path, options);
                    if (result != AssetDeleteResult.DidNotDelete)
                        return result;
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
            return AssetDeleteResult.DidNotDelete;
        }

        /// <summary>
        /// Called when assets are processed through import pipeline.
        /// </summary>
        /// <param name="importedAssets">Array of paths to imported assets.</param>
        /// <param name="deletedAssets">Array of paths to deleted assets.</param>
        /// <param name="movedAssets">Array of paths to moved assets.</param>
        /// <param name="movedFromAssetPaths">Array of original paths for moved assets.</param>
        /// <param name="didDomainReload">Boolean set to true if this process is invoked after a domain reload.</param>
        internal static void NotifyPostProcess(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths, bool didDomainReload)
        {
#if DISPLAY_LOGS
            Debug.Log($"{nameof(AssetsProcessor)}.{nameof(NotifyPostProcess)} ({nameof(didDomainReload)} = {didDomainReload}):\nImported assets:\n\t{string.Join("\n\t", importedAssets)}\nMoved assets:\n\t{string.Join("\n\t", movedAssets)}\nDeleted assets:\n\t{string.Join("\n\t", deletedAssets)}");
#endif

            // For each moved asset
            for (int i = 0; i < movedAssets.Length; i++)
            {
                foreach (AfterMoveAssetDelegate callback in s_afterMoveAssetCallbacks)
                {
                    try
                    {
                        callback(movedFromAssetPaths[i], movedAssets[i]);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
            }

            foreach (AfterMoveAllDelegate callback in s_afterMoveAllCallbacks)
            {
                try
                {
                    callback(movedFromAssetPaths, movedAssets);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }

            // For each imported asset
            foreach (string path in importedAssets)
            {
                bool isNew = false;
                if (s_createdAssetPaths.Contains(path))
                {
                    isNew = true;
                    s_createdAssetPaths.Remove(path);
                }

                foreach (AfterImportAssetWithDetailsDelegate callback in s_afterImportAssetCallbacks)
                {
                    try
                    {
                        // Get the previous path of the asset if it's reimported after being moved
                        string previousPath = null;
                        for (int i = 0; i < movedAssets.Length; i++)
                        {
                            if (path == movedAssets[i])
                                previousPath = movedFromAssetPaths[i];
                        }

                        callback(path, isNew, didDomainReload, previousPath);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
            }

            foreach (AfterImportAllDelegate callback in s_afterImportAllCallbacks)
            {
                try
                {
                    callback(importedAssets);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }

            // For each deleted asset
            foreach (string path in deletedAssets)
            {
                foreach (AfterDeleteAssetDelegate callback in s_afterDeleteAssetCallbacks)
                {
                    try
                    {
                        callback(path);
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
            }

            foreach (AfterDeleteAllDelegate callback in s_afterDeleteAllCallbacks)
            {
                try
                {
                    callback(deletedAssets);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }

#endregion

    }

}