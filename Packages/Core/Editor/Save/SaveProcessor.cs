/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEditor;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Handles editor events and asset modifications to save static classes and loaded assets marked with <see cref="SaveAttribute"/>, so
    /// they can be saved on disk automatically.
    /// </summary>
    [InitializeOnLoad]
    public class SaveProcessor : AssetModificationProcessor
    {

        /// <summary>
        /// Static constructor.
        /// </summary>
        static SaveProcessor()
        {
            AssemblyReloadEvents.beforeAssemblyReload += SaveUtility.SaveAll;
            EditorApplication.playModeStateChanged += _ => SaveUtility.SaveAll();
        }

#pragma warning disable IDE0051 // Remove unused private members
        /// <summary>
        /// Called when assets are saved in the editor.
        /// </summary>
        private static string[] OnWillSaveAssets(string[] paths)
        {
            SaveUtility.SaveAll();
            return paths;
        }
#pragma warning restore IDE0051 // Remove unused private members

    }


}