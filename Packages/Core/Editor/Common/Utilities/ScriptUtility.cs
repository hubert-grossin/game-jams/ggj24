/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;
using System.IO;
using System.Text.RegularExpressions;

using SideXP.Core.Reflection;

using UnityEditor;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Miscellaneous utility functions for working with scripts.
    /// </summary>
    public class ScriptUtility
    {

        public const string CsExtension = ".cs";
        public const string MetaExtension = ".meta";

        /// <summary>
        /// Pattern used to extract the first namespace declared in a script.
        /// </summary>
        public static readonly Regex NamespaceDeclarationPattern = new Regex(@"namespace (?<namespace>[A-Za-z0-9_.]+)");

        /// <summary>
        /// Pattern used to extract the first class declared in a script, and its base type if applicable.
        /// </summary>
        public static readonly Regex ClassDeclarationPattern = new Regex(@"(?:public|internal) +(?:(?:static|abstract|sealed) +)?(?:class|struct|interface|enum) +(?<class>[A-Za-z0-9_]+)(?>\s+:\s+(?<base>[A-Za-z0-9_]+))?");

        /// <summary>
        /// Gets the declared type in a script file.
        /// </summary>
        /// <remarks>
        /// Unity API has a <see cref="MonoScript.GetClass"/> function, but it doesn't work with structs and interfaces. This function uses
        /// both regular expressions and reflection, and so is able to get the declared type whatever its nature.<br/>
        /// This also applies to <see cref="UnityEngine.TextAsset.text"/>, which logs an error that can't be handled as an exception if the
        /// asset is about to be deleted. We prefer using a custom implementation to read the script, making this function actually usable
        /// in an OnWillDelete() message.
        /// </remarks>
        /// <param name="scriptPath">The path to the script from which to extract the type.</param>
        /// <param name="type">Outputs the found type in the given script.</param>
        /// <returns>Returns true if the type has been extracted successfully.</returns>
        public static bool GetScriptType(string scriptPath, out Type type)
        {
            type = null;
            scriptPath = scriptPath.ToAbsolutePath();

            // Cancel if the path doesn't lead to a *.cs script
            if (Path.GetExtension(scriptPath) != CsExtension)
                return false;

            string scriptContent = null;
            try
            {
                // Cancel if the file doesn't exist
                if (!File.Exists(scriptPath))
                    return false;

                scriptContent = File.ReadAllText(scriptPath);
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogException(e);
                return false;
            }

            Match match = ClassDeclarationPattern.Match(scriptContent);

            // Skip if no declared class or struct can be found in the script
            if (!match.Success)
                return false;

            Match namespaceMatch = NamespaceDeclarationPattern.Match(scriptContent);
            string namespaceValue = namespaceMatch.Success ? namespaceMatch.Groups["namespace"].Value : null;

            return ReflectionUtility.FindType(match.Groups["class"].Value, namespaceValue, out type) && type != null;
        }

        /// <param name="script">The script from which to extract the type.</param>
        /// <inheritdoc cref="GetScriptType(string, out Type)"/>
        public static bool GetScriptType(MonoScript script, out Type type)
        {
            string scriptPath = AssetDatabase.GetAssetPath(script).ToAbsolutePath();
            return GetScriptType(scriptPath, out type);
        }

    }

}