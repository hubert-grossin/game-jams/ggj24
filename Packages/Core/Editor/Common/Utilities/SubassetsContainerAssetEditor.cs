/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEditor;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Custom editor for <see cref="SubassetsContainerAsset{T}"/>.
    /// </summary>
    [CustomEditor(typeof(SubassetsContainerAsset<>), true)]
    public class SubassetsContainerAssetEditor : Editor
    {

        #region Fields

        private const string ElementsProp = "_elements";

        private SerializedProperty _elementsProp = null;
        private SubassetsList _elementsSubassetsList = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this editor is loaded.
        /// </summary>
        private void OnEnable()
        {
            _elementsProp = serializedObject.FindProperty(ElementsProp);
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws the custom inspector.
        /// </summary>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            ElementsSubassetsList.DoLayoutList();
        }

        #endregion


        #region Private API

        /// <summary>
        /// Gets (or create) the reorderable list for managing subassets.
        /// </summary>
        private SubassetsList ElementsSubassetsList
        {
            get
            {
                if (_elementsSubassetsList == null)
                    _elementsSubassetsList = new SubassetsList(serializedObject, _elementsProp);

                return _elementsSubassetsList;
            }
        }

        #endregion

    }

}