/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEngine;
using UnityEditor;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Generate menus to edit a specific asset type by drawing its default inspector.
    /// </summary>
    public abstract class DefaultEditorSettingsProvider
    {

        #region Fields

        private static Editor s_projectSettingsEditor = null;
        private static Editor s_preferencesEditor = null;

        #endregion


        #region Protected API

        /// <summary>
        /// Creates a <see cref="SettingsProvider"/> instance from
        /// </summary>
        /// <param name="settingsAsset">The asset that contains the settings to display.</param>
        /// <param name="menu">The menu from which these settings can be edited, relative to the appropriate window depending on the
        /// scope.</param>
        /// <param name="scope">Defines when the settings are displayed.</param>
        /// <param name="keywords">The keywords to search for these settings to be filtered.</param>
        /// <param name="useCustomGUI">If enabled, uses <see cref="MoreEditorGUI.DrawDefaultInspector(Object, string[])"/> to draw the
        /// inspector, instead of using the native <see cref="Editor"/> GUI.</param>
        /// <returns>Returns the created <see cref="SettingsProvider"/>.</returns>
        protected static SettingsProvider MakeSettingsProvider(Object settingsAsset, string menu, SettingsScope scope, string[] keywords = null, bool useCustomGUI = false)
        {
            return new SettingsProvider(menu, scope, keywords)
            {
                activateHandler = (search, ui) =>
                {
                    if (useCustomGUI)
                        return;

                    switch (scope)
                    {
                        case SettingsScope.Project:
                        {
                            if (s_projectSettingsEditor == null)
                                s_projectSettingsEditor = Editor.CreateEditor(settingsAsset);
                        }
                        break;

                        default:
                        {
                            if (s_preferencesEditor == null)
                                s_preferencesEditor = Editor.CreateEditor(settingsAsset);
                        }
                        break;
                    }
                },
                guiHandler = str =>
                {
                    if (useCustomGUI)
                    {
                        MoreEditorGUI.DrawDefaultInspector(settingsAsset);
                    }
                    else
                    {
                        Editor editor = scope == SettingsScope.Project ? s_projectSettingsEditor : s_preferencesEditor;
                        editor.OnInspectorGUI();
                    }
                },
                deactivateHandler = () =>
                {
                    if (useCustomGUI)
                        return;

                    Editor editor = scope == SettingsScope.Project ? s_projectSettingsEditor : s_preferencesEditor;
                    if (editor != null)
                    {
                        if (EditorApplication.isPlaying)
                            Object.Destroy(editor);
                        else
                            Object.DestroyImmediate(editor);
                    }
                }
            };
        }

        #endregion

    }

}