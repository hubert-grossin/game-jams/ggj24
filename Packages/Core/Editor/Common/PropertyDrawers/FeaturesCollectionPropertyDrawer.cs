/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;

using UnityEngine;
using UnityEditor;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// 
    /// </summary>
    [CustomPropertyDrawer(typeof(FeaturesCollection), true)]
    public class FeaturesCollectionPropertyDrawer : PropertyDrawer
    {

        private const string FeaturesArrayProp = "_features";

        private SubassetsList _featuresSubassetsList = null;
        private bool _processedTargetFeatureType = false;
        private Type _targetFeatureType = null;
    
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Use normal editor if multiple values selected
            if (property.hasMultipleDifferentValues)
            {
                EditorGUI.PropertyField(position, property, label, true);
                return;
            }

            Type featureType = GetTargetType(property);
            if (featureType == null)
            {
                EditorGUI.PropertyField(position, property, label, true);
            }

            GetFeaturesSubassetsList(property).DoList(position);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return property.hasMultipleDifferentValues || GetTargetType(property) == null
                ? base.GetPropertyHeight(property, label)
                : GetFeaturesSubassetsList(property).GetHeight();
        }

        /// <summary>
        /// Try to get the target feature asset type from property.
        /// </summary>
        /// <param name="property">The property of which to get the target type.</param>
        /// <returns>Returns the found target type.</returns>
        private Type GetTargetType(SerializedProperty property)
        {
            if (!_processedTargetFeatureType && _targetFeatureType == null)
            {
                _processedTargetFeatureType = true;
                _targetFeatureType = property.GetTargetType();

                if (_targetFeatureType != null)
                {
                    _targetFeatureType = _targetFeatureType.GenericTypeArguments.Length > 0
                        ? _targetFeatureType.GenericTypeArguments[0]
                        : null;
                }

                if (_targetFeatureType == null)
                {
                    Debug.LogWarning($"The expected feature asset type can't be found on property {property.serializedObject.targetObject.GetType()}.{property.propertyPath}", property.serializedObject.targetObject);
                }
            }
            return _targetFeatureType;
        }

        /// <summary>
        /// Gets the reorderable feature assets list.
        /// </summary>
        /// <param name="property">The property used to store feature assets.</param>
        /// <returns>Returns the list.</returns>
        private SubassetsList GetFeaturesSubassetsList(SerializedProperty property)
        {
            if (_featuresSubassetsList == null)
            {
                SubassetsList.SubassetsListOptions options = SubassetsList.SubassetsListOptions.Default;
                options.SelectInheritors = true;
                options.UniqueSubassetTypeEntry = true;
                options.ForceSubassetType = GetTargetType(property);

                _featuresSubassetsList = new SubassetsList(property.serializedObject, property.FindPropertyRelative(FeaturesArrayProp), options);
            }
            return _featuresSubassetsList;
        }

    }

}