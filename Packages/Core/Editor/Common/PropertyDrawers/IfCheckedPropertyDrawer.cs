/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEditor;
using UnityEngine;

namespace SideXP.Core.EditorOnly
{

    /// <inheritdoc cref="IfCheckedAttribute"/>
    [CustomPropertyDrawer(typeof(IfCheckedAttribute))]
    public class IfCheckedPropertyDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            IfCheckedAttribute attr = attribute as IfCheckedAttribute;
            bool valid = Evaluate(property);

            // Cancel if the condition is not fulfilled and the field should be hidden
            if (!valid && attr.HideIfDisabled)
                return;

            GUI.enabled = valid;
            EditorGUI.PropertyField(position, property, label);
            GUI.enabled = true;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            IfCheckedAttribute attr = attribute as IfCheckedAttribute;
            return !Evaluate(property) && attr.HideIfDisabled
                ? 0f
                : base.GetPropertyHeight(property, label);
        }

        /// <summary>
        /// Checks if the condition property is valid and checked.
        /// </summary>
        /// <param name="property">The property decorated with <see cref="IfCheckedAttribute"/>.</param>
        /// <returns>Returns true if the condition property is valid and checked.</returns>
        private bool Evaluate(SerializedProperty property)
        {
            IfCheckedAttribute attr = attribute as IfCheckedAttribute;
            SerializedProperty conditionProperty = property.serializedObject.FindProperty(attr.PropertyName);
            return conditionProperty != null && conditionProperty.propertyType == SerializedPropertyType.Boolean && conditionProperty.boolValue;
        }

    }

}