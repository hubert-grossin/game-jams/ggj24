/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEditor;
using UnityEngine;

namespace SideXP.Core.EditorOnly
{

    /// <inheritdoc cref="IfNotCheckedAttribute"/>
    [CustomPropertyDrawer(typeof(IfNotCheckedAttribute))]
    public class IfNotCheckedPropertyDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            IfNotCheckedAttribute attr = attribute as IfNotCheckedAttribute;
            bool valid = Evaluate(property);

            // Cancel if the condition is not fulfilled and the field should be hidden
            if (!valid && attr.HideIfDisabled)
                return;

            GUI.enabled = valid;
            EditorGUI.PropertyField(position, property, label);
            GUI.enabled = true;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            IfNotCheckedAttribute attr = attribute as IfNotCheckedAttribute;
            return !Evaluate(property) && attr.HideIfDisabled
                ? 0f
                : base.GetPropertyHeight(property, label);
        }

        /// <summary>
        /// Checks if the condition property is not checked.
        /// </summary>
        /// <param name="property">The property decorated with <see cref="IfNotCheckedAttribute"/>.</param>
        /// <returns>Returns true if the condition property is not valid or not checked.</returns>
        private bool Evaluate(SerializedProperty property)
        {
            IfNotCheckedAttribute attr = attribute as IfNotCheckedAttribute;
            SerializedProperty conditionProperty = property.serializedObject.FindProperty(attr.PropertyName);
            return conditionProperty == null || conditionProperty.propertyType != SerializedPropertyType.Boolean || !conditionProperty.boolValue;
        }

    }

}