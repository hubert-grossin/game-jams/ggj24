/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEditor;

using UnityEngine;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Indents the property field.
    /// </summary>
    [CustomPropertyDrawer(typeof(IndentAttribute))]
    public class IndentPropertyDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            using (new IndentedScope((attribute as IndentAttribute).Levels))
            {
                EditorGUI.PropertyField(position, property, label);
            }
        }

    }

}