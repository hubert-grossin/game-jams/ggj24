/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using UnityEditor;

using UnityEngine;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Renders the property field as a disabled in the inspector.
    /// </summary>
    [CustomPropertyDrawer(typeof(ReadonlyAttribute))]
    public class ReadonlyPropertyDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            using (new EnabledScope(false))
            {
                EditorGUI.PropertyField(position, property, label);
            }
        }

    }

}