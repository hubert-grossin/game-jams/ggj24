/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Miscellaneous functions for drawing user interfaces in the editor.
    /// </summary>
    public class MoreEditorGUI
    {

        #region Fields

        // Inspectors
        public const string ScriptProperty = "m_Script";

        // Separators
        public static readonly Color SeparatorColor = new Color(1, 1, 1, .4f);
        public static readonly Color DarkSeparatorColor = new Color(0, 0, 0, .53f);
        public const float DefaultSeparatorSize = 2f;

        // Pagination

        /// <summary>
        /// Defines the width ratio for drawing pagination field's page index and total number of pages.
        /// </summary>
        private const float PaginationMainFieldWidthRatio = .5f;

        /// <summary>
        /// Defines the width ratio of the single-page change button, relative to the remaining space after drawing the main page field.
        /// </summary>
        private const float PaginationButtonWidthRatio = .6f;

        private const float PaginationMainFieldSeparatorWidth = 6f;

        // Styles

        /// <summary>
        /// The custom style used for buttons that just contains an icon.
        /// </summary>
        private static GUIStyle s_iconButtonStyle = null;

        /// <summary>
        /// The custom style for titles.
        /// </summary>
        private static GUIStyle s_titleLabelStyle = null;

        #endregion


        #region Inspector

        /// <inheritdoc cref="DrawDefaultInspector(Object, IEnumerable{string}, IEnumerable{string})"/>
        public static void DrawDefaultInspector(Object obj, params string[] ignoredProperties)
        {
            DrawDefaultInspector(new SerializedObject(obj), ignoredProperties);
        }

        /// <param name="obj">The object of which you want to draw the inspector.</param>
        /// <inheritdoc cref="DrawDefaultInspector(SerializedObject, IEnumerable{string}, IEnumerable{string})"/>
        public static void DrawDefaultInspector(Object obj, IEnumerable<string> ignoredProperties, IEnumerable<string> delveProperties)
        {
            DrawDefaultInspector(new SerializedObject(obj), ignoredProperties, delveProperties);
        }

        /// <summary>
        /// Draws the default inspector of a given object, without the script property.
        /// </summary>
        /// <param name="serializedObj">The serialized representation of the object of which you want to draw the inspector.</param>
        /// <inheritdoc cref="DrawDefaultInspector(SerializedObject, IEnumerable{string}, IEnumerable{string})"/>
        public static void DrawDefaultInspector(SerializedObject serializedObj, params string[] ignoredProperties)
        {
            DrawDefaultInspector(serializedObj, ignoredProperties, null);
        }

        /// <summary>
        /// Draws the default inspector of a given object, without the script property.
        /// </summary>
        /// <param name="serializedObj">The serialized representation of the object of which you want to draw the inspector.</param>
        /// <inheritdoc cref="DrawPropertyField(SerializedProperty, IEnumerable{string}, IEnumerable{string})"/>
        public static void DrawDefaultInspector(SerializedObject serializedObj, IEnumerable<string> ignoredProperties, IEnumerable<string> delveProperties)
        {
            SerializedProperty prop = serializedObj.GetIterator();
            prop.NextVisible(true);

            do
            {
                DrawPropertyField(prop, ignoredProperties, delveProperties);
            }
            while (prop.NextVisible(false));

            serializedObj.ApplyModifiedProperties();
        }

        /// <inheritdoc cref="DrawPropertyField(SerializedProperty, IEnumerable{string}, IEnumerable{string})"/>
        public static bool DrawPropertyField(SerializedProperty property)
        {
            return DrawPropertyField(property, null, null);
        }

        /// <summary>
        /// Draws the property field for the given property if it's not ignored.
        /// </summary>
        /// <param name="property">The property of which you want to draw the field, if applicable.</param>
        /// <param name="ignoredProperties">The name of the properties that should not appear in the inspector.</param>
        /// <param name="delveProperties">The name of the properties that should be "delved". Delved properties are skipped, but they child properties are still displayed. In the inspector, this will result in displaying the child properties without foldout field above to expand the parent.</param>
        /// <returns>Returns true if the property field has been drawn on GUI.</returns>
        public static bool DrawPropertyField(SerializedProperty property, IEnumerable<string> ignoredProperties, IEnumerable<string> delveProperties)
        {
            // Cancel if the property is the script property
            if (property.name == ScriptProperty)
                return false;

            if (ignoredProperties != null)
            {
                // Cancel if the property should be hidden
                foreach (string ignoredPropName in ignoredProperties)
                {
                    if (property.name == ignoredPropName)
                        return false;
                }
            }

            if (delveProperties != null)
            {
                // Check if the property is meant to be "delved"
                foreach (string delvePropName in delveProperties)
                {
                    // If the property should be "delved"
                    if (property.name == delvePropName)
                    {
                        // if the property has children properties
                        if (property.hasVisibleChildren)
                        {
                            property.isExpanded = true;
                            // The property to draw is now the next inner property
                            if (property.NextVisible(true))
                                break;
                            else
                                return false;
                        }
                        // Else, cancel if the property doesn't has child properties (and so it's hidden)
                        else
                        {
                            return false;
                        }
                    }
                }
            }

            EditorGUILayout.PropertyField(property, true);
            return true;
        }

        #endregion


        #region Fields

        /// <inheritdoc cref="AssetNameField(GUIContent, Object, GUILayoutOption[])"/>
        public static bool AssetNameField(Object asset, params GUILayoutOption[] options)
        {
            return AssetNameField(new GUIContent(), asset, options);
        }

        /// <inheritdoc cref="AssetNameField(GUIContent, Object, GUILayoutOption[])"/>
        public static bool AssetNameField(string label, Object asset, params GUILayoutOption[] options)
        {
            return AssetNameField(new GUIContent(label), asset, options);
        }

        /// <param name="options">Options for drawing the field.</param>
        /// <inheritdoc cref="AssetNameField(Rect, GUIContent, Object)"/>
        public static bool AssetNameField(GUIContent label, Object asset, params GUILayoutOption[] options)
        {
            return AssetNameField(EditorGUILayout.GetControlRect(options), label, asset);
        }

        /// <inheritdoc cref="AssetNameField(Rect, GUIContent, Object)"/>
        public static bool AssetNameField(Rect position, Object asset)
        {
            return AssetNameField(position, new GUIContent(), asset);
        }

        /// <inheritdoc cref="AssetNameField(Rect, GUIContent, Object)"/>
        public static bool AssetNameField(Rect position, string label, Object asset)
        {
            return AssetNameField(position, new GUIContent(label), asset);
        }

        /// <summary>
        /// Draws a delayed text field 
        /// </summary>
        /// <param name="position">The available area for drawing the field.</param>
        /// <param name="label">The label of the field. If not provided, only the field is displayed.</param>
        /// <param name="asset">The asset to rename.</param>
        /// <returns>Returns true if the name has been changed.</returns>
        public static bool AssetNameField(Rect position, GUIContent label, Object asset)
        {
            EditorGUI.BeginChangeCheck();

            string name = label == null || string.IsNullOrEmpty(label.text)
                ? EditorGUI.DelayedTextField(position, asset.name)
                : EditorGUI.DelayedTextField(position, label, asset.name);

            if (EditorGUI.EndChangeCheck())
            {
                asset.Rename(name);
                return true;
            }
            return false;
        }

        #endregion


        #region Pagination

        /// <inheritdoc cref="PaginationField(Rect, GUIContent, ref Pagination)"/>
        public static void PaginationField(ref Pagination pagination)
        {
            PaginationField(EditorGUILayout.GetControlRect(false), ref pagination);
        }

        /// <inheritdoc cref="PaginationField(Rect, GUIContent, ref Pagination)"/>
        public static void PaginationField(string label, ref Pagination pagination)
        {
            PaginationField(EditorGUILayout.GetControlRect(false), label, ref pagination);
        }

        /// <inheritdoc cref="PaginationField(Rect, GUIContent, ref Pagination)"/>
        public static void PaginationField(GUIContent label, ref Pagination pagination)
        {
            PaginationField(EditorGUILayout.GetControlRect(false), label, ref pagination);
        }

        /// <inheritdoc cref="PaginationField(Rect, GUIContent, ref Pagination)"/>
        public static void PaginationField(Rect position, ref Pagination pagination)
        {
            float mainFieldWidth = position.width * PaginationMainFieldWidthRatio;
            float controlsAvailableWidth = (position.width - mainFieldWidth - MoreGUI.HMargin * 2) / 2;
            float largeButtonWidth = controlsAvailableWidth * PaginationButtonWidthRatio;
            float smallButtonWidth = controlsAvailableWidth - largeButtonWidth - MoreGUI.HMargin;

            Rect rect = new Rect(position);

            // Draw "previous" controls
            using (new EnabledScope(pagination.Page > 0))
            {
                rect.width = smallButtonWidth;
                if (GUI.Button(rect, "<<"))
                    pagination.Page = 0;

                rect.x += rect.width + MoreGUI.HMargin;
                rect.width = largeButtonWidth;
                if (GUI.Button(rect, "<"))
                    pagination.Page--;
            }

            // Draw page index
            int indexPlus1 = pagination.Page + 1;
            rect.x += rect.width + MoreGUI.HMargin;
            rect.width = mainFieldWidth / 2;
            indexPlus1 = EditorGUI.IntField(rect, indexPlus1, EditorStyles.textField.TextAlignment(TextAnchor.MiddleCenter));
            pagination.Page = indexPlus1 - 1;

            // Draw separator
            rect.x += rect.width;
            rect.width = PaginationMainFieldSeparatorWidth;
            EditorGUI.LabelField(rect, "/");

            // Draw pages count
            rect.x += rect.width + MoreGUI.HMargin;
            rect.width = mainFieldWidth / 2 - MoreGUI.HMargin - PaginationMainFieldSeparatorWidth;
            EditorGUI.LabelField(rect, pagination.PagesCount.ToString(), EditorStyles.label.TextAlignment(TextAnchor.MiddleCenter));

            rect.x += rect.width + MoreGUI.HMargin;
            // Draw "next" controls
            using (new EnabledScope(pagination.Page < pagination.PagesCount - 1))
            {
                rect.width = largeButtonWidth;
                if (GUI.Button(rect, ">"))
                    pagination.Page++;

                rect.x += rect.width + MoreGUI.HMargin;
                rect.width = smallButtonWidth;
                if (GUI.Button(rect, ">>"))
                    pagination.Page = pagination.PagesCount;
            }
        }

        /// <inheritdoc cref="PaginationField(Rect, GUIContent, ref Pagination)"/>
        public static void PaginationField(Rect position, string label, ref Pagination pagination)
        {
            PaginationField(position, new GUIContent(label), ref pagination);
        }

        /// <summary>
        /// Draws a field to edit the given pagination value.
        /// </summary>
        /// <param name="label">The label of the field.</param>
        /// <param name="position">The position and size of the field.</param>
        /// <param name="pagination">The pagination value to edit.</param>
        public static void PaginationField(Rect position, GUIContent label, ref Pagination pagination)
        {
            Rect rect = new Rect(position);

            // Draw label
            if (label != null && !string.IsNullOrEmpty(label.text))
            {
                rect.width = EditorGUIUtility.labelWidth;
                EditorGUI.LabelField(rect, label);

                // Draw pagination field
                rect.x += rect.width + MoreGUI.HMargin;
                rect.width -= rect.width + MoreGUI.HMargin;
            }

            // Draw pagination field
            PaginationField(rect, ref pagination);
        }

        #endregion


        #region Styles

        /// <inheritdoc cref="s_iconButtonStyle"/>
        public static GUIStyle IconButtonStyle
        {
            get
            {
                if (s_iconButtonStyle == null)
                {
                    s_iconButtonStyle = GUI.skin.button.Padding(0, 0);
                }
                return s_iconButtonStyle;
            }
        }

        /// <inheritdoc cref="s_titleLabelStyle"/>
        public static GUIStyle TitleStyle
        {
            get
            {
                if (s_titleLabelStyle == null)
                {
                    s_titleLabelStyle = new GUIStyle(EditorStyles.largeLabel);
                    s_titleLabelStyle.fontSize += 2;
                    s_titleLabelStyle.fontStyle = FontStyle.Bold;
                    s_titleLabelStyle.richText = true;
                }
                return s_titleLabelStyle;
            }
        }

        #endregion


        #region Separators

        /// <inheritdoc cref="HorizontalSeparator(float, bool, bool)"/>
        public static void HorizontalSeparator(bool wide = false, bool dark = false)
        {
            HorizontalSeparator(DefaultSeparatorSize, wide, dark);
        }

        /// <param name="dark">If enabled, uses the dark separator color.</param>
        /// <inheritdoc cref="HorizontalSeparator(float, Color, bool)"/>
        public static void HorizontalSeparator(float size, bool wide = false, bool dark = false)
        {
            HorizontalSeparator(size, dark ? DarkSeparatorColor : SeparatorColor, wide);
        }

        /// <summary>
        /// Draws an horizontal line.
        /// </summary>
        /// <param name="size">The height of the separator.</param>
        /// <param name="color">The color of the separator.</param>
        /// <param name="wide">If enabled, the separator will use the full view width. This is designed to draw a separator that doesn't
        /// use the margins in the inspector window.</param>
        public static void HorizontalSeparator(float size, Color color, bool wide = false)
        {
            Rect rect = EditorGUILayout.GetControlRect(false, size);

            if (wide && rect.width < EditorGUIUtility.currentViewWidth)
            {
                rect.x = 0;
                rect.width = EditorGUIUtility.currentViewWidth;
            }

            EditorGUI.DrawRect(rect, color);
        }

        /// <summary>
        /// Draws a vertical line.
        /// </summary>
        /// <param name="size">The width of the separator.</param>
        /// <param name="color">The color of the separator.</param>
        public static void VerticalSeparator(float size, Color color)
        {
            Rect rect = EditorGUILayout.GetControlRect(false, GUILayout.Width(size));
            EditorGUI.DrawRect(rect, color);
        }

        /// <inheritdoc cref="VerticalSeparator(float, Color)"/>
        public static void VerticalSeparator(float size)
        {
            VerticalSeparator(size, SeparatorColor);
        }

        /// <inheritdoc cref="VerticalSeparator(float, Color)"/>
        public static void VerticalSeparator()
        {
            VerticalSeparator(DefaultSeparatorSize, SeparatorColor);
        }

        #endregion

    }

}