/**
 * Sideways Experiments (c) 2023
 * https://sideways-experiments.com
 * Contact: dev@side-xp.com
 */

using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using SideXP.Core.Reflection;

using Object = UnityEngine.Object;

namespace SideXP.Core.EditorOnly
{

    /// <summary>
    /// Overload of <see cref="ReorderableList"/> designed for a property that lists assets attached as subassets to a parent asset.
    /// </summary>
    public class SubassetsList : ReorderableList
    {

        #region Subclasses

        public struct SubassetsListOptions
        {

            public static readonly SubassetsListOptions Default = new SubassetsListOptions
            {
                Draggable = true,
                DisplayHeader = true,
                DisplayAddButton = true,
                DisplayRemoveButton = true,

                SelectFirstItem = true,
                CanRenameSubassets = true,
                SelectInheritors = false,
                ForceSubassetType = null,
                UniqueSubassetTypeEntry = false
            };

            // Reorderable list options
            public bool Draggable;
            public bool DisplayHeader;
            public bool DisplayAddButton;
            public bool DisplayRemoveButton;

            // Custom options

            /// <summary>
            /// If enabled, the first item of the list is always selected after the list is initialized.
            /// </summary>
            public bool SelectFirstItem;

            /// <summary>
            /// If enabled, makes the label of the element editable when displaying an element property.
            /// </summary>
            public bool CanRenameSubassets;

            /// <summary>
            /// If enabled, allow user to select an asset type that derives from the property's element type (using a dropdown with a
            /// context menu), instead of just adding an element of the expected type.
            /// </summary>
            public bool SelectInheritors;

            /// <summary>
            /// If defined, the subassets list can be filled only with the given type of objects, instead of being based on the property's
            /// target type.
            /// </summary>
            public Type ForceSubassetType;

            /// <summary>
            /// If enabled, only one subasset of a given type can be created in the list. Only used if <see cref="SelectInheritors"/> is
            /// enabled too, avoiding to get only one available element to add.
            /// </summary>
            public bool UniqueSubassetTypeEntry;

        }

        #endregion


        #region Fields

        /// <summary>
        /// The options used for this list.
        /// </summary>
        private SubassetsListOptions _options = SubassetsListOptions.Default;

        /// <summary>
        /// Caches all the available classes that derive from the expected subasset type in this project. Note that only the types actually usable (abstract or generic types are excluded) are stored. Keys are the types, values are the labels to display for selecting a type in the list.
        /// </summary>
        /// <remarks>
        /// This is only used if the <see cref="SubassetsListOptions.SelectInheritors"/> option is enabled.
        /// </remarks>
        private Dictionary<Type, string> _subassetTypes = null;

        /// <summary>
        /// The type (or parent type if <see cref="SubassetsListOptions.SelectInheritors"/> option is enabled) of an element in the list.
        /// </summary>
        private Type _elementType = null;

        #endregion


        #region Lifecycle

        /// <param name="selectInheritors">If enabled, allow user to select an asset type that derives from the property's element type (using a dropdown with a context menu), instead of just adding an element of the expected type.</param>
        /// <inheritdoc cref="ReorderableList(SerializedObject, SerializedProperty)"/>
        public SubassetsList(SerializedObject serializedObject, SerializedProperty elements, bool selectInheritors = false)
            : base(serializedObject, elements)
        {
            _options.SelectInheritors = selectInheritors;

            Init();
            Setup();
        }

        /// <inheritdoc cref="ReorderableList(SerializedObject, SerializedProperty, bool, bool, bool, bool)"/>
        /// <inheritdoc cref="SubassetsList(SerializedObject, SerializedProperty, bool)"/>
        public SubassetsList(SerializedObject serializedObject, SerializedProperty elements, bool draggable, bool displayHeader, bool displayAddButton, bool displayRemoveButton, bool selectInheritors = false)
            : base(serializedObject, elements, draggable, displayHeader, displayAddButton, displayRemoveButton)
        {
            _options = SubassetsListOptions.Default;
            _options.Draggable = draggable;
            _options.DisplayHeader = displayHeader;
            _options.DisplayAddButton = displayAddButton;
            _options.DisplayRemoveButton = displayRemoveButton;
            _options.SelectInheritors = selectInheritors;

            Init();
            Setup();
        }

        /// <inheritdoc cref="ReorderableList(SerializedObject, SerializedProperty, bool, bool, bool, bool)"/>
        public SubassetsList(SerializedObject serializedObject, SerializedProperty elements, SubassetsListOptions options)
            : base(serializedObject, elements, options.Draggable, options.DisplayHeader, options.DisplayAddButton, options.DisplayRemoveButton)
        {
            _options = options;

            Init();
            Setup();
        }

        /// <summary>
        /// Common initialization operations.
        /// </summary>
        private void Init()
        {
            if (_options.ForceSubassetType != null)
            {
                _elementType = _options.ForceSubassetType;
            }
            else
            {
                _elementType = serializedProperty.GetTargetType();
                if (_elementType.IsArray)
                    _elementType = _elementType.GetElementType();
            }
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_elementType"/>
        public Type ElementType => _elementType;

        /// <inheritdoc cref="SelectWithCallback(int, bool)"/>
        public void SelectWithCallback(int index)
        {
            SelectWithCallback(index, false);
        }

        /// <summary>
        /// Sets the selected index, and ensure the <see cref="ReorderableList.onSelectCallback"/> is invoked.
        /// </summary>
        /// <inheritdoc cref="ReorderableList.Select(int, bool)"/>
        public void SelectWithCallback(int index, bool append = false)
        {
            Select(index, append);
            onSelectCallback?.Invoke(this);
        }

        /// <summary>
        /// Finds a subasset of the given type in the list.
        /// </summary>
        /// <param name="subassetType">The type of the subasset to find.</param>
        /// <returns>Returns the found subasset.</returns>
        public Object FindSubassetOfType(Type subassetType)
        {
            for (int i = 0; i < serializedProperty.arraySize; i++)
            {
                SerializedProperty itemProp = serializedProperty.GetArrayElementAtIndex(i);
                if (itemProp.objectReferenceValue != null && itemProp.objectReferenceValue.GetType() == subassetType)
                    return itemProp.objectReferenceValue;
            }
            return null;
        }

        #endregion


        #region Protected API

        /// <inheritdoc cref="_options"/>
        protected SubassetsListOptions Options => _options;

        /// <summary>
        /// Initialize the GUI of the reorderable list.
        /// </summary>
        /// <param name="options">The options to use for this subassets list.</param>
        protected virtual void Setup()
        {
            drawHeaderCallback = rect => EditorGUI.LabelField(rect, serializedProperty.GetLabel());

            // Draw element GUI (only if header is displayed)
            if (_options.DisplayHeader)
            {
                drawElementCallback = (rect, index, isActive, isFocused) =>
                {
                    Rect tmpRect = rect;
                    tmpRect.height = EditorGUIUtility.singleLineHeight;
                    tmpRect.y += (rect.height - tmpRect.height) / 2;
                    tmpRect.width = MoreGUI.WidthXS;
                    EditorGUI.LabelField(tmpRect, index.ToString());

                    tmpRect.x += tmpRect.width + MoreGUI.HMargin;
                    tmpRect.width = rect.width - MoreGUI.WidthXS - MoreGUI.WidthS - MoreGUI.HMargin * 2;

                    SerializedProperty elementProp = serializedProperty.GetArrayElementAtIndex(index);
                    if (elementProp.objectReferenceValue == null)
                    {
                        EditorGUI.HelpBox(tmpRect, "NULL", MessageType.Warning);
                        return;
                    }

                    if (_options.CanRenameSubassets)
                    {
                        EditorGUI.BeginChangeCheck();
                        string name = EditorGUI.DelayedTextField(tmpRect, elementProp.objectReferenceValue.name);
                        if (EditorGUI.EndChangeCheck())
                        {
                            elementProp.objectReferenceValue.Rename(name);
                        }
                    }
                    else
                    {
                        EditorGUI.LabelField(tmpRect, elementProp.objectReferenceValue.name);
                    }

                    tmpRect.x += tmpRect.width + MoreGUI.HMargin;
                    tmpRect.width = MoreGUI.WidthS;
                    using (new EnabledScope(false))
                    {
                        EditorGUI.ObjectField(tmpRect, elementProp.objectReferenceValue, _elementType, false);
                    }
                };
            }

            // Add element behavior: use a dropdown if all inheritors of the main type can be selected, or just an add button if only the main type of asset can be created
            if (_options.SelectInheritors)
            {
                onAddDropdownCallback = (rect, list) =>
                {
                    if (_elementType == null)
                    {
                        Debug.LogError("Can't find the expected element type for this subassets list.");
                        return;
                    }

                    if (_subassetTypes == null)
                    {
                        _subassetTypes = new Dictionary<Type, string>();
                        foreach (Type type in ReflectionUtility.GetAllTypesInProjectAssignableFrom(_elementType))
                        {
                            if (type.IsAbstract)
                                continue;
                            
                            // Nicify label for each type
                            string name = type.Name;
                            if (name.StartsWith(_elementType.Name))
                            {
                                name = name.Substring(_elementType.Name.Length);
                            }
                            name = name.Replace('_', ' ').Trim();
                            // Regisster type/label binding
                            _subassetTypes.Add(type, ObjectNames.NicifyVariableName(name));
                        }
                    }

                    if (_subassetTypes.Count == 0)
                    {
                        Debug.LogWarning($"No valid implementation of {_elementType} has been found in the project, so there's no available asset to create from this list.");
                        return;
                    }

                    GenericMenu menu = new GenericMenu();
                    foreach (KeyValuePair<Type, string> availableSubasset in _subassetTypes)
                    {
                        if (_options.UniqueSubassetTypeEntry && FindSubassetOfType(availableSubasset.Key) != null)
                        {
                            menu.AddDisabledItem(new GUIContent(availableSubasset.Value), true);
                        }
                        else
                        {
                            menu.AddItem(new GUIContent(availableSubasset.Value), false, () =>
                            {
                                ScriptableObject newAsset = ScriptableObject.CreateInstance(availableSubasset.Key);
                                newAsset.name = availableSubasset.Value;
                                list.serializedProperty.serializedObject.targetObject.AttachObject(newAsset);

                                int index = list.serializedProperty.arraySize;
                                list.serializedProperty.InsertArrayElementAtIndex(index);
                                list.serializedProperty.GetArrayElementAtIndex(index).objectReferenceValue = newAsset;
                                list.serializedProperty.serializedObject.ApplyModifiedProperties();

                                SelectWithCallback(index);
                            });
                        }
                    }

                    menu.ShowAsContext();
                };
            }
            else
            {
                onAddCallback = (list) =>
                {
                    if (_elementType == null)
                    {
                        Debug.LogError("Can't find the expected element type for this subassets list.");
                        return;
                    }

                    // Create and attach new subasset
                    ScriptableObject newSubasset = ScriptableObject.CreateInstance(_elementType);
                    if (_options.CanRenameSubassets)
                    {
                        newSubasset.name = $"New{_elementType.Name}";
                    }
                    list.serializedProperty.serializedObject.targetObject.AttachObject(newSubasset);

                    // Add new subasset to the list
                    int index = list.serializedProperty.arraySize;
                    list.serializedProperty.InsertArrayElementAtIndex(index);
                    list.serializedProperty.GetArrayElementAtIndex(index).objectReferenceValue = newSubasset;
                    list.serializedProperty.serializedObject.ApplyModifiedProperties();

                    SelectWithCallback(index);
                };
            }

            // Remove element behavior
            onRemoveCallback = list =>
            {
                int index = list.index;

                SerializedProperty elementProp = list.serializedProperty.GetArrayElementAtIndex(index);
                if (elementProp.objectReferenceValue != null)
                {
                    string message = "Are you sure you want to remove the element ";
                    message += !string.IsNullOrEmpty(elementProp.objectReferenceValue.name)
                        ? $"\"{elementProp.objectReferenceValue.name}\""
                        : $"of type {_elementType}";
                    message += "?";

                    if (!EditorUtility.DisplayDialog("Remove Element", message, "Yes", "No"))
                        return;
                }

                if (elementProp.objectReferenceValue != null)
                {
                    elementProp.objectReferenceValue.Destroy(true);
                }

                elementProp.objectReferenceValue = null;
                list.serializedProperty.DeleteArrayElementAtIndex(index);
                list.serializedProperty.serializedObject.ApplyModifiedProperties();
                list.serializedProperty.serializedObject.targetObject.SaveAndReimport();

                index--;
                if (list.serializedProperty.arraySize > 0)
                    index = 0;

                SelectWithCallback(index);
            };

            // @fix FW-BUG-6
            onReorderCallback = list =>
            {
                list.serializedProperty.serializedObject.ApplyModifiedProperties();
            };

            // Force selection to first item if applicable
            if (serializedProperty.arraySize > 0 && _options.SelectFirstItem)
            {
                SelectWithCallback(0, false);
            }
        }

        #endregion

    }

}